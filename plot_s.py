import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import ctypes
import imageio

libc = ctypes.CDLL("/home/stage/gsl/G_images.o")
libc.sumfre.restype = ctypes.c_double
libc.sumfre.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sumfim.restype = ctypes.c_double
libc.sumfim.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

print(libc.sumfre(1, 1, 1))

N = 100
resol = 3
M = resol * 20
d = 0.1
start = 1

re = np.zeros(M - start)
im = np.zeros(M - start)
x = np.zeros(M - start)
for i in range(1, N):
    for j in range(0, M - start):
        re[j] = libc.sumfre(0, (j + start) / resol, d * i)
        im[j] = libc.sumfim(0, (j + start) / resol, d * i)
        x[j] = (j + start) / resol
    plt.plot(x, re, color="red")  # , label="Real part")
    plt.plot(x, im, color="green")  # , label="Imag part")
    plt.title(r"$G^{XX}(k\rho)$")
    plt.axis((start, M / resol, -5, 5))
    plt.text(10, 2.5, r"$d=$" + str(round(d * i / np.pi, 2)), fontsize=12)
    # plt.legend()
    plt.savefig(str(i) + ".png")
    plt.close()

with imageio.get_writer("mygif.gif", mode="I") as writer:
    for filename in [str(i) + ".png" for i in range(1, N)]:
        image = imageio.imread(filename)
        writer.append_data(image)

csvFile = pd.read_csv("s.dat")


print(csvFile["re"][:6])
