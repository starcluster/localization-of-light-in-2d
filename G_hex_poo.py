import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib

# matplotlib.use('Qt5Agg')
plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

from matplotlib.widgets import Slider, Button, TextBox
from mpl_toolkits.axes_grid1 import make_axes_locatable
import Gd
import ll2D
import generate_lattice as gl
import intensity_map as imap
import time
import random_matrices as rm
import bott2 as bott
from scipy.optimize import curve_fit


# optimization to do: no need to recompute the matrix completely for a
# delta update, just the diagonal.


class ComplexPlane:
    def __init__(self, ax, N, a, k0, no):
        self.k0 = k0
        self.no = no
        self.N, self.a = N, a
        self.typeOfGrid = "HexStretched"
        self.scaleType = "log"

        fig, ax = plt.subplots()

        axd = plt.axes([0.05, 0.11, 0.02, 0.75])
        self.d_slider = Slider(
            ax=axd,
            label="$d$",
            valmin=0.1,
            valmax=100,
            valinit=100,
            orientation="vertical",
            color="black",
        )

        axDB = plt.axes([0.1, 0.11, 0.02, 0.75])
        self.DB_slider = Slider(
            ax=axDB,
            label="$\\Delta_B$",
            valmin=-12,
            valmax=12,
            valinit=0,
            orientation="vertical",
            color="black",
        )

        axDAB = plt.axes([0.15, 0.11, 0.02, 0.75])
        self.DAB_slider = Slider(
            ax=axDAB,
            label="$\\Delta_{AB}$",
            valmin=-12,
            valmax=12,
            valinit=0,
            orientation="vertical",
            color="black",
        )

        axrho = plt.axes([0.20, 0.11, 0.02, 0.75])
        self.rho_slider = Slider(
            ax=axrho,
            label="$\\rho$",
            valmin=0.01,
            valmax=100,
            valinit=1,
            orientation="vertical",
            color="black",
        )
        self.DB_slider.on_changed(lambda x: self.reDisplayModes())
        self.DAB_slider.on_changed(lambda x: self.reDisplayModes())
        self.d_slider.on_changed(lambda x: self.displayModes())
        self.rho_slider.on_changed(lambda x: self.refresh())

        ICON_PLAY = plt.imread("histo_logo.png")
        axButton = plt.axes([0.01, 0.1, 0.02, 0.05])
        self.bhist = Button(axButton, "", image=ICON_PLAY)
        self.bhist.on_clicked(lambda f: self.displayHist())

        ICON_PLAY = plt.imread("random_logo.png")
        axButton = plt.axes([0.01, 0.15, 0.02, 0.05])
        self.brandomLattice = Button(axButton, "", image=ICON_PLAY)
        self.brandomLattice.on_clicked(lambda f: self.random_lattice())

        ICON_PLAY = plt.imread("hexgrid_logo.png")
        axButton = plt.axes([0.01, 0.2, 0.02, 0.05])
        self.bhexGridLattice = Button(axButton, "", image=ICON_PLAY)
        self.bhexGridLattice.on_clicked(lambda f: self.hexgrid_lattice())

        ICON_PLAY = plt.imread("hexhex_logo.png")
        axButton = plt.axes([0.01, 0.25, 0.02, 0.05])
        self.bhexHexLattice = Button(axButton, "", image=ICON_PLAY)
        self.bhexHexLattice.on_clicked(lambda f: self.hexhex_lattice())

        ICON_PLAY = plt.imread("delta_logo.png")
        axButton = plt.axes([0.01, 0.3, 0.02, 0.05])
        self.bdelta = Button(axButton, "", image=ICON_PLAY)
        self.bdelta.on_clicked(lambda f: self.askEndHistDelta())

        ICON_PLAY = plt.imread("quit_logo.png")
        axButton = plt.axes([0.01, 0.35, 0.02, 0.05])
        self.bclose = Button(axButton, "", image=ICON_PLAY)
        self.bclose.on_clicked(lambda f: plt.close())

        ICON_PLAY = plt.imread("log_logo.png")
        axButton = plt.axes([0.01, 0.40, 0.02, 0.05])
        self.blog = Button(axButton, "", image=ICON_PLAY)
        self.blog.on_clicked(lambda f: self.plotLog())

        plt.get_current_fig_manager().full_screen_toggle()

        if self.typeOfGrid == "HexGrid":
            self.g = gl.generateHexGrid(self.N, self.a)

        if self.typeOfGrid == "HexStretched":
            self.g = gl.generate_hex_grid_stretched(self.N, self.a, 1.05)

        elif self.typeOfGrid == "Random":
            R = np.sqrt(self.N * self.N / np.pi / self.rho_slider.val)
            self.g = ll2D.createDisc(R, self.N * self.N)
        else:
            self.g = gl.generate_hex_hex(m=8, a=self.a)
            # deprecated, was replaced 03/05/2023 by
            # generate_hex_hex_stretch

        self.omega, self.gamma, self.iprs, self.v = self.computeModes()

        for i in [-10 + i for i in range(30)]:
            print("CB  " + str(i) + " = ", bott.bott(self.g, self.eigvec, self.eigv, i))

        ax.axis((-120, 120, 1e-4, 1e2))

        # ax.axis((-120,25,1e-4,1e2))
        # ax.axis((-10,10,1e-4,1))
        if self.scaleType == "log":
            ax.set_yscale("log")

        line = ax.scatter(
            self.omega,
            self.gamma,
            marker=".",
            c=self.iprs,
            cmap="jet",
            picker=True,
            pickradius=5,
        )  # 5 points tolerance
        divider = make_axes_locatable(ax)
        self.cax = divider.append_axes("right", size="5%", pad=0.05)
        self.cb = plt.colorbar(line, cax=self.cax)
        plt.subplots_adjust(left=0.30)
        ax.set_xlabel(r"$\omega_n$", fontsize=20)
        ax.set_ylabel(r"$\gamma_n$", fontsize=20)

        self.updateTitle()
        ax.set_title(self.t, fontsize=20)

        def onpick(event):
            ind = event.ind[0]
            print(ind)
            plt.figure()
            imap.intensity_map(
                self.v, self.g, ind, self.omega[ind], self.iprs[ind], self.DB_slider.val
            )
            plt.show()

        fig.canvas.mpl_connect("pick_event", onpick)
        fig.canvas.draw_idle()

        # enable the following for a topological gap and restrain omega to -20 20
        # for j in range(len(self.omega)):
        #     if self.iprs[j] > 0.004:
        #         self.omega[j] = -300 # will be eliminated by omega30

        # for j in range(len(self.omega)):
        #     if self.iprs[j] > 0.02:
        #         # print("edges removed")
        #         self.omega[j] = -300 # will be eliminated by omega30

        # self.omega30 = []
        # for o in self.omega:
        #     if o < 20 and o > -15:
        #         self.omega30.append(o)
        # self.omega30.sort()

        # self.omega30 = np.array(self.omega30)
        # self.omegadiff = np.diff(self.omega30)
        # print(self.omega30)
        # print(np.max(self.omegadiff))

        ## to be removed
        # fig.savefig("big_zoom_gap.svg",format="svg")
        # plt.figure()
        # for i in range(len(self.omega)):
        #     if -10 < self.omega[i] < 25:
        #         u.intensity_map(self.v, self.g, i, self.omega[i],self.iprs[i], self.DB_slider.val)
        #         plt.savefig("./imap/" + str(np.round(i,4)) + ".png", format="png")
        #         plt.close("all")
        ##
        # self.displayOnlyModes()

    def Imap(self, w):
        plt.figure()
        ind = np.argwhere(self.omega == w)[0][0]
        print(ind)
        imap.intensity_map(self.v, self.g, ind, w, self.iprs[ind], self.DB_slider.val)

    def computeModes(self):
        t0 = time.time()
        self.M = Gd.createMatrixVectorSum(
            self.g, self.d_slider.val, self.DB_slider.val, self.DAB_slider.val, self.k0
        )
        t1 = time.time()
        # print(t1-t0)
        w, v = np.linalg.eig(self.M)
        self.eigvec = v
        self.eigv = w
        N = self.M.shape[0]

        omega, gamma, iprs = [0] * N, [0] * N, [0] * N

        for i in range(N):
            omega[i], gamma[i], iprs[i] = (
                w[i].real / 2,
                -w[i].imag,
                ll2D.IPRVector(i, v),
            )

        return omega, gamma, iprs, v

    def updateTitle(self):
        self.N_atoms = int(self.g.shape[0])
        self.t = (
            "$N_{\\mathrm{atoms}} = $"
            + str(self.N_atoms)
            + " | $d=$ "
            + str(np.round(self.d_slider.val, 2))
            + " | $\\Delta_B =$ "
            + str(np.round(self.DB_slider.val, 2))
            + " | $\\Delta_{AB} =$ "
            + str(np.round(self.DAB_slider.val, 2))
            + " | $\\rho=$ "
            + str(np.round(self.rho_slider.val, 2))
            + " | "
            + self.typeOfGrid
        )

    def displayModes(self, re=False):
        if not (re):
            self.omega, self.gamma, self.iprs, self.v = self.computeModes()

        ax.cla()
        ax.axis((-120, 120, 1e-4, 1e2))
        # ax.axis((-10,10,1e-4,1))
        k0a = 2 * np.pi * 0.05
        line = ax.scatter(
            np.array(self.omega),
            np.array(self.gamma),
            marker=".",
            c=self.iprs,
            cmap="jet",
            picker=True,
            pickradius=5,
        )  # 5 points tolerance
        line.set_picker(True)
        self.cb.update_normal(line)
        plt.subplots_adjust(left=0.25)
        ax.set_xlabel(r"$\omega_n$")
        ax.set_ylabel(r"$\gamma_n$")
        # plt.yscale('log')
        # ax.axis((-120,120,0.01,1000))
        if self.scaleType == "log":
            ax.set_yscale("log")

        # self.updateTitle() has to be reactivated !!!
        # ax.set_title(self.t, fontsize=20)

    def displayOnlyModes(self):
        fig1, ax1 = plt.subplots()
        ax1.cla()
        ax1.axis((-120, 120, 1e-4, 1e2))
        # ax.axis((-10,10,1e-4,1))

        line = ax1.scatter(
            np.array(self.omega),
            np.array(self.gamma),
            marker=".",
            c=self.iprs,
            cmap="jet",
        )
        self.cb.update_normal(line)
        fig.subplots_adjust(left=0.25)
        ax1.set_xlabel(r"$\omega_n$")
        ax1.set_ylabel(r"$\gamma_n$")

        if self.scaleType == "log":
            ax1.set_yscale("log")
        ax1.set_title(r"$k_0 a /2\pi = $" + str(self.k0 / 2 / np.pi))
        fig1.savefig(
            "eigenvalues_complex_plane" + str(self.no) + ".png",
            format="png",
            bbox_inches="tight",
        )

    def reDisplayModes(self):
        t0 = time.time()
        self.M = Gd.modifyMatrixVectorSum(
            self.M, self.d_slider.val, self.DB_slider.val, self.DAB_slider.val
        )
        t1 = time.time()
        print(t1 - t0)
        w, v = np.linalg.eig(self.M)
        N = self.M.shape[0]

        self.omega, self.gamma, self.iprs = [0] * N, [0] * N, [0] * N
        for i in range(N):
            self.omega[i], self.gamma[i], self.iprs[i] = (
                w[i].real / 2,
                -w[i].imag,
                ll2D.IPRVector(i, v),
            )
        self.displayModes(re=True)

    def displayNeg(self, L):
        lneg = [0 for i in self.iprs]
        ll = []
        for e in L:
            ind = np.argwhere(self.omega == find_nearest(self.omega, e[0]))[0][0]
            print("coupable !!!", ind)
            lneg[ind] = 1
            ll.append(ind)
        print(list(set(ll)))
        plt.figure()
        plt.title("Real Space")
        plt.scatter(self.omega, self.gamma, c=lneg)
        # plt.colorbar()
        plt.show()

    def displayHist(self):
        plt.figure()
        plt.style.use("seaborn-white")
        plt.hist(
            self.omega,
            bins=100,
            density=True,
            range=(-160, 160),
            facecolor="#00dd10",
            edgecolor="#00870a",
            linewidth=0.5,
        )
        self.updateTitle()
        plt.title(self.t, fontsize=20)
        plt.show()

    def refresh(self):
        if self.typeOfGrid == "HexGrid":
            self.g = gl.generateHexGrid(self.N, self.a)

        if self.typeOfGrid == "HexStretched":
            self.g = gl.generate_hex_grid_stretched(self.N, self.a, 1.2)

        elif self.typeOfGrid == "Random":
            R = np.sqrt(self.N * self.N / np.pi / self.rho_slider.val)
            self.g = ll2D.createDisc(R, self.N * self.N)
        else:
            print("HexHex")
            self.g = gl.generate_hex_hex(a=self.a)
        self.displayModes()

    def random_lattice(self):
        self.typeOfGrid = "Random"
        self.refresh()

    def hexgrid_lattice(self):
        self.typeOfGrid = "HexGrid"
        self.refresh()

    def hexhex_lattice(self):
        self.typeOfGrid = "HexHex"
        self.refresh()

    def askEndHistDelta(self):
        plt.figure(figsize=(8, 1))
        initial_text = "200"

        def submit(text):
            self.end_hist_delta = float(text)
            plt.close()
            self.plotDelta()

        axbox = plt.axes([0.35, 0.5, 0.55, 0.3])
        r1 = TextBox(
            axbox, "Histogram of $\\delta$ goes from 0 to ", initial=initial_text
        )
        r1.on_submit(submit)
        plt.show()

    def plotDelta(self):
        plt.figure()
        x, y = [], []
        x = sorted(self.omega)
        for i in range(len(self.omega) - 1):
            y.append(np.abs(x[i + 1] - x[i]))
        z = plt.hist(
            y,
            bins=100,
            density=True,
            range=(0, self.end_hist_delta),
            facecolor="#9100cb",
            edgecolor="#df8fff",
            linewidth=0.5,
        )
        plt.title("Level spacing statistics")
        plt.ylabel("$p(\delta\lambda)$")
        plt.show()

    def plotLog(self):
        if self.scaleType == "lin":
            self.scaleType = "log"
        else:
            self.scaleType = "lin"
        self.refresh()


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()

    return array[idx]


# fig, ax = plt.subplots()

if __name__ == "__main__":
    fig, ax = plt.subplots()
    for i in range(1):
        h = 0.05 + i / 200
        print(h, ",", end=" ")
        cp = ComplexPlane(ax, 4, 1, h * 2 * np.pi, i)
        plt.show()
        plt.close("all")
        plt.cla()
        plt.clf()
