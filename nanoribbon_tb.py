from sympy import diag, BlockMatrix, eye, cos, Matrix, pprint
import numpy as np

from modele import Modele
from plot import plot_bands


def on_site_triang_v1(a, t1, t2, kx):
    return np.array(
        [
            [
                0,
                t1 * np.exp(-1j * kx * a / 2),
                0,
                t2 * np.exp(1j * kx * a),
                0,
                t1 * np.exp(-1j * kx * a / 2),
            ],
            [0, 0, t1 * np.exp(-1j * kx * a), 0, 0, 0],
            [0, 0, 0, t1 * np.exp(-1j * kx * a / 2), 0, 0],
            [0, 0, 0, 0, t1 * np.exp(1j * kx * a / 2), 0],
            [0, 0, 0, 0, 0, t1 * np.exp(1j * kx * a)],
            [0, 0, 0, 0, 0, 0],
        ],
        dtype=complex,
    )


def on_site_triang_v2(a, t1, t2, kx):
    return np.array(
        [
            [
                0,
                t1,
                0,
                t2 * np.exp(1j * kx * 3 * a),
                0,
                t1,
            ],
            [0, 0, t1, 0, 0, 0],
            [0, 0, 0, t1, 0, 0],
            [0, 0, 0, 0, t1, 0],
            [0, 0, 0, 0, 0, t1],
            [0, 0, 0, 0, 0, 0],
        ],
        dtype=complex,
    )


class HoneycombNanoribbonTightBinding(Modele):
    def __init__(self, a, t1, t2, nb_of_layer):
        super().__init__()
        self.a = a
        self.t1 = t1
        self.t2 = t2
        self.nb_of_layer = nb_of_layer

    def compute_ham(self, kx):
        on_site_triang = on_site_triang_v1(self.a, self.t1, self.t2, kx)
        on_site = on_site_triang + np.conj(on_site_triang).T
        over_site_up = np.zeros((6, 6), dtype=complex)
        over_site_up[1, 4] = self.t2 * np.exp(1j * kx * self.a / 2)
        over_site_up[2, 5] = self.t2 * np.exp(-1j * kx * self.a / 2)
        over_site_down = np.conj(over_site_up).T
        H_nanoribbon = np.kron(np.eye(self.nb_of_layer), on_site)
        H_nanoribbon += np.kron(np.eye(self.nb_of_layer, k=1), over_site_up)
        H_nanoribbon += np.kron(np.eye(self.nb_of_layer, k=-1), over_site_down)
        self.ham = H_nanoribbon


if __name__ == "__main__":
    a = 1
    t1 = 1
    t2 = 1.5
    t3 = 1
    res_bands = 200
    nb_of_layer = 10

    path_kx = np.linspace(np.pi / 3, np.pi, res_bands, dtype=float)
    path = np.column_stack((path_kx, np.zeros(res_bands)))

    R = 3
    bands = []

    hntb_trivial = HoneycombNanoribbonTightBinding(a, t1, t3, nb_of_layer)
    hntb_topo = HoneycombNanoribbonTightBinding(a, t1, t2, nb_of_layer)

    mini_gap = []

    # for nb_of_layer in range(2, 40, 4):
    #     hntb_topo = HoneycombNanoribbonTightBinding(a, t1, t2, nb_of_layer)
    #     hntb_topo.compute_bands(path_kx)
    #     bands = hntb_topo.bands
    #     bands = [item for sublist in bands for item in sublist]
    #     bands = list(map(abs, bands))
    #     print(np.min(bands))

    # for kx in path_kx:
    #     hntb_topo.compute_ham(kx)
    #     hntb_trivial.compute_ham(kx)

    #     ham_topo = hntb_topo.ham
    #     ham_trivial = hntb_trivial.ham
    #     m, n = ham_topo.shape
    #     p, q = ham_trivial.shape

    #     O = np.zeros((m, q))

    #     ham_total = np.hstack((np.vstack((ham_topo, O)), np.vstack((O, ham_trivial))))
    #     ham_total[m + 1, n + 4] = t3 * np.exp(1j * kx * a / 2)
    #     ham_total[m + 2, n + 5] = t3 * np.exp(-1j * kx * a / 2)
    #     ham_total[n + 4, m + 1] = t3 * np.exp(-1j * kx * a / 2)
    #     ham_total[n + 5, m + 2] = t3 * np.exp(1j * kx * a / 2)
    #     eigenvalues, _ = np.linalg.eig(ham_total)
    #     energies = np.real(eigenvalues)
    #     bands.append(energies)

    hntb_topo.compute_bands(path_kx)
    bands = hntb_topo.bands
    print(np.array(bands).shape)

    title = r"$\textrm{Nanoribbon}\quad $" + f"$t_2 = {t2}\quad$" + f"$N={nb_of_layer}$"

    dim_fig = 0, res_bands, -4, 4.2
    plot_bands(
        path,
        bands,
        title,
        [0, res_bands / 2, res_bands],
        [r"$\pi/3$", r"$2\pi/3 $", r"$\pi $"],
        "black",
        dim_fig,
    )
