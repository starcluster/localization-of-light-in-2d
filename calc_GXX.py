from sympy import *
from sympy.physics.quantum.dagger import Dagger

# NEVER USE import scipy (only submodules !)
from scipy import integrate

init_printing()
x, y, z = var("x y z")


def f(x, y, z):
    return exp(1j * sqrt(x**2 + y**2 + z**2)) / sqrt(x**2 + y**2 + z**2)


# pprint(diff(f(x,y,z),x))
def df(x, y, z):
    return diff(f(x, y, z), x)


# pprint(diff(df(x,y,z),x))


def I(x, y, z):
    return hankel1(0, sqrt(x**2 + y**2 + z**2))


# pprint(simplify(diff(I(x,y,z),x,x) - 1/x*diff(I(x,y,z),x)))
P, Q, r = var("P Q r")
d = Matrix([[1, 1j], [-1, 1j]]) / sqrt(2)
G = Matrix(
    [
        [P + Q * x**2 / r**2, Q * x * y / r**2],
        [Q * x * y / r**2, P + Q * y**2 / r**2],
    ]
)
g = d * G * Dagger(d)
# pprint(simplify(g[1,1]))
pprint(G.eigenvals())
