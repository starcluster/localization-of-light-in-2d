import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib
import os
import shutil
import pandas as pd
from multiprocessing import Pool
import svgutils.transform as sg

from mpmath import polylog
from mpmath import exp
from scipy.optimize import curve_fit

import tex
import lattice_f as lf
import band_diagram as bd
import chern as ch
import generate_mk as gMk

tex.useTex()

plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)


def band_diagram(
    k0=2 * np.pi * 0.05, a=1, distance=0, Ndots=400, N=80, aho=0.1, rebake=True, name=0
):
    """Draw band diagram of an hexagonal lattice between two plates of
    perfectible conductive metal separated by distance. If distance=0
    then there are no plates (free space).

    """
    print(
        f"Computing band diagram for distance = {distance}, Ndots = {Ndots}, N = {N}, aho = {aho}..."
    )
    n = 4 * int(Ndots / 3)
    lattice = lf.generate_path_bz(a, Ndots)
    n2 = int(Ndots / 3) * 4
    lattice = lattice[115:150]
    fig, axes = plt.subplots(nrows=2, ncols=2)
    for ax, dB, dAB, lettre in zip(
        axes.flat, [0, 12, 0, 12], [0, 0, 12, -12], ["(a)", "(b)", "(c)", "(d)"]
    ):
        try:
            if rebake:
                raise OSError
            csv = np.genfromtxt(f"data/omegas_{dB}_{dAB}_0.csv", delimiter=",")
            xprime, omegas, gammas = tuple(csv.transpose().tolist())
            csv2 = np.genfromtxt(f"data/info_{dB}_{dAB}_0.csv", delimiter=",")
            # n2,nn,m2,m3 = tuple(csv2.tolist())

        except OSError:
            B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = bd.band_diagram(
                lattice, distance, dB, dAB, k0, aho, N, a
            )
            xprime = [x for x in range(len(omegas))]
            n2 = int(Ndots / 3) * 4
            nn = len(B2)
            # m2 = min(B2[int(nn/2):nn])
            # m3 = max(B3[int(nn/2):nn])
            np.savetxt(
                f"data/omegas_{dB}_{dAB}_0.csv",
                [p for p in zip(xprime, omegas, gammas)],
                delimiter=",",
                fmt="%s",
            )
            # np.savetxt(f"data/info_{dB}_{dAB}_0.csv", [p for p in zip([n2,nn,m2,m3])],delimiter=',',fmt='%s' )

        if distance == 0:
            im = ax.scatter(
                xprime,
                omegas,
                c=gammas,
                cmap="plasma",
                marker="o",
                s=0.2,
                norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
                zorder=2,
            )
        else:
            ax.scatter(xprime, omegas, c="gray", s=0.2)
            try:
                if rebake:
                    raise OSError
                csv = np.genfromtxt(
                    f"data/omegas_{dB}_{dAB}_{distance}_plates.csv", delimiter=","
                )
                xprime, omegas, gammas = tuple(csv.transpose().tolist())
                csv2 = np.genfromtxt(
                    f"data/info_{dB}_{dAB}_{distance}_plates.csv", delimiter=","
                )
                # n2,nn,m2,m3 = tuple(csv2.tolist())

            except OSError:
                (
                    B1,
                    B2,
                    B3,
                    B4,
                    X,
                    Y,
                    omegas,
                    gammas,
                    weightas,
                    sigmas,
                ) = bd.band_diagram(lattice, distance, dB, dAB, k0, aho, N, a)
                xprime = [x for x in range(len(omegas))]
                np.savetxt(
                    f"data/omegas_{dB}_{dAB}_{distance}_plates.csv",
                    [p for p in zip(xprime, omegas, gammas)],
                    delimiter=",",
                    fmt="%s",
                )
                # np.savetxt(f"data/info_{dB}_{dAB}_{distance}_plates.csv", [p for p in zip([n2,nn,m2,m3])],delimiter=',',fmt='%s' )
                
            im = ax.scatter(
                xprime,
                omegas,
                c=gammas,
                cmap="plasma",
                marker="o",
                s=0.2,
                zorder=2,
                norm=matplotlib.colors.Normalize(vmin=0.001, vmax=1),
            )
            alpha_s = 1 - (np.pi / (distance)) ** 2
            alpha_s2 = 1 - (np.pi * 3 / (distance)) ** 2
            alpha = np.sqrt(alpha_s)
            alpha2 = np.sqrt(alpha_s2)
            def diver(x):
                q2 = np.linalg.norm(x)
                return 1/(q2-alpha)
            yy2 = []
            for q in lattice:
                yy2.append(diver(q))
            print(yy2)
            xprime2= [i*4 for i in range(len(yy2))]
            print(len(xprime2))
            ax.scatter(xprime2,yy2,c="red")
    


        print(f"{distance=},{alpha=}")
        nnn = len(xprime) / 2
        print(nnn)
        ax.axvline(x=(1 + alpha) * nnn, color="red", ls="--", zorder=1)
        ax.axvline(x=(1 - alpha) * nnn, color="red", ls="--", zorder=1)
        # ax.axvline(x=(1 + alpha2) * nnn, color="red", ls="--", zorder=1)
        # ax.axvline(x=(1 - alpha2) * nnn, color="red", ls="--", zorder=1)

        ax.axis((0, len(xprime), -1000, 1000))
        # ax.text(1/64*len(omegas),115,s='$' + lettre + '$')

        # if (dB==12 and dAB==0) or (dAB==12 and dB == 0):
        #     ax.axhspan(m2,m3,alpha=0.5,color="gray")

        if (dB == 0 and dAB == 0) or (dB == 12 and dAB == 0):
            # ax.set_xticks([])
            # ax.set_xlabel("")
            ax.tick_params(labelsize=12)
        else:
            # ax.set_xticks(ticks=[0,n,2*n],labels=["$M$","$\Gamma$","$K$"])
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
            ax.tick_params(labelsize=12)
        if (dB == 12 and dAB == -12) or (dB == 12 and dAB == 0):
            ax.set_ylabel("")
            ax.set_yticks([])
        else:
            ax.set_ylabel(
                r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12
            )

        # ax.text(5/8*len(omegas),-140,s=r"\noindent $\Delta_{\mathbf{B}}=" + str(dB) + "\\\\ \\Delta_{AB}=" + str(dAB) + " \\\\ k_0d= " + str(distance) + "$", fontsize=10)

    fig.subplots_adjust(wspace=0.03, hspace=0.12)
    # plt.subplot_tool() AMAZING !
    cbar_ax = fig.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma/\Gamma_0$")
    plt.subplots_adjust(
        top=0.885, bottom=0.11, left=0.14, right=0.9, hspace=0.025, wspace=0.025
    )

    plt.savefig(f"bd/band_diagram_{name}.png", format="png", bbox_inches="tight")
    print("Done.")


def re_G():
    def G_plates(d):
        return (
            (
                polylog(1, -exp(1j * d)) / d
                + 1j * polylog(2, -exp(1j * d)) / d**2
                - polylog(3, -exp(1j * d)) / d**3
            )
            + 1j / 3
        ) * 3

    N = 500
    d = np.linspace(1, 50, N)
    y = np.zeros_like(d)
    for i in range(N):
        y[i] = np.real(G_plates(d[i]))
    y2 = np.ones_like(d)
    # d = d/np.pi
    f = plt.figure()
    ax = f.add_subplot(111)
    # plt.plot(d,y2,c="gray",linewidth=5.0,linestyle="--")
    plt.plot(d, y, c=plasma_blue, linewidth=5.0, label=r"$\textrm{Re}(G(d))$")
    # plt.axis((0,12,-0.05,3.2))
    # plt.xticks([i for i in range(13)])
    plt.xlabel(r"$k_0d$", fontsize=30)
    # plt.ylabel(r"$",fontsize=30)
    plt.xticks(fontsize=20)

    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    plt.yticks(fontsize=20)


def plates_contrib():
    N = 1000
    d = np.linspace(1, 20, N)
    y = np.zeros_like(d)
    y2 = np.zeros_like(d)
    qnorm2 = 2 * np.pi * 0.05 + 1
    k0 = 2 * np.pi * 0.05
    for i in range(N):
        omega = k0**2 - qnorm2
        y[i] = np.real(1j / omega / k0**2 / (1 + np.exp(1j * omega * d[i])))
        y2[i] = np.imag(1j / omega / k0**2 / (1 + np.exp(1j * omega * d[i])))

    f = plt.figure()
    ax = f.add_subplot(111)
    # plt.plot(d,y2,c="gray",linewidth=5.0,linestyle="--")
    plt.scatter(d, y, c=plasma_blue, linewidth=5.0, label=r"$\textrm{Re}$")
    plt.scatter(d, y2, c=plasma_pink, linewidth=5.0, label=r"$\textrm{Im}$")
    # plt.axis((0,12,-0.05,3.2))
    # plt.xticks([i for i in range(13)])
    plt.xlabel(r"$k_0d$", fontsize=30)
    # plt.ylabel(r"$",fontsize=30)
    plt.xticks(fontsize=20)

    # plt.plot(d, 1/(d-10*np.pi))
    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    plt.yticks(fontsize=20)


if __name__ == "__main__":
    # PHYSICS
    k0 = 2 * np.pi * 0.05
    a = 1
    # NUMERICS
    N = 80
    aho = 0.1
    Ndots = 400
    rebake = True
    deltaB = 0
    deltaAB = 0

    # plates_contrib()
    # plt.show()
    # exit()
    BZ = lf.generate_path_bz(a, Ndots)
    # print(BZ)
    # exit()
    k = np.array([0, 0], dtype=float)
    k = np.array([7.65437235e-02, 1.00000000e-03], dtype=float)
    B1, B2, B3, B4 = [], [], [], []
    distances = []
    coeff1 = []
    for i in range(1, 500):
        distance = 1 + i / 3
        # distance = 3.14
        # Mk = gMk.generate_mk_wrapper(k, distance, deltaB, deltaAB, k0, aho, N, a)
        # coeff1.append(Mk[0,0])
        # print(np.round(Mk,2))

        # lambdas, vects = np.linalg.eig(Mk)
        # # print(np.round(lambdas,2))
        # # exit()
        # omega = [0]*4

        # for j in range(4):
        #     omega[j] = -0.5*lambdas[j].real
        # B1.append(omega[0])
        # B2.append(omega[1])
        # B3.append(omega[2])
        # B4.append(omega[3])
        # distances.append(distance)
        band_diagram(
            k0=k0,
            a=a,
            distance=distance,
            Ndots=Ndots,
            N=N,
            aho=aho,
            rebake=rebake,
            name=i,
        )

    # plt.scatter(distances, B1)
    # plt.scatter(distances, B2)
    # plt.scatter(distances, B3)
    # plt.scatter(distances, B4)
    # plt.show()

    # re_G()
    # B1 = -(np.array(B1)-B1[-1])
    # plt.scatter(distances, B1,color="red",label="B1 for $k=0$")

    # plt.legend()
    # plt.savefig('re_G.pdf', format='pdf', bbox_inches='tight')
    # plt.show()
    plt.scatter(distances, coeff1)
    plt.show()
