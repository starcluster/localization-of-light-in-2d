#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include <unistd.h>
#include "sumG.h"


double complex Srnn(int p, double *kb, int alpha, int beta, double a, double k0, int N){
    double b = a*sqrt(3);
    double c = 3./2.*a;
    double complex s = 0.;

    double nn1A[3][2] = {{0.,a},
                         {sqrt(3.)*a/2.,-a/2.},
                         {-sqrt(3.)*a/2.,-a/2.}};

    double nn2A[6][2] = {{a,0},
                         {-a,0},
                         {sqrt(3)*a/2.,3.*a/2.},
                         {-sqrt(3)*a/2.,3.*a/2.},
                         {sqrt(3)*a/2.,-3.*a/2.},
                         {-sqrt(3)*a/2.,-3.*a/2.}};
    double nn1B[3][2] = {{0.,-a},
                         {sqrt(3.)*a/2.,a/2.},
                         {-sqrt(3.)*a/2.,a/2.}};

    double nn2B[6][2] = {{a,0},
                         {-a,0},
                         {sqrt(3)*a/2.,3.*a/2.},
                         {-sqrt(3)*a/2.,3.*a/2.},
                         {sqrt(3)*a/2.,-3.*a/2.},
                         {-sqrt(3)*a/2.,-3.*a/2.}};

    if (p == 2){
        for(int i =0; i<3;i++){
            double x = nn1A[i][0]*k0;
            double y = nn1A[i][1]*k0;
            if (alpha == 0 && beta == 0)
                s += gxx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else if (alpha == 1 && beta == 0)
                s += gyx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else if (alpha == 0 && beta == 1)
                s += gxy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else
                s += gyy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
        }
    } else if (p==3){
        for(int i =0; i<3;i++){
            double x = nn1B[i][0]*k0;
            double y = nn1B[i][1]*k0;
            if (alpha == 0 && beta == 0)
                s += gxx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else if (alpha == 1 && beta == 0)
                s += gyx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else if (alpha == 0 && beta == 1)
                s += gxy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else
                s += gyy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
        }
        } else {
        for(int i =0; i<6;i++){
            double x = nn2A[i][0]*k0;
            double y = nn2A[i][1]*k0;
            if (alpha == 0 && beta == 0)
                s += gxx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else if (alpha == 1 && beta == 0)
                s += gyx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else if (alpha == 0 && beta == 1)
                s += gxy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
            else
                s += gyy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*y)/k0);
        }
    }
    return s;
}

double complex Sr(int p, double *kb, int alpha, int beta, double a, double k0, int N, int nn_onoff){
    if (nn_onoff == 1){
        return Srnn(p, kb, alpha, beta,  a,  k0, N);
    }
    a = 1;
    double b = a*sqrt(3);
    double c = 3./2.*a;
    double complex s = 0.;
    for(int i = -N; i<N;i++){
        for(int j = -N; j<N;j++){
            if (i==0 && j==0){
                if (p == 2 || p == 3){
                    double x = (i+1./4.*(1.+cpow(-1.,j+1.)))*b*k0;
                    double y;
                    if (p == 1 || p == 4)
                        y = j*c;
                    else if (p == 2)
                        y = j*c+a;
                    else
                        y = j*c-a;
                    y *= k0;

                    if (alpha == 0 && beta == 0)
                        s += gxx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                    else if (alpha == 1 && beta == 0)
                        s += gyx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                    else if (alpha == 0 && beta == 1)
                        s += gxy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                    else
                        s += gyy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                } else
                    s += 0;
            } else {
                double x = (i+1./4.*(1.+cpow(-1.,j+1.)))*b*k0;
                double y;
                if (p == 1 || p == 4)
                    y = j*c;
                else if (p == 2)
                    y = j*c+a;
                else
                    y = j*c-a;
                y *= k0;

                if (alpha == 0 && beta == 0)
                    s += gxx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                else if (alpha == 1 && beta == 0)
                    s += gyx(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                else if (alpha == 0 && beta == 1)
                    s += gxy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
                else
                    s += gyy(x,y,0,0)*cexp(I*(kb[0]*x+kb[1]*j*c*k0)/k0);
            }
        }
    }
    return s;
}


void generateMr(double *kb, double a, double *M_re, double *M_im, int N){
  double k0 = 2*M_PI*0.05;
  double nn_onoff = 0;
  double complex tmp = 0.;
  tmp = Sr(1, kb, 0, 0, a, k0,N,nn_onoff);
  M_re[0] = creal(tmp);
  M_im[0] = cimag(tmp);
  tmp = Sr(1, kb, 0, 1, a, k0,N,nn_onoff);
  M_re[1] = creal(tmp);
  M_im[1] = cimag(tmp);
  tmp = Sr(2, kb, 0, 0, a, k0,N,nn_onoff);
  M_re[2] = creal(tmp);
  M_im[2] = cimag(tmp);
  tmp = Sr(2, kb, 0, 1, a, k0,N,nn_onoff);
  M_re[3] = creal(tmp);
  M_im[3] = cimag(tmp);
  /****/
  tmp = Sr(1, kb, 1, 0, a, k0,N,nn_onoff);
  M_re[4] = creal(tmp);
  M_im[4] = cimag(tmp);
  tmp = Sr(1, kb, 1, 1, a, k0,N,nn_onoff);
  M_re[5] = creal(tmp);
  M_im[5] = cimag(tmp);
  tmp = Sr(2, kb, 1, 0, a, k0,N,nn_onoff);
  M_re[6] = creal(tmp);
  M_im[6] = cimag(tmp);
  tmp = Sr(2, kb, 1, 1, a, k0,N,nn_onoff);
  M_re[7] = creal(tmp);
  M_im[7] = cimag(tmp);
  /* printf("%lf\n",creall(tmp)); */
  /****/
  tmp = Sr(3,kb, 0, 0, a, k0,N,nn_onoff);
  M_re[8] = creal(tmp);
  M_im[8] = cimag(tmp);
  tmp = Sr(3, kb, 0, 1, a, k0,N,nn_onoff);
  M_re[9] = creal(tmp);
  M_im[9] = cimag(tmp);

  tmp = Sr(4, kb, 0, 0, a, k0,N,nn_onoff);
  M_re[10] = creal(tmp);
  M_im[10] = cimag(tmp);

  tmp = Sr(4, kb, 0, 1, a, k0,N,nn_onoff);

  M_re[11] = creal(tmp);
  M_im[11] = cimag(tmp);
  /****/
  tmp = Sr(3, kb, 1, 0, a, k0,N,nn_onoff);
  M_re[12] = creal(tmp);
  M_im[12] = cimag(tmp);
  tmp = Sr(3, kb, 1, 1, a, k0,N,nn_onoff);
  M_re[13] = creal(tmp);
  M_im[13] = cimag(tmp);

  tmp = Sr(4, kb, 1, 0, a, k0,N,nn_onoff);
  M_re[14] = creal(tmp);
  M_im[14] = cimag(tmp);

  tmp = Sr(4, kb, 1, 1, a, k0,N,nn_onoff);
  M_re[15] = creal(tmp);
  M_im[15] = cimag(tmp);
}
