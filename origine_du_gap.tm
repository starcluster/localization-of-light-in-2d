<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Origine du gap>>

  Le but de ce document est de comprendre l'origine de la formule:

  <\equation*>
    L<rsub|gap>=2<around*|\||<around*|\||\<Delta\><rsub|B>|\|>-<around*|\||\<Delta\><rsub|A*B>|\|>|\|>
  </equation*>

  <section|Calcul sur les sommets de l'hexagone>

  Si on calcule la matrice <math|M<around*|(|k|)>> pour <math|k> appartenant
  � l'ensemble des sommets de l'hexagone de l'espace r�ciproque:

  <\big-figure|<image|diagram_no.png|0.5par|||>>
    \;
  </big-figure>

  \;

  Alors la matrice est de la forme (points 1,3,5):

  <\equation*>
    M<around*|(|k|)>=<matrix|<tformat|<table|<row|<cell|c+2\<Delta\><rsub|B>+2\<Delta\><rsub|A*B>>|<cell|0>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|c-2\<Delta\><rsub|B>+2\<Delta\><rsub|A*B>>|<cell|\<Omega\>>|<cell|0>>|<row|<cell|0>|<cell|\<Omega\>>|<cell|c+2\<Delta\><rsub|B>-2\<Delta\><rsub|A*B>>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|0>|<cell|c-2\<Delta\><rsub|B>-2\<Delta\><rsub|A*B>>>>>>
  </equation*>

  ou de la forme (points 2,4,6):

  <\equation*>
    M<around*|(|k|)>=<matrix|<tformat|<table|<row|<cell|c+2\<Delta\><rsub|B>+2\<Delta\><rsub|A*B>>|<cell|0>|<cell|0>|<cell|\<Omega\>>>|<row|<cell|0>|<cell|c-2\<Delta\><rsub|B>+2\<Delta\><rsub|A*B>>|<cell|0>|<cell|0>>|<row|<cell|0>|<cell|0>|<cell|c+2\<Delta\><rsub|B>-2\<Delta\><rsub|A*B>>|<cell|0>>|<row|<cell|\<Omega\>>|<cell|0>|<cell|0>|<cell|c-2\<Delta\><rsub|B>-2\<Delta\><rsub|A*B>>>>>>
  </equation*>

  <em|Dans la suite on d�signe <math|\<Delta\><rsub|B>> par <math|B> et
  <math|\<Delta\><rsub|A*B>> par <math|A*B>.>

  Les valeurs propres sont dans le premiers cas:

  <\equation*>
    <around*|{|c-<sqrt|4*A*B<rsup|2>-8*A*B\<cdot\>B+4B<rsup|2>+\<Omega\><rsup|2>>,c+<sqrt|4*A*B<rsup|2>-8*A*B\<cdot\>B+4B<rsup|2>+\<Omega\><rsup|2>>,-2A*B-2*B+c,2*A*B+2B+c|}>
  </equation*>

  Et pour le deuxi�me cas:

  <math|<around*|{|c-<sqrt|4*A*B<rsup|2>+8*A*B\<cdot\>B+4B<rsup|2>+\<Omega\><rsup|2>>,c+<sqrt|4*A*B<rsup|2>+8*A*B\<cdot\>B+4B<rsup|2>+\<Omega\><rsup|2>>,-2A*B+2*B+c,2*A*B-2B+c|}>>

  Or on sait que <math|\<Omega\>\<ggg\>1> donc:

  <\equation*>
    <sqrt|4*A*B<rsup|2>+8*A*B\<cdot\>B+4B<rsup|2>+\<Omega\><rsup|2>>\<approx\>\<Omega\>
  </equation*>

  On peut donc approximer les valeurs propres de la fa�on suivante:

  <\equation*>
    <around*|{|c-\<Omega\>,c+\<Omega\>,c-2*A*B-2B,c+2*A*B+2B|}>
  </equation*>

  Ce qui permet d'�tablir la relation d'ordre suivante:

  <\equation*>
    c-\<Omega\>\<less\><around*|(|c-2*A*B-2B,c+2*A*B+2B|)>\<less\>c+\<Omega\>
  </equation*>

  ou l'ordre des deux valeurs au centre d�pend du signe de <math|A*B> et
  <math|B>. On peut appliquer un raisonnement sur les valeurs propres de la
  deuxi�me matrice. On peut en d�duire que le gap est donc d�fini par la
  distance entre les deux valeurs du centre:

  <\equation*>
    L<rsub|gap1>=<around*|\||4A*B+4B|\|>=4<around*|\||A*B+B|\|>
  </equation*>

  <\equation*>
    L<rsub|gap2>=<around*|\||-4A*B+4B|\|>=4<around*|\||A*B-B|\|>
  </equation*>

  Donc au total le gap est le minimum entre ces largeurs:

  <\equation*>
    min<around*|(|<around*|\||A*B-B|\|>,<around*|\||A*B+B|\|>|)>=<frac|<around*|\||A*B-B|\|>+<around*|\||A*B+B|\|>-<around*|\||<around*|\||A*B+B|\|>-<around*|\||A*B-B|\|>|\|>|2>=<around*|\||<around*|\||A*B|\|>-<around*|\||B|\|>|\|>
  </equation*>

  Rappelons nous qu'en pratique (pour faire le lien avec la physique):

  <\equation*>
    \<omega\>=-<frac|1|2>\<Re\><around*|(|\<lambda\>|)>
  </equation*>

  Donc au final:

  <\equation*>
    L<rsub|gap>=<frac|4<around*|\||<around*|\||A*B|\|>-<around*|\||B|\|>|\|>|2>=2<around*|\||<around*|\||A*B|\|>-<around*|\||B|\|>|\|>
  </equation*>

  Il faut maintenant s'assurer que sur le reste du contour les deux valeurs
  propres du centre ne se rapprochent pas plus que sur les sommets.\ 

  <section|Calcul du coefficient diagonal>

  Essayons maintenant de calculer analytiquement le coefficient <math|c> pour
  retrouver la valeur num�rique de <math|13.9>. On commence par essayer de
  montrer:

  <\equation*>
    S<rsup|1><rsub|x*x><around*|(|k<rsub|>|)>=<big|sum><rsub|i=-\<infty\>><rsup|+\<infty\>><big|sum><rsub|j=-\<infty\>><rsup|+\<infty\>>g<rsup|\<ast\>><rsub|x*x><around*|(|<frac|2\<pi\>i|<sqrt|3>>-k<rsub|x>,<frac|4\<pi\>|3><around*|(|0.5i+j|)>-k<rsub|y>|)>-G<rsup|\<ast\>><rsub|x*x><around*|(|0|)>=0
  </equation*>

  Avec\ 

  <\equation*>
    g<rsup|\<ast\>><rsub|x*x><around*|(|<frac|2\<pi\>i|<sqrt|3>>-k<rsub|x>,<frac|4\<pi\>|3><around*|(|0.5i+j|)>-k<rsub|y>|)>=<around*|(|k<rsub|0><rsup|2>-q<rsub|x><rsup|2>|)><frac|1|2\<pi\>k<rsup|2>><frac|\<pi\>\<mathe\><rsup|-k<rsub|0><rsup|2>a<rsup|2>>|\<Lambda\><around*|(|<math-bf|q>|)>><around*|(|-i+*erfi<around*|(|a\<Lambda\><around*|(|<math-bf|q>|)>/<sqrt|2>|)>|)>
  </equation*>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|2>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Calcul
      sur les sommets de l'hexagone> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Calcul
      du coefficient diagonal> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>