import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
import Gd
import ll2D
import generate_lattice as gl
import utils as u


def computeModes(N, g, a=0.43, d=5, delta_B=12, delta_AB=0):
    M = Gd.createMatrixVectorSum(g, d, delta_B, delta_AB)
    w, v = np.linalg.eig(M)
    N = M.shape[0]

    omega, gamma, iprs = [0] * N, [0] * N, [0] * N

    for i in range(N):
        omega[i], gamma[i], iprs[i] = w[i].real, -w[i].imag, ll2D.IPRVector(i, v)

    # f = open("vector_sum.dat",'w')
    # f.write("x,y,ipr\n")
    # for i in range(N):
    #     f.write(str(gamma[i]) + ',' + str(omega[i]) + ',' +str(iprs[i]) + '\n')
    # f.close()
    return omega, gamma, iprs, v


def displayModes_first(N, g, a=0.43, d=5, delta_B=12, delta_AB=0):
    ax.cla()
    ax.axis((-120, 120, -0.1, 30))
    omega, gamma, iprs, v = computeModes(N, g, a, d, delta_B, delta_AB)

    line = ax.scatter(
        omega, gamma, marker=".", c=iprs, cmap="jet", picker=True, pickradius=5
    )  # 5 points tolerance
    line.set_picker(True)

    plt.subplots_adjust(left=0.25)
    ax.set_xlabel(r"$\omega_n$")
    ax.set_ylabel(r"$\gamma_n$")
    plt.colorbar(line)

    N = int(len(omega) / 2)
    t = (
        "$N_{\\mathrm{atoms}} = $"
        + str(int(N / 2))
        + " | $d=$ "
        + str(d)
        + " | $\\Delta_B =$ "
        + str(delta_B)
        + " | $\\Delta_{AB} =$ "
        + str(delta_AB)
    )
    ax.set_title(t)

    def onpick(event):
        print(event.ind)
        ind = event.ind[0]
        plt.figure()
        u.intensity_map(v, g, ind, omega[ind], iprs[ind], delta_B)

    fig.canvas.mpl_connect("pick_event", onpick)
    fig.canvas.draw_idle()
    # return omega


def displayModes(N, g, a=0.43, d=5, delta_B=12, delta_AB=0):
    ax.cla()

    ax.axis((-120, 120, -0.1, 30))
    omega, gamma, iprs, v = computeModes(N, g, a, d, delta_B, delta_AB)

    line = ax.scatter(
        omega, gamma, marker=".", c=iprs, cmap="jet", picker=True, pickradius=5
    )  # 5 points tolerance
    line.set_picker(True)

    plt.subplots_adjust(left=0.25)
    ax.set_xlabel(r"$\omega_n$")
    ax.set_ylabel(r"$\gamma_n$")
    ax.colorbar()
    # plt.colorbar()
    N = int(len(omega) / 2)
    t = (
        "$N_{\\mathrm{atoms}} = $"
        + str(int(N / 2))
        + " | $d=$ "
        + str(d)
        + " | $\\Delta_B =$ "
        + str(delta_B)
        + " | $\\Delta_{AB} =$ "
        + str(delta_AB)
    )
    ax.set_title(t)

    fig.canvas.draw_idle()
    # return omega


def displayHist(N, g, a, d, delta_B, delta_AB):
    plt.figure()
    omega, gamma, iprs, v = computeModes(N, g, a, d, delta_B, delta_AB)
    plt.style.use("seaborn-white")
    plt.hist(
        omega,
        bins=100,
        density=True,
        range=(-120, 120),
        facecolor="#00dd10",
        edgecolor="#00870a",
        linewidth=0.5,
    )
    t = (
        "$N_{\\mathrm{atoms}} = $"
        + str(int(N / 2))
        + " | $d=$ "
        + str(d)
        + " | $\\Delta_B =$ "
        + str(delta_B)
        + " | $\\Delta_{AB} =$ "
        + str(delta_AB)
    )
    plt.title(t)
    plt.show()


if __name__ == "__main__":
    N = 6
    a = 0.43
    d = 5
    g = gl.generateHexGrid(N, a)
    # g = gl.generate_hex_hex()
    fig, ax = plt.subplots()
    omega = []
    displayModes_first(N, g)
    print(omega)
    axd = plt.axes([0.05, 0.11, 0.02, 0.75])
    d_slider = Slider(
        ax=axd,
        label="$d$",
        valmin=0.1,
        valmax=100,
        valinit=5,
        orientation="vertical",
        color="black",
    )

    axDB = plt.axes([0.1, 0.11, 0.02, 0.75])
    DB_slider = Slider(
        ax=axDB,
        label="$\\Delta_B$",
        valmin=-12,
        valmax=12,
        valinit=12,
        orientation="vertical",
        color="black",
    )

    axDAB = plt.axes([0.15, 0.11, 0.02, 0.75])
    DAB_slider = Slider(
        ax=axDAB,
        label="$\\Delta_{AB}$",
        valmin=-12,
        valmax=12,
        valinit=0,
        orientation="vertical",
        color="black",
    )
    DB_slider.on_changed(
        lambda e: displayModes(N, g, a, d_slider.val, e, DAB_slider.val)
    )
    DAB_slider.on_changed(
        lambda e: displayModes(N, g, a, d_slider.val, DB_slider.val, e)
    )
    d_slider.on_changed(
        lambda e: displayModes(N, g, a, e, DB_slider.val, DAB_slider.val)
    )

    ICON_PLAY = plt.imread("histo_logo.png")
    axButton = plt.axes([0.01, 0.1, 0.02, 0.05])
    bhist = Button(axButton, "", image=ICON_PLAY)
    bhist.on_clicked(lambda f: displayHist(N, g, a, d, DB_slider.val, DAB_slider.val))
    plt.get_current_fig_manager().full_screen_toggle()
    plt.show()
