#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "Faddeeva.h"
#include "li.h"
#include <unistd.h>
#include "sumG.h"
#include <unistd.h>

double complex my_sqrt(double x){
    if (x < 0) return I*sqrt(-x); else return sqrt(x);
}

double complex erfi(double complex z){
  double r = 0.;
  return Faddeeva_erfi(z,r);
}

double complex Lambda(double *q, double k0){
  double qnorm2 = q[0]*q[0] + q[1]*q[1];
  return my_sqrt(k0*k0-qnorm2);
}

double complex chi(double a, double k0){
  return 1./2./M_PI/(k0*k0)*cexp(-a*a*k0*k0/2);
}

double complex I_f(double *q, double k0, double a){
    double complex L = Lambda(q,k0);
    return M_PI*chi(a,k0)/L*(-I+erfi(a*L/sqrt(2)));
}

double complex gstar(int alpha, int beta, double *q, double k0, double a){
  if (alpha==0 && beta==0){
    return (k0*k0-q[0]*q[0])*I_f(q, k0, a);
  } else if ((alpha==0 && beta==1) || (alpha==1 && beta==0)){
    return -q[0]*q[1]*I_f(q,k0,a);
  } else {
    return (k0*k0-q[1]*q[1])*I_f(q, k0, a);
  }
}

double complex gstar_plates(int alpha, int beta, double *q, double k0, double a,  double d){
    double qnorm2 = q[0]*q[0] + q[1]*q[1];
    double complex Iq = 0.;
    if (qnorm2 < k0*k0){   Iq = I/sqrt(k0*k0-qnorm2)/(1.+cexpf(-I*d*sqrt(1-qnorm2/(k0*k0))));
    } else {  Iq = 1./sqrt(qnorm2-k0*k0)/(1.+expf(d*sqrt(qnorm2/(k0*k0) - 1))); };

    if (alpha==0 && beta==0){
        return (k0*k0-q[0]*q[0])*I_f(q,k0,a) + Iq*(1-q[0]*q[0]/(k0*k0));
    } else if ((alpha==0 && beta==1) || (alpha==1 && beta==0)){
        return -q[0]*q[1]*I_f(q,k0,a) + Iq*(-q[0]*q[1]/(k0*k0));
    } else {
        return (k0*k0-q[1]*q[1])*I_f(q,k0,a) + Iq*(1-q[1]*q[1]/(k0*k0));
    }
}

double complex G_star(int alpha, int beta, double k0, double aho){
    if (alpha!=beta)
    return 0.;
  else
    return (erfi(k0*aho/sqrt(2))-I)/exp(k0*k0*aho*aho/2) - (-0.5+k0*k0*aho*aho)/(sqrt(M_PI/2)*cpow(k0*aho,3));
}

double complex G_plates(int alpha, int beta, double k0,double d){
  if (alpha!=beta)
    return 0.;
  else{
    k0 = 1;
    double complex z = cexpf(I*k0*d);
    double complex z2 = -clogf(1+z)/(k0*d)+I*cli2(-z)/cpowf((k0*d),2)-1/cpowf((k0*d),3)*cli3(-z);
    return  3.*z2;
  }
}

void compute_coeff(double kx, double ky, int alpha, int beta, int i, int j, double aho, double k0, double a, double d, int N, double *c_re, double *c_im){
    double complex s = 0.;
    double factor_in_exp = 0.;
    if (i!=j){
      factor_in_exp = 1.;
      if (i==1){
	factor_in_exp = -1.;
      }
    }	
      
    for(int i = -N; i < N+1; i++){
        for(int j = -N; j < N+1; j++){
            double g[] = {2*M_PI/(sqrt(3)*a)*i-kx,2*M_PI*2/(3*a)*(0.5*i+j)-ky};
            if (d!=0)
	      s += gstar_plates(alpha, beta, g, k0, aho, d)*cexp(-factor_in_exp*I*g[1]);
            else
	      s += gstar(alpha, beta, g, k0, aho)*cexp(-factor_in_exp*I*g[1]);
        }
    }
    double complex A = 3*sqrt(3)/2;
    s = s/A;
    if (factor_in_exp==0){
      if (d!=0){
	s -= (k0/6/M_PI)*(G_star(alpha, beta, k0, aho) - G_plates(alpha, beta,k0,d));
      }	else {
	s -= (k0/6/M_PI)*G_star(alpha, beta, k0, aho);
      }
    }
		
    *c_re = creall(s);
    *c_im = cimagl(s);
}

int main(void){
  return 0;
}
