import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np

import lattice_r as lr

class CirclePlotter:
    def __init__(self, coordinates):
        self.coordinates = coordinates
        self.fig, self.ax = plt.subplots(figsize=(8,8))
        self.circles = [Circle(coord, radius=0.1, color='black') for coord in coordinates]
        self.nearest_index = None
        self.list_line_nnn = []


        for circle in self.circles:
            self.ax.add_patch(circle)

        self.fig.canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
        xc,yc = coordinates.T
        self.lx = np.max(xc)-np.min(xc)-a/2
        self.ly = np.max(yc)-np.min(yc)+a/2*np.sqrt(3)
        plt.xlim(min(xc)-a*3, max(xc)+a*3)
        plt.ylim(min(yc)-a*3, max(yc)+a*3)
        plt.gca().set_aspect('equal', adjustable='box')
        plt.show()

    def redraw_circles(self):
        for circle in self.circles:
            self.ax.add_patch(circle)
        self.fig.canvas.mpl_connect('motion_notify_event', self.on_mouse_move)


    def on_mouse_move(self, event):
        for k in range(self.coordinates.shape[0]):
            self.circles[k].set_color('black')
        if event.inaxes is None:
            return

        x, y = event.xdata, event.ydata
        distances = [np.sqrt((x - coord[0]) ** 2 + (y - coord[1]) ** 2) for coord in self.coordinates]
        min_distance_index = np.argmin(distances)

        if self.nearest_index is not None:
            self.circles[self.nearest_index].set_color('black')

        self.circles[min_distance_index].set_color('blue') 
        lnn = list_nn(self.coordinates,self.coordinates[min_distance_index],a)
        lnnn = list_nnn_border_transposed(self.coordinates,self.coordinates[min_distance_index],a)
        
        for p in lnn:
            ind = np.where(np.all(self.coordinates == p, axis=1))[0]
            self.circles[ind[0]].set_color('red')
            
        for line in self.list_line_nnn:
            line.set_visible(False)
        self.list_line_nnn = []

        for k in lnnn:
            p,p_border_transposed = k
            ind = np.where(np.all(self.coordinates == p, axis=1))[0]
            self.circles[ind[0]].set_color('green')
            current = self.coordinates[min_distance_index]

            vect = -(p - current)
            gvnnn = gen_vect_nnn(a)
            bord = False
            if distance(p,current) >=2:
                ll = self.ax.scatter([p_border_transposed[0]],[p_border_transposed[1]],color="orange")
                self.list_line_nnn.append(ll)
                vect = -(p_border_transposed - current)

            trigo = any(np.sum(np.abs(e - vect)) < 1e-9 for e in gvnnn[:3])

            lat1 = self.coordinates[::2]
            lat2 = self.coordinates[1::2]

            if np.any(np.all(current == lat1, axis=1)):
                if trigo:                    
                    line2 = self.ax.plot([current[0],p_border_transposed[0]],[current[1],p_border_transposed[1]],"purple",lw=4)
                else:
                    line2 = self.ax.plot([current[0],p_border_transposed[0]],[current[1],p_border_transposed[1]],"yellow")
            else:
                if trigo:
                    line2 = self.ax.plot([current[0],p_border_transposed[0]],[current[1],p_border_transposed[1]],"yellow")
                else:
                    line2 = self.ax.plot([current[0],p_border_transposed[0]],[current[1],p_border_transposed[1]],"purple",lw=4)

            self.list_line_nnn.append(line2[0])
        self.nearest_index = min_distance_index
        self.fig.canvas.draw()


        
def distance(a,b):
    xa,ya = a
    xb, yb = b
    return np.sqrt((xa-xb)**2+(ya-yb)**2)

def pbc(site_a, site_b, a, lx, ly):
    dx = abs(site_a[0]-site_b[0])
    dy = abs(site_a[1]-site_b[1])
    gen = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1),(0,0)]
    list_quad = [(dx + c[0]*lx,dy+c[1]*ly) for c in gen]
    return list_quad

def pbc_border_transposed(site, a, lx, ly):
    gen = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1),(0,0)]
    list_quad = [(site[0] + c[0]*lx,site[1]+c[1]*ly) for c in gen]
    return list_quad

def are_nn(site_a, site_b, a):
    return distance(site_a,site_b) <= a+0.1

def are_nn_pbc(site_a, site_b, a, lx, ly):
    list_quad = pbc(site_a, site_b, a, lx, ly)
    list_are_nn = [np.sqrt(dx**2+dy**2) < a +0.1 for dx,dy in list_quad]
    return any(list_are_nn)

def are_nnn(site_a,site_b,a):
    dab = distance(site_a,site_b)
    return dab > a*np.sqrt(3)-0.01 and dab < a*np.sqrt(3)+0.01

def are_nnn_pbc(site_a, site_b, a, lx, ly):
    list_quad = pbc(site_a, site_b, a, lx, ly)
    list_are_nn = [np.sqrt(dx**2+dy**2) > a*np.sqrt(3)-0.1 and np.sqrt(dx**2+dy**2) < a*np.sqrt(3)+0.1 for dx,dy in list_quad]
    return any(list_are_nn)

def are_nnn_pbc_border_transposed(pt, potential_neighbour, a, lx, ly):
    list_quad = pbc_border_transposed(potential_neighbour, a, lx, ly)
    list_are_nn = [distance(pt,pn) > a*np.sqrt(3)-0.1 and distance(pt,pn) < a*np.sqrt(3)+0.1 for pn in list_quad]

    for k1,k2 in zip(list_quad, list_are_nn):
        if k2:
            return True, k1
    
    return False, None

def list_nn(lattice, point, a):
    x,y = lattice.T
    lx = np.max(x)-np.min(x)-a/2
    ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    ind = np.argwhere(np.all(lattice==point,axis=1))
    l = lattice[ind[0]]
    selection = np.arange(len(lattice)) != ind[0][0]
    l = lattice[selection]
    l2 = []
    for p in l:
        if are_nn_pbc(p,point,a,lx,ly):
            l2.append(p)
    return l2

def list_nnn(lattice, point, a):
    x,y = lattice.T
    lx = np.max(x)-np.min(x)-a/2
    ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    ind = np.argwhere(np.all(lattice==point,axis=1))
    l = lattice[ind[0]]
    selection = np.arange(len(lattice)) != ind[0][0]
    l = lattice[selection]

    l2 = []
    for p in l:
        if are_nnn_pbc(p,point,a,lx,ly):
            l2.append(p)
    return l2


def list_nnn_border_transposed(lattice, point, a):
    x,y = lattice.T
    lx = np.max(x)-np.min(x)-a/2
    ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    ind = np.argwhere(np.all(lattice==point,axis=1))
    l = lattice[ind[0]]
    selection = np.arange(len(lattice)) != ind[0][0]
    l = lattice[selection]

    l2 = []
    for p in l:
        an, coord_border_transposed = are_nnn_pbc_border_transposed(point,p,a,lx,ly)
        if an:
            l2.append((p,coord_border_transposed))
    return l2



def gen_vect_nnn(a):
    e0 = np.array([-3/2*a,-a*np.sqrt(3)/2])
    e1 = np.array([0,a*np.sqrt(3)])
    em1 = np.array([3/2*a,-a*np.sqrt(3)/2])
    return [e0,e1,em1,-e0,-e1,-em1]



if __name__ == "__main__":
    N_cluster_side = 4
    a = 1
    lattice =  lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=a)
    # lattice = lr.generate_hex_grid(100,1)
    circle_plotter = CirclePlotter(lattice)
