import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import lattice_r as lr


def display_lattice_with_edges(a):
    ax.cla()
    triang = lr.generate_triangular_lattice(4, 3)
    ax.set_xlim(-2, 12)

    for c in triang:
        x, y = np.array(lr.hex_coords(c[0], c[1], a)).T
        x = np.hstack([x, x[0]])
        y = np.hstack([y, y[0]])

        ax.plot(x, y, c="gray", linestyle="--", zorder=1)
        ax.scatter(x, y, c="black", zorder=2)


# Création du graphique
fig, ax = plt.subplots(figsize=(13, 8))
plt.subplots_adjust(left=0.25, bottom=0.25)

# Paramètres du curseur
a_init = 1  # Valeur initiale de a
a_min = 0.0  # Valeur minimale de a
a_max = 3.0  # Valeur maximale de a
a_step = 0.01  # Incrément de a

# Affichage initial de la fonction
display_lattice_with_edges(a_init)

# Création du curseur
ax_a = plt.axes([0.25, 0.1, 0.65, 0.03])
slider_a = Slider(ax_a, "a", a_min, a_max, valinit=a_init, valstep=a_step)


# Fonction de mise à jour
def update_a(val):
    a = slider_a.val
    display_lattice_with_edges(a)
    fig.canvas.draw_idle()


# Association de la fonction de mise à jour au curseur
slider_a.on_changed(update_a)

# Affichage du graphique
plt.show()
