"""Computing Chern number of H+ and H-
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import eig
import csv
from functools import partial

import Gd
import lattice_r as lr
import lattice_f as lf
import tight_binding as tb
from bott2 import all_bott, bott, compute_vxvy_no_pol
import tex
import chern as ch

tex.useTex()

σ_x = np.array([[0, 1], [1, 0]])
σ_y = np.array([[0, -1j], [1j, 0]])
σ_z = np.array([[1, 0], [0, -1]])


def H_p(k, t1, t2):
    return np.sqrt(3)/2*t2*(-k[0]*σ_x+k[1]*σ_y)+(t2-t1)*σ_z

def H_m(k, t1, t2):
    return np.sqrt(3)/2*t2*(k[0]*σ_x+k[1]*σ_y)+(t2-t1)*σ_z

def H(k,t1,t2):
    return np.block([[H_p(k,t1,t2),np.zeros((2,2))],
                     [np.zeros((2,2)),H_m(k,t1,t2)]])

if __name__ == "__main__":
    t1 = 1
    t2 = 1.5
    a = 1
    step = 0.1
    dim = 4
    lattice, BZ, Dk = lf.generate_bz(a, step)
    #lattice = np.flip(lattice, axis=1) * np.array([1, -1])

    ch.plot_lattice_field(partial(H,t1=t1, t2=t2), lattice, Dk, dim)
    plt.show()
    C1 = ch.chern(partial(H, t1=t1, t2=t2), lattice, Dk, dim, [0])
    C2 = ch.chern(partial(H, t1=t1, t2=t2), lattice, Dk, dim, [0])
    print(C1,C2)
