import numpy as np
import matplotlib.pyplot as plt
import ll2D
import Gd
import lattice_r as lr
import ctypes
import sys
from numpy.ctypeslib import ndpointer
import time
from scipy.linalg import eig
from scipy.special import hankel1
import tex

tex.useTex()


def gauss(ω, ω_c, σ):
    return np.exp(-(ω-ω_c)**2/σ**2)

def an(λ,ψ_l,ψ_r,n,t,J,r_src,lattice,k):
    ω_k = np.real(λ[k]) 
    ω_n = np.real(λ[n]) 
    Γ_n = np.imag(λ[n])
    L = np.conj(ψ_l[:,n])
    dot_product = []
    c = 1 # 3*10**8 # ???
    N_sites = lattice.shape[0]
    for i in range(N_sites):
        ri =  r_src - lattice[i]
        ri_norm = np.linalg.norm(ri)
        kr = (ω_n - 1j*Γ_n/2)*ri_norm/c # revenir à cette version 
        k0 = 2*np.pi*0.05
        k0r = k0*ri_norm
        # dot_product.append((L[2*i]*ri[0]*ri[1]/ri_norm**2*ll2D.Q(kr)+L[2*i+1]*(ll2D.P(kr)+ll2D.Q(kr)*ri[1]**2/ri_norm**2))*np.exp(1j*ri_norm)/ri_norm)
        dot_product.append((L[2*i]*ri[0]*ri[1]/ri_norm**2*ll2D.Q(k0r)+L[2*i+1]*(ll2D.P(k0r)+ll2D.Q(k0r)*ri[1]**2/ri_norm**2))*np.exp(1j*k0r)/k0r)
    return np.exp(-1j*ω_n*t-Γ_n/2*t)*np.sum(dot_product)*(ω_n-1j*(1-Γ_n)/2)*gauss(ω_n-1j*Γ_n/2, ω_k, 10)

def field(λ,ψ_l,ψ_r,k,t,J,r_src,lattice):
    N = λ.shape[0]
    E = np.zeros_like(ψ_l[:,0])
    for n in range(N):

        E += ψ_r[:,n]*an(λ, ψ_l, ψ_r, n, t, J, r_src, lattice, k)
    return E


def distance(a,b):
    return np.sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

if __name__ == "__main__":
    print(
        "Python {:s} {:03d}bit on {:s}\n".format(
            " ".join(item.strip() for item in sys.version.split("\n")),
            64 if sys.maxsize > 0x100000000 else 32,
            sys.platform,
        )
    )
    print("NumPy: {0:s}\n".format(np.version.version))

    N = 60
    a = 1
    s = 1.00

    g = lr.generate_hex_grid(N, a)
    g = lr.generate_hex_grid_lb_center(N, a)
    g = lr.generate_hex_hex_stretch(n=100)

    # g,c = gl.generate_hex_grid_stretched(N, a, s)
    pos_src = 2
    g = np.array(g[:pos_src].tolist()+g[pos_src+1:].tolist())
    n = g.shape[0]
    lr.display_lattice(g[:n//2], "r")
    lr.display_lattice(g[n//2:], "b")
    # lr.display_lattice(g[pos_src], "black")
    plt.show()


    delta_B = -12
    delta_AB = 0
    d = 2

    k0 = 2*0.05*np.pi/a
    # M = Gd.create_matrix_TE(g, delta_B, delta_AB, k0)
    M = Gd.create_matrix_TE_plates(g, delta_B, delta_AB, k0, 2)

    N = M.shape[0]
    w, vl, vr = eig(M, left=True, right=True)
    f = -np.real(w)/2
    idx = f.argsort()
    w = w[idx]
    vl = vl[:,idx]
    vr = vr[:,idx]
    # plt.hist(-np.real(w)/2, bins=80, histtype="step", color="red")
    # plt.show()
    J = np.zeros((N,1),dtype=complex)
    for i in range(n):
        if i!=pos_src:
            J[2*i] = hankel1(0, k0*distance(g[pos_src], g[i]))*1
            J[2*i+1] = 0 # hankel1(0, k0*distance(g[pos_src], g[i]))*1
        else:
            J[2*i] = 0
            J[2*i+1] = 0

    # print(J)
    for i,l in enumerate(w):
        print(i,l)


    k =  n//2 
    x,y = g.T

    ψ_even = vr[:,k][::2]
    ψ_odd = vr[:,k][1::2]
    intensity_field = (np.abs(ψ_even) + np.abs(ψ_odd))*100
    plt.scatter(x,y,c=intensity_field, s=intensity_field)
    plt.colorbar()
    plt.show()
        


    r_src = np.array([10,10])
    N_t = 100
    ts = np.linspace(0.01,10,N_t)
    vmax = 0
        

    for i,t in enumerate(ts):
        E = field(w, vl, vr, k, t, J, r_src, g)
        E_even = E[::2]
        E_odd = E[1::2]
        intensity_field = np.abs(E_even) + np.abs(E_odd)
        if vmax == 0:
            vmax = np.max(intensity_field)
        plt.scatter(x,y,c=intensity_field,vmin=0, vmax=vmax)
        plt.colorbar()
        plt.scatter(r_src[0], r_src[1], c="black")
        plt.xlabel(r"$x$",fontsize=20)
        plt.ylabel(r"$y$",fontsize=20)
        plt.title(f"$t={np.round(t,2)}$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.savefig(f"wave_bake_{delta_B}_{delta_AB}_{d}_defect/{i}.png",format="png",bbox_inches='tight')
        plt.clf()
        plt.cla()
        print(f"{i}/{N_t}")
        # plt.show()
    exit()
