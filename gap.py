import numpy as np
from scipy.special import erfi
import generate_lattice as gl
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import generate_mk as gMk
import GS15 as gs15

Omega = 229.95
c = -13.93
B = 6


def l1(AB):
    return c - np.sqrt(4 * AB**2 + 8 * AB * B + 4 * B**2 + Omega**2)


def l2(AB):
    return c + np.sqrt(4 * AB**2 + 8 * AB * B + 4 * B**2 + Omega**2)


def l3(AB):
    return c - 2 * AB + 2 * B


def l4(AB):
    return c + 2 * AB - 2 * B


def l5(AB):
    return c + 2 * AB + 2 * B


def l6(AB):
    return c - 2 * AB - 2 * B


ABs = np.array([i for i in range(-100, 100)])


def gap(AB):
    x3, x4, x5, x6 = l3(AB), l4(AB), l5(AB), l6(AB)
    l = [x3, x4, x5, x6]
    l.sort()
    return l[2] - l[1]


a = 1
Ndots = 200
lattice = gl.generate_bz(a, Ndots, 1)
gl.display_lattice(lattice)
N = 20
K = 0
aho = 0.1
deltaB, deltaAB = 30, 6
distance = 2
k0 = 2 * np.pi * 0.05 / a
step = 0.05
plates = 0

deltaAB = 0
B1, B2, B3, B4, X, Y, omegas, gammas = gs15.band_diagram(
    lattice, distance, deltaB, deltaAB, k0, aho, K, N, plates
)
delta_bandes = np.array(B3) - np.array(B2)
ind = np.argmin(delta_bandes)
print(np.min(delta_bandes))
print(lattice[ind])

plt.show()

plt.plot(ABs, l1(ABs))
plt.plot(ABs, l2(ABs))
plt.plot(ABs, l3(ABs))
plt.plot(ABs, l4(ABs))
plt.plot(ABs, l5(ABs))
plt.plot(ABs, l6(ABs))
plt.show()
plt.plot(ABs, [gap(x) for x in ABs])
plt.show()
