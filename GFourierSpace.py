import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
from ll2D import P, Q
import generate_lattice as gl
import ctypes
import G3D
import GS15

libc = ctypes.CDLL("./sumG.o")

libc.sum_gxx_re.restype = ctypes.c_double
libc.sum_gxx_re.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gyy_re.restype = ctypes.c_double
libc.sum_gyy_re.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gxy_re.restype = ctypes.c_double
libc.sum_gxy_re.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gyx_re.restype = ctypes.c_double
libc.sum_gyx_re.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gxx_im.restype = ctypes.c_double
libc.sum_gxx_im.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gyy_im.restype = ctypes.c_double
libc.sum_gyy_im.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gxy_im.restype = ctypes.c_double
libc.sum_gxy_im.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_gyx_im.restype = ctypes.c_double
libc.sum_gyx_im.argtypes = (ctypes.c_double, ctypes.c_double, ctypes.c_double)

libc.sum_diag_im.restype = ctypes.c_double
libc.sum_diag_im.argtypes = (ctypes.c_double,)


def prod(x, y):
    return np.array([[x[0] * y[0], x[1] * y[0]], [x[0] * y[1], x[1] * y[1]]])


def GHex(r):
    d = 1 / np.sqrt(2) * np.array([[1, 1j], [-1, 1j]])
    r_norm = np.linalg.norm(r)
    A = (
        3
        / 2
        * np.exp(1j * r_norm)
        / r_norm
        * (P(1j * r_norm) * np.eye(2) + Q(1j * r_norm) * prod(r, r) / (r_norm**2))
    )
    return (d @ A) @ (d.conj().T)


def G(alpha, beta, r):
    return GHex(r)[alpha, beta]


def GAA(alpha, beta, r, d):
    imagSum = G3D.diagImSum(d)
    mat = np.array(
        [
            [1j * imagSum + 2 * delta_AB + 2 * delta_B, 0],
            [0, 1j * imagSum + 2 * delta_AB - 2 * delta_B],
        ]
    )
    print(mat)

    if np.linalg.norm(r) == 0:
        return mat[alpha, beta]
    else:
        return GHex(r)[alpha, beta]


def GBB(alpha, beta, r, d):
    imagSum = G3D.diagImSum(d)
    mat = np.array(
        [
            [1j * imagSum - 2 * delta_AB + 2 * delta_B, 0],
            [0, 1j * imagSum - 2 * delta_AB - 2 * delta_B],
        ]
    )
    if np.linalg.norm(r) == 0:
        return mat[alpha, beta]
    else:
        return GHex(r)[alpha, beta]


def G_plates(alpha, beta, r, d, typeSite="AA"):
    x = r[0]
    y = r[1]
    if np.linalg.norm(r) == 0:
        if typeSite == "AA":
            return GAA(alpha, beta, r, d)
        else:
            return GBB(alpha, beta, r, d)
    N = 10
    R = r
    A = np.zeros((2, 2), dtype=complex)
    # for i in range(-N,N+1):
    #     r = np.array([R[0],R[1],i*d])
    #     A += GHex(r)
    A[0, 0] = 3 / 2 * (libc.sum_gxx_re(x, y, d) + 1j * libc.sum_gxx_im(x, y, d))
    A[1, 1] = A[0, 0]
    A[0, 1] = 3 / 2 * (libc.sum_gxy_re(x, y, d) + 1j * libc.sum_gxy_im(x, y, d))
    A[1, 0] = 3 / 2 * (libc.sum_gyx_re(x, y, d) + 1j * libc.sum_gyx_im(x, y, d))
    return A[alpha, beta]


def S1(k, alpha, beta, grid, d):
    s = 0
    j = 0
    N = grid.shape[0]
    for i, e in enumerate(grid):
        if i < int(N / 2):
            # s += GAA(alpha,beta,e*k0a)*np.exp(1j*k@e*k0a)
            s += G_plates(alpha, beta, e * k0a, d, "AA") * np.exp(1j * k @ e * k0a)

    return s


def S2(k, alpha, beta, grid, d):
    s = 0
    j = 0
    N = grid.shape[0]
    for i, e in enumerate(grid):
        if i < int(N / 2):
            # s += G(alpha,beta,(e+a1)*k0a)*np.exp(1j*k@e*k0a)
            s += G_plates(alpha, beta, (e + a1) * k0a, d) * np.exp(1j * k @ e * k0a)
    return s


def S3(k, alpha, beta, grid, d):
    s = 0
    j = 0
    N = grid.shape[0]
    for i, e in enumerate(grid):
        if i < int(N / 2):
            # s += G(alpha,beta,(e-a1)*k0a)*np.exp(1j*k@e*k0a)
            s += G_plates(alpha, beta, (e - a1) * k0a, d) * np.exp(1j * k @ e * k0a)
    return s


def S4(k, alpha, beta, grid, d):
    s = 0
    j = 0
    N = grid.shape[0]
    for i, e in enumerate(grid):
        if i < int(N / 2):
            # s += GBB(alpha,beta,e*k0a)*np.exp(1j*k@e*k0a)
            s += G_plates(alpha, beta, e * k0a, d, "BB") * np.exp(1j * k @ e * k0a)
    return s


def generate_mk(k, grid, d):
    return np.array(
        [
            [
                S1(k, 0, 0, grid, d),
                S1(k, 0, 1, grid, d),
                S2(k, 0, 0, grid, d),
                S2(k, 0, 1, grid, d),
            ],
            [
                S1(k, 1, 0, grid, d),
                S1(k, 1, 1, grid, d),
                S2(k, 1, 0, grid, d),
                S2(k, 1, 1, grid, d),
            ],
            [
                S3(k, 0, 0, grid, d),
                S3(k, 0, 1, grid, d),
                S4(k, 0, 0, grid, d),
                S4(k, 0, 1, grid, d),
            ],
            [
                S3(k, 1, 0, grid, d),
                S3(k, 1, 1, grid, d),
                S4(k, 1, 0, grid, d),
                S4(k, 1, 1, grid, d),
            ],
        ]
    )


if __name__ == "__main__":
    N = 8
    d = 1000
    a = 1
    a1 = np.array([0, a])

    number_of_dots = 100
    kx1 = np.linspace(np.pi / (np.sqrt(3) * a), 0, int(number_of_dots / 3))
    ky1 = np.linspace(np.pi / (3 * a), 0, int(number_of_dots / 3))
    kx2 = np.linspace(0, 4 * np.pi / (3 * np.sqrt(3) * a), int(number_of_dots / 3))
    ky2 = np.linspace(0, 0, int(number_of_dots / 3))
    kx3 = np.linspace(
        4 * np.pi / (3 * np.sqrt(3) * a),
        np.pi / (np.sqrt(3) * a),
        int(number_of_dots / 3),
    )
    ky3 = np.linspace(0, np.pi / (3 * a), int(number_of_dots / 3))

    lattice = gl.generateHexGrid(N, a)
    # lattice = gl.generate_hex_hex()
    gl.display_lattice(lattice)
    delta_B, delta_AB = 0, 0
    k0a = 2 * np.pi * 0.05
    colors = ["green", "blue", "red", "magenta", "yellow", "peru"]
    colors_shades = ["black", "dimgray", "gray", "darkgray", "silver", "lightgray"]
    t0 = time.time()
    # for h,color in zip([0.04,0.05,0.06,0.08,0.09,0.1], colors_shades):#[0.04 + i/1000 for i in range(0,70)]:
    h = 0.05
    for d in [0.1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 30, 100]:
        h = np.round(h, 3)
        k0a = 2 * np.pi * h
        kx = np.concatenate([kx1, kx2, kx3]) / k0a
        ky = np.concatenate([ky1, ky2, ky3]) / k0a
        i = 0
        omegas = []
        gammas = []
        tt0 = time.time()
        for kxx, kyy in zip(kx, ky):
            k = np.array([kxx, kyy])
            Mk = generate_mk(k, lattice, d * k0a)

            lambdas, vects = np.linalg.eig(Mk)

            omega = [0] * 4
            gamma = [0] * 4
            k_norm = np.linalg.norm(k)
            for j in range(4):
                omega[j] = -0.5 * lambdas[j].real  # *(k0a)**3
                gamma[j] = lambdas[j].imag
                if abs(gamma[j]) > 100:
                    # gamma[j] = 0
                    print("oh la")
                    print(f"1/gamma {gamma[j]}")
            omegas += omega
            gammas += gamma

            i += 1
        tt1 = time.time()
        print(tt1 - tt0)

        x = [i for i in range(0, len(omegas))]
        sgn_gammas = np.array([np.sign(e) for e in gammas])
        sc = plt.scatter(x, omegas, marker="+", c=gammas)
        # sc = plt.scatter(x,omegas, marker='+', c=sgn_gammas)
        n1 = np.count_nonzero(sgn_gammas == -1.0)
        print(n1)
        # print(sgn_gammas)
        # print(n1)
        # print(n1/len(sgn_gammas))
        plt.colorbar()
        minis = sorted(gammas)[0:189]
        # print(minis)

        print("[", end="")
        for k in range(len(minis)):
            for i in range(len(gammas)):
                if gammas[i] == minis[k]:
                    print(f"({omegas[i]},{minis[k]}),", end="")
        print("]")
        plt.title(f"d={d}")
        plt.axis((0, 400, -150, 150))
        plt.savefig(f"d/{d}.png")
        # plt.show()
        plt.clf()

        # plt.scatter(omegas,gammas,c=-sgn_gammas)
        # plt.show()
        # sc = plt.scatter(x,omegas, marker='+', color=color, label=f"{h}$\\times 2\\pi$")

        # plt.plot(x,omegas, color=color, label=f"{h}$\times 2\pi$")
        # plt.title(f"$N=$ {lattice.shape[0]} | $a=$ {a} | $k_0a = 2\\pi \\times$ {h} | $\\Delta_B$ = {delta_B}", fontsize=20)
        # plt.xlabel("Brillouin Zone", fontsize=20)
        # plt.ylabel("$-\\frac{1}{2}\\mathcal{R}(\\lambda)\\times (k_0a)^3$", fontsize=20)
        yc = 7
        y1 = [yc + 12 for i in range(0, len(omegas))]
        y2 = [yc - 12 for i in range(0, len(omegas))]
        # plt.plot(x,y1,color="green")
        # plt.plot(x,y2, color="green")
        # plt.fill_between(x,y1,y2,color=(68/255,164/255,71/255,0.2))
        # plt.colorbar(sc)
        # plt.axis((0,400,-150,150))
    t1 = time.time()
    print(t1 - t0)
    plt.tight_layout()
    plt.legend()
    # plt.show()

    # plt.savefig(f"h/{h}.png")
    # plt.clf()
    # plt.cla()
