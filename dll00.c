#include <stdint.h>

#if defined(_WIN32)
#  define DLL00_EXPORT_API __declspec(dllexport)
#else
#  define DLL00_EXPORT_API
#endif


#if defined(__cplusplus)
extern "C" {
#endif

DLL00_EXPORT_API double dll00Func00(double **ppArr);

#if defined(__cplusplus)
}
#endif


double dll00Func00(double **ppArr)
{
  ppArr[5][5] = 2.;
  return ppArr[5][5];
}
