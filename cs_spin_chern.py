from sympy import *
from sympy.vector import CoordSys3D
import numpy as np

import six_cell

N = CoordSys3D("N")

if __name__ == "__main__":
    kx = var("k_x", real=True)
    ky = var("k_y", real=True)
    # kx = 0
    # ky = 0
    k = Matrix([kx, ky])
    ######################################################################################
    a = 1
    a1 = Matrix([sqrt(3), 0]) * a
    a2 = Matrix([sqrt(3) / 2, 3 / 2]) * a
    ######################################################################################
    t1 = var("t_1", real=True, positive=True)
    t2 = var("t_2", real=True, positive=True)
    ######################################################################################
    σ_x = Matrix([[0, 1], [1, 0]])
    σ_y = Matrix([[0, -I], [I, 0]])
    σ_z = Matrix([[1, 0], [0, -1]])

    # From Barik et al.
    H_p = t2 * a * (-kx * σ_x + ky * σ_y) + σ_z * (t2 - t1)
    H_m = t2 * a * (kx * σ_x + ky * σ_y) + σ_z * (t2 - t1)
    O = zeros(2, 2)

    H_barik = Matrix(BlockMatrix([[H_p, O], [O, H_m]]))
    H_barik.applyfunc(nsimplify)
    pprint(H_barik)

    l, m, v = H_barik.eigenvects()[0]

    P = six_cell.spdf_sympy()
    pprint(H_barik.eigenvals())

    P = v[0] * v[0].T.conjugate() + v[1] * v[1].T.conjugate()
    sigma = zeros(4, 4)
    for i in range(4):
        sigma[i, i] = (-1) ** i
    psp = P * sigma * P
    pprint(psp.eigenvects())
