import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme()
import numpy as np

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

chern_map_csv = pd.read_csv("chern_map_hd.csv")
chern_map = chern_map_csv.pivot("deltaB", "deltaAB", "C")

plt.xlabel(r"$\Delta_{AB}$", fontsize=30)
plt.xticks(fontsize=20)

plt.ylabel(r"$\Delta_{B}$", fontsize=30)
plt.yticks(fontsize=20)

plt.imshow(chern_map, cmap="plasma", interpolation="none", extent=[-12, 12, -12, 12])
plt.grid(False)
plt.savefig("chern_map.pdf", format="pdf", bbox_inches="tight", dpi=1200)
plt.show()
