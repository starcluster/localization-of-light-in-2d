import numpy as np
import matplotlib.pyplot as plt
import generate_lattice as gl
import G_hex_poo as ghp

if __name__ == "__main__":
    N = 10
    a = 1
    s = 0.7
    g = gl.generate_hex_grid_stretched(N, a, s)
    gl.display_lattice(g, "red")
    # gl.display_lattice(gc,'blue')
    plt.show()
