<TeXmacs|2.1>

<style|<tuple|beamer|french>>

<\body>
  <screens|<\hidden>
    <tit|Parties imaginaires>

    <\big-figure|<image|imaginary_part_B0AB0dinf.png|1par|||>>
      Une partie imaginaire <math|\<approx\> 250>
    </big-figure>
  </hidden>|<\hidden>
    <tit|Calcul d'int�grale>

    On veut donc calculer pour <math|n\<neq\>0>:

    <\equation*>
      <big|int><rsub|\<bbb-R\>><frac|\<mathe\><rsup|i*q<rsub|z>n*d>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2><rsup|>>\<mathd\>q<rsub|z>=<big|int><rsub|><frac|\<mathe\><rsup|i*q<rsub|z>*n*d>|\<omega\><rsup|2>-q<rsub|z><rsub|><rsup|2><rsup|>>\<mathd\>q<rsub|z>
    </equation*>

    O� on a pos� <math|\<omega\><rsup|2>=k<rsup|2>-><math|<math-bf|q><rsup|2>>,
    on d�cale les poles � l'int�rieur et l'ext�rieur du contour:

    <\equation*>
      <big|int><rsub|><frac|\<mathe\><rsup|i*q<rsub|z>*n*d>|\<omega\><rsup|2>-q<rsub|z><rsub|><rsup|2><rsup|>>\<mathd\>q<rsub|z>=2\<pi\>i*Res<around*|(|\<omega\>|)>=\<pi\>i*<frac|\<mathe\><rsup|-i*\<omega\>*n*d>|-\<omega\>>=<frac|\<pi\><around*|(|sin<around*|(|n*d*<sqrt|k<rsup|2>-q<rsup|2>>|)>-i*cos<around*|(|n*d**<sqrt|k<rsup|2>-q<rsup|2>>|)>|)>|<sqrt|k<rsup|2>-q<rsup|2>>>
    </equation*>

    Donc:

    <\equation*>
      <big|sum><rsub|<tabular|<tformat|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>><frac|<around*|(|-1|)><rsup|n>|\<cal-A\>><frac|\<pi\><around*|(|sin<around*|(|n*d*<sqrt|k<rsup|2>-q<rsup|2>>|)>-i*cos<around*|(|n*d**<sqrt|k<rsup|2>-q<rsup|2>>|)>|)>|<sqrt|k<rsup|2>-q<rsup|2>>>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>+Terme
      0
    </equation*>
  </hidden>|<\shown>
    <tit|??>

    Parties imaginaires incoh�rentes. Bug num�rique ?

    <math|\<longrightarrow\>> Bug math�matique <math|\<omega\>> peut �tre
    imaginaire pur !

    \;
  </shown>|<\hidden>
    <tit|Diff�rentes zones de Brillouin>

    <\big-figure|<image|haxagon_BZs.png|0.5par|||>>
      Bleu = 1, Orange = 2, Rouge = 3, Vert = 4
    </big-figure>
  </hidden>|<\hidden>
    <tit|Largeur du gap en fonction de <math|\<Delta\><rsub|B>/\<Delta\><rsub|A*B>>>

    <\big-figure>
      <image|width_gap_BZ1.png|0.35par|||><image|width_gap_BZ2.png|0.35par|||>

      <image|width_gap_BZ3.png|0.35par|||><image|width_gap_BZ4.png|0.35par|||>
    <|big-figure>
      Ne correspond pas � longtext.
    </big-figure>
  </hidden>|<\hidden>
    <tit|Surfaces pour un hexagone dans l'espace r�ciproque>

    <\big-figure|<image|3D.png|1par|||>>
      \;
    </big-figure>
  </hidden>|<\hidden>
    \;
  </hidden>>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-2|<tuple|2|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-3|<tuple|3|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
    <associate|auto-4|<tuple|4|?|../.TeXmacs/texts/scratch/no_name_1.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Une partie imaginaire <with|color|<quote|#503050>|font-family|<quote|rm>|<with|mode|<quote|math>|\<approx\>
        250>>
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Bleu = 1, Orange = 2, Rouge = 3, Vert = 4
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Ne correspond pas � longtext.
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>