from sympy import *
from sympy.vector import CoordSys3D
import numpy as np

from taylor_2d import Taylor_polynomial_sympy
import six_cell

N = CoordSys3D("N")


def drop_small_terms(expr, vars):
    poly = Poly(expr, *vars)
    threshold = 1e-5
    poly_coeffs = poly.coeffs()
    to_remove = [i for i in poly_coeffs if Abs(i) < threshold]
    print(to_remove)
    for i in to_remove:
        poly = poly.subs(i, 0)
    if isinstance(poly, Poly):
        return poly.expr
    else:
        return poly


def drop_small_terms_2(expr):
    threshold = 1e-5
    terms = expr.as_ordered_terms()
    filtered_terms = [
        term for term in terms if abs(term.as_coefficient(kx)) > threshold
    ]
    new_expr = sp.Add(*filtered_terms)
    return new_expr


def round_expr(expr, num_digits):
    """https://stackoverflow.com/questions/48491577/printing-the-output-rounded-to-3-decimals-in-sympy"""
    return expr.xreplace({n: round(n, num_digits) for n in expr.atoms(Number)})


if __name__ == "__main__":
    kx = var("k_x", real=True)
    ky = var("k_y", real=True)
    # kx = 0
    # ky = 0
    k = Matrix([kx, ky])
    ######################################################################################
    a = 1
    a1 = Matrix([sqrt(3), 0]) * a
    a2 = Matrix([sqrt(3) / 2, 3 / 2]) * a
    ######################################################################################
    t1 = var("t_1", real=True, positive=True)
    t2 = var("t_2", real=True, positive=True)
    ######################################################################################
    H1 = Matrix(
        [
            [0, 1, 0, 0, 0, 1],
            [1, 0, 1, 0, 0, 0],
            [0, 1, 0, 1, 0, 0],
            [0, 0, 1, 0, 1, 0],
            [0, 0, 0, 1, 0, 1],
            [1, 0, 0, 0, 1, 0],
        ]
    )
    H2 = Matrix(
        [
            [0, 0, 0, exp(1j * k.dot(a1)), 0, 0],
            [0, 0, 0, 0, exp(1j * k.dot(a2)), 0],
            [0, 0, 0, 0, 0, exp(1j * k.dot(a2 - a1))],
            [exp(-1j * k.dot(a1)), 0, 0, 0, 0, 0],
            [0, exp(-1j * k.dot(a2)), 0, 0, 0, 0],
            [0, 0, exp(-1j * k.dot(a2 - a1)), 0, 0, 0],
        ]
    )

    H = -t1 * H1 - t2 * H2
    eigenvalues_of_H_gamma = [
        -2 * t1 * cos(pi * n / 3) - (-1) ** n * t2 for n in range(6)
    ]
    P = six_cell.spdf_sympy()
    # pprint(P)

    # pprint(simplify(P*(P.transpose().conjugate())))

    # not normed !!!!!!!!
    H2_dev = H2.applyfunc(lambda t: Taylor_polynomial_sympy(t, [kx, ky], [0, 0], 1))


    H_handmade = Matrix(
        [
            [0, 1, 0, (1 + 3*I * kx) * t2, 0, 1],
            [0, 0, 1, 0, (1 + I * (3 / 2 * kx + 3 * sqrt(3) / 2 * ky)) * t2, 0],
            [0, 0, 0, 1, 0, (1 + I * (-3 / 2 * kx + 3 * sqrt(3) * ky / 2)) * t2],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
        ]
    )
    # H2_dev = H2_dev.subs(kx,0)

    # pprint(simplify(P.inv(method='ADJ')))
    # P_np = np.array(P).astype(complex)
    # P_inv = np.linalg.inv(P_np)
    # P_inv_re = np.real(P_inv)
    # P_inv_im = np.imag(P_inv)
    # P_inv_re[np.abs(P_inv_re) < 1e-6] = 0
    # P_inv_im[np.abs(P_inv_im) < 1e-6] = 0
    # P_inv = P_inv_re + 1j * P_inv_im
    # P_inv = Matrix(P_inv)
    pprint(P)
    pprint(P.inv())
    
    # PH2P = (P.inv() * H2_dev * P).extract(list(range(0, 4)), list(range(0, 4)))
    H_handmade = H_handmade.H + H_handmade
    # pprint(H_handmade)
    # exit()
    PH2P = (P.inv() * H_handmade * P).extract(list(range(0, 4)), list(range(0, 4)))

    # pprint(PH2P[0, 1])

    pprint(round_expr(simplify(PH2P), 4))
    exit()

    # print(drop_small_terms_2(1e-7*kx + ky))
    # print(drop_small_terms_2())
    # print(drop_small_terms(-1.38777878078145e-17*sqrt(3)*1j*kx, [kx,ky]))
    # exit()

    # pprint(simplify(PHP))
    # assume(t1, 'real')
    # assume(t1 > 0)
    s_ph2p = simplify(PH2P.applyfunc(lambda x: drop_small_terms(x, [kx, ky])))
    s_ph2p = s_ph2p.subs(s_ph2p[0, 2], 0)
    s_ph2p = s_ph2p.subs(s_ph2p[1, 2], sqrt(3) * kx * 0.25 * 1j + 0.25 * ky * 1j)
    s_ph1p = simplify(P.inv() * H1 * P).extract(list(range(0, 4)), list(range(0, 4)))
    s_php = -t1 * s_ph1p - t2 * s_ph2p

    pprint(s_php)
    exit()
    # pprint(nsimplify(PHP,rational=True))
    pprint(list(map(lambda x: round_expr(x, 5), list(s_php.eigenvals().keys()))))
    print(s_php.eigenvects())
    pprint(list(map(lambda x: round_expr(x, 5), s_php.eigenvects())))

    exit()

    # pprint(H2_dev)

    # pprint((-t1*H1-t2*H2_dev).eigenvals()) it works with the approx.
    σx = Matrix([[0, 1], [1, 0]])
    σy = Matrix([[0, -1j], [1j, 0]])
    σz = Matrix([[1, 0], [0, -1]])

    submatrix = H2_dev.extract(list(range(0, 3)), list(range(3, 6)))
    pprint(submatrix)

    Hp = sqrt(3) / 2 * t2 * (-kx * σx + ky * σy) + (t2 - t1) * σz
    pprint(Hp)
    # pprint(Hp.eigenvects())
