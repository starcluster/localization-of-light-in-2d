#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

#define MAX_ROWS 196
#define MAX_COLS 196

void charger_matrice(const char* nom_fichier, int* rows, int* cols, double complex **matrice) {
    FILE* fichier = fopen(nom_fichier, "r");
    if (fichier == NULL) {
        fprintf(stderr, "Erreur lors de l'ouverture du fichier.\n");
        exit(EXIT_FAILURE);
    }

    *rows = 0;
    *cols = 0;
    double re, im;

    while (fscanf(fichier, "%lf,%lf", &re, &im) == 2) {
        if (*cols >= MAX_COLS) {
            fprintf(stderr, "Nombre de colonnes dépassé.\n");
            exit(EXIT_FAILURE);
        }

        matrice[*rows][*cols] = re + im * I;
        *cols += 1;
        if (*cols == 1)
            *rows += 1;
    }

    fclose(fichier);
}

void afficher_matrice(int rows, int cols, double complex matrice[MAX_ROWS][MAX_COLS]) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%.2f%+.2fi ", creal(matrice[i][j]), cimag(matrice[i][j]));
        }
        printf("\n");
    }
}

int main() {
    int rows, cols;
    double complex matrice[MAX_ROWS][MAX_COLS];

    charger_matrice("v_topo.csv", &rows, &cols, matrice);
    afficher_matrice(rows, cols, matrice);

    return 0;
}
