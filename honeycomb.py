"""
Author: Pierre Wulles
Credits: Pierre Wulles, Sergey Skipetrov
License: GNU GPL
Maintainer: Pierre Wulles
Email: pierre.wulles@lpmmc.cnrs.fr
Status: Phd Student
Laboratory: LPMMC -- Grenoble -- France
Date: 2023-04-14
Username: Starcluster
Description: Honeycomb class
Repository git: https://gitlab.com/starcluster
Python 3.10.9 (main, Dec 19 2022, 17:35:49) [GCC 12.2.0]
NumPy: 1.22.3
"""
from numpy import pi

import generate_mk as gMk
import plot
import generate_lattice as gl
from modele import Modele


class Honeycomb(Modele):
    """Describes the physics of wave propagation in a honeycomb
    lattice of point scatterers.
    Relevant parameters are:
        a: spacing between sites.
        Δ_B: Zeeman shift due to the magnetic field = B*μ/Γ0
        Δ_AB: Frequency detuning between sublattices A and B in units of Γ0
        distance: If 0 no plates, else distance between plates in units of k0
        k0: wavevector such that k0*a = 2π*0.05
        aho: cut off see https://doi.org/10.1103/PhysRevLett.119.023603
        n_sum: number of term in the sommation over Fourier space
    """

    def __init__(
        self, a=1, Δ_B=0, Δ_AB=0, distance=0, k0=2 * pi * 0.05, aho=0.1, n_sum=80
    ):
        super().__init__()
        self.a = a
        self.Δ_B = Δ_B
        self.Δ_AB = Δ_AB
        self.distance = distance
        self.k0 = k0
        self.aho = aho
        self.n_sum = n_sum

    def compute_ham(self, k):
        return gMk.generate_mk_wrapper(
            k, self.distance, self.Δ_B, self.Δ_AB, self.k0, self.aho, self.n_sum, self.a
        )

    def compute_bands(self, path):
        super().compute_bands(path)
        self.bands *= -0.5


if __name__ == "__main__":
    a = 1
    Δ_B = 12
    Δ_AB = 0
    distance = 0
    k0 = 2 * pi * 0.05 / a
    aho = 0.1
    n_sum = 80
    hc = Honeycomb(a=a, Δ_B=12)
    lattice = gl.generate_bz(a, 100, 1)
    hc.compute_bands(lattice)
    plot.plot_bands(lattice, hc.get_bands(), "honeycomb", [], [], hc.get_gammas())
