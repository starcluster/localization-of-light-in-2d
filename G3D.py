from sympy import *
from sympy.plotting import plot3d, plot
import matplotlib.pyplot as plt
import numpy as np
import ctypes
from sympy.physics.quantum.dagger import Dagger
from sympy import hankel1
import time

libc = ctypes.CDLL("./sumG.o")

libc.sum_diag_re.restype = ctypes.c_double
libc.sum_diag_re.argtypes = (ctypes.c_double,)

libc.sum_diag_im.restype = ctypes.c_double
libc.sum_diag_im.argtypes = (ctypes.c_double,)

libc.sum_diag_bis.restype = ctypes.c_double
libc.sum_diag_bis.argtypes = (ctypes.c_double, ctypes.c_double)


def f(x, y, z):
    return exp(1j * sqrt(x**2 + y**2 + z**2)) / sqrt(x**2 + y**2 + z**2)


def P(x):
    return 1 - 1 / x + 1 / x**2


def Q(x):
    return -1 + 3 / x - 3 / x**2


def g(x, y, z):
    r = sqrt(x**2 + y**2 + z**2)
    return f(x, y, z) * (P(1j * r) + Q(1j * r) * (x**2 + y**2) / r**2)


def h(rho, z):
    r = sqrt(rho**2 + z**2)
    return exp(1j * r) / r * (P(1j * r) + Q(1j * r) * rho**2 / r**2)


def sinusCard(n):
    return sin(n) / n


def lop1(t, d):
    return t * sin(d) / (sin(d) ** 2 + (t + cos(d)) ** 2)


def lop1bis(t, d):
    return -sin(d) / (1 + 2 * cos(d) * t + t**2)


def lop2(t, d):
    return t * (cos(d) * exp(t) + 1) / (exp(2 * t) + 2 * exp(t) * cos(d) + 1)


def lop3(t, d):
    return t**2 * (sin(d) * exp(t)) / (exp(2 * t) + 2 * exp(t) * cos(d) + 1)


x, y, z, t, d, r = symbols("x y z t d r")
n = symbols("n")
k = var("k")
rho = var("rho")


def an(r, n, d):
    return (
        pow(-1, n) * exp(1j * r) * (P(1j * r) + Q(1j * r) * (n * d) ** 2 / r**2) / r
    )


def terme(rho):
    k = 1 / pi
    return cos(k * sqrt(rho**2 + (1 * pi) ** 2))


def diagIm1(d):
    return -Integral(lop1(t, d), (t, 0, 1)).evalf()


def diagIm1bis(d):
    return Integral(lop1bis(t, d), (t, 0, 1)).evalf()


def diagIm2(d):
    return Integral(lop2(t, d), (t, 0, oo)).evalf() / d**2


def diagIm3(d):
    return Integral(lop3(t, d), (t, 0, oo)).evalf() / d**3 / 2


def diagIm(d):
    return -1 / 2 / pi * (diagIm1bis(d) + diagIm2(d) + diagIm3(d)) - 1 / 6 / pi


def diagImSum(d):
    # if d < pi: return 0
    # else:
    return -(2 * libc.sum_diag_im(d)) * 3 / 2


if __name__ == "__main__":
    N = 1000
    x = np.linspace(1, 50, N)
    y = np.zeros(N)
    y2 = np.zeros(N)
    y3 = np.zeros(N)
    y4 = np.zeros(N)
    print(2 * libc.sum_diag_im(2) + 2 / 3)
    print(2 * libc.sum_diag_bis(0, 2) + 2 / 3)

    t0 = time.time()
    for i in range(len(x)):
        y[i] = (2 * libc.sum_diag_im(x[i]) + 2 / 3) * 3 / 2  # /4/np.pi
        y3[i] = 2 / 3  # /4/np.pi

    t1 = time.time()
    print(f"Temps pris:{(t1-t0)/N}")

    plt.plot(x, y, color="red")
    plt.plot(x, y3, color="blue", linestyle="dashed", label="$y=2/3\\times 1/4\\pi$")
    plt.axis((0, 50, 0, 5))
    plt.xlabel("$d$")
    plt.legend()
    plt.ylabel("Im diagonale")
    plt.title("Imaginary Part of $G_{XX}$, from G3D.py, summing over 10 terms")
    plt.show()

    M = Matrix(
        [
            [0, 0, 1 + 1j, 2 + 1j],
            [0, 0, 2 - 1j, 1 + 1j],
            [1 + 1j, 2 + 1j, 0, 0],
            [2 - 1j, 1 + 1j, 0, 0],
        ]
    )

    v = M.eigenvals()

    x, y = [], []
    for i in v:
        x.append(re(i))
        y.append(im(i))
    # plt.plot(x,y,linestyle='None', marker='o', color = 'red')
    # plt.show()

    l, a, b, c = var("l a b c")
    M = Matrix(
        [[l, 0, a, b], [0, l, conjugate(b), a], [a, b, l, 0], [conjugate(b), a, 0, l]]
    )

    # pprint(solve(det(M), l))

    M = Matrix([[l, 0, a, b], [0, l, b, c], [a, b, l, 0], [b, c, 0, l]])

    d = Matrix([[1, 1j], [-1, 1j]])

    h0, h2, phi = var("h0 h2 phi")

    M_hankel = Matrix(
        [
            [l - 1 / 2, 0, h0, h2 * exp(2j * phi)],
            [0, l - 1 / 2, h2 * exp(-2j * phi), h0],
            [h0, h2 * exp(2j * phi), l - 1 / 2, 0],
            [h2 * exp(-2j * phi), h0, 0, l - 1 / 2],
        ]
    )

    M_hankel_num = Matrix(
        [
            [l, 0, hankel1(0, 2).evalf(), -hankel1(2, 2).evalf()],
            [0, l, -hankel1(2, 2).evalf(), hankel1(0, 2).evalf()],
            [hankel1(0, 2).evalf(), -hankel1(2, 2).evalf(), l, 0],
            [-hankel1(2, 2).evalf(), hankel1(0, 2).evalf(), 0, l],
        ]
    )

    S = solve(det(M_hankel_num), l)
