import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib

# matplotlib.use('Qt5Agg')
plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

from matplotlib.widgets import Slider, Button, TextBox
from mpl_toolkits.axes_grid1 import make_axes_locatable
import Gd
import ll2D
import generate_lattice as gl
import utils as u
import time
import random_matrices as rm
import bott2 as bott
from scipy.optimize import curve_fit


if __name__ == "__main__":
    N = 10
    a = 1
    s = 1.05
    d = 100
    deltaB, deltaAB = 0, 0
    k0 = 2 * np.pi * 0.05 / a
    g = gl.generate_hex_grid_stretched(N, a, s)
    gl.display_lattice(g, "r")
    plt.show()

    # M = Gd.createMatrixVectorSum(g, d, deltaB, deltaAB, k0)
    M = Gd.create_matrix_TM(g, deltaB, deltaAB, k0)
    w, v = np.linalg.eig(M)
    N = M.shape[0]

    omega, gamma, iprs = [0] * N, [0] * N, [0] * N

    for i in range(N):
        omega[i], gamma[i], iprs[i] = w[i].real / 2, -w[i].imag, ll2D.IPRVector(i, v)
    fig, ax = plt.subplots()

    # cbs = bott.all_bott(g, v, w, omega)

    line = ax.scatter(
        np.array(omega),
        np.array(gamma),
        marker=".",
        c=iprs,
        cmap="jet",
        picker=True,
        pickradius=5,
    )  # 5 points tolerance
    line.set_picker(True)

    def onpick(event):
        ind = event.ind[0]
        print(ind)
        plt.figure()
        u.intensity_map(v, g, ind, omega[ind], iprs[ind], deltaB)
        plt.show()

    fig.canvas.mpl_connect("pick_event", onpick)
    fig.canvas.draw_idle()

    # ax.scatter(omega,cbs,color="black")

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(line, cax=cax)
    # plt.subplots_adjust(left=0.30)
    ax.set_xlabel(r"$\omega_n$", fontsize=60)
    ax.set_ylabel(r"$\gamma_n$", fontsize=60)
    # ax.axis((-20,20,1e-4,1e2))
    # ax.set_yscale('log')
    plt.get_current_fig_manager().full_screen_toggle()

    plt.show()

    plt.hist(
        omega,
        bins=100,
        density=True,
        range=(-160, 160),
        facecolor="#00dd10",
        edgecolor="#00870a",
        linewidth=0.5,
    )
    plt.show()

    omega = 0

    print(cb)
