import numpy as np
import matplotlib.pyplot as plt


def intensity_map(v, grid, m, omega, IPR, delta_B):
    N = v.shape[0]
    intensities = []
    x, y = [], []
    for i in range(0, N, 2):
        intensities.append(np.abs(v[i][m]) ** 2 + np.abs(v[i + 1][m]) ** 2)
        x.append(grid[int(i / 2)][0])
        y.append(grid[int(i / 2)][1])

    # plt.style.use("classic")
    plt.title(
        "$\omega = $ "
        + str(np.round(omega, 2))
        + " |  IPR = "
        + str(np.round(IPR, 6))
        + " | $\\Delta_B =$ "
        + str(delta_B)
    )
    sizes = [x * 10000 for x in intensities]
    plt.scatter(x, y, c=intensities, s=sizes, cmap="jet")
    plt.colorbar()
    # plt.show()
