import numpy as np
import matplotlib.pyplot as plt
import GS15 as gs15
import generate_lattice as gl
import seaborn as sns
import pandas as pd

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)


def plot_band_diagram_3D(B1, B2, B3, B4, X, Y, weightas):
    fig = plt.figure(figsize=(20, 20), constrained_layout=True)
    ax = fig.add_subplot(projection="3d")
    # im = ax.scatter(X,Y,B1,c=weightas[0])
    im = ax.scatter(X, Y, B2, c=weightas[1])
    ax.scatter(X, Y, B3, c=weightas[2])
    # ax.scatter(X,Y,B4,c=weightas[3])

    plt.colorbar(im)


def plot_band_diagram_2D(B1, B2, B3, B4, X, Y, weightas, ax):
    df = pd.DataFrame.from_dict(np.array([X, Y, weightas[2]]).T)
    df.columns = ["X_value", "Y_value", "Z_value"]
    df["Z_value"] = pd.to_numeric(df["Z_value"])
    pivotted = df.pivot("Y_value", "X_value", "Z_value")

    n, m = pivotted.shape
    im = ax.imshow(pivotted, cmap="plasma", interpolation="nearest", vmin=0, vmax=1)

    ax.set_aspect(1.7)
    return im, n, m


if __name__ == "__main__":
    """-------------------"""
    N = 40  # 80
    K = 0
    a = 1
    aho = 0.1  # 0.01
    deltaB, deltaAB = 0, 0
    distance = 100
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05  # 0.006
    Ndots = 400
    plates = 0
    """-------------------"""
    # BZ = []
    # l1,l2 = gl.generate_bz(a, 900, step)
    # for ljk in l1:
    #     BZ.append(ljk)
    # for ljk in l2:
    #     BZ.append(ljk)
    # BZ = np.array(BZ)
    # print(gl.generate_bz(a,Ndots,1))
    # BZ = np.unique(gl.generate_bz(a,Ndots,1),axis=0)
    # print(BZ)

    # fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(5,8))
    # fig.set_figheight(20)
    # fig.set_figwidth(15)

    # for deltaB,deltaAB,ax in zip([12,0],[0,12],[ax1,ax2]):#[i for i in range(-10,11)]:
    #     B1,B2,B3,B4,X,Y,omegas,gammas,weightas = gs15.band_diagram(BZ, distance, deltaB, deltaAB, k0, aho, K, N, plates,a)
    # plot_band_diagram_3D(B1,B2,B3,B4,X,Y,weightas)
    # plt.show()
    # im,n,m  = plot_band_diagram_2D(B1,B2,B3,B4,X,Y,weightas,ax)

    # if deltaB == 0:
    #     ax.set_xlabel(r"$k_x$",fontsize=20)
    # ax.set_xticks(fontsize=20)

    #     ax.set_ylabel(r"$k_y$",fontsize=20)
    #     ax.set_xticks([1/20*m,int(m/2),m-1/20*m],[-2,0,2])
    #     ax.set_yticks([1/21*n,int(n/2),n-1/21*n],[2,0,-2])

    #     ax.set_title(r"$ \Delta_B=" + str(deltaB) + " \qquad \\Delta_{AB}=" + str(deltaAB) + "$", fontsize=20)
    # fig.subplots_adjust(wspace=0.03,hspace=0.12)
    # clb = fig.colorbar(im, ax=[ax1,ax2], shrink=0.95)
    # clb.ax.set_title(r'$W_A$', fontsize = 16)

    # fig.savefig(str(deltaB) +  str(deltaAB) + '.pdf', format='pdf', bbox_inches='tight')
