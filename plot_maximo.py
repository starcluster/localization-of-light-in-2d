import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

scalar = pd.read_csv("scalar.dat")
scalar_nf = pd.read_csv("scalar_nf.dat")
vector_nf = pd.read_csv("vector_nf.dat")
vector = pd.read_csv("vector.dat")

cm = plt.cm.get_cmap("RdYlBu")

fig, axs = plt.subplots(2, 2, figsize=(12, 6))
fig.suptitle("Modes for $N=5000$")

# Upper-left
axs[0, 0].scatter(scalar["x"], scalar["y"], c=scalar["ipr"], cmap="jet", marker=".")
axs[0, 0].set_yscale("log")
axs[0, 0].set(ylabel="Without near-field")
axs[0, 0].set_title("Scalar Kernel")
axs[0, 0].axis((-10, 10, 1e-14, 1e2))

# Bottom-left
axs[1, 0].scatter(
    scalar_nf["x"],
    scalar_nf["y"],
    c=scalar_nf["ipr"],
    cmap="jet",
    marker=".",
    vmin=0,
    vmax=0.5,
)
axs[1, 0].set_yscale("log")
axs[1, 0].set(ylabel="With near-field")
axs[1, 0].axis((-60, 60, 1e-14, 1e2))

# Upper-right
axs[0, 1].scatter(vector["x"], vector["y"], c=vector["ipr"], cmap="jet", marker=".")
axs[0, 1].set_yscale("log")
axs[0, 1].set_title("Vector Kernel")
axs[0, 1].tick_params(axis="y", which="both", labelleft=False)
axs[0, 1].yaxis.set_label_position("right")
axs[0, 1].set(ylabel=r"$\gamma_n$")
axs[0, 1].axis((-10, 10, 1e-14, 1e2))

# Bottom-right
im = axs[1, 1].scatter(
    vector_nf["x"],
    vector_nf["y"],
    c=vector_nf["ipr"],
    cmap="jet",
    marker=".",
    vmin=0,
    vmax=0.5,
)
axs[1, 1].set_yscale("log")
axs[1, 1].yaxis.set_label_position("right")
axs[1, 1].tick_params(axis="y", which="both", labelleft=False)
axs[1, 1].set(ylabel=r"$\gamma_n$")
axs[1, 1].axis((-60, 60, 1e-14, 1e2))


fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax)

for ax in axs.flat:
    ax.set(xlabel=r"$\omega_n$")

plt.show()
