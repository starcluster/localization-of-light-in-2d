import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma
from scipy.optimize import curve_fit


def generateSymmetricMatrix(n):
    H = np.zeros((n, n), dtype=complex)
    for i in range(n):
        for j in range(i, n):
            H[i, j] = np.random.rand() * np.random.choice([-1, 1])

    return H + H.T


def generateHermitianMatrix(n):
    H = np.zeros((n, n), dtype=complex)
    for i in range(n):
        for j in range(i, n):
            H[i, j] = np.random.rand() * np.random.choice(
                [-1, 1]
            ) + 1j * np.random.rand() * np.random.choice([-1, 1])

    return H + H.conj().T


def plotEigenvalues(v):
    x, y = [], []
    for i in v:
        x.append(np.real(i))
        y.append(np.imag(i))
    plt.plot(x, y, linestyle="None", marker="o", color="red")
    plt.show()


def plotDOS(v):
    x = []
    for i in v:
        x.append(np.real(i))
    plt.hist(x, bins=100, density=True, range=(-60, 60))
    plt.show()


def plotDelta(v):
    x, y = [], []
    for i in v:
        x.append(np.real(i))
    x.sort()
    for i in range(len(x) - 1):
        y.append(x[i + 1] - x[i])
    y.append(0)
    z = plt.hist(y, bins=40, density=True, range=(0, 0.3))
    print(z)
    # x = np.linspace(0.01,0.2,z[0].shape[0], dtype=complex)
    # popt, pcov = curve_fit(poisson, x, z[0])
    # plt.plot(x,poisson(x,popt[0]))
    # print(popt)
    plt.ylabel("$p(\delta\lambda)$")
    plt.show()


def poisson(x, l):
    return np.exp(-l) * np.power(l, x) / gamma(1 + x)


def plotSpectrum(v):
    x, y = [], []
    for e in v:
        x.append(e.real)
        y.append(e.imag)
    plt.scatter(x, y)


if __name__ == "__main__":
    vs = []
    for i in range(10):
        H = generateHermitianMatrix(300)

        v, w = np.linalg.eig(H)
        for e in v:
            vs.append(e)
    # plotSpectrum(v)
    # v,w = np.linalg.eigh(H)
    # plotSpectrum(v)
    # plt.show()
    # plotDOS(v)
    plotDelta(vs)
    # A = np.array([[1,1j,1j],[-1j,1,1j],[-1j,-1j,1]])
    # v,w = np.linalg.eig(A)
    # print(v)
