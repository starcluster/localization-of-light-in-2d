"""
Author: Pierre Wulles
Credits: Pierre Wulles, Sergey Skipetrov
License: GNU GPL
Maintainer: Pierre Wulles
Email: pierre.wulles@lpmmc.cnrs.fr
Status: Phd Student
Laboratory: LPMMC -- Grenoble -- France
Date: 2023-02-08
Username: Starcluster
Description: Generates figs of the paper
Repository git: https://gitlab.com/starcluster
Python 3.10.9 (main, Dec 19 2022, 17:35:49) [GCC 12.2.0]
NumPy: 1.22.3
 """

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from matplotlib.ticker import FuncFormatter
import matplotlib


import pandas as pd
from multiprocessing import Pool
import svgutils.transform as sg


from scipy.optimize import curve_fit

import tex
import lattice_f as lf
import lattice_r as lr
import band_diagram as bd
import chern as ch
import generate_mk as gmk
from utils import timer, enable_timer

tex.useTex()

plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)


# TODO: Continuer de factoriser encore des fonctions bien trop
# longues: band_diagram, band_diagram_weighta ...

# TODO: faire une classe ou un dictionnaire pour stocker les valeurs
# k0, a, distance etc ... super lourd dans chaque fonction !

# TODO: Factoriser le code, une seule fonction decorate etc pour le matplotlib


@timer
def plot_band_diagram(
    k0=2 * np.pi * 0.05, a=1, distance=0, Ndots=400, N=80, aho=0.1, rebake=True
):
    """Draw band diagram of an hexagonal lattice between two plates of
    perfectible conductive metal separated by distance. If distance=0
    then there are no plates (free space).

    """
    n = 4 * int(Ndots / 3)
    lattice = lf.generate_path_bz(a, Ndots, "NE")
    fig, axes = plt.subplots(nrows=2, ncols=2)
    for ax, dB, dAB, lettre in zip(
        axes.flat, [0, 12, 0, 12], [0, 0, 12, -12], ["(a)", "(b)", "(c)", "(d)"]
    ):
        try:
            if rebake:
                raise OSError
            csv = np.genfromtxt(f"data/omegas_{dB}_{dAB}_0.csv", delimiter=",")
            xprime, omegas, gammas = tuple(csv.transpose().tolist())
            csv2 = np.genfromtxt(f"data/info_{dB}_{dAB}_0.csv", delimiter=",")
            n2, nn, m2, m3 = tuple(csv2.tolist())

        except OSError:
            B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = bd.band_diagram(
                lattice, 0, dB, dAB, k0, aho, N, a
            )
            xprime = [x for x in range(len(omegas))]
            n2 = int(Ndots / 3) * 4
            nn = len(B2)
            m2 = min(B2[int(nn / 2) : nn])
            m3 = max(B3[int(nn / 2) : nn])
            np.savetxt(
                f"data/omegas_{dB}_{dAB}_0.csv",
                [p for p in zip(xprime, omegas, gammas)],
                delimiter=",",
                fmt="%s",
            )
            np.savetxt(
                f"data/info_{dB}_{dAB}_0.csv",
                [p for p in zip([n2, nn, m2, m3])],
                delimiter=",",
                fmt="%s",
            )

        if distance == 0:
            im = ax.scatter(
                xprime,
                omegas,
                c=gammas,
                cmap="plasma",
                marker="o",
                s=0.2,
                norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
                zorder=2,
            )
        else:
            ax.scatter(xprime, omegas, c="gray", s=0.2)
            try:
                if rebake:
                    raise OSError
                csv = np.genfromtxt(
                    f"data/omegas_{dB}_{dAB}_{distance}_plates.csv", delimiter=","
                )
                xprime, omegas, gammas = tuple(csv.transpose().tolist())
                csv2 = np.genfromtxt(
                    f"data/info_{dB}_{dAB}_{distance}_plates.csv", delimiter=","
                )
                n2, nn, m2, m3 = tuple(csv2.tolist())

            except OSError:
                (
                    B1,
                    B2,
                    B3,
                    B4,
                    X,
                    Y,
                    omegas,
                    gammas,
                    weightas,
                    sigmas,
                ) = bd.band_diagram(lattice, distance, dB, dAB, k0, aho, N, a)
                

                xprime = [x for x in range(len(omegas))]
                np.savetxt(
                    f"data/omegas_{dB}_{dAB}_{distance}_plates.csv",
                    [p for p in zip(xprime, omegas, gammas)],
                    delimiter=",",
                    fmt="%s",
                )
                np.savetxt(
                    f"data/info_{dB}_{dAB}_{distance}_plates.csv",
                    [p for p in zip([n2, nn, m2, m3])],
                    delimiter=",",
                    fmt="%s",
                )

            im = ax.scatter(
                xprime,
                omegas,
                c=gammas,
                cmap="plasma",
                marker="o",
                s=0.2,
                zorder=2,
                norm=matplotlib.colors.Normalize(vmin=0.001, vmax=1),
            )

        ax.axvline(
            x=n2 + n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axvline(
            x=n2 - n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axis((0, len(xprime), -200, 140))
        ax.text(1 / 64 * len(omegas), 115, s="$" + lettre + "$")

        if (dB == 12 and dAB == 0) or (dAB == 12 and dB == 0):
            ax.axhspan(m2, m3, alpha=0.5, color="gray")

        if (dB == 0 and dAB == 0 and distance ==11):
            alpha = np.sqrt(1 - (3*np.pi / (distance)) ** 2)
            nnn = 3*np.sqrt(3)*n2*0.05/2
            # ax.axvline(x=n2+alpha*nnn, color="red", ls="-", zorder=1)
            # ax.axvline(x=n2-alpha*nnn, color="red", ls="-", zorder=1)

        if (dB == 0 and dAB == 0) or (dB == 12 and dAB == 0):
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=12)
        else:
            ax.set_xticks(ticks=[0, n, 2 * n], labels=[r"$M$", r"$\Gamma$", r"$K$"])
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
            ax.tick_params(labelsize=12)
        if (dB == 12 and dAB == -12) or (dB == 12 and dAB == 0):
            ax.set_ylabel("")
            ax.set_yticks([])
        else:
            ax.set_ylabel(
                r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12
            )

        ax.text(
            5 / 8 * len(omegas),
            -140,
            s=r"\noindent $\Delta_{\mathbf{B}}="
            + str(dB)
            + "\\\\ \\Delta_{AB}="
            + str(dAB)
            + "$",
            fontsize=10,
        )

    fig.subplots_adjust(wspace=0.03, hspace=0.12)
    cbar_ax = fig.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma/\Gamma_0$")
    plt.subplots_adjust(
        top=0.885, bottom=0.11, left=0.14, right=0.9, hspace=0.025, wspace=0.025
    )

    plt.savefig(f"band_diagram_{distance}.pdf", format="pdf", bbox_inches="tight")


@timer
def gap(k0=2 * np.pi * 0.05, a=1, distance=0, step=0.1, N=80, aho=0.1, rebake=True):
    """Plot gap(Δ_B) for different Δ_AB showing the four different regime. For
    optimisation, M(k) matrix is not recomputed everytime, only the diagonal is
    changed. M(k) are stored temporary into Mks.

    """
    fig, ax = plt.subplots(figsize=(15, 7))

    l1, l2, _ = lf.generate_bz(a, step)
    lattice = np.array(l1.tolist() + l2.tolist()) + (0.001, 0.001)

    deltaABs = [0, 3, -6]
    deltaAB_color = [plasma_blue, plasma_purple, plasma_pink]
    markers = ["^", "o", "v"]
    deltaBs = [-60 + 2 * i for i in range(61)]
    for deltaAB, color, m in zip(deltaABs, deltaAB_color, markers):
        gap_l = []
        if rebake:
            for deltaB in deltaBs:
                (
                    B1,
                    B2,
                    B3,
                    B4,
                    X,
                    Y,
                    omegas,
                    gammas,
                    weightas,
                    sigmas,
                ) = bd.band_diagram(lattice, distance, deltaB, deltaAB, k0, aho, N, a)
                nn = len(B1)
                g = min(B2[int(nn / 2) : -1]) - max(B3[int(nn / 2) : -1])
                if g < 0:
                    g = 0
                gap_l.append(g)
                print(deltaB, g)
            np.savetxt(
                f"data/gap{deltaAB}.csv",
                [p for p in zip(deltaBs, gap_l)],
                delimiter=",",
                fmt="%s",
            )
        else:
            csv = np.genfromtxt(f"data/gap{deltaAB}.csv", delimiter=",")
            deltaBs = csv[:, 0]
            gap_l = csv[:, 1]

        ax.plot(
            deltaBs,
            gap_l,
            c=color,
            label=r"$\Delta_{AB} = " + str(deltaAB) + "$",
            linestyle="None",
            marker=m,
            zorder=2,
        )

    ax.legend(bbox_to_anchor=(0.88, 0.4), prop={"size": 26})

    deltaBs = [-100 + i / 10 for i in range(2000)]

    for deltaAB, color in zip(deltaABs, deltaAB_color):
        gap_l_th = []
        for deltaB in deltaBs:
            C0 = -(
                0.1489803 / (k0 * a / 2 / np.pi)
                + 0.0002726 / (k0 * a / 2 / np.pi) ** 2
                + 0.00135505 / (k0 * a / 2 / np.pi) ** 3
            )
            C1 = (
                -0.43169 / (k0 * a / 2 / np.pi)
                - 1.37252819e-04 / (k0 * a / 2 / np.pi) ** 2
                + 6.42156198e-03 / (k0 * a / 2 / np.pi) ** 3
            )
            C2 = (
                -1.62950412e-01 / (k0 * a / 2 / np.pi)
                + 2.31735224e-05 / (k0 * a / 2 / np.pi) ** 2
                + 1.34708817e-02 / (k0 * a / 2 / np.pi) ** 3
            )
            C3 = 0.09188391 / (k0 * a / 2 / np.pi) ** 2

            S = 2 * np.real(
                np.sqrt(
                    deltaAB**2 + 0.25 * C2**2 + 0.5 * 1j * C2 * C3 - 0.25 * C3**2
                )
            )
            R1 = 0.25 * (C0 - C1 + S + 2 * abs(deltaAB))
            R2 = 0.25 * (C0 - C1 - S - 2 * abs(deltaAB))

            if abs(deltaB) > S / 2:  # outer regime
                gap_l_th.append(0)
            elif abs(deltaB) < S / 2 and abs(deltaB) > abs(R2):  # linear regime W_gap3
                gap_l_th.append(-2 * abs(deltaB) + S)
            elif abs(deltaB) < R1:  # linear regime W_gap1
                gap_l_th.append(2 * abs(abs(deltaB) - abs(deltaAB)))
            else:  # plateau (W_gap2)
                gap_l_th.append(0.5 * (-C1 + C0 + S - 2 * abs(deltaAB)))

        ax.plot(deltaBs, gap_l_th, c="gray", zorder=1, ls="--")

    ax.tick_params(axis="both", labelsize=40)
    # ax.set_xticks(fontsize=40)
    # ax.set_yticks(fontsize=40)
    ax.set_xlabel(
        r"$\textrm{Zeeman shift } \Delta_{\bf{B}}$ ",
        fontsize=40,
    )
    ax.set_ylabel(r"$\textrm{Gap width } \Delta_{\textrm{gap}}$", fontsize=40)
    ax.set_xlim(-60, 60)
    ax.set_ylim(-3, 25)
    fig.savefig("gap.pdf", format="pdf", bbox_inches="tight")


def chern_for_parallel(delta_AB, delta_B, p):
    shift, a, N, distance, aho, k0, step = p
    file_path = f"data/chern_map_{distance}.csv"
    BZ, _, dk = lf.generate_bz(a, step)
    H = gmk.Hk(distance, delta_B + shift, delta_AB, k0, aho, N, a)
    c = np.real(ch.chern(H, BZ, dk, 4))
    # print(f"{delta_B=}, {delta_AB=}, {c=}")
    with open(file_path, "a") as f_chern:
        f_chern.write(f"{delta_B + shift},{delta_AB},{c}\n")
        f_chern.flush()


@timer
def chern_map(
    k0=2 * np.pi * 0.05,
    a=1,
    distance=0,
    N=80,
    aho=0.1,
    shift=0.1,
    step=0.01,
    rebake=True,
):
    """Plot chern number for different values of Δ_AB and Δ_B, the step of the
    discretized brilllouin zone needs to be quite small (0.01) to ensure correct
    values.

    """
    if rebake:
        f_chern = open(f"data/chern_map_{distance}.csv", "w")
        f_chern.write("deltaB,deltaAB,C\n")
        f_chern.close()
        pool = Pool(8)
        size_chern_map = 11
        dbs = np.linspace(-12, 12, size_chern_map)
        dabs = np.linspace(-12, 12, size_chern_map)

        p = (shift, a, N, distance, aho, k0, step)
        pool.starmap(
            chern_for_parallel,
            [
                (dbs[i], dabs[j], p)
                for j in range(size_chern_map)
                for i in range(size_chern_map)
            ],
        )

    chern_map_csv = pd.read_csv("data/chern_map_0.csv")
    # High res backup exists in data/chern_map_0_backup.csv
    chern_map = chern_map_csv.pivot("deltaB", "deltaAB", "C")

    fig, ax = plt.subplots()

    ax.set_xlabel(r"$\Delta_{AB}$", fontsize=30)
    ax.tick_params(axis="x", labelsize=20)

    ax.set_ylabel(r"$\Delta_{\mathbf{B}}$", fontsize=30)
    ax.tick_params(axis="y", labelsize=20)

    ax.imshow(chern_map, cmap="plasma", interpolation="none", extent=[-12, 12, -12, 12])
    ax.grid(False)

    fig.savefig(
        f"chern_map_{distance}.pdf", format="pdf", bbox_inches="tight", dpi=1200
    )
    fig.savefig(
        f"chern_map_{distance}.svg", format="svg", bbox_inches="tight", dpi=1200
    )


@timer
def discretized_BZ():
    BZ, BZ2, _ = lf.generate_bz(1, 0.1)
    BZ += (0.001, 0.001)

    f = plt.figure()
    ax = f.add_subplot(111)
    lr.display_lattice(BZ, plasma_blue)
    lr.display_lattice(BZ2, plasma_pink)

    plt.xlabel(r"$k_x a$", fontsize=30)
    plt.xticks(fontsize=20)

    plt.ylabel(r"$k_y a$", fontsize=30)
    plt.tick_params(axis="y", which="major", labelleft=False, labelright=True, size=20)
    plt.yticks(fontsize=20)

    ax.yaxis.tick_right()
    ax.yaxis.set_label_position("right")
    plt.savefig("discrete_BZ.pdf", format="pdf", bbox_inches="tight")
    plt.savefig("discrete_BZ.svg", format="svg", bbox_inches="tight")


@timer
def merge_chern_BZ():
    """Merging of the chern map plus the discrete BZ used to compute the
    chern number.

    """
    fig = sg.SVGFigure("100cm", "100cm")

    fig1 = sg.fromfile("chern_map_0.svg")
    fig2 = sg.fromfile("discrete_BZ.svg")

    plot1 = fig1.getroot()
    plot2 = fig2.getroot()
    plot2.moveto(350, 0)

    txt1 = sg.fromfile("a.svg")
    txt2 = sg.fromfile("b.svg")
    plottxt1 = txt1.getroot()
    plottxt2 = txt2.getroot()
    plottxt1.moveto(10, 10, 4, 4)
    plottxt2.moveto(370, 10, 4, 4)

    txtnum1 = sg.fromfile("1_blanc.svg")
    txtnumm1 = sg.fromfile("m1_blanc.svg")
    txtnum0 = sg.fromfile("0_blanc.svg")
    txtnum0b = sg.fromfile("0_blanc.svg")

    plotnum1 = txtnum1.getroot()
    plotnumm1 = txtnumm1.getroot()
    plotnum0 = txtnum0.getroot()
    plotnum0b = txtnum0b.getroot()

    plotnum1.moveto(182, 185, scale_x=6, scale_y=6)
    plotnumm1.moveto(122, 20, scale_x=6, scale_y=6)
    plotnum0.moveto(250, 100, scale_x=6, scale_y=6)
    plotnum0b.moveto(100, 100, scale_x=6, scale_y=6)

    fig.append([plot1, plot2])
    fig.append([plottxt1, plottxt2])
    fig.append([plotnum1, plotnumm1, plotnum0, plotnum0b])

    fig.save("chern_BZ.svg")


@timer
def band_diagram_weighta(
    k0=2 * np.pi * 0.05,
    a=1,
    distance=0,
    Ndots=400,
    N=80,
    aho=0.1,
    step=0.01,
    rebake=True,
):

    n = 4 * int(Ndots / 3)
    lattice = lf.generate_path_bz(a, Ndots, "NE")
    fig, axs = plt.subplots(
        nrows=2, ncols=2, figsize=(18, 14), gridspec_kw={"width_ratios": [1.28, 1]}
    )

    for ax, dB, dAB, lettre in zip(
        [axs[0, 0], axs[1, 0]], [12, 0], [0, -12], ["(a)", "(b)"]
    ):
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = bd.band_diagram(
            lattice, distance, dB, dAB, k0, aho, N, a
        )
        xprime = [i for i in range(len(B1))]
        for B, weighta in zip([B1, B2, B3, B4], weightas):
            im = ax.scatter(
                xprime,
                B,
                c=weighta,
                cmap="plasma",
                marker="o",
                s=4,
                norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
            )
        ax.axis((0, len(xprime), -200, 140))
        ax.text(1, 115, s="$" + lettre + "$", size=30)
        nn = int(len(B2) / 3)

        if dB == 12 and dAB == 0:
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=30)
        else:
            ax.set_xticks(
                ticks=[0, nn, 2 * nn, 3*nn], labels=[r"$M$", r"$\Gamma$", r"$K$", r"$M$"], fontsize=30
            )
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=30)
            ax.tick_params(labelsize=30)
        if dB == 0:
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=30)

        t1 = r"\begin{align*}"
        t2 = r"\Delta_{\bf{B}} &=" + str(dB) + "\\"
        t3 = r"\\Delta_{AB} &= " + str(dAB)
        t4 = r"\end{align*}"
        ax.text(200, -140, s=t1 + t2 + t3 + t4, fontsize=30)

        ax.tick_params(labelsize=30)

        ax.set_ylabel(
            r"$\textrm{Frequency}$ $(\omega -\omega_0)/\Gamma_0$", fontsize=30
        )

    l1, l2, _ = lf.generate_bz(a, step)
    BZ = np.array(l1.tolist() + l2.tolist()) + (0.001, 0.001)
    for deltaB, deltaAB, ax, lettre in zip(
        [12, 0], [0, -12], [axs[0, 1], axs[1, 1]], ["(c)", "(d)"]
    ):
        try:
            if rebake:
                raise OSError
            csv = np.genfromtxt(
                f"data/weightas_{deltaB}_{deltaAB}_{distance}.csv", delimiter=","
            )

            X, Y, weightas[2] = tuple(csv.transpose().tolist())
            X, Y = np.array(X), np.array(Y)
            csv2 = np.genfromtxt(
                f"data/info_weightas_{deltaB}_{deltaAB}_{distance}.csv", delimiter=","
            )
            n2, nn, m2, m3 = tuple(csv2.tolist())

        except OSError:
            B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = (
                B1,
                B2,
                B3,
                B4,
                X,
                Y,
                omegas,
                gammas,
                weightas,
                sigmas,
            ) = bd.band_diagram(BZ, distance, deltaB, deltaAB, k0, aho, N, a)
            xprime = [x for x in range(len(omegas))]
            n2 = int(Ndots / 3) * 4
            nn = len(B2)
            m2 = min(B2[int(nn / 2) : nn])
            m3 = max(B3[int(nn / 2) : nn])
            np.savetxt(
                f"data/weightas_{deltaB}_{deltaAB}_{distance}.csv",
                [p for p in zip(X, Y, weightas[2])],
                delimiter=",",
                fmt="%s",
            )
            np.savetxt(
                f"data/info_weightas_{deltaB}_{deltaAB}_{distance}.csv",
                [p for p in zip([n2, nn, m2, m3])],
                delimiter=",",
                fmt="%s",
            )

        from matplotlib.tri import Triangulation
        tri = Triangulation(X, Y)
        ax.tripcolor(tri, weightas[2], cmap='plasma', shading='gouraud')
        # im, n, m = plot_band_diagram_2D(X, Y, weightas, ax)
        print(f"{len(weightas[2])=}")
        print(f"{len(X)=}")
        ax.text(-2.5, 2 , s="$" + lettre + "$", size=30)
        ax.tick_params(axis='both',labelsize=30)
        ax.set_xlabel(r"$k_x a$",fontsize=30)
        ax.set_ylabel(r"$k_y a$", fontsize=30, labelpad=-500,ha='right')

        # ax.set_xticks(
        #     [1 / 20 * m, int(m / 2), m - 1 / 20 * m],
        #     ["$-2$", "$0$", "$2$"],
        #     fontsize=20,
        # )
        # ax.set_yticks(
        #     [1 / 21 * n, int(n / 2), n - 1 / 21 * n],
        #     ["$2$", "$0$", "$-2$"],
        #     fontsize=20,
        # )

    # plt.subplot_tool()
    fig.subplots_adjust(
        top=0.88, bottom=0.11, left=0.125, right=0.900, hspace=0.1, wspace=0.1
    )
    cbar_ax = fig.add_axes([0.94, 0.11, 0.03, 0.74])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$W_{\alpha}^A$", fontsize=25,pad=15)
    clb.ax.tick_params(labelsize=30)
    # arr_img = plt.imread("arrows.png")
    # im2 = OffsetImage(arr_img, zoom=0.1)
    # ab = AnnotationBbox(
    #     im2,
    #     (0.0, 0.0),
    #     xycoords="axes fraction",
    #     box_alignment=(-0.1, -0.1),
    #     frameon=False,
    # )
    # ax.add_artist(ab)

    plt.savefig("bd_sites_A.pdf", format="pdf", bbox_inches="tight", dpi=600)


@timer
def re_im_g(N=5000):
    """Plot the real and imaginary part of the gamma function"""
    d_values = np.linspace(0.1, 50, N)
    im_values = np.zeros_like(d_values)
    re_values = np.zeros_like(d_values)
    shape_values_p = np.zeros_like(d_values)
    shape_values_m = np.zeros_like(d_values)
    milloni = np.zeros_like(d_values)

    def re_delta_parall(d):
        N = 100
        s = 0
        for n in range(1, N):
            if n < d/np.pi:
                s += (1+n**2*np.pi**2/d**2)*np.sin(np.pi*n/2)**2

        return 3*np.pi*s/d/2


    milonni_sergey = np.loadtxt("milonni.csv", delimiter=",")
    x = milonni_sergey[:, 0]
    y = milonni_sergey[:, 1]

    for i in range(N):
        d = d_values[i]
        re_values[i] = np.real(gmk.gamma(d) + 1j)/2
        im_values[i] = -np.imag(gmk.gamma(d)-1j)
        shape_values_p[i] = 1 + 0.5*(1/(d/np.pi)**2+3/(d/np.pi))
        shape_values_m[i] = 1 + 0.5*(1/(d/np.pi)**2-3/(d/np.pi))
        milloni[i] = re_delta_parall(d)
    ones_values = np.ones_like(d_values)
    d_values /= np.pi

    f = plt.figure()
    ax = f.add_subplot(111)


    plt.plot(d_values, re_values, c=plasma_pink, linewidth=5.0)
    plt.plot(x, y, c=plasma_blue, linewidth=3.0,linestyle="--")
    plt.axis((0, 6, -0.5, 3.2))
    # plt.xticks([i for i in range(13)])
    plt.xlabel(r"$\textrm{Distance between mirrors }k_0d/\pi$", fontsize=30)
    plt.ylabel(r"$\textrm{\qquad Single-atom} \newline \textrm{ frequency shift } \Delta\omega/\Gamma_0$", fontsize=30)
    ax.yaxis.tick_right()
    plt.xticks(fontsize=20)
    plt.text(0.5, 2.95, s="$(a)$", size=20)
    plt.yticks(fontsize=20)
    plt.savefig("re_G.pdf", format="pdf", bbox_inches="tight")

    f = plt.figure()
    ax = f.add_subplot(111)

    start = 300
    plt.plot(d_values[start:], shape_values_p[start:], c="gray", linestyle="dotted", linewidth=5.0)
    plt.plot(d_values[start:], shape_values_m[start:], c="gray", linestyle="dotted", linewidth=5.0)
    plt.plot(d_values, im_values, c=plasma_pink, linewidth=5.0)
    plt.plot(d_values, milloni, c=plasma_blue, linewidth=3.0,linestyle="--")
    plt.axis((0, 6, -0.5, 3.2))
    # plt.xticks([i for i in range(13)])
    plt.xlabel(r"$\textrm{Distance between mirrors } k_0d/\pi$", fontsize=30)
    plt.ylabel(r"$\textrm{Single-atom}\newline\textrm{ decay rate } \Gamma/\Gamma_0$", fontsize=30,labelpad=10)
    plt.xticks(fontsize=20)
    plt.text(0.2, 2.95, s="$(b)$", size=20)
    ax.yaxis.set_label_position("right")
    plt.yticks(fontsize=20)
    # plt.yticks(fontsize=20)
    plt.savefig("im_G.pdf", format="pdf", bbox_inches="tight")


@timer
def smaller_imag_part(rebake):
    f = plt.figure(figsize=(10,10))
    ax = f.add_subplot(111)
    a = 1
    lattice, _, _ = lf.generate_bz(a, 0.1)
    N = 30
    aho = 0.01
    deltaB, deltaAB = 12, 0
    distance = 2
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    y = []
    x = [0 + i for i in range(50)]
    x2 = [0.3 - i / 100 for i in range(30)]

    if rebake:
        for aho in x2:
            B1, B2, B3, B4, X, Y, omegas, gammas, _, _ = bd.band_diagram(
                lattice, distance, deltaB, deltaAB, k0, aho, N, a
            )
            print(max(map(abs, gammas)))
            y.append(max(map(abs, gammas)))

        X = np.array(x2)
        Y = np.array(y)
        np.savetxt(
            "data/smaller_imag_part.csv",
            [p for p in zip(X, Y)],
            delimiter=",",
            fmt="%s",
        )

    else:
        csv = np.genfromtxt("data/smaller_imag_part.csv", delimiter=",")
        X = csv[:, 0]
        Y = csv[:, 1]

    def carre(x, a):
        return a * x**2

    popt, pcov = curve_fit(carre, X, Y)
    print(popt, pcov)
    th = X**2 * popt[0]

    plt.plot(X, th, c=plasma_blue, linewidth=3, linestyle="--", label=r"$\propto (k_0h)^2$")
    
    plt.legend(bbox_to_anchor=(0.83, 0.9), fontsize=30) 

    plt.xlabel(r"\textrm{Cut-off length }$k_0h$", fontsize=40)
    plt.ylabel(r"$\textrm{Maximum decay rate }\textrm{max}(\Gamma(\mathbf{k})/\Gamma_0)$", fontsize=40)
    plt.xticks(fontsize=30, rotation=30)
    plt.yticks(fontsize=30)
    # plt.gca().invert_xaxis()
    plt.scatter(X, Y, marker="o", facecolors='white', edgecolors='r', s=100)

    # plt.text(s='$(a)$',x=0.1,y=0.,fontsize=20)
    plt.text(
        0.03, 0.98, s="$(a)$", ha="left", va="top", transform=ax.transAxes, fontsize=40
    )

    plt.savefig("smaller_imag_part.pdf", format="pdf", bbox_inches="tight")


def compute_band_chern(lattice, distance, deltaB, deltaAB, k0, aho, N, a, BZ, dk):
    B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = bd.band_diagram(
        lattice, distance, deltaB, deltaAB, k0, aho, N, a
    )
    H = gmk.Hk(distance, deltaB, deltaAB, k0, aho, N, a)
    c = np.real(ch.chern(H, BZ, dk, 4, [0,1]))
    gap = abs(min(B2) - max(B3))
    return gap, c


@timer
def compute_gap_chern(
    a=1, distance=2, Ndots=400, N=80, aho=0.1, step=0.01, rebake=True
):
    lattice = lf.generate_path_bz(a, Ndots, "NE")
    gap_l_topo, gap_l_trivial = [], []

    hs = [0.05 + i / 1000 for i in range(0, 200)]
    cherns_topo, cherns_trivial = [], []

    if rebake:
        BZ, _, dk = lf.generate_bz(a, step)
        for h in hs:
            k0 = 2 * np.pi * h / a
            gap_topo, c_topo = compute_band_chern(
                lattice, distance, 12, 0, k0, aho, N, a, BZ, dk
            )
            cherns_topo.append(c_topo)
            gap_l_topo.append(gap_topo)
            print(f"{gap_topo=},{c_topo=}")

            gap_trivial, c_trivial = compute_band_chern(
                lattice, distance, 0, 12, k0, aho, N, a, BZ, dk
            )
            cherns_trivial.append(c_trivial)
            gap_l_trivial.append(gap_trivial)
            print(f"{gap_trivial=},{c_trivial=}")

        np.savetxt(
            "data/gap_and_chern_topo.csv",
            [p for p in zip(hs, gap_l_topo, cherns_topo)],
            delimiter=",",
            fmt="%s",
        )
        np.savetxt(
            "data/gap_and_chern_trivial.csv",
            [p for p in zip(hs, gap_l_trivial, cherns_trivial)],
            delimiter=",",
            fmt="%s",
        )


@timer
def plot_gap_chern(a=1, distance=2):
    plt.figure(figsize=(10, 10))
    plt.axhline(y=24, xmin=0, xmax=1, linestyle="--", color="gray", zorder=1)
    plt.axhline(y=0, xmin=0, xmax=1, linestyle="--", color="black", zorder=1)

    csv = np.genfromtxt("data/gap_and_chern_trivial.csv", delimiter=",")
    hs = csv[:, 0]
    gap_l_trivial = csv[:, 1]

    plt.plot(hs, gap_l_trivial, color=plasma_blue, lw=3)

    csv = np.genfromtxt("data/gap_and_chern_topo.csv", delimiter=",")
    hs = csv[:, 0]
    gap_l = csv[:, 1]
    cherns = csv[:, 2]

    hs0, hs1 = [], []
    gap_l0, gap_l1 = [], []
    for i in range(len(cherns)):
        if abs(cherns[i]) < 1e-1:
            hs0.append(hs[i])
            gap_l0.append(gap_l[i])
        else:
            hs1.append(hs[i])
            gap_l1.append(gap_l[i])

    hs0, gap_l0 = zip(*sorted(zip(hs0, gap_l0)))
    hs1, gap_l1 = zip(*sorted(zip(hs1, gap_l1)))

    plt.plot(hs0, gap_l0, color=plasma_blue, label=r"$C = 0$", lw=3)
    plt.plot(hs1, gap_l1, color="red", label=r"$C = 1$", lw=3)

    plt.axis((0.05, 0.24, -1, 27))
    plt.legend(fontsize=30)

    plt.text(
        0.055, 26.5, s="$(b)$", ha="left", va="top", fontsize=40
    )

    def format_ticks(x, pos):
        return f'${x:.2f}$'  # Format x with 2 decimal places

    plt.gca().xaxis.set_major_formatter(FuncFormatter(format_ticks))
    plt.xlabel(r"$\textrm{Lattice spacing } k_0a/2\pi$", fontsize=40)
    plt.ylabel(r"$\textrm{Gap width } \Delta_{\textrm{gap}}$", fontsize=40)
    plt.xticks(fontsize=30, rotation=30)
    plt.yticks(fontsize=30)
    plt.savefig("gap_and_chern.svg", format="svg", bbox_inches="tight")
    plt.savefig("gap_and_chern.pdf", format="pdf", bbox_inches="tight")


@timer
def add_text_gap_chern():
    """Merging of the chern map plus the discrete BZ used to compute the
    chern number.

    """
    fig = sg.SVGFigure("100cm", "100cm")
    fig1 = sg.fromfile("gap_and_chern.svg")
    plot1 = fig1.getroot()

    txt1 = sg.fromfile("dB12.svg")
    txt2 = sg.fromfile("dAB12.svg")
    plottxt1 = txt1.getroot()
    plottxt2 = txt2.getroot()
    plottxt1.moveto(285, 285, 4, 4)
    plottxt2.moveto(200, 94, 4, 4)

    fig.append([plot1, plottxt1, plottxt2])

    fig.save("gap_and_chern_annotated.svg")


def delta_AB_function(rebake):
    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1
    step = 0.05
    fig, (ax1, ax2) = plt.subplots(
        nrows=1, ncols=2, figsize=(18, 10), gridspec_kw={"width_ratios": [1.28, 1]}
    )
    l1, l2, _ = lf.generate_bz(a, step)
    lattice = np.array(l1.tolist() + l2.tolist()) + (0.001, 0.001)

    N = 50
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 100
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    plates = 0

    deltaB = 30

    deltaABs = [-30 + i for i in range(61)]
    gap_l = []

    for h, col in zip([0.045, 0.05, 0.055], [plasma_blue, plasma_purple, plasma_pink]):
        k0 = 2 * np.pi * h / a
        gap_l = []
        gap_l_th = []
        if rebake:
            for deltaAB in deltaABs:
                (
                    B1,
                    B2,
                    B3,
                    B4,
                    X,
                    Y,
                    omegas,
                    gammas,
                    weightas,
                    sigmas,
                ) = gs15.band_diagram(
                    lattice, distance, deltaB, deltaAB, k0, aho, N, plates, a
                )
                g = min(B2) - max(B3)
                if g < 0:
                    g = 0
                gap_l.append(g)

            np.savetxt(
                "gap_dB_cst" + str(h) + ".csv",
                [p for p in zip(deltaABs, gap_l)],
                delimiter=",",
                fmt="%s",
            )

        else:
            csv = np.genfromtxt("gap_dB_cst" + str(h) + ".csv", delimiter=",")
            deltaBs = csv[:, 0]
            gap_l = csv[:, 1]
        for deltaAB in deltaABs:
            C0 = -(
                0.1489803 / (k0 * a / 2 / np.pi)
                + 0.0002726 / (k0 * a / 2 / np.pi) ** 2
                + 0.00135505 / (k0 * a / 2 / np.pi) ** 3
            )
            C1 = (
                -0.43169 / (k0 * a / 2 / np.pi)
                - 1.37252819e-04 / (k0 * a / 2 / np.pi) ** 2
                + 6.42156198e-03 / (k0 * a / 2 / np.pi) ** 3
            )
            C2 = (
                -1.62950412e-01 / (k0 * a / 2 / np.pi)
                + 2.31735224e-05 / (k0 * a / 2 / np.pi) ** 2
                + 1.34708817e-02 / (k0 * a / 2 / np.pi) ** 3
            )
            C3 = 0.09188391 / (k0 * a / 2 / np.pi) ** 2

            S = 2 * np.real(
                np.sqrt(
                    deltaAB**2 + 0.25 * C2**2 + 0.5 * 1j * C2 * C3 - 0.25 * C3**2
                )
            )
            R1 = 0.25 * (C0 - C1 + S + 2 * abs(deltaAB))
            R2 = 0.25 * (C0 - C1 - S - 2 * abs(deltaAB))

            gap_l_th.append(0.5 * (-C1 + C0 + S - 2 * abs(deltaAB)))

        ax1.plot(deltaABs, gap_l_th, color="gray", linewidth=7, linestyle="--")
        ax1.plot(
            deltaABs,
            gap_l,
            color=col,
            label=r"$k_0 a = 2\pi\times" + str(h) + "$",
            linewidth=4,
        )
        

    ax1.legend(bbox_to_anchor=(0.5, 0.3), prop={"size": 30},frameon=False)

    ax1.tick_params(labelsize=30)
    ax1.text(-17, 33, s="$(a)$", size=30)
    ax1.set_xlabel(r" $\Delta_{AB}$", fontsize=40)
    ax1.set_ylabel(r"$\textrm{Maximum gap width } \textrm{max}(\Delta_{\textrm{gap}}) $", fontsize=40)
    ax1.axis((-18, 18, 0, 35))


    #### B
    x = np.array([0.02 + i / 100 for i in range(11)])
    x2 = np.linspace(0.02, 0.13, 100)
    y = [
        358.74732057530093,
        107.37692146202085,
        45.94454017647327,
        23.953636389580335,
        14.171163647608326,
        9.15868746914277,
        6.321099858791805,
        4.591077793399086,
        3.4741015483721065,
        2.719329530900203,
        2.1901321889996286,
    ]

    def func(x, A):
        return A * x ** (-3)

    popt, pcov = curve_fit(func, x, y)
    A = popt[0]

    y2 = A * x2 ** (-3)

    ax2.set_xlabel(r"$\textrm{Lattice spacing } k_0a/2\pi$", fontsize=40)
    ax2.tick_params(
        labelsize=30, left=False, right=True, labelright=True, labelleft=False
    )

    t2 = r"$\textrm{max}(\Delta_{\textrm{gap}}) \propto  (k_0a/2\pi)^{-3}$"
    ax2.text(x=0.029, y=150, s=t2, fontsize=30)
    ax2.plot(x2, y2, zorder=1, color="gray", lw=4, ls="--")
    ax2.scatter(x, y, color=plasma_blue, zorder=2, s=60)
    ax2.loglog()
    ax2.minorticks_off()
    ax2.set_xticks(ticks=[])
    ax2.set_xticks(
        [0.01 * i * 2 for i in range(1, 6)],
        ["$" + str(0.01 * i * 2) + "$" for i in range(1, 6)],
        fontsize=30,
        rotation=30,
    )
    # ax2.set_yticks(fontsize=20)
    ax2.text(0.115, 330, s="$(b)$", size=30)
    fig.subplots_adjust(wspace=0.07, hspace=0.07)
    fig.savefig("gap_dB_cst.pdf", format="pdf", bbox_inches="tight")
    plt.show()
    


if __name__ == "__main__":
    # PHYSICS
    k0 = 2 * np.pi * 0.05
    a = 1
    # NUMERICS
    N = 80
    aho = 0.1
    Ndots = 400
    rebake = False
    # GENERATING FIGURES
    enable_timer(True)
    # plot_band_diagram(k0=k0, a=a, distance=0, Ndots=Ndots, N=N, aho=aho, rebake=rebake)
    # plot_band_diagram(k0=k0, a=a, distance=2, Ndots=Ndots, N=N, aho=aho, rebake=rebake)
    # plot_band_diagram(k0=k0, a=a, distance=11, Ndots=Ndots, N=N, aho=aho, rebake=rebake)
    # gap(k0=k0, a=a, distance=0, step=0.01, N=80, aho=0.1, rebake=rebake)
    # chern_map(k0=k0, a=a, distance=0, N=N, aho=aho, shift=0.1, step=0.1, rebake=False)
    # discretized_BZ()
    # merge_chern_BZ()
    # band_diagram_weighta(
    #     k0=k0, a=a, distance=0, Ndots=Ndots, N=N, aho=aho, step=0.01, rebake=False
    # )
    # milloni()
    
    # re_im_g()
    # delta_AB_function(False)
    
    smaller_imag_part(rebake=False)
    # compute_gap_chern(a=a, distance=2, Ndots=50, N=N, aho=aho, step=0.1, rebake=False)
    # plot_gap_chern(a=a, distance=2)
    # add_text_gap_chern()
