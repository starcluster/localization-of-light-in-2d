import sys
import math
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QLabel,
    QSlider,
    QHBoxLayout,
)
from PyQt5.QtCore import Qt, QPoint, QRectF, QLineF
from PyQt5.QtGui import QPainter, QPolygon, QBrush, QColor, QPen
import numpy as np
import lattice_r as lr


class HexagonWidget(QWidget):
    def __init__(self, size):
        super().__init__()
        self.setStyleSheet("background-color: white;")  # Définit le fond en blanc
        w, h = 1200, 800
        self.size = size
        self.side_length = 10 * self.size
        self.setMinimumSize(w, h)

        self.slider = QSlider(Qt.Vertical)

        self.slider.setRange(1, 200)
        self.slider.setSingleStep(10)
        self.slider.setValue(40)
        self.slider.valueChanged.connect(self.update_side_length)
        self.label = QLabel(str(self.slider.value() / 40))

        layout = QHBoxLayout()

        layout.addWidget(self.slider)
        layout.addWidget(self.label)
        layout.addSpacing(w)

        self.setLayout(layout)

    def update_side_length(self, value):
        self.side_length = value
        self.label.setText(str(self.slider.value() / 40))
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.fillRect(
            event.rect(), QColor(255, 255, 255)
        )  # Remplit le rectangle avec du blanc

        triang = lr.generate_triangular_lattice(6, 3 * self.size)

        center_x = self.width() // 2 - 200
        center_y = self.height() // 2 - 200

        for c in triang:
            xl, yl = np.array(
                lr.hex_coords(
                    c[0] * 10 * self.size + center_x,
                    c[1] * 10 * self.size + center_y,
                    self.side_length,
                )
            ).T
            xl = np.hstack([xl, xl[0]]).tolist()
            yl = np.hstack([yl, yl[0]]).tolist()

            # ax.plot(x, y, c="gray", linestyle="--", zorder=1)
            # ax.scatter(x, y, c="black", zorder=2)

            points = self.calculate_hexagon_points(center_x, center_y)
            points = list(zip(xl, yl))
            # hexagon = QPolygon([QPoint(int(x), int(y)) for x, y in zip(xl,yl)])
            painter.setPen(QPen(Qt.black, 2, Qt.SolidLine))
            fill_color = QColor(0, 0, 0)
            brush = QBrush(fill_color)
            painter.setBrush(brush)
            for x, y in points:
                painter.drawEllipse(QRectF(x - 5, y - 5, 10, 10))
            painter.setPen(QPen(Qt.black, 2, Qt.DashLine))
            for i in range(6):
                x1, y1 = points[i]
                x2, y2 = points[(i + 1) % 6]
                lined = QLineF(x1, y1, x2, y2)
                painter.drawLine(lined)
            painter.setPen(QPen(Qt.black, 2, Qt.SolidLine))

            # painter.setBrush(QBrush(QColor(255, 0, 0, 127)))
            # painter.drawPolygon(hexagon)

    def calculate_hexagon_points(self, center_x, center_y):
        points = []
        for i in range(6):
            angle_deg = 60 * i
            angle_rad = math.radians(angle_deg)
            x = center_x + self.side_length * math.cos(angle_rad)
            y = center_y + self.side_length * math.sin(angle_rad)
            points.append((x, y))
        return points


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Hexagone interactif")
        self.central_widget = HexagonWidget(2)
        self.setCentralWidget(self.central_widget)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
