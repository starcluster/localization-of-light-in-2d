import G_hex_poo as ghp
import generate_lattice as gl
import GS15 as gs15
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import bands

matplotlib.rcParams["axes.unicode_minus"] = False
from labellines import *
from mpmath import polylog
from mpmath import exp
from scipy.optimize import curve_fit
from matplotlib.figure import Figure
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

plt.rc("text.latex", preamble=r"\usepackage{amsmath}")

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)


def eigenvalues_zoom(cp):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(20, 16))
    fig, ax = plt.subplots()
    fig, axes = plt.subplots(nrows=2, ncols=2)
    for ax, dB, dAB in zip(axes.flat, [0, 12, 0, 6], [0, 0, 12, 6]):
        cp.DAB_slider.val = dAB
        cp.DB_slider.val = dB
        cp.refresh()
        im = ax.scatter(cp.omega, cp.gamma, c=cp.iprs, cmap="jet")
        ax.axis((-10, 25, 1e-4, 3))
        ax.set_title(
            r"$N=$"
            + str(int(len(cp.omega) / 2))
            + " | $\\Delta_B=$"
            + str(cp.DB_slider.val)
            + " | $\\Delta_{AB}=$"
            + str(cp.DAB_slider.val),
            fontsize=10,
        )
        if (dB == 0 and dAB == 0) or (dB == 12 and dAB == 0):
            ax.set_xticks([])
            ax.set_xlabel("")
        else:
            ax.set_xlabel(r"$\omega$")
        if (dB == 6 and dAB == 6) or (dB == 12 and dAB == 0):
            ax.set_ylabel("")
        else:
            ax.set_ylabel(r"$\gamma_n$")

    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma$")

    plt.savefig("eigenvalues_complex_plane_zoom.pgf", bbox_inches="tight")


def eigenvalues(cp):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(20, 16))
    fig, ax = plt.subplots()
    # fig, axes = plt.subplots(nrows=2, ncols=2)
    plt.scatter(cp.omega, cp.gamma, c=cp.iprs, cmap="jet")
    clb = plt.colorbar()
    clb.ax.set_title("IPR")
    plt.axis((-120, 120, -1, 30))
    plt.title(
        r"$N=$"
        + str(int(len(cp.omega) / 2))
        + " | $\Delta_B=$"
        + str(cp.DB_slider.val)
        + " | $\Delta_{AB}=$"
        + str(cp.DAB_slider.val),
        fontsize=10,
    )

    plt.savefig("eigenvalues_complex_plane.pgf", bbox_inches="tight")


def band_diagram(with_distance):
    plates = 0
    if with_distance:
        plates = 1
    plt.clf()
    plt.cla()
    # plt.figure(figsize=(30,16))
    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1
    lattice = gl.generate_bz(1, Ndots, 1)

    N = 80
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 100
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    fig, axes = plt.subplots(nrows=2, ncols=2)
    vmin, vmax = 0, 0
    for ax, dB, dAB, lettre in zip(
        axes.flat, [0, 12, 0, 12], [0, 0, 12, -12], ["(a)", "(b)", "(c)", "(d)"]
    ):
        # for ax,dB,dAB,lettre in zip(axes.flat,[0,20,20,20],[0,6,10,15],['a','b','c','d']):

        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            lattice, distance, dB, dAB, k0, aho, N, plates, 1
        )

        xprime = [x for x in range(len(omegas))]
        gammas = np.abs(gammas)
        if vmin == 0 and vmax == 0:
            vmin = np.min(gammas)
            vmax = np.max(gammas)
        print(vmin, vmax)
        ax.axis((0, len(xprime), -200, 140))
        n2 = int(Ndots / 3) * 4
        nn = len(B2)

        ax.axvline(
            x=n2 + n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axvline(
            x=n2 - n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        # im = ax.scatter(xprime,omegas,c=gammas,cmap='plasma',marker='o',s=0.2,zorder=2)
        im = ax.scatter(
            xprime,
            omegas,
            c=gammas,
            cmap="plasma",
            marker="o",
            s=0.2,
            norm=matplotlib.colors.Normalize(vmin=vmin + 0.001, vmax=100),
            zorder=2,
        )

        print(min(B2) - max(B3))

        if (dB == 12 and dAB == 0) or (dAB == 12 and dB == 0):
            ax.axhspan(
                min(B2[int(nn / 2) : nn]),
                max(B3[int(nn / 2) : nn]),
                alpha=0.5,
                color="gray",
            )

        ax.text(
            1000,
            -140,
            s=r"\noindent $\Delta_{\mathbf{B}}="
            + str(dB)
            + "\\\\ \\Delta_{AB}="
            + str(dAB)
            + "$",
            fontsize=10,
        )
        ax.text(25, 115, s="$" + lettre + "$")

        if (dB == 0 and dAB == 0) or (dB == 12 and dAB == 0):
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=12)
        else:
            ax.set_xticks(ticks=[0, n, 2 * n], labels=["$M$", "$\Gamma$", "$K$"])
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
            ax.tick_params(labelsize=12)
        if (dB == 12 and dAB == -12) or (dB == 12 and dAB == 0):
            ax.set_ylabel("")
            ax.set_yticks([])
        else:
            ax.set_ylabel(
                r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12
            )

    cbar_ax = fig.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma$")
    plt.subplots_adjust(
        top=0.885, bottom=0.11, left=0.14, right=0.9, hspace=0.025, wspace=0.025
    )
    fig.subplots_adjust(wspace=0.03, hspace=0.12)

    if with_distance and distance > 10:
        plt.savefig(
            "band_diagram_plates_far_away_fig.pdf", format="pdf", bbox_inches="tight"
        )
    elif with_distance and distance <= 10:
        plt.savefig("band_diagram_plates_fig.pdf", format="pdf", bbox_inches="tight")
    else:
        plt.savefig("band_diagram_fig.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def band_diagram_anim(with_distance):
    plates = 0
    if with_distance:
        plates = 1
    plt.clf()
    plt.cla()
    plt.figure(figsize=(30, 16))
    Ndots = 10
    n = 4 * int(Ndots / 3)
    a = 1
    lattice = gl.generate_bz(1, Ndots, 1)
    N = 50
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 11
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05

    vmin, vmax = 0, 0

    for dB in [i for i in range(60)]:
        fig, ax = plt.subplots(1, 1, figsize=(28, 20))
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            lattice, distance, dB, deltaAB, k0, aho, N, plates, 1, 1
        )

        xprime = [x for x in range(len(omegas))]
        gammas = np.abs(gammas)
        if vmin == 0 and vmax == 0:
            vmin = np.min(gammas)
            vmax = np.max(gammas)
        print(vmin, vmax)
        ax.axis((0, len(xprime), -200, 140))
        n2 = int(Ndots / 3) * 4
        nn = len(B2)

        ax.axvline(
            x=n2 + n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axvline(
            x=n2 - n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        im = ax.scatter(
            xprime,
            omegas,
            c=gammas,
            cmap="jet",
            marker="o",
            s=20,
            norm=matplotlib.colors.Normalize(vmin=vmin + 0.001, vmax=15),
            zorder=2,
        )

        print(min(B2[int(nn / 2) : nn]))
        print(max(B3[int(nn / 2) : nn]))

        if abs(max(B3) - min(B2)) > 0.5 and min(B2) > max(B3):
            ax.axhspan(min(B2), max(B3), alpha=0.5, color="gray")
        if with_distance:
            ax.set_title(
                r"$ \Delta_{\mathbf{B}} = " + str(dB) + "| \Delta_{AB}=" + str(0) + "$",
                fontsize=60,
            )
        else:
            ax.set_title(
                r"$ \Delta_{\mathbf{B}}=" + str(dB) + "| \Delta_{AB}=" + str(0) + "$",
                fontsize=60,
            )

        ax.tick_params(labelsize=60)
        ax.set_xticks(
            ticks=[0, n, 2 * n], labels=["$M$", "$\Gamma$", "$K$"], fontsize=60
        )
        ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=60)
        ax.set_ylabel(r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=60)

        fig.subplots_adjust(wspace=0.03, hspace=0.12)
        cbar_ax = fig.add_axes([0.91, 0.15, 0.05, 0.7])
        clb = fig.colorbar(im, cax=cbar_ax)
        im.figure.axes[1].tick_params(axis="y", labelsize=60)
        clb.ax.set_title(r"$\Gamma$", fontsize=60)

        plt.savefig(
            "./band_anim/" + str(dB) + ".png", format="png", bbox_inches="tight"
        )


def band_diagram_background(with_distance):
    plates = 0
    if with_distance:
        plates = 1
    plt.clf()
    plt.cla()
    plt.figure(figsize=(30, 16))
    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1
    r = 1
    lattice = gl.generate_bz(a, Ndots, 1)
    N = 80
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 2  # *np.pi
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    fig, axes = plt.subplots(nrows=2, ncols=2)
    vmin, vmax = 0, 0
    for ax, dB, dAB, lettre in zip(
        axes.flat, [0, 12, 0, 12], [0, 0, 12, -12], ["(a)", "(b)", "(c)", "(d)"]
    ):
        plates = 0  # plotting background dots
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            lattice, distance, dB, dAB, k0, aho, N, plates, a
        )
        n2 = int(Ndots / 3) * 4
        nn = len(B2)
        ax.axvline(
            x=n2 + n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        ax.axvline(
            x=n2 - n2 * 3 * np.sqrt(3) * 0.05 / 2, color="gray", ls="--", zorder=1
        )
        xprime = [x for x in range(len(omegas))]
        ax.scatter(xprime, omegas, c="gray", s=0.2)

        plates = 1  # plotting with plates

        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            lattice, distance, dB, dAB, k0, aho, N, plates, a
        )

        xprime = [x for x in range(len(omegas))]
        gammas = np.abs(gammas)
        if vmin == 0 and vmax == 0:
            vmin = np.min(gammas)
            vmax = np.max(gammas)
        print(vmin, vmax)
        im = ax.scatter(
            xprime,
            omegas,
            c=gammas,
            cmap="plasma",
            marker="o",
            s=0.2,
            zorder=2,
            norm=matplotlib.colors.Normalize(vmin=vmin + 0.001, vmax=1),
        )
        ax.axis((0, len(xprime), -200, 140))
        ax.text(25, 115, s="$" + lettre + "$")
        print(min(B2[int(nn / 2) : nn]))
        print(max(B3[int(nn / 2) : nn]))

        if (dB == 12 and dAB == 0) or (dAB == 12 and dB == 0):
            ax.axhspan(
                min(B2[int(nn / 2) : nn]),
                max(B3[int(nn / 2) : nn]),
                alpha=0.5,
                color="gray",
            )

        if (dB == 0 and dAB == 0) or (dB == 12 and dAB == 0):
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=12)
        else:
            ax.set_xticks(ticks=[0, n, 2 * n], labels=["$M$", "$\Gamma$", "$K$"])
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
            ax.tick_params(labelsize=12)
        if (dB == 12 and dAB == -12) or (dB == 12 and dAB == 0):
            ax.set_ylabel("")
            ax.set_yticks([])
        else:
            ax.set_ylabel(
                r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12
            )

        ax.text(
            1000,
            -140,
            s=r"\noindent $\Delta_{\mathbf{B}}="
            + str(dB)
            + "\\\\ \\Delta_{AB}="
            + str(dAB)
            + "$",
            fontsize=10,
        )

    fig.subplots_adjust(wspace=0.03, hspace=0.12)
    # plt.subplot_tool() AMAZING !
    cbar_ax = fig.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma$")
    plt.subplots_adjust(
        top=0.885, bottom=0.11, left=0.14, right=0.9, hspace=0.025, wspace=0.025
    )

    if distance == 11:
        plt.savefig("band_diagram_backgroundd11.pdf", format="pdf", bbox_inches="tight")
    elif distance == 2:
        plt.savefig("band_diagram_backgroundd2.pdf", format="pdf", bbox_inches="tight")
    else:
        plt.savefig("autre.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def band_diagram_weighta(with_distance):
    plates = 0
    if with_distance:
        plates = 1
    plt.clf()
    plt.cla()

    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1
    lattice = gl.generate_bz(a, Ndots, 1)

    N = 80
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 11
    k0 = 2 * np.pi * 0.05 / a
    step = 0.01  # 0.01 for the paper

    fig, axs = plt.subplots(
        nrows=2, ncols=2, figsize=(18, 14), gridspec_kw={"width_ratios": [1.28, 1]}
    )

    vmin, vmax = 0, 0

    for ax, dB, dAB, lettre in zip(
        [axs[0, 0], axs[1, 0]], [12, 0], [0, -12], ["(a)", "(b)"]
    ):
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            lattice, distance, dB, dAB, k0, aho, N, plates, a
        )
        xprime = [i for i in range(len(B1))]
        im = ax.scatter(
            xprime,
            B1,
            c=weightas[0],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )
        ax.scatter(
            xprime,
            B2,
            c=weightas[1],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )
        ax.scatter(
            xprime,
            B3,
            c=weightas[2],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )
        ax.scatter(
            xprime,
            B4,
            c=weightas[3],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )

        ax.axis((0, len(xprime), -200, 140))
        ax.text(1, 115, s="$" + lettre + "$", size=30)
        nn = int(len(B2) / 3)
        # if dB==12 or dAB==12:
        #     ax.axhspan(max(B2[int(nn/2):nn]),min(B3[int(nn/2):nn]),alpha=0.5,color="gray")
        # if with_distance:
        #     ax.set_title(r"$ \Delta_{\mathbf{B}}=" + str(dB) + " \qquad \\Delta_{AB}=" + str(dAB) + "$", fontsize=10)
        # else:
        #     ax.set_title(r"$ \Delta_{\mathbf{B}}=" + str(dB) + " \qquad \\Delta_{AB}=" + str(dAB) + "$", fontsize=10)

        if dB == 12 and dAB == 0:
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=30)
        else:
            ax.set_xticks(
                ticks=[0, nn, 2 * nn], labels=["$M$", "$\Gamma$", "$K$"], fontsize=30
            )
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=30)
            ax.tick_params(labelsize=30)
        if dB == 0:
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=30)

        t1 = r"\begin{align*}"
        t2 = r"\Delta_{\bf{B}} &=" + str(dB) + "\\"
        t3 = r"\\Delta_{AB} &= " + str(dAB)
        t4 = r"\end{align*}"
        ax.text(200, -140, s=t1 + t2 + t3 + t4, fontsize=30)

        ax.tick_params(labelsize=30)

        ax.set_ylabel(
            r"$\textrm{Frequency}$ $(\omega -\omega_0)/\Gamma_0$", fontsize=30
        )

    BZ = []
    l1, l2 = gl.generate_bz(a, 900, step)
    for ljk in l1:
        BZ.append(ljk)
    for ljk in l2:
        BZ.append(ljk)
    BZ = np.array(BZ)
    for deltaB, deltaAB, ax, lettre in zip(
        [12, 0], [0, 12], [axs[0, 1], axs[1, 1]], ["(c)", "(d)"]
    ):  # [i for i in range(-10,11)]:
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            BZ, distance, deltaB, deltaAB, k0, aho, N, plates, a
        )
        # plot_band_diagram_3D(B1,B2,B3,B4,X,Y,weightas)
        # plt.show()
        im, n, m = bands.plot_band_diagram_2D(B1, B2, B3, B4, X, Y, weightas, ax)
        ax.text(0.0, 6.55, s="$" + lettre + "$", size=30)

        # if deltaB == 0:
        #     ax.set_xlabel(r"$k_x$",fontsize=25)
        # ax.set_xticks(fontsize=20)

        # ax.set_ylabel(r"$k_y$",fontsize=25)
        # ax.set_yticks(fontsize=20)

        # ax.set_xticks([1,10,19],[-2,0,2])
        ax.set_xticks(
            [1 / 20 * m, int(m / 2), m - 1 / 20 * m],
            ["$-2$", "$0$", "$2$"],
            fontsize=20,
        )
        ax.set_yticks(
            [1 / 21 * n, int(n / 2), n - 1 / 21 * n],
            ["$2$", "$0$", "$-2$"],
            fontsize=20,
        )
        # ax.arrow(y=n-1/21*n,x=1/20*m,dx=1/20*m,dy=0, width=0.2*1/21*n,color="black",head_length=1/20*m)
        # ax.arrow(y=n-1/21*n,x=1/20*m,dx=0,dy=-1/21*n, width=0.2*1/20*m,color="black",head_length=1/21*n)

        # ax.text(x = 3/20*m, y = n-1/21*n,s=r"$k_x$",fontsize = 25)
        # ax.text(x = 1/20*m, y = n-4/21*n,s=r"$k_y$",fontsize = 25)

        # ax.set_title(r"$ \Delta_{\mathbf{B}}=" + str(deltaB) + " \qquad \\Delta_{AB}=" + str(deltaAB) + "$", fontsize=20)

    # asp = np.abs(np.diff(axs[0,1].get_xlim())[0] / np.diff(axs[0,1].get_ylim())[0])
    # axs[1,0].set_aspect(asp)
    # axs[1,1].set_aspect(asp)

    # plt.subplot_tool()
    fig.subplots_adjust(
        top=0.88, bottom=0.11, left=0.125, right=0.900, hspace=0.1, wspace=0.07
    )
    cbar_ax = fig.add_axes([0.91, 0.11, 0.03, 0.74])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$W_A$", fontsize=25)
    clb.ax.tick_params(labelsize=20)
    arr_img = plt.imread("arrows.png")
    im2 = OffsetImage(arr_img, zoom=0.1)
    ab = AnnotationBbox(
        im2,
        (0.0, 0.0),
        xycoords="axes fraction",
        box_alignment=(-0.1, -0.1),
        frameon=False,
    )
    ax.add_artist(ab)

    plt.savefig("bd_sites_A.pdf", format="pdf", bbox_inches="tight", dpi=600)
    plt.show()


def band_diagram_sigma(with_distance):
    plates = 0
    if with_distance:
        plates = 1
    plt.clf()
    plt.cla()

    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1
    lattice = gl.generate_bz(a, Ndots, 1)

    N = 40
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 11
    k0 = 2 * np.pi * 0.05 / a
    step = 0.1

    fig, axs = plt.subplots(
        nrows=2, ncols=2, figsize=(18, 14), gridspec_kw={"width_ratios": [1.28, 1]}
    )

    vmin, vmax = 0, 0

    for ax, dB, dAB, lettre in zip(
        [axs[0, 0], axs[1, 0]], [-12, 0], [0, 12], ["(a)", "(b)"]
    ):
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            lattice, distance, dB, dAB, k0, aho, N, plates, a
        )
        xprime = [i for i in range(len(B1))]
        im = ax.scatter(
            xprime,
            B1,
            c=sigmas[0],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )
        ax.scatter(
            xprime,
            B2,
            c=sigmas[1],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )
        ax.scatter(
            xprime,
            B3,
            c=sigmas[2],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )
        ax.scatter(
            xprime,
            B4,
            c=sigmas[3],
            cmap="plasma",
            marker="o",
            s=4,
            norm=matplotlib.colors.Normalize(vmin=0, vmax=1),
        )

        ax.axis((0, len(xprime), -200, 140))
        ax.text(1, 115, s="$" + lettre + "$", size=30)
        nn = int(len(B2) / 3)
        # if dB==12 or dAB==12:
        #     ax.axhspan(max(B2[int(nn/2):nn]),min(B3[int(nn/2):nn]),alpha=0.5,color="gray")
        # if with_distance:
        #     ax.set_title(r"$ \Delta_{\mathbf{B}}=" + str(dB) + " \qquad \\Delta_{AB}=" + str(dAB) + "$", fontsize=10)
        # else:
        #     ax.set_title(r"$ \Delta_{\mathbf{B}}=" + str(dB) + " \qquad \\Delta_{AB}=" + str(dAB) + "$", fontsize=10)

        if dB == 12 and dAB == 0:
            ax.set_xticks([])
            ax.set_xlabel("")
            ax.tick_params(labelsize=30)
        else:
            ax.set_xticks(
                ticks=[0, nn, 2 * nn], labels=["$M$", "$\Gamma$", "$K$"], fontsize=30
            )
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
            ax.tick_params(labelsize=30)
        if dB == 0:
            ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)

        t1 = r"\begin{align*}"
        t2 = r"\Delta_{\bf{B}} &=" + str(dB) + "\\"
        t3 = r"\\Delta_{AB} &= " + str(dAB)
        t4 = r"\end{align*}"
        ax.text(200, -140, s=t1 + t2 + t3 + t4, fontsize=30)

        ax.tick_params(labelsize=30)

        ax.set_ylabel(
            r"$\textrm{Frequency}$ $(\omega -\omega_0)/\Gamma_0$", fontsize=30
        )

    BZ = []
    l1, l2 = gl.generate_bz(a, 900, step)
    for ljk in l1:
        BZ.append(ljk)
    for ljk in l2:
        BZ.append(ljk)
    BZ = np.array(BZ)
    for deltaB, deltaAB, ax, lettre in zip(
        [12, 0], [0, 12], [axs[0, 1], axs[1, 1]], ["(c)", "(d)"]
    ):  # [i for i in range(-10,11)]:
        B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
            BZ, distance, deltaB, deltaAB, k0, aho, N, plates, a
        )
        # plot_band_diagram_3D(B1,B2,B3,B4,X,Y,weightas)
        # plt.show()
        im, n, m = bands.plot_band_diagram_2D(B1, B2, B3, B4, X, Y, sigmas, ax)
        ax.text(0.0, 6.55, s="$" + lettre + "$", size=30)
        # if deltaB == 0:
        #     ax.set_xlabel(r"$k_x$",fontsize=25)
        # ax.set_xticks(fontsize=20)

        # ax.set_ylabel(r"$k_y$",fontsize=25)
        # ax.set_yticks(fontsize=20)

        # ax.set_xticks([1,10,19],[-2,0,2])
        ax.set_xticks(
            [1 / 20 * m, int(m / 2), m - 1 / 20 * m],
            ["$-2$", "$0$", "$2$"],
            fontsize=20,
        )
        ax.set_yticks(
            [1 / 21 * n, int(n / 2), n - 1 / 21 * n],
            ["$2$", "$0$", "$-2$"],
            fontsize=20,
        )
        ax.arrow(
            y=n - 1 / 21 * n,
            x=1 / 20 * m,
            dx=1 / 20 * m,
            dy=0,
            width=0.2 * 1 / 21 * n,
            color="black",
            head_length=1 / 20 * m,
        )
        ax.arrow(
            y=n - 1 / 21 * n,
            x=1 / 20 * m,
            dx=0,
            dy=-1 / 21 * n,
            width=0.2 * 1 / 20 * m,
            color="black",
            head_length=1 / 21 * n,
        )

        ax.text(x=3 / 20 * m, y=n - 1 / 21 * n, s=r"$k_x$", fontsize=25)
        ax.text(x=1 / 20 * m, y=n - 4 / 21 * n, s=r"$k_y$", fontsize=25)

        # ax.set_title(r"$ \Delta_{\mathbf{B}}=" + str(deltaB) + " \qquad \\Delta_{AB}=" + str(deltaAB) + "$", fontsize=20)

    fig.subplots_adjust(wspace=0.07, hspace=0.07)
    cbar_ax = fig.add_axes([0.91, 0.11, 0.03, 0.73])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\sigma_+$", fontsize=25)
    clb.ax.tick_params(labelsize=20)

    plt.savefig("bd_polarisation_plus.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def gap(rebake):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(15, 7))
    Ndots = 200
    n = 4 * int(Ndots / 3)
    a = 1
    step = 0.01

    lattice, l2 = gl.generate_bz(a, 900, step)
    l = []
    for ljk in lattice:
        l.append(ljk)
    for ljk in l2:
        l.append(ljk)
    lattice = np.array(l)
    lattice += (0.001, 0.001)
    # lattice  = gl.generate_bz(1,Ndots,2)
    N = 50
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 2
    k0 = 2 * np.pi * 0.05
    step = 0.05
    plates = 1

    deltaAB = 0
    deltaABs = [0, 3, -6]  # ,12,18]
    deltaAB_color = [plasma_blue, plasma_purple, plasma_pink]  # ,'orange','purple']

    deltaBs = [-60 + 2 * i for i in range(61)]

    for deltaAB, color in zip(deltaABs, deltaAB_color):
        # for deltaAB,color in zip([12,16,20],["red","green","blue"]):
        gap_l = []
        if rebake:
            for deltaB in deltaBs:
                if abs(deltaB) > 52:
                    gap_l.append(0)
                else:
                    (
                        B1,
                        B2,
                        B3,
                        B4,
                        X,
                        Y,
                        omegas,
                        gammas,
                        weightas,
                        sigmas,
                    ) = gs15.band_diagram(
                        lattice, distance, deltaB, deltaAB, k0, aho, N, plates, a
                    )
                    xprime = [x for x in range(len(B1))]
                    plt.plot(xprime, B1)
                    plt.plot(xprime, B2)
                    plt.plot(xprime, B3)
                    plt.plot(xprime, B4)
                    plt.axis((0, len(xprime), -200, 140))
                    plt.show()
                    exit()
                    print(max(B3))
                    print(min(B2))

                    nn = len(B1)
                    g = min(B2[int(nn / 2) : -1]) - max(B3[int(nn / 2) : -1])
                    if g < 0:
                        g = 0
                    gap_l.append(g)
                    print(deltaB, g)

            np.savetxt(
                "gap" + str(deltaAB) + ".csv",
                [p for p in zip(deltaBs, gap_l)],
                delimiter=",",
                fmt="%s",
            )
        else:
            csv = np.genfromtxt("gap" + str(deltaAB) + ".csv", delimiter=",")
            deltaBs = csv[:, 0]
            gap_l = csv[:, 1]

        plt.plot(
            deltaBs,
            gap_l,
            c=color,
            label=r"$\Delta_{AB} = " + str(deltaAB) + "$",
            linestyle="None",
            marker="o",
            zorder=2,
        )

        # plt.plot(deltaBs, gap_l_th,c='gray',label=r"$\Delta_{AB} = $" + str(deltaAB))

    # labelLines(plt.gca().get_lines(),zorder=100,fontsize=40,xvals=[-14,12,-14],bbox=dict(facecolor='white', alpha=1, edgecolor='none')) # to keep in case of putting the legend on the curve
    plt.legend(bbox_to_anchor=(0.88, 0.4), prop={"size": 26})

    deltaBs = [-100 + i / 10 for i in range(2000)]

    for deltaAB, color in zip(deltaABs, deltaAB_color):
        # for deltaAB,color in zip([12,16,20],["red","green","blue"]):
        gap_l_th = []
        for deltaB in deltaBs:
            C0 = -(
                0.1489803 / (k0 * a / 2 / np.pi)
                + 0.0002726 / (k0 * a / 2 / np.pi) ** 2
                + 0.00135505 / (k0 * a / 2 / np.pi) ** 3
            )
            C1 = (
                -0.43169 / (k0 * a / 2 / np.pi)
                - 1.37252819e-04 / (k0 * a / 2 / np.pi) ** 2
                + 6.42156198e-03 / (k0 * a / 2 / np.pi) ** 3
            )
            C2 = (
                -1.62950412e-01 / (k0 * a / 2 / np.pi)
                + 2.31735224e-05 / (k0 * a / 2 / np.pi) ** 2
                + 1.34708817e-02 / (k0 * a / 2 / np.pi) ** 3
            )
            C3 = 0.09188391 / (k0 * a / 2 / np.pi) ** 2

            S = 2 * np.real(
                np.sqrt(
                    deltaAB**2 + 0.25 * C2**2 + 0.5 * 1j * C2 * C3 - 0.25 * C3**2
                )
            )
            R1 = 0.25 * (C0 - C1 + S + 2 * abs(deltaAB))
            R2 = 0.25 * (C0 - C1 - S - 2 * abs(deltaAB))

            if abs(deltaB) > S / 2:  # outer regime
                gap_l_th.append(0)
            elif abs(deltaB) < S / 2 and abs(deltaB) > abs(R2):  # linear regime W_gap3
                gap_l_th.append(-2 * abs(deltaB) + S)
            elif abs(deltaB) < R1:  # linear regime W_gap1
                gap_l_th.append(2 * abs(abs(deltaB) - abs(deltaAB)))
            else:  # plateau (W_gap2)
                gap_l_th.append(0.5 * (-C1 + C0 + S - 2 * abs(deltaAB)))

            print(deltaB, deltaAB, gap_l_th[-1])

        plt.plot(deltaBs, gap_l_th, c="gray", zorder=1, ls="--")

    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.xlabel(r" $\Delta_{B}$", fontsize=40)
    plt.ylabel(r"$\textrm{gap}$", fontsize=40)
    # plt.axis((-60,60,-3,26))
    plt.axis((-60, 60, -3, 25))

    plt.savefig("gap.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def delta_AB_function(rebake):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(15, 15))
    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1
    step = 0.05
    fig, (ax1, ax2) = plt.subplots(
        nrows=1, ncols=2, figsize=(18, 10), gridspec_kw={"width_ratios": [1.28, 1]}
    )
    lattice, l2 = gl.generate_bz(a, 900, step)
    l = []
    for ljk in lattice:
        l.append(ljk)
    for ljk in l2:
        l.append(ljk)
    lattice = np.array(l)
    lattice += (0.001, 0.001)
    # lattice  = gl.generate_hexa_reciprocal(a,Ndots)
    N = 50
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 100
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    plates = 0

    deltaB = 30

    deltaABs = [-30 + i for i in range(61)]
    gap_l = []

    for h, col in zip([0.045, 0.05, 0.055], [plasma_blue, plasma_purple, plasma_pink]):
        k0 = 2 * np.pi * h / a
        gap_l = []
        gap_l_th = []
        if rebake:
            for deltaAB in deltaABs:
                (
                    B1,
                    B2,
                    B3,
                    B4,
                    X,
                    Y,
                    omegas,
                    gammas,
                    weightas,
                    sigmas,
                ) = gs15.band_diagram(
                    lattice, distance, deltaB, deltaAB, k0, aho, N, plates, a
                )
                g = min(B2) - max(B3)
                if g < 0:
                    g = 0
                gap_l.append(g)

            np.savetxt(
                "gap_dB_cst" + str(h) + ".csv",
                [p for p in zip(deltaABs, gap_l)],
                delimiter=",",
                fmt="%s",
            )

        else:
            csv = np.genfromtxt("gap_dB_cst" + str(h) + ".csv", delimiter=",")
            deltaBs = csv[:, 0]
            gap_l = csv[:, 1]
        for deltaAB in deltaABs:
            C0 = -(
                0.1489803 / (k0 * a / 2 / np.pi)
                + 0.0002726 / (k0 * a / 2 / np.pi) ** 2
                + 0.00135505 / (k0 * a / 2 / np.pi) ** 3
            )
            C1 = (
                -0.43169 / (k0 * a / 2 / np.pi)
                - 1.37252819e-04 / (k0 * a / 2 / np.pi) ** 2
                + 6.42156198e-03 / (k0 * a / 2 / np.pi) ** 3
            )
            C2 = (
                -1.62950412e-01 / (k0 * a / 2 / np.pi)
                + 2.31735224e-05 / (k0 * a / 2 / np.pi) ** 2
                + 1.34708817e-02 / (k0 * a / 2 / np.pi) ** 3
            )
            C3 = 0.09188391 / (k0 * a / 2 / np.pi) ** 2

            S = 2 * np.real(
                np.sqrt(
                    deltaAB**2 + 0.25 * C2**2 + 0.5 * 1j * C2 * C3 - 0.25 * C3**2
                )
            )
            R1 = 0.25 * (C0 - C1 + S + 2 * abs(deltaAB))
            R2 = 0.25 * (C0 - C1 - S - 2 * abs(deltaAB))

            gap_l_th.append(0.5 * (-C1 + C0 + S - 2 * abs(deltaAB)))

        ax1.plot(deltaABs, gap_l_th, color="gray", linewidth=7, linestyle="--")
        ax1.plot(
            deltaABs,
            gap_l,
            color=col,
            label=r"$k_0 a = 2\pi\times" + str(h) + "$",
            linewidth=4,
        )

    ax1.text(-16, 30, r"$\Delta_{\bf{B}} = 30$", fontsize=30)
    ax1.legend(bbox_to_anchor=(0.5, 0.3), prop={"size": 30})

    # ax1.xticks(fontsize=40)
    # ax1.yticks(fontsize=40)
    ax1.tick_params(labelsize=30)
    ax1.text(-17, 33, s="$(a)$", size=30)

    ax1.set_xlabel(r" $\Delta_{AB}$", fontsize=30)
    ax1.set_ylabel(r"$\textrm{Maximum gap width}$", fontsize=60)
    ax1.axis((-18, 18, 0, 35))
    # plt.title()

    #### B
    x = np.array([0.02 + i / 100 for i in range(11)])
    x2 = np.linspace(0.02, 0.13, 100)
    y = [
        358.74732057530093,
        107.37692146202085,
        45.94454017647327,
        23.953636389580335,
        14.171163647608326,
        9.15868746914277,
        6.321099858791805,
        4.591077793399086,
        3.4741015483721065,
        2.719329530900203,
        2.1901321889996286,
    ]

    def func(x, A):
        return A * x ** (-3)

    popt, pcov = curve_fit(func, x, y)
    A = popt[0]

    y2 = A * x2 ** (-3)

    print(popt)
    print(pcov)

    ax2.set_xlabel(r"$k_0a/2\pi$", fontsize=30)
    # ax2.set_ylabel(r"$\textrm{plateau}$", fontsize=30)
    ax2.tick_params(
        labelsize=30, left=False, right=True, labelright=True, labelleft=False
    )
    t1 = r"\begin{align*}"
    t2 = r"y &= A(k_0a/2\pi)^{-3}\\"
    t3 = r"A &= " + str(np.round(A, 3))
    t4 = r"\end{align*}"
    ax2.text(x=0.045, y=150, s=t1 + t2 + t3 + t4, fontsize=30)
    ax2.plot(x2, y2, zorder=1, color="gray", lw=4, ls="--")
    ax2.scatter(x, y, color=plasma_blue, zorder=2, s=60)
    ax2.loglog()
    ax2.minorticks_off()
    ax2.set_xticks(ticks=[])
    ax2.set_xticks(
        [0.01 * i * 2 for i in range(1, 6)],
        ["$" + str(0.01 * i * 2) + "$" for i in range(1, 6)],
        fontsize=30,
        rotation=30,
    )
    # ax2.set_yticks(fontsize=20)
    ax2.text(0.115, 330, s="$(b)$", size=30)
    fig.subplots_adjust(wspace=0.07, hspace=0.07)
    fig.savefig("gap_dB_cst.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def smaller_imag_part(rebake):
    plt.clf()
    plt.cla()
    # plt.figure(figsize=(15,7))
    Ndots = 100
    n = 4 * int(Ndots / 3)
    a = 1
    lattice = gl.generate_bz(a, Ndots, 1)
    N = 30
    aho = 0.01
    deltaB, deltaAB = 12, 0
    distance = 2
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    plates = 1
    y = []
    x = [0 + i for i in range(50)]
    x2 = [0.3 - i / 1000 for i in range(200)]

    if rebake:
        for aho in x2:
            B1, B2, B3, B4, X, Y, omegas, gammas = gs15.band_diagram(
                lattice, distance, deltaB, deltaAB, k0, aho, N, plates
            )
            print(max(map(abs, gammas)))
            y.append(max(map(abs, gammas)))

        X = np.array(x2)
        Y = np.array(y)
        np.savetxt(
            "smaller_imag_part.csv", [p for p in zip(X, Y)], delimiter=",", fmt="%s"
        )

    else:
        csv = np.genfromtxt("smaller_imag_part.csv", delimiter=",")
        X = csv[:, 0]
        Y = csv[:, 1]

    def carre(x, a):
        return a * x**2

    popt, pcov = curve_fit(carre, X, Y)
    print(popt, pcov)
    th = X**2 * 9.0

    plt.plot(X, th, c=plasma_orange, linewidth=3, label=r"$y=9x^2$")
    plt.legend(bbox_to_anchor=(0.65, 0.9), fontsize=30)

    plt.xlabel(r"$a_{\textrm{ho}}$", fontsize=30)
    plt.ylabel(r"$\Gamma_{\textrm{max}}$", fontsize=30)
    plt.xticks(fontsize=20, rotation=30)
    plt.yticks(fontsize=20)
    # plt.gca().invert_xaxis()
    plt.scatter(X, Y, c=plasma_blue)

    plt.text(s="$(a)$", x=0.1, y=0.79, fontsize=20)

    plt.savefig("smaller_imag_part.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def im_G():
    def G_plates(d):
        return (
            (
                polylog(1, -exp(1j * d)) / d
                + 1j * polylog(2, -exp(1j * d)) / d**2
                - polylog(3, -exp(1j * d)) / d**3
            )
            + 1j / 3
        ) * 3

    plt.clf()
    plt.cla()
    # plt.figure(figsize=(15,7))
    N = 500
    d = np.linspace(0.1, 50, N)
    y = np.zeros_like(d)
    for i in range(N):
        y[i] = np.imag(G_plates(d[i]))
    y2 = np.ones_like(d)
    # for i in [2*i+1 for i in range(10)]:
    #     plt.axvline(i*np.pi,ls='--',linewidth=5.0, color="gray")

    d = d / np.pi
    plt.plot(d, y2, c="gray", linewidth=5.0, linestyle="--")
    plt.plot(d, y, c="blue", linewidth=5.0)
    plt.axis((0, 12, -0.05, 3.2))
    plt.xticks([i for i in range(13)])

    plt.xlabel(r"$k_0d/\pi$", fontsize=30)
    # plt.ylabel(r"$\frac{6\pi}{k_0}(\Im(G_{\alpha\beta}(0)) + 1)$",fontsize=40)
    plt.ylabel(r"$\gamma_{\rm{plates}}(d)$", fontsize=30)
    # plt.savefig("im_G.pgf", bbox_inches = 'tight')
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.text(0.2, 2.95, s="$(b)$", size=20)
    plt.savefig("im_G.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def gap_k0a(rebake):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(15, 7))
    Ndots = 200
    n = 4 * int(Ndots / 3)
    a = 1
    step = 0.05
    lattice, l2 = gl.generate_bz(a, 900, step)
    l = []
    for ljk in lattice:
        l.append(ljk)
    for ljk in l2:
        l.append(ljk)
    lattice = np.array(l)
    lattice += (0.001, 0.001)

    N = 40
    aho = 0.08
    deltaB, deltaAB = 0, 6
    distance = 2
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05
    plates = 0

    k0s = np.array([0.4 + 0.01 * i for i in range(100)]) * 2 * np.pi / 10
    gap_l = []
    if rebake:
        for k0 in k0s:
            print(k0)
            B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
                lattice, distance, deltaB, deltaAB, k0, aho, N, plates
            )
            print(sorted(B3))
            gap_l.append(abs(min(B3) - max(B2)))

        np.savetxt("gap_k0a.csv", [p for p in zip(k0s, gap_l)], delimiter=",", fmt="%s")

    else:
        csv = np.genfromtxt("gap_k0a.csv", delimiter=",")
        deltaBs = csv[:, 0]
        gap_l = csv[:, 1]

    plt.plot(
        k0s,
        gap_l,
        color="orange",
        linestyle="None",
        marker="o",
        label=r"$\Delta_{B}=12 |  \Delta_{AB} = 0$",
    )
    plt.legend(prop={"size": 26})

    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.xlabel(r" $k_0a$", fontsize=40)
    plt.ylabel(r"$\textrm{gap  saturation}$", fontsize=40)

    plt.savefig("gap_k0a.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def gap_dBcst_Nincreasing(rebake):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(15, 7))
    Ndots = 200
    n = 4 * int(Ndots / 3)
    a = 1
    lattice = gl.generate_hexa_reciprocal(a, Ndots)
    N = 40
    aho = 0.7
    deltaB, deltaAB = 12, 0
    distance = 2
    k0 = 2 * np.pi * 0.08 / a
    step = 0.05
    plates = 0

    deltaB = 30
    deltaAB = 0
    k0s = 2 * np.pi * np.array([i for i in range(4, 100)]) / 20 / 10
    gap_l = []
    steps = [0.01 * i for i in range(2, 10)]
    if rebake:
        for step in steps:
            l = []
            lattice, l2 = gl.generate_bz(a, 900, step)

            for ljk in lattice:
                l.append(ljk)
            for ljk in l2:
                l.append(ljk)

            lattice = np.array(l)
            lattice += (0.001, 0.001)

            gl.display_lattice(lattice, "red")
            # gl.display_lattice(l2, "blue")
            plt.show()
            print(lattice.shape)
            B1, B2, B3, B4, X, Y, omegas, gammas = gs15.band_diagram(
                lattice, distance, deltaB, deltaAB, k0, aho, N, plates
            )
            gap_l.append(abs(min(B3) - max(B2)))

        np.savetxt(
            "gap_dBcst_Nincreasing.csv",
            [p for p in zip(steps, gap_l)],
            delimiter=",",
            fmt="%s",
        )

    else:
        csv = np.genfromtxt("gap_dBcst_Nincreasing.csv", delimiter=",")
        steps = csv[:, 0]
        gap_l = csv[:, 1]

    plt.plot(
        steps,
        gap_l,
        color="orange",
        linestyle="None",
        marker="o",
        label=r"$\Delta_{B}=12 |  \Delta_{AB} = 0$",
    )
    plt.legend(prop={"size": 26})

    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.xlabel(r" $N_{dots}$", fontsize=40)
    plt.ylabel(r"$\textrm{gap  saturation}$", fontsize=40)

    plt.savefig("gap_dBcst_Nincreasing.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def gap_k0a_var(rebake):
    plt.clf()
    plt.cla()
    plt.figure(figsize=(15, 7))
    Ndots = 400
    n = 4 * int(Ndots / 3)
    a = 1

    N = 50
    aho = 0.1

    distance = 100
    k0 = 2 * np.pi * 0.05 / a
    step = 0.01
    plates = 0

    deltaB = 0
    deltaAB = 12
    k0s = 2 * np.pi * np.array([i for i in range(20, 60)]) / 20 / 10
    ass = [i / 100 + 1 for i in range(1, 200)]
    gap_l = []
    j = 0
    if rebake:
        for a in ass:
            k0 = 2 * np.pi * 0.05 * a
            print(a)
            l = []
            # lattice  = gl.generate_bz(a,Ndots,1)
            lattice, l2 = gl.generate_bz(a, Ndots, step)

            l = []
            for ljk in lattice:
                l.append(ljk)
            for ljk in l2:
                l.append(ljk)
            lattice = np.array(l)
            lattice += (0.001, 0.001)

            print(lattice.shape)
            B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas = gs15.band_diagram(
                lattice, distance, deltaB, deltaAB, k0, aho, N, plates
            )
            x = [i for i in range(len(B2))]

            # plt.scatter(x,B1)
            # plt.scatter(x,B2)
            # plt.scatter(x,B3)
            # plt.scatter(x,B4)
            # plt.axis((0,len(B2),-40,40))
            # plt.title(r"$a$ = " + str(a))
            # plt.savefig('a/' + str(j) + '.png', format='png', bbox_inches='tight')
            # plt.clf()
            # plt.cla()
            # j += 1

            B = B1 + B2 + B3 + B4
            B20 = []
            B20.sort()
            for b in B:
                if abs(b) < 20:
                    B20.append(b)
            B20 = np.array(B20)
            Bdiff = np.diff(B20)
            gap_l.append(np.max(Bdiff))
            # u = len(B3)
            # print(max(B3[int(u/2):]))
            # print(min(B2[int(u/2):]))
            # gap_l.append(abs(min(B2[int(u/2):])-max(B3[int(u/2):])))

        np.savetxt(
            "gap_k0a_var.csv", [p for p in zip(ass, gap_l)], delimiter=",", fmt="%s"
        )

    else:
        csv = np.genfromtxt("gap_k0a_var.csv", delimiter=",")
        ass = csv[:, 0]
        gap_l = csv[:, 1]

    plt.plot(
        ass,
        gap_l,
        color="orange",
        linestyle="None",
        marker="o",
        label=r"$\Delta_{B}=0 |  \Delta_{AB} = 12$",
    )
    plt.legend(prop={"size": 26})

    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlabel(r"$a$", fontsize=30)
    plt.ylabel(r"$\textrm{gap}$", fontsize=30)

    plt.savefig("gap_k0a_var.pdf", format="pdf", bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    # fig, ax = plt.subplots()
    # cp = ghp.ComplexPlane(ax, 2, 1)
    # gap(False)
    # im_G()
    # smaller_imag_part(False)
    # gap_k0a_var(True)
    # band_diagram(False)
    band_diagram_background(True)  # parameter must be the distance !
    # band_diagram_anim(False)
    # band_diagram_weighta(False)
    # delta_AB_function(False)
    # gap_dBcst_Nincreasing(True)
    # band_diagram_sigma(False)
