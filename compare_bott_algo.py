import matplotlib.pyplot as plt
import numpy as np

import Gd
import lattice_r as lr
from bott2 import bott,compute_frequencies
import tex

tex.useTex()

def compute_bott_maxwell(delta_B, dagger):
    N = 600
    a = 1
    s = 1.00

    grid = lr.generate_hex_grid_lb_center(N, a)

    M = Gd.create_matrix_TE(grid, delta_B, 6, 0.05 * 2 * np.pi)
    N = M.shape[0]

    w, v = np.linalg.eig(M)

    idx = w.argsort()[::-1]
    w = w[idx]
    v = v[:, idx]
    ω = 7

    f, v = compute_frequencies(w, v)


    return bott(grid, v, f, ω, pol=True, dagger=dagger)


if __name__ == "__main__":
    bm_dagger = []
    bm_inv = []
    delta_Bs = list(range(-12,13))
    for delta_B in delta_Bs:
        print(delta_B)
        bm_dagger.append(compute_bott_maxwell(delta_B, True))
        bm_inv.append(compute_bott_maxwell(delta_B, False))

    plt.title(r"$\textrm{Real part of Bott index}$",fontsize=20)
    plt.scatter(delta_Bs, np.real(bm_dagger),color="blue",marker="^",label=r"$UVU^\dagger V^\dagger$")
    plt.scatter(delta_Bs, np.real(bm_inv),color="red",marker="v",label=r"$UVU^{-1}V^{-1}$")
    plt.legend()
    plt.xlabel(r"$\Delta_B$",fontsize=20)
    plt.ylabel(r"$\textrm{Re}(C_B)$",fontsize=20)
    plt.savefig("re_part_bott_W_600.pdf",format="pdf",bbox_inches='tight')    
    plt.show()
    

    plt.title(r"$\textrm{Imaginary part of Bott index}$",fontsize=20)
    plt.scatter(delta_Bs, np.imag(bm_dagger),color="blue",marker="^",label=r"$UVU^\dagger V^\dagger$")
    plt.scatter(delta_Bs, np.imag(bm_inv),color="red",marker="v",label=r"$UVU^{-1}V^{-1}$")
    plt.legend()
    plt.xlabel(r"$\Delta_B$",fontsize=20)
    plt.ylabel(r"$\textrm{Im}(C_B)$",fontsize=20)
    plt.savefig("im_part_bott_W_600.pdf",format="pdf",bbox_inches='tight')    
    plt.show()

