#include <math.h>
#include <complex.h>
#include <stdio.h>
#include "omp.h"

// gcc -shared -Wall -lm sum.c -o sum.o

#define N 100

double complex P(double complex x){
  return 1.-1./x+1./(x*x);
}

double complex Q(double complex x){
  return -1.+3./x-3./(x*x);
}

double complex diag(double d, int n){
  double complex dbln = n;
  double complex f = P(I*dbln*d)*pow(-1,n)*cexp(I*dbln*d)/dbln/d;
  return f;
}

double complex sum_diag(double d){
  double complex s = 0+0*I;
  for(int n = 1; n < N+1; n++){
    s += diag(d,n);
  }
  return s;
}
/////////////////////////////////////////////////// a supprimer
double diag_bis(double p, double d, int n){
  double dbln = n;
  double rn = sqrt(p*p+(dbln*d*dbln*d));
  double rn2 = rn*rn;
  double p2 = p*p;
  double f = pow(-1,n)/rn*(cos(rn)/rn*(1-p2/rn2)+sin(rn)*(1-1/rn2+p2/rn2*(-1+3/rn2)));
  return f;
}

double sum_diag_bis(double p, double d){
  double  s = 0;

  for(int n = 1; n < N+1; n++){
    printf("%f || %f\n", s, diag_bis(p,d,n));
    s += diag_bis(p,d,n);
  }
  return s;
}
//////////////////////////
double sum_diag_re(double d){
  return creall(sum_diag(d));
}

double sum_diag_im( double d){
  return cimagl(sum_diag(d));
}


double complex gxx(double x, double y, double d, int n){
  double dbln = n;
  double r2 = x*x+y*y;
  double dn2 = d*dbln*d*dbln;
  double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(2*P(I*csqrt(r2+dn2))*(r2+dn2) + Q(I*csqrt(r2+dn2))*r2)*pow(-1,n)/2/(r2+dn2);
  //double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(P(I*csqrt(r2+dn2)) + Q(I*csqrt(r2+dn2))*x2/(r2+dn2))*pow(-1,n); //polarisation linéaire
  if (isnan(creall(f))){
      printf("r2 = %lf\n",r2);
  }
  return f;
}

double complex gzz_no_plates(double x, double y){
   double r2 = x*x+y*y;
  double complex f = cexp(I*csqrt(r2))/csqrt(r2)*P(I*csqrt(r2));
  return f;
}

double gzz_no_plates_re(double x, double y){  return creall(gzz_no_plates(x, y));}
double gzz_no_plates_im(double x, double y){  return cimagl(gzz_no_plates(x, y));}

double complex gxx_no_plates(double x, double y, int n){
  double r2 = x*x+y*y;
  double complex f = cexp(I*csqrt(r2))/csqrt(r2)*(2*P(I*csqrt(r2))*r2 + Q(I*csqrt(r2))*r2)/2/(r2);
  return f;
}

double complex sum_gxx(double x, double y, double d){
  double complex s = 0+0*I;

  for(int n = -N; n < N+1; n++){
    s += gxx(x,y,d,n);
  }
  return s;
}

double sum_gxx_re(double x, double y, double d){  return creall(sum_gxx(x, y, d));}

double sum_gxx_im(double x, double y, double d){  return cimagl(sum_gxx(x, y, d));}

double complex gyy(double x, double y, double d, int n){
  double dbln = n;
  double r2 = x*x+y*y;
  double dn2 = d*dbln*d*dbln;
  double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(2*P(I*csqrt(r2+dn2))*(r2+dn2) + Q(I*csqrt(r2+dn2))*r2)*pow(-1,n)/2/(r2+dn2);
  //double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(P(I*csqrt(r2+dn2)) + Q(I*csqrt(r2+dn2))*y2/(r2+dn2))*pow(-1,n); //polarisation linéaire
  return f;
}

double complex sum_gyy(double x, double y, double d){
  double complex s = 0+0*I;

  for(int n = -N; n < N+1; n++){
    s += gyy(x,y,d,n);
  }
  return s;
}

double sum_gyy_re(double x, double y, double d){  return creall(sum_gyy(x, y, d));}

double sum_gyy_im(double x, double y, double d){  return cimagl(sum_gyy(x, y, d));}

double complex gxy(double x, double y, double d, int n){
  double dbln = n;
  double x2 = x*x, y2=y*y, dn2 = d*dbln*d*dbln;
  double r2 = x2 + y2;
  double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(Q(I*csqrt(r2+dn2))*cpow(I*x+y,2))*pow(-1,n)/2/(r2+dn2);
  //double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(Q(I*csqrt(r2+dn2))*x*y)*pow(-1,n)/(r2+dn2);
  return f;
}


double complex sum_gxy(double x, double y,  double d){
  double complex s = 0+0*I;
  for(int n = -N; n < N+1; n++){
    s += gxy(x,y,d,n);
  }
  return s;
}

double sum_gxy_re(double x, double y, double d){
  return creall(sum_gxy(x, y, d));
}

double sum_gxy_im(double x, double y, double d){
  return cimagl(sum_gxy(x, y, d));
}

double complex gyx(double x, double y, double d, int n){
  double dbln = n;
  double x2 = x*x, y2=y*y, dn2 = d*dbln*d*dbln;
  double r2 = x2 + y2;
  double complex f = cexp(I*csqrt(r2+dn2))/csqrt(r2+dn2)*(Q(I*csqrt(r2+dn2))*cpow(-I*x+y,2))*pow(-1,n)/2/(r2+dn2);
  return f;
}


double complex sum_gyx(double x, double y,  double d){
  double complex s = 0+0*I;
  for(int n = -N; n < N+1; n++){
    s += gyx(x,y,d,n);
  }
  return s;
}

double sum_gyx_re(double x, double y, double d){
  return creall(sum_gyx(x, y, d));
}

double sum_gyx_im(double x, double y, double d){
  return cimagl(sum_gyx(x, y, d));
}
