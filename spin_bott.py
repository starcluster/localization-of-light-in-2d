import numpy as np
import matplotlib.pyplot as plt
import ll2D
import Gd
import ctypes
import sys
import generate_mk as gMk
from multiprocessing import Pool
from numpy.ctypeslib import ndpointer
import time
import scipy
import csv
from scipy.linalg import eig

import os

import tight_binding as tb
sys.path.append('../chapter-2-thesis/code')
import bott
import lattice_r as lr
import six_cell 

plt.rc("text.latex", preamble=r"\usepackage{amsmath}")

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

def get_sigma_bott(N):
    """N is the number of cluster"""
    return np.kron(np.eye(N),tb.get_sigma())

def make_projector(vl, vr):
    """Create a projector from left and right eigenvectors"""
    n = vl[0].shape[0]
    P = np.zeros((n, n), dtype="complex128")
    for i in range(len(vl)):
        vi = vl[i]
        vit = vr[i]
        P += np.outer(vi, vit.conj())/np.dot(vit.conj(), vi)

    return P

def get_p_sigma_p_bott(w,vl,vr,sigma,omega):
    i = np.searchsorted(w,omega)
    P = make_projector(vl[0:i],vr[0:i])
    return P @ sigma @ P

def sort_vectors(w,vl,vr):
    idx = w.argsort()
    vl = vl[:,idx]
    vr = vr[:,idx]
    w = w[idx]
    return w,vl,vr

def spin_bott(grid, vl, vr, w, threshold_psp, threshold_energy, N_cluster):
    Σ = get_sigma_bott(N_cluster)
    psp = get_p_sigma_p_bott(w,vl,vr,Σ,threshold_psp)
    w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))
    
    # x = np.linspace(0,w_psp.shape[0],w_psp.shape[0])
    # plt.scatter(x,np.array(w_psp).real,color="black")
    # plt.xlabel(r"$\textrm{Eigenvalues}$ $\lambda$--$P\sigma P$",fontsize=20)
    # plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    # plt.title(f"$t_2={t2}$",fontsize=20)
    # plt.savefig(f"SPECTRUM_psp_{t1=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    # plt.show()
    # idx = w_psp.argsort()

    bm = bott.bott(grid, vr_psp, w_psp , threshold_energy, pol=False, dagger=False, verbose=False)
    idx = (-w_psp).argsort()  
    bp = bott.bott(grid, vr_psp[:,idx], -w_psp[idx] , threshold_energy, pol=False, dagger=False, verbose=False)
    return (bp-bm)/2

def spin_all_bott(grid, vl, vr, w, threshold_psp, N_cluster):
    Σ = get_sigma_bott(N_cluster)
    psp = get_p_sigma_p_bott(w,vl,vr,Σ,threshold_psp)
    w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))
    
    bm = bott.all_bott(grid, vr_psp, w_psp, pol=False, dagger=False, verbose=False)
    idx = (-w_psp).argsort()  
    bp = bott.all_bott(grid, vr_psp[:,idx], -w_psp[idx], pol=False, dagger=False, verbose=False)
    bs = {}
    for w in w_psp:
        bs[w] = (bp[-w]-bm[w])/2
    return bs

def all_spin_bott(grid, vl, vr, w, threshold_energy, N_cluster):
    Σ = get_sigma_bott(N_cluster)
    bs = {}
    for threshold_psp in w:

        psp = get_p_sigma_p_bott(w,vl,vr,Σ,threshold_psp+0.01)
        w_psp, vl_psp, vr_psp = sort_vectors(*eig(psp, left=True, right=True))
    
        bm = bott.bott(grid, vr_psp, w_psp , threshold_energy, pol=False, dagger=False, verbose=False)
        idx = (-w_psp).argsort()  
        bp = bott.bott(grid, vr_psp[:,idx], -w_psp[idx] , threshold_energy, pol=False, dagger=False, verbose=False)
        bs[threshold_psp] = (bp-bm)/2
        
    return bs

def compute_evolution_dos():
    data = np.genfromtxt("./evolution_gap_6cell.csv", delimiter=',', dtype=float, encoding=None)
    ass = data[:, 0]
    bss = data[:, 1]
    bis = data[:, 2]
    distance = 2
    N = 400
    a = 1
    for s,bs,bi in zip(ass,bss,bis):
        g = lr.generate_hex_hex_stretch(n=N,a=a,s=s)
        n = g.shape[0]
        # lr.display_lattice(g[:n//2],color="blue")
        # lr.display_lattice(g[n//2:],color="red")
        # plt.show()

        # M = Gd.create_matrix_TE(g, 0, 0, 0.05 * 2 * np.pi)
        M = Gd.create_matrix_TE_plates(g, 0, 0, 0.05 * 2 * np.pi,distance)
        N_eff = M.shape[0]
        λ,ψ = np.linalg.eig(M)
        f = -np.real(λ)/2
        idx = f.argsort()
        f = f[idx]
        ψ = ψ[:,idx]
        ψ = [ψ[:,i] for i in range(N)]
        plt.hist(f,bins=N_eff//4,fill=False,histtype='step',color='black')
        plt.axvline(x=bi,color="red")
        plt.axvline(x=bs,color="red")
        plt.xlabel(r"$(\omega-\omega_0)/\Gamma_0$",fontsize=20)
        plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
        plt.title(f"$a={np.round(s,2)}$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.axis((-200,200,0,30))
        plt.savefig(f"./evolution_dos_6cell/{np.round(s,2)*100}.png",format="png",bbox_inches='tight',dpi=300)
        plt.clf()
        plt.cla()
            
        # plt.show()

if __name__ == "__main__":
    
    N = 400
    a = 1
    s = 1.02
    distance = 2

    g = lr.generate_hex_hex_stretch(n=N,a=a,s=s)
    g = lr.generate_hex_grid_stretch(N_cluster_side=2, a=s, φ=0, s=0, half=False)

    n = g.shape[0]
    lr.display_lattice(g[:n//2],color="blue")
    lr.display_lattice(g[n//2:],color="red")
    plt.show()

    # compute_evolution_dos()
    # exit()

    # M = Gd.create_matrix_TE(g, 0, 0, 0.05 * 2 * np.pi)
    M = Gd.create_matrix_TE_plates(g, 0, 0, 0.05 * 2 * np.pi,distance)

    # plt.imshow(np.imag(M-np.conj(M.T)))
    # plt.colorbar()
    # plt.show()
    N = M.shape[0]
    # λ,ψ = np.linalg.eig(M)
    λ,ψl,ψr = eig(M, left=True, right=True)
    f = -np.real(λ)/2
    idx = f.argsort()
    f = f[idx]
    ψl = ψl[:,idx]
    ψl = [ψl[:,i] for i in range(N)]

    ψr = ψr[:,idx]
    ψr = [ψr[:,i] for i in range(N)]
    plt.hist(f,bins=N//4)
    plt.show()

    sigma = np.kron(np.eye(n//6), six_cell.get_sigma())

    botts = []
    # omegas = np.linspace(10,11,1)
    # tes = np.linspace(-0.05,0,1)
    omegas = np.linspace(-10,20,30)
    tes = np.linspace(-1,1,10)
    
    xb = []
    yb = []
    print(f"{s=},{N=}")
    for omega in omegas:
        psp = get_p_sigma_p_bott(f,ψl,ψr,sigma,omega)
        λ_psp, ψl_psp, ψr_psp = sort_vectors(*eig(psp, left=True, right=True))
        idx = λ_psp.argsort()
        # x = np.linspace(0,λ_psp.shape[0],λ_psp.shape[0])
        # plt.scatter(x,np.array(λ_psp).real,color="black")
        # plt.xlabel(r"$\textrm{Eigenvalues}$ $\lambda$--$P\sigma P$",fontsize=20)
        # plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
        # plt.title(f"$a={a}$",fontsize=20)
        # plt.show()
        for threshold_energy in tes:
            # threshold_energy = -0.05
            bm = bott.bott(g, ψr_psp, λ_psp , threshold_energy, pol=True, dagger=False, verbose=False)
            idx = (-λ_psp).argsort()  
            bp = bott.bott(g, ψr_psp[:,idx], -λ_psp[idx] , threshold_energy, pol=True, dagger=False, verbose=False)
            print(bm,bp)
            b = (bp-bm)/2 
            print(omega, threshold_energy, b)
            xb.append(omega)
            yb.append(threshold_energy)
            botts.append(b)

    plt.scatter(xb, yb, c=botts)
    print(botts)
    plt.colorbar()
    plt.xlabel(r"$\omega$",fontsize=20)
    plt.ylabel(r"$\sigma$",fontsize=20)
    plt.title("",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig(f"spin_bott_map_{s=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    
