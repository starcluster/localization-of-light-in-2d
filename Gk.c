#include <math.h>
#include <complex.h>
#include <stdio.h>

double complex P(double complex x){
  return 1.-1./x+1./(x*x);
}

double complex Q(double complex x){
  return -1.+3./x-3./(x*x);
}

double complex G(int alpha, int beta, double r1, double r2){
  double r = sqrt(r1*r1+r2*r2);
  double complex x = 3./2.*cexp(I*r)/r*(P(I*r)+Q(I*r)*r1*r1/(r*r));
  double complex t = 3./2.*cexp(I*r)/r*(P(I*r)+Q(I*r)*r2*r2/(r*r));
  double complex y = 3./2.*cexp(I*r)/r*(Q(I*r)*r1*r2/(r*r));
  double complex z = 3./2.*cexp(I*r)/r*(Q(I*r)*r1*r2/(r*r));
  if ((alpha == 0) && (beta == 0)){
    return .5*(t + x - I*y  + I*z);
  } else if ((alpha == 1) && (beta == 1)){
    return .5*(t + x + I*y  - I*z);
  } else if ((alpha == 1) && (beta == 0)){
    return .5*(t - x + I*y  + I*z);
  }  else if ((alpha == 0) && (beta == 1)){
    return .5*(t - x - I*y  - I*z);
  } else {printf("ERROR IN Gk.c\n");return 0;}
}



double complex GAA(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB){
  double r = sqrt(r1*r1+r2*r2);
  double complex mat[4] = {I+2*delta_AB+2*delta_B,0,0,I+2*delta_AB-2*delta_B};

  if (r==0){
    return mat[alpha*2+beta];
  } else { return G(alpha, beta, r1, r2); }
}

double GAA_real(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB){
  return creall(GAA(alpha, beta, r1, r2, delta_B, delta_AB));
}

double GAA_imag(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB){
  return cimagl(GAA(alpha, beta, r1, r2, delta_B, delta_AB));
}

double complex GBB(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB){
  double r = sqrt(r1*r1+r2*r2);
  double complex mat[4] = {I-2*delta_AB+2*delta_B,0,0,I-2*delta_AB-2*delta_B};
  if (r==0){
    return mat[alpha*2+beta];
  } else { return G(alpha, beta, r1, r2); }
}

double GBB_real(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB){
  return creall(GBB(alpha, beta, r1, r2, delta_B, delta_AB));
}

double GBB_imag(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB){
  return cimagl(GBB(alpha, beta, r1, r2, delta_B, delta_AB));
}

//double complex S1

double complex S1(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB, double *lattice_x, double *lattice_y){
  printf("%f\n",lattice_x[1]);
  printf("%f\n",lattice_y[1]);
}

double S1_real(int alpha, int beta, double r1, double r2, double delta_B, double delta_AB, double *lattice_x, double *lattice_y){
  return creall(S1(alpha, beta, r1, r2, delta_B, delta_AB,lattice_x,lattice_y));
}
