#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include "Faddeeva.h"
#include "li.h"

#define area (3*sqrt(3))/2

double complex my_sqrt(double x){
  if (x < 0) return I*sqrt(-x); else return sqrt(x);
}


double complex erfi(double complex z){
  double r = 0.;
  return Faddeeva_erfi(z,r);
}

double complex Lambda(double *q, double k0){
  double qnorm2 = q[0]*q[0] + q[1]*q[1];
  return my_sqrt(k0*k0-qnorm2);
}

double complex chi(double a, double k0){
  return 1./2./M_PI/(k0*k0)*cexp(-a*a*k0*k0/2);
}

double complex I_f(double *q, double k0, double a){
  double complex L = Lambda(q,k0);
  return M_PI*chi(a,k0)/L*(-I+erfi(a*L/sqrt(2))) ;
}

double complex gstar(int alpha, int beta, double *q, double k0, double a){
  if (alpha==0 && beta==0){
    return (k0*k0-q[0]*q[0])*I_f(q, k0, a);
  } else if ((alpha==0 && beta==1) || (alpha==1 && beta==0)){
    return -q[0]*q[1]*I_f(q,k0,a);
  } else {
    return (k0*k0-q[1]*q[1])*I_f(q, k0, a);
  }
}

double complex gstar_plates(int alpha, int beta, double *q, double k0, double a,  double d){
  double qnorm2 = q[0]*q[0] + q[1]*q[1];
  double complex s = my_sqrt(k0*k0-qnorm2);
  double complex Iq =  I/s/cpow(k0,2)/(1+cexpf(-I*s*d)) +  I_f(q,k0,a);;
  double complex alert = I/s/cpow(k0,2)/(1+cexpf(-I*s*d));
  /* if (creall(alert)>1e-6 || cimagl(alert) > 1e-6)  */
  /*   printf("I/s/ ... %Lf %Lf\n", creall(alert), cimagl(alert)); */
  if (alpha==0 && beta==0){
    return (k0*k0-q[0]*q[0])*Iq;
  } else if ((alpha==0 && beta==1) || (alpha==1 && beta==0)){
    return -q[0]*q[1]*Iq;
  } else {
    return (k0*k0-q[1]*q[1])*Iq;
  }
}

double gstar_plates_real(int alpha, int beta, double *q, double k0, double a,  double d){
  return creall(gstar_plates(alpha,beta,q,k0,a,d));
}

double gstar_plates_imag(int alpha, int beta, double *q, double k0, double a,  double d){
  return cimagl(gstar_plates(alpha,beta,q,k0,a,d));
}

double complex G_star(int alpha, int beta, double k0, double a){
  if (alpha!=beta)
    return 0.;
  else
    return k0/6./M_PI*((erfi(k0*a/sqrt(2))-I)/exp(k0*k0*a*a/2) - (-0.5+k0*k0*a*a)/(sqrt(M_PI/2)*cpow(k0*a,3)));
}

double complex G_plates(int alpha, int beta, double k0,double d){
  if (alpha!=beta)
    return 0.;
  else{
    double complex z = cexpf(I*k0*d);
    double complex z2 = -clogf(1+z)/(k0*d)+I*cli2(-z)/cpowf((k0*d),2)+1/cpowf((k0*d),3)*cli3(-z);
    return z2*k0/2/M_PI;
  }
}

double complex S1(double *kb, int alpha, int beta, double a, double k0, double d, int N, int plates){
  double complex s = 0.;
  for(int i = -N; i < N+1; i++){
    for(int j =  -N; j < N+1; j++){
      double g[] = {2*M_PI/(sqrt(3))*i-kb[0],2*M_PI*2/3*(0.5*i+j)-kb[1]};
           if (plates)
	s += gstar_plates(alpha, beta, g, k0, a, d);
      else
	s += gstar(alpha, beta, g, k0, a);
    }
  }

  double complex A = 3*sqrt(3)/2;
   if (plates)
  return s/A - creall(G_star(alpha, beta, k0, a));
  else
    return s/A -  G_star(alpha, beta, k0, a);
}
 
double S1_real(double *kb, int alpha, int beta, double a, double k0, double d, int N, int plates){
  return crealf(S1(kb,alpha,beta,a,k0,d,N,plates));
}

double S1_imag(double *kb, int alpha, int beta, double a, double k0, double d, int N, int plates){
  return cimagf(S1(kb,alpha,beta,a,k0,d,N,plates));
}

double complex S2(double *kb, int alpha, int beta, double a, double k0, double d, int N,int plates){
  double complex s = 0.;
  for(int i = -N; i < N+1; i++){
    for(int j = -N; j < N+1; j++){
      double g[] = {2*M_PI/(sqrt(3))*i-kb[0],2*M_PI*2/3*(0.5*i+j)-kb[1]};
      if (plates)
	s += gstar_plates(alpha, beta, g, k0, a, d)*cexp(-I*g[1]);
      else
	s += gstar(alpha, beta, g, k0, a)*cexp(-I*g[1]);
    }
  }
  double complex A = 3*sqrt(3)/2;
  return s/A;
}

double S2_real(double *kb, int alpha, int beta, double a, double k0, double d,  int N, int plates){
  return crealf(S2(kb,alpha,beta,a,k0,d, N, plates));
}

double S2_imag(double *kb, int alpha, int beta, double a, double k0, double d,  int N, int plates){
  return cimagf(S2(kb,alpha,beta,a,k0,d, N, plates));
}

double complex S3(double *kb, int alpha, int beta, double a, double k0, double d, int N, int plates){
  double complex s = 0.;
  for(int i = -N; i < N+1; i++){
    for(int j = -N; j < N+1; j++){
      double g[] = {2*M_PI/(sqrt(3))*i-kb[0],2*M_PI*2/3*(0.5*i+j)-kb[1]};
      if (plates)
	s += gstar_plates(alpha, beta, g, k0, a, d)*cexp(I*g[1]);
      else
	s += gstar(alpha, beta, g, k0, a)*cexp(I*g[1]);
    }
  }
  double complex A = 3*sqrt(3)/2;
  return s/A;
}

void generateMk(double *kb, double a, double *M_re, double *M_im, double d, int N){
  double k0 = 2*M_PI*0.05;
  double complex tmp = 0.;
  int plates = 1;
  tmp = S1(kb, 0, 0, a, k0,d,N,plates);

  M_re[0] = creal(tmp);
  M_im[0] = cimag(tmp);
  tmp = S1(kb, 0, 1, a, k0,d,N,plates);
  M_re[1] = creal(tmp);
  M_im[1] = cimag(tmp);
  tmp = S2(kb, 0, 0, a, k0,d,N,plates);
  M_re[2] = creal(tmp);
  M_im[2] = cimag(tmp);
  tmp = S2(kb, 0, 1, a, k0,d,N,plates);
  M_re[3] = creal(tmp);
  M_im[3] = cimag(tmp);
  /****/
  tmp = S1(kb, 1, 0, a, k0,d,N,plates);
  M_re[4] = creal(tmp);
  M_im[4] = cimag(tmp);
  tmp = S1(kb, 1, 1, a, k0,d,N,plates);
  M_re[5] = creal(tmp);
  M_im[5] = cimag(tmp);
  tmp = S2(kb, 1, 0, a, k0,d,N,plates);
  M_re[6] = creal(tmp);
  M_im[6] = cimag(tmp);
  tmp = S2(kb, 1, 1, a, k0,d,N,plates);
  M_re[7] = creal(tmp);
  M_im[7] = cimag(tmp);
  /****/
  tmp = S3(kb, 0, 0, a, k0,d,N,plates);
  M_re[8] = creal(tmp);
  M_im[8] = cimag(tmp);
  tmp = S3(kb, 0, 1, a, k0,d,N,plates);
  M_re[9] = creal(tmp);
  M_im[9] = cimag(tmp);

  M_re[10] = M_re[0];
  M_im[10] = M_im[0];

  M_re[11] = M_re[1];
  M_im[11] = M_im[1];
  /****/
  tmp = S3(kb, 1, 0, a, k0,d,N,plates);
  M_re[12] = creal(tmp);
  M_im[12] = cimag(tmp);
  tmp = S3(kb, 1, 1, a, k0,d,N,plates);
  M_re[13] = creal(tmp);
  M_im[13] = cimag(tmp);

  M_re[14] = M_re[4];
  M_im[14] = M_im[4];

  M_re[15] = M_re[5];
  M_im[15] = M_im[5];
}

int main(void){
  return 0;
}
