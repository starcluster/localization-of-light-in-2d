import sys
import numpy as np
from datetime import datetime

__author__ = "Pierre Wulles"
__credits__ = ["Pierre Wulles", "Sergey Skipetrov"]
__license__ = "GNU GPL"
__maintainer__ = "Pierre Wulles"
__status__ = "Phd Student"
__email__ = "pierre.wulles@lpmmc.cnrs.fr"
__laboratory__ = "LPMMC -- Grenoble -- France"
__date__ = datetime.today().strftime("%Y-%m-%d")
__username__ = "Starcluster"
__git__ = "https://gitlab.com/starcluster"
__description__ = "Generates figs of the paper"
v = sys.version.split("\n")[0]
print(' """ ')
print("Author: " + __author__)
print("Credits: " + ", ".join(__credits__))
print("License: " + __license__)
print("Maintainer: " + __maintainer__)
print("Email: " + __email__)
print("Status: " + __status__)
print("Laboratory: " + __laboratory__)
print("Date: " + __date__)
print("Username: " + __username__)
print("Description: " + __description__)
print("Repository git: " + __git__)
print(f"Python {v}")
print("NumPy: {0:s}".format(np.version.version))
print(' """ ')
