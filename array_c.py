ghh = np.array([0.1, 0.8])
libc.S1_real.restype = ctypes.c_double
libc.S1_real.argtypes = (
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    ctypes.c_double,
    np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags="C_CONTIGUOUS"),
    np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags="C_CONTIGUOUS"),
)

libc.S1_real(
    0,
    0,
    0,
    0,
    0,
    0,
    np.ascontiguousarray(lattice[0:-1, 0]),
    np.ascontiguousarray(lattice[0:-1, 1]),
)
# print(lattice[0:-1,0])
delta_B, delta_AB = 12, 0
print(GAA(0, 1, lattice[0]))
print(
    libc.GAA_real(0, 1, 0, 0, delta_B, delta_AB)
    + 1j * libc.GAA_imag(0, 1, 0, 0, delta_B, delta_AB)
)
