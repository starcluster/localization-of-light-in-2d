import numpy as np
import matplotlib.pyplot as plt
import ll2D
import Gd
import generate_lattice as gl
import ctypes
import sys
import generate_mk as gMk
from multiprocessing import Pool
from numpy.ctypeslib import ndpointer
import time
import scipy
import csv
from scipy.linalg import eig
import bott2
import os
import seaborn as sns
import tex

tex.useTex()


def hist_stretch_disorder(N_grid, a, s, d, k0, phi):
    g = gl.generate_hex_hex_stretch(N_grid, a, s, phi)
    M = Gd.create_matrix_TE_plates(g, delta_B, delta_AB, k0, d)
    w, vl, vr = eig(M, left=True, right=True)
    N = M.shape[0]
    idx = w.argsort()[::-1]
    w = w[idx]
    vl = vl[:, idx]
    vr = vr[:, idx]

    N_sites = np.size(g, 0)
    frequencies = -np.real(w) / 2
    gammas = np.imag(w)

    plt.figure(figsize=(16, 12))
    plt.axis((-100, 100, -3, 70))
    plt.xticks(fontsize=40)
    plt.yticks(fontsize=40)
    plt.xlabel(r" $(\omega_n-\omega_0)/2$", fontsize=40)
    plt.ylabel(r"$\Gamma_n$", fontsize=40)
    plt.scatter(frequencies, gammas, vmin=0, vmax=1)
    plt.text(0, 50, "$s = " + str(np.round(s, 2)) + "$", fontsize=40)
    plt.savefig("eig_hex_hex.png")

    sns.set_style("white")
    sns.color_palette("rocket", as_cmap=True)
    tex.useTex()
    plt.figure(figsize=(16, 12))
    plt.axis((-80, 80, 0, 30))
    plt.text(0, 25, "$s = " + str(np.round(s, 2)) + "$", fontsize=30)
    plt.text(0, 23, "$\phi = " + str(np.round(phi, 2)) + "$", fontsize=30)
    plt.xticks(fontsize=30)
    plt.yticks(fontsize=30)
    sns.histplot(data=frequencies, binwidth=3, color="firebrick", element="step")
    plt.ylabel(r"$\textrm{Count}$", fontsize=30)
    plt.xlabel(r" $(\omega_n-\omega_0)/2$", fontsize=30)
    # plt.savefig(f"disorder_wuhu/s={np.round(s,2)}_phi={np.round(phi,2)}",format="pdf")
    plt.savefig(
        f"disorder_wuhu/s={np.round(s,2)}_phi={np.round(phi,2)}.png", format="png"
    )
    plt.clf()
    plt.cla()


if __name__ == "__main__":
    N_grid = 7
    a = 1
    s = 0.92
    d = 1
    delta_B = 0
    delta_AB = 0
    k0 = 2 * np.pi * 0.05 / a
    phi = 0
    for phi in [0 + i / 100 for i in range(50)]:
        hist_stretch_disorder(N_grid, a, s, d, k0, phi)
