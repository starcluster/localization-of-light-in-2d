#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <math.h>

void VXVY(double *x, double *y, double *eigvec_re, double *eigvec_im, double *Vx_re, double *Vx_im, double *Vy_re, double *Vy_im, double Lx, double Ly, int N);

void VXVY(double *x, double *y, double *eigvec_re, double *eigvec_im, double *Vx_re, double *Vx_im, double *Vy_re, double *Vy_im, double Lx, double Ly, int N)
{
  double complex eigvec_c[2*N][2*N];
  for (int m =0; m <  2*N; m++){
    for (int n =0;n <  2*N; n++ ){
	  double complex c = eigvec_re[m*2*N + n] + I*eigvec_im[m*2*N + n];
	  eigvec_c[n][m] =  c;
    }
  }
  double complex coeff_x = 0.;
  double complex coeff_y = 0.;
  double complex PIX = 2*M_PI/Lx*I;
  double complex PIY = 2*M_PI/Ly*I;
  
  for (int m =0;  m <  2*N; m++){
    for (int n =0;  n <  2*N; n++){
      coeff_x = 0.;
      coeff_y = 0.;
      for (int alpha =0; alpha <  N; alpha++ ){
        double complex argexpx = PIX*x[alpha];
        double complex argexpy = PIY*y[alpha];
	double complex coeff = (conj(eigvec_c[m][2*alpha])*eigvec_c[n][2*alpha] + conj(eigvec_c[m][2*alpha+1])*eigvec_c[n][2*alpha+1]);
	coeff_x += coeff*cexp(argexpx);
	coeff_y += coeff*cexp(argexpy);
      }
      Vx_re[m*2*N + n] = creall(coeff_x);
      Vx_im[m*2*N + n] = cimagl(coeff_x);
      Vy_re[m*2*N + n] = creall(coeff_y);
      Vy_im[m*2*N + n] = cimagl(coeff_y);
    }
  }
}
