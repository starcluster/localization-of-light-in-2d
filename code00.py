#!/usr/bin/env python3

import sys
import ctypes
import numpy as np


DLL_NAME = "./dll00.{:s}".format("dll" if sys.platform[:3].lower() == "win" else "so")


def main(*argv):
    DOUBLE_Ptr = ctypes.POINTER(ctypes.c_double)
    DOUBLE_Ptr_Ptr = ctypes.POINTER(DOUBLE_Ptr)

    dll00 = ctypes.CDLL(DLL_NAME)
    dll00Func00 = dll00.dll00Func00
    dll00Func00.argtypes = [DOUBLE_Ptr_Ptr]
    dll00Func00.restype = ctypes.c_double

    dim0 = 10
    dim1 = 10
    np_arr_2d = np.empty([dim0, dim1], dtype=float)

    np_arr_2d[5][5] = 5.0
    print(np_arr_2d)

    # The "magic" happens in the following lines of code
    ct_arr = np.ctypeslib.as_ctypes(np_arr_2d)
    DOUBLE_Ptr_Arr = DOUBLE_Ptr * ct_arr._length_
    ct_ptr = ctypes.cast(
        DOUBLE_Ptr_Arr(*(ctypes.cast(row, DOUBLE_Ptr) for row in ct_arr)),
        DOUBLE_Ptr_Ptr,
    )
    res = dll00Func00(ct_ptr)

    print("\n{0:s} returned: {1:f}".format(dll00Func00.__name__, res))

    print(np_arr_2d)


if __name__ == "__main__":
    print(
        "Python {:s} {:03d}bit on {:s}\n".format(
            " ".join(item.strip() for item in sys.version.split("\n")),
            64 if sys.maxsize > 0x100000000 else 32,
            sys.platform,
        )
    )
    print("NumPy: {0:s}\n".format(np.version.version))
    rc = main(*sys.argv[1:])
    print("\nDone.")
    sys.exit(rc)
