<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  .

  <\with|font-base-size|8>
    <doc-data|<doc-title|Fonction de Green de la 3D vers la
    2D>|<doc-subtitle|Calcul du champ EM dans un r�seau de r�sonnateurs
    sub-longueur d'onde>>

    <section|Introduction>

    On souhaite ici calculer la propagation d'une onde �lectromagn�tique dans
    un r�seau de r�sonnateurs sub-longueur d'onde (cela pourrait par exemple
    �tre la propagation de photons dans un r�seau d'atomes) en partant du cas
    3D d'une onde se propageant entre deux plaques, en supposant la distance
    entre les deux plaques infiniment faibles on aimerait retomber sur la
    th�orie d�crite pour un champ en 2D.

    <section|Cas scalaire>

    Des �quations de Maxwell on tire l'�quation suivante sur l'op�rateur de
    Green <math|G>:

    <\equation*>
      \<nabla\><rsup|2>G<around*|(|r,r<rprime|'>|)>+k<rsup|2>G<around*|(|r,r<rprime|'>|)>=\<delta\><around*|(|r-r<rprime|'>|)>
    </equation*>

    Dans le cas 3D on trouve:

    <\equation*>
      G<rsub|3D><around*|(|r,r<rprime|'>|)>=-<frac|\<mathe\><rsup|i*k*\|r-r<rprime|'><around*|\|||\<nobracket\>>>|4\<pi\>*\|r-r<rprime|'><around*|\|||\<nobracket\>>>
    </equation*>

    Les plaques imposent les conditions aux bords:

    <\equation*>
      <frac|\<partial\>G<around*|(|r,r<rprime|'>|)>|\<partial\>z><around*|\||<rsub|plaques>=0|\<nobracket\>>
    </equation*>

    On a alors que:

    <\equation*>
      G<around*|(|r,r<rprime|'>|)>=<big|sum><rsup|\<infty\>><rsub|n=-\<infty\>>G<rsub|3D><around*|(|r,r<rprime|'>=<around*|{|0,0,n*d|}>|)>=-<big|sum><rsup|\<infty\>><rsub|n=-\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>>|4\<pi\><sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>>
    </equation*>

    or ici on travaille dans le cas <math|d\<lll\>1> on peut donc se
    permettre de passer � l'int�grale en posant une variable infinit�simale
    <math|z=n*d>

    <\equation*>
      G<around*|(|r,r<rprime|'>|)>=-<frac|1|d><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|4\<pi\><sqrt|\<rho\><rsup|2>+z<rsup|2>>>\<mathd\>z<long-arrow|\<rubber-rightarrow\>||u=k*z>-<frac|1|d><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|k<rsup|2>\<rho\><rsup|2>+u<rsup|2>>>|4\<pi\><sqrt|k<rsup|2>\<rho\><rsup|2>+u<rsup|2>>>\<mathd\>u
    </equation*>

    cette int�grale est connue dans les tables:

    <math|<\text>
      <\equation*>
        G<around*|(|r,r<rprime|'>|)>=-<frac|1|4\<pi\>d><around*|(|i\<pi\>H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>|)>
      </equation*>
    </text>>

    o� <math|H<rsub|0><rsup|<around*|(|1|)>>> repr�sente la fonction de
    Hankel de premi�re esp�ce. On retrouve bien la fonction de Green 2D.

    <section|Cas vectoriel>

    Cette fois ci l'�quation issue des �quations de Maxwell s'�crit:

    <\equation*>
      \<nabla\>\<times\>\<nabla\>\<times\>G<around*|(|r,r<rprime|'>|)>-<frac|\<omega\><rsup|2>|c<rsup|2>>G<around*|(|r,r<rprime|'>|)>=\<delta\><around*|(|r,r<rprime|'>|)>
    </equation*>

    Et la r�solution dans le cas 3D donne:

    <\equation*>
      G<around*|(|r,r<rprime|'>|)>=-<frac|3|2><frac|\<mathe\><rsup|i*k*r>|r><around*|(|P<around*|(|i*k*r|)>+Q<around*|(|i*k*r|)><frac|r\<otimes\>r|r<rsup|2>>|)>
    </equation*>

    avec:

    <\itemize>
      <item><math|P<around*|(|x|)>=1-1/x+1/x<rsup|2>>

      <item><math|Q<around*|(|x|)>=-1+3/x-3/x<rsup|2>>
    </itemize>

    <subsection|Calcul de <math|G<rsup|Z*Z>>>

    En reprenant la d�marche de la section pr�c�dente, on aboutit sur le
    calcul de l'int�grale pour la composante <math|G<rsup|Z*Z>>:

    <\equation*>
      G<rsup|<rsup|Z*Z>><around*|(|\<rho\>|)>=-<frac|3|2><frac|1|d><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|<sqrt|\<rho\><rsup|2>+z<rsup|2>>><around*|(|1-<frac|1|i*k<sqrt|\<rho\><rsup|2>+z<rsup|2>>*>-<frac|1|k<rsup|2><around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>+<around*|(|-1+<frac|3|i*k<sqrt|\<rho\><rsup|2>+z<rsup|2>>*>-<frac|3|k<rsup|2><around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>|)><frac|z<rsup|2>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>|)>\<mathd\>z
    </equation*>

    Que l'on peut couper en plusieurs int�grales toutes convergentes:

    <\eqnarray*>
      <tformat|<table|<row|<cell|-<frac|2d|3>G<rsup|<rsup|Z*Z>><around*|(|\<rho\>|)>>|<cell|=>|<cell|<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|<sqrt|\<rho\><rsup|2>+z<rsup|2>>>\<mathd\>z>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>\<mathd\>z>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|k<rsup|2><around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|3/2>>\<mathd\>z>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>z<rsup|2>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|3/2>>\<mathd\>z>>|<row|<cell|>|<cell|>|<cell|+3<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k><frac|z<rsup|2>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|3/2>>\<mathd\>z>>|<row|<cell|>|<cell|>|<cell|-3<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|k<rsup|2><around*|(|\<rho\><rsup|2>+z<rsup|2>|)>><frac|z<rsup|2>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|>>\<mathd\>z>>>>
    </eqnarray*>

    \;

    Que l'on peut r��crire sous la forme:

    <\eqnarray*>
      <tformat|<table|<row|<cell|-<frac|2d|3>G<rsup|<rsup|Z*Z>><around*|(|\<rho\>|)>>|<cell|=>|<cell|<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|<sqrt|\<rho\><rsup|2>+z<rsup|2>>>\<mathd\>z<eq-number>>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>\<mathd\>z<eq-number>>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|3/2>><around*|(|<frac|1|k<rsup|2>>+z<rsup|2>+<frac|3z<rsup|2>|i*k>|)>\<mathd\>z<eq-number>>>|<row|<cell|>|<cell|>|<cell|-3<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|k<rsup|2><around*|(|\<rho\><rsup|2>+z<rsup|2>|)>><frac|z<rsup|2>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|>>\<mathd\>z<eq-number>>>>>
    </eqnarray*>

    On a donc plusieurs int�grales � calculer, proc�dons par �tape: nous
    avons d�j� calcul� la premi�re int�grale dans la premi�re partie.
    Attaquons nous alors au calcul de la seconde int�grale par int�gration
    dans le plan complexe:

    <\equation*>
      <big|int><rsub|\<Gamma\><rsup|>><rsup|><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>\<mathd\>z=<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>\<mathd\>z+<big|int><rsub|i\<Gamma\><around*|(|0,R|)><rsup|>><rsup|><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>\<mathd\>z
    </equation*>

    On factorise le d�nominateur en <math|<around*|(|z+i\<rho\>|)><around*|(|z-i\<rho\>|)>>
    puis en appliquant le lemme de Cauchy et le th�or�me des r�sidus:

    <\equation*>
      <big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|i*k<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>>\<mathd\>z=2\<pi\>i*Res<around*|(|i\<rho\>|)>=2\<pi\>i<frac|1|i\<rho\>+i\<rho\>><frac|1|i*k>=<frac|\<pi\>|i\<rho\>*k>
    </equation*>

    \;

    Pour la troisi�me int�grale:

    <\equation*>
      <big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|3/2>><around*|(|<frac|1|k<rsup|2>>+z<rsup|2>+<frac|3z<rsup|2>|i*k>|)>\<mathd\>z
    </equation*>

    On commence par calculer le premier terme en effectuant le changement de
    variable <math|sh<around*|(|x|)>=z/\<rho\>>:

    <\equation*>
      <big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|<around*|(|\<rho\><rsup|2>+z<rsup|2>|)><rsup|3/2>><frac|1|k<rsup|2>>\<mathd\>z=<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>|<around*|(|\<rho\>
      ch<around*|(|x|)>|)><rsup|3>><frac|1|k<rsup|2>>
      ch<around*|(|x|)>\<rho\>\<mathd\>x=<frac|1|k<rsup|2>><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>|<around*|(|\<rho\>
      ch<around*|(|x|)>|)><rsup|2>> \<mathd\>x
    </equation*>

    On effectue maintenant une int�gration par parties (possible ?) et
    quelques bidouilles:

    <\eqnarray*>
      <tformat|<table|<row|<cell|<frac|1|i*k*\<rho\>><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>|<around*|(|\<rho\>
      ch<around*|(|x|)>|)><rsup|2>> \<mathd\>x>|<cell|=>|<cell|<around*|[|\<mathe\><rsup|i*k**\<rho\>
      ch<around*|(|x|)>>th<around*|(|x|)>|]><rsup|+\<infty\>><rsub|-\<infty\>>-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>th<around*|(|x|)>sh<around*|(|x|)>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>
      \<mathd\>x>>|<row|<cell|>|<cell|=>|<cell|C-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|sh<around*|(|x|)><rsup|2>|ch<around*|(|x|)>>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>
      \<mathd\>x>>|<row|<cell|>|<cell|=>|<cell|C-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|ch<around*|(|x|)><rsup|2>-1|ch<around*|(|x|)>>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>
      \<mathd\>x>>|<row|<cell|>|<cell|=>|<cell|C-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|-1|ch<around*|(|x|)>>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>
      \<mathd\>x+<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>ch<around*|(|x|)>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>
      \<mathd\>x>>|<row|<cell|>|<cell|=>|<cell|C+<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>ch<around*|(|x|)><rsup|-1>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>>
      \<mathd\>x+<frac|\<partial\>|\<partial\>y><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>y>
      \<mathd\>x>>>>
    </eqnarray*>

    D'apr�s la Table d'int�grales de <strong|Gradshteyn-Ryzhik> (p966):

    <\equation*>
      H<rsub|\<nu\>><rsup|<around*|(|1|)>><around*|(|z|)>=<frac|-2i*\<mathe\><rsup|-i\<nu\>\<pi\>><around*|(|<frac|z|2>|)><rsup|\<nu\>>|<sqrt|\<pi\>>\<Gamma\><around*|(|\<nu\>+<frac|1|2>|)>><big|int><rsub|0><rsup|+\<infty\>>\<mathe\><rsup|i*z*ch
      t> sh<around*|(|t|)><rsup|2\<nu\>>\<mathd\>t
    </equation*>

    Donc ici:

    <\equation*>
      <frac|\<partial\>|\<partial\>y><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>\<mathe\><rsup|i*k**\<rho\>ch<around*|(|x|)>y>
      \<mathd\>x=<sqrt|\<pi\>>\<Gamma\><around*|(|<frac|1|2>|)>2<frac|\<partial\>|\<partial\>y>H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>y|)>=-2\<pi\>k\<rho\>H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>y|)>
    </equation*>

    En �valuant en <math|y=1:>

    <\equation*>
      -2\<pi\>k\<rho\>H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>
    </equation*>

    Que l'on peut r�exprimer � l'aide de la formule de r�currence:

    <\equation*>
      H<rsub|\<nu\>-1><around*|(|x|)>+H<rsub|\<nu\>+1><around*|(|x|)>=<frac|2\<nu\>|x>H<rsub|\<nu\>><around*|(|x|)>
    </equation*>

    en:

    <\equation*>
      -2\<pi\>k\<rho\>H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>=-2\<pi\>k\<rho\><around*|(|H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>-H<rsub|2><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>|)><frac|k\<rho\>|2>=\<pi\><around*|(|k\<rho\>|)><rsup|2><around*|(|H<rsub|2><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>-H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>|)>
    </equation*>

    Mais comment calculer l'autre moiti� ?\ 

    Comment caluler la derni�re int�grale ? Le passage par le plan complexe
    ne donne rien non plus.

    � ce stade l'utilisation d'un int�grateur num�rique permet de remarquer
    que seul le premier membre de l'int�gral � une valeur non nulle. On tente
    alors une autre approche pour le calcul de l'int�grale totale: l'exprimer
    comme une d�riv�e de fonction bien choisie.

    Commen�ons par le changement de variable <math|u=k*z>:

    <\eqnarray*>
      <tformat|<table|<row|<cell|-<frac|2d|3>G<rsup|<rsup|Z*Z>><around*|(|\<rho\>|)>>|<cell|=>|<cell|<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|i*<around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)>>\<mathd\>u>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)><rsup|3/2>>\<mathd\>u>>|<row|<cell|>|<cell|>|<cell|-<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>u<rsup|2>|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)><rsup|3/2>>\<mathd\>u>>|<row|<cell|>|<cell|>|<cell|+3<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|i*k><frac|u<rsup|2>|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)><rsup|3/2>>\<mathd\>u>>|<row|<cell|>|<cell|>|<cell|-3<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|k<rsup|><around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)>><frac|u<rsup|2>|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)><rsup|>>\<mathd\>u>>>>
    </eqnarray*>

    On remarque ensuite que:

    <\equation*>
      <frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>><around*|(|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|)>=<frac|i*k\<rho\>\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>
    </equation*>

    Donc:

    <\equation*>
      <frac|1|i*k\<rho\>><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|i*k\<rho\>\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u=<frac|1|i*k\<rho\>><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>>\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u
    </equation*>

    Puis par interversion d�riv�es-int�grales:

    <\equation*>
      <frac|1|i*k\<rho\>><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>>\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u=<frac|1|i*k\<rho\>><frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>><big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u=<frac|-i|*k\<rho\>><frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>><around*|(|<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>k\<rho\>\<mathe\><rsup|i**k\<rho\>
      ch<around*|(|t|)>>ch<around*|(|t|)>\<mathd\>t|)>
    </equation*>

    Or:

    <\equation*>
      <big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>\<mathe\><rsup|i**k\<rho\>
      ch<around*|(|t|)>>ch<around*|(|t|)>\<mathd\>t=<frac|\<mathe\><rsup|<frac|\<pi\>i|2>>\<pi\>i|4>
      H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>
    </equation*>

    Donc:

    <\equation*>
      <with|font-base-size|9|<frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>><around*|(|<big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>>k\<rho\>\<mathe\><rsup|i**k\<rho\>
      ch<around*|(|t|)>>ch<around*|(|t|)>\<mathd\>t|)>=<frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>><around*|(|<frac|\<mathe\><rsup|<frac|\<pi\>i|2>>\<pi\>i|4>
      H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>k\<rho\>|)>=<frac|\<pi\>i<rsup|2>|4><around*|(|H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>+<frac|1|2><around*|(|H<rsub|0><rsup|<around*|(|1|)>>-H<rsub|2><rsup|<around*|(|1|)>>|)>k\<rho\>|)>>
    </equation*>

    Et en utilisant l'autre formule de r�currence sur <math|H>:

    <\equation*>
      =-<frac|\<pi\>|4><around*|(|<frac|1|2><around*|(|H<rsub|0><rsup|<around*|(|1|)>>+<neg|H<rsub|2><rsup|<around*|(|1|)>>>|)>k\<rho\>+<frac|1|2><around*|(|H<rsub|0><rsup|<around*|(|1|)>>-<neg|H<rsub|2><rsup|<around*|(|1|)>>>|)>k\<rho\>|)>=-<frac|\<pi\>|4>H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>k\<rho\>
    </equation*>

    Finalement:

    <\equation*>
      <big|int><rsub|-\<infty\><rsup|>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u=<frac|i\<pi\>|4>H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>=I<around*|(|k\<rho\>|)>
    </equation*>

    On retrouve bien le r�sultat calcul� pr�c�demment. En appliquant une
    m�thode similaire on va calculer les autres int�grales, d'abord:

    <\equation*>
      <with|font-base-size|8|<frac|\<partial\>|\<partial\><around*|(|k\<rho\>|)>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>=\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|<frac|i*k\<rho\>|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>-<frac|k\<rho\>|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>|)><rsup|3/2>>|)>=<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|<frac|i*k\<rho\>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|k\<rho\>|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>>
    </equation*>

    <\equation*>
      <with|font-base-size|9|<with|font-base-size|6|<frac|\<partial\><rsup|2>|\<partial\><around*|(|k\<rho\>|)><rsup|2>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>=<around*|(|-<frac|<around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|<frac|3|2>>>-<frac|3*i*<space|0.17em><around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|2>>+<frac|3*<space|0.17em><around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|<frac|5|2>>>+<frac|i*<space|0.17em>|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>>-<frac|1|<around*|(|<around*|(|k\<rho\>|)><rsup|><rsup|2>+z<rsup|2>|)><rsup|<frac|3|2>>>|)>>e<rsup|i*<space|0.17em><sqrt|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>>>>
    </equation*>

    En factorisant:

    <\equation*>
      =<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|-<frac|<around*|(|k\<rho\>|)><rsup|2>*|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>>-<frac|3*i*<space|0.17em><around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|3/2>>+<frac|3*<space|0.17em><around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|2>>+<frac|i*<space|0.17em>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|1|<around*|(|k\<rho\>|)><rsup|><rsup|2>+u<rsup|2><rsup|>>|)>
    </equation*>

    <\equation*>
      \;
    </equation*>

    Donc en sommant:

    <\equation*>
      <frac|\<partial\><rsup|2>|\<partial\><around*|(|k\<rho\>|)><rsup|2>>+<frac|1|k\<rho\>><frac|\<partial\><rsup|>|\<partial\><around*|(|k\<rho\>|)><rsup|>>=<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|-<frac|<around*|(|k\<rho\>|)><rsup|2>*|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>>-<frac|3*i*<space|0.17em><around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|3/2>>+<frac|3*<space|0.17em><around*|(|k\<rho\>|)><rsup|2>*|<around*|(|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>|)><rsup|2>>+<frac|i*<space|0.17em>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|1|<around*|(|k\<rho\>|)><rsup|><rsup|2>+z<rsup|2><rsup|>>+<frac|i*|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|1|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>
    </equation*>

    <\equation*>
      =<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|<frac|<around*|(|k\<rho\>|)><rsup|2>|<around*|(|k\<rho\>|)><rsup|2>+z<rsup|2>><around*|(|-<frac|3*i*<space|0.17em>*|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>+<frac|3*<space|0.17em>*|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2><rsup|>>-1|)>+<frac|2i*<space|0.17em>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|2|<around*|(|k\<rho\>|)><rsup|><rsup|2>+u<rsup|2><rsup|>>|)>
    </equation*>

    <\equation*>
      =<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|Q<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>-<frac|u<rsup|2>|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>+<frac|2i*<space|0.17em>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|2|<around*|(|k\<rho\>|)><rsup|><rsup|2>+u<rsup|2><rsup|>>|)>
    </equation*>

    <\equation*>
      =<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|-<frac|u<rsup|2>|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>-<frac|i*<space|0.17em>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>+<frac|1|<around*|(|k\<rho\>|)><rsup|><rsup|2>+u<rsup|2><rsup|>>-1|)>
    </equation*>

    <\equation*>
      =-<frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|<frac|u<rsup|2>|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>+P<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>|)>
    </equation*>

    En int�grant sur <math|\<bbb-R\>> la relation pr�c�dente:

    <\with|font-base-size|7>
      <\equation*>
        <big|int><rsub|-\<infty\>><rsup|+\<infty\>><around*|(|<frac|\<partial\><rsup|2>|\<partial\><around*|(|k\<rho\>|)><rsup|2>>+<frac|1|k\<rho\>><frac|\<partial\><rsup|>|\<partial\><around*|(|k\<rho\>|)><rsup|>>|)><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u=-<big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|<frac|u<rsup|2>|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>+P<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>|)>\<mathd\>u
      </equation*>
    </with>

    Par interversion d�riv�es-int�grales:

    <\equation*>
      <around*|(|<frac|\<partial\><rsup|2>|\<partial\><around*|(|k\<rho\>|)><rsup|2>>+<frac|1|k\<rho\>><frac|\<partial\><rsup|>|\<partial\><around*|(|k\<rho\>|)><rsup|>>|)><big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>\<mathd\>u=-G<rsup|Z*Z><around*|(|k\<rho\>|)>
    </equation*>

    <\equation*>
      <around*|(|<frac|\<partial\><rsup|2>|\<partial\><around*|(|k\<rho\>|)><rsup|2>>+<frac|1|k\<rho\>><frac|\<partial\><rsup|>|\<partial\><around*|(|k\<rho\>|)><rsup|>>|)>H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>=-G<rsup|Z*Z><around*|(|k\<rho\>|)><frac|4|i\<pi\>>
    </equation*>

    <\equation*>
      <frac|\<partial\><rsup|>|\<partial\><around*|(|k\<rho\>|)><rsup|>>H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>+<frac|1|k\<rho\>>H<rsub|1><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>=-G<rsup|Z*Z><around*|(|k\<rho\>|)><frac|4|i\<pi\>>
    </equation*>

    <\equation*>
      <frac|1|2><around*|(|H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>-<neg|H<rsub|2><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>>|)>+<frac|1|k\<rho\>><around*|(|H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>+<neg|H<rsub|2><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>>|)><frac|k\<rho\>|2>=-G<rsup|Z*Z><around*|(|k\<rho\>|)><frac|4|i\<pi\>>
    </equation*>

    <\equation*>
      H<rsub|0><rsup|<around*|(|1|)>><around*|(|k\<rho\>|)>=-G<rsup|Z*Z><around*|(|k\<rho\>|)><frac|4|i\<pi\>>
    </equation*>

    <\equation*>
      \;
    </equation*>

    \;

    <subsection|Calcul de <math|G<rsup|X*X>>>

    <\eqnarray*>
      <tformat|<table|<row|<cell|-<frac|2|3>G<rsup|<rsup|X*X>><around*|(|\<rho\>|)>>|<cell|=>|<cell|<big|sum><rsup|\<infty\>><rsub|n=-\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>>|4\<pi\><sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>><around*|(|P<around*|(|i*k*<sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>|)>+Q<around*|(|i*k*<sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>|)><frac|x<rsup|2>|r<rsup|2>>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|d><big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|4\<pi\><sqrt|\<rho\><rsup|2>+z<rsup|2>>><around*|(|P<around*|(|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>|)>+Q<around*|(|i*k*<sqrt|\<rho\><rsup|2>+z<rsup|2>>|)><frac|x<rsup|2>|r<rsup|2>>|)>\<mathd\>z<space|2em><around*|(|p=k\<rho\>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|d><big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|i**<sqrt|p<rsup|2>+u<rsup|2>>>|4\<pi\><sqrt|p<rsup|2>+u<rsup|2>>><around*|(|P<around*|(|i*<sqrt|p<rsup|2>+u<rsup|2>>|)>+Q<around*|(|i**<sqrt|p<rsup|2>+u<rsup|2>>|)><frac|x<rsup|2>|r<rsup|2>>|)>\<mathd\>u>>>>
    </eqnarray*>

    Le calcul des d�riv�es est similaire, au moment de faire appara�tre les
    polynomes on doit faire attention � bien faire appara�tre le facteur en
    <math|x<rsup|2>>:

    <\equation*>
      <frac|\<mathe\><rsup|i**<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>><around*|(|<frac|k<rsup|2><around*|(|x<rsup|2>+y<rsup|2>|)>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>|)>+<frac|2i*<space|0.17em>|<sqrt|<around*|(|k\<rho\>|)><rsup|2>+u<rsup|2>>>-<frac|2|<around*|(|k\<rho\>|)><rsup|><rsup|2>+u<rsup|2><rsup|>>|)>
    </equation*>

    <\equation*>
      f<around*|(|p,u|)><around*|(|<frac|<around*|(|k*x|)><rsup|2>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>+<frac|<around*|(|k*y|)><rsup|2>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>+P<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>+Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>|)>
    </equation*>

    <\with|font-base-size|7>
      <with|font-base-size|9|<\eqnarray*>
        <tformat|<table|<row|<cell|f<around*|(|p,u|)><around*|(|<frac|<around*|(|k*x|)><rsup|2>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>+P<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>|)>>|<cell|+>|<cell|f<around*|(|p,u|)><around*|(|<frac|<around*|(|k*y|)><rsup|2>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>
        +Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>|)>>>>>
      </eqnarray*>>

      \;
    </with>

    \;

    <\equation*>
      <frac|\<mathe\><rsup|i**<sqrt|p<rsup|2>+u<rsup|2>>>|<sqrt|p<rsup|2>+u<rsup|2>>><around*|(|<frac|<around*|(|k*x|)><rsup|2>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>+<frac|<around*|(|k*y|)><rsup|2>|p<rsup|2>+u<rsup|2>>Q<around*|(|i<sqrt|p<rsup|2>+u<rsup|2>>|)>+<frac|2i*<space|0.17em>|<sqrt|p<rsup|2>+u<rsup|2>>>-<frac|2|p<rsup|><rsup|2>+u<rsup|2><rsup|>>|)>
    </equation*>
  </with>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
    <associate|auto-4|<tuple|3.1|2>>
    <associate|auto-5|<tuple|3.2|5>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Cas
      scalaire> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Cas
      vectoriel> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <with|par-left|<quote|1tab>|3.1<space|2spc>Calcul de
      <with|mode|<quote|math>|G<rsup|Z*Z>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|3.2<space|2spc>Calcul de
      <with|mode|<quote|math>|G<rsup|X*X>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>