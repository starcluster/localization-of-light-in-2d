<TeXmacs|2.1>

<style|<tuple|article|french>>

<\body>
  <doc-data|<doc-title|Calcul de <math|<wide|G|^><rsub|3D>>>>

  <section|Position du probl�me>

  On part de la formule pour <math|<wide|G|^><rsub|3D><rsup|<around*|(|0|)>>>
  et on suppose <math|r\<neq\>0:>

  <\equation*>
    <wide|G|^><rsub|3D><rsup|<around*|(|0|)>><around*|(|k*r|)>=-<frac|\<mathe\><rsup|i*k*r>|4\<pi\>r><around*|[|P<around*|(|i*k*r|)><with|font|Bbb|1>+Q<around*|(|i*k*r|)><frac|<wide|r|\<wide-varrightarrow\>>\<otimes\><wide|r|\<wide-varrightarrow\>>|r<rsup|2>>|]>
  </equation*>

  o�

  <\equation*>
    P<around*|(|x|)>=1-<frac|1|x>+<frac|1|x<rsup|2>> <infix-and>
    Q<around*|(|x|)>=-1+<frac|3|x>-<frac|3|x<rsup|2>>
  </equation*>

  Puis on calcule pour <math|z=n*d>, <math|r<rsub|n>=<sqrt|x<rsup|2>+y<rsup|2>+<around*|(|n*d|)><rsup|2>>=<rsup|>><math|<sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>>
  (m�thode des images):

  <\equation*>
    <wide|G<rsub|>|^><rsub|3D><rsub|><around*|(|k*r|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>-<frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|><with|font|Bbb|><with|font|Bbb|1>+Q<around*|(|i*k*r<rsub|n>|)><frac|<wide|r|\<wide-varrightarrow\>>\<otimes\><wide|r|\<wide-varrightarrow\>>|r<rsub|n><rsup|2>>|]>
  </equation*>

  La matrice pour les <math|N> atomes est constitu�s des <math|N> matrices
  blocs <math|3\<times\>3>:

  <\equation*>
    <wide|G<rsub|>|^><rsub|3N\<times\>*3N><rsub|><around*|(|k*r|)>=<matrix|<tformat|<table|<row|<cell|<wide|G<rsub|>|^><rsub|3D><rsub|><around*|(|k*r<rsub|1,1>|)>>|<cell|\<cdots\>>|<cell|<wide|G<rsub|>|^><rsub|3D><rsub|><around*|(|k*r<rsub|1,N>|)>>>|<row|<cell|\<vdots\>>|<cell|\<ddots\>>|<cell|\<vdots\>>>|<row|<cell|<wide|G<rsub|>|^><rsub|3D><rsub|><around*|(|k*r<rsub|N,1>|)>>|<cell|\<cdots\>>|<cell|<wide|G<rsub|>|^><rsub|3D><rsub|><around*|(|k*r<rsub|N,N>|)>>>>>>
  </equation*>

  <section|Calcul des termes de couplage longitudinal-transverse>

  On montre dans cette section que ces termes sont nuls, c'est-�-dire qu'il
  n'y a pas de couplages entre les excitations transverses et longitudinales.
  On commence par �crire les sommes pour <math|X*Z>:

  <\equation*>
    <wide|G<rsub|>|^><rsub|X*Z><rsub|><around*|(|k*r|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>-<frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|Q<around*|(|i*k*r<rsub|n>|)><frac|n*d*x|r<rsub|n><rsup|2>>|]>=<wide|G<rsub|>|^><rsub|Z*X><rsub|><around*|(|k*r|)>
  </equation*>

  Le terme pour <math|n=0> vaut z�ro et par opposition des signes on peut
  �liminer le reste de la somme:

  <\equation*>
    <big|sum><rsub|n=1><rsup|+\<infty\>>-<frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|Q<around*|(|i*k*r<rsub|n>|)><frac|n*d*x|r<rsub|n><rsup|2>>|]>=-<big|sum><rsub|n=-1><rsup|-\<infty\>>-<frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|Q<around*|(|i*k*r<rsub|n>|)><frac|n*d*x|r<rsub|n><rsup|2>>|]>
  </equation*>

  Par un raisonnement analogue pour <math|Y*Z*> on obtient que:

  <\equation*>
    <wide|G<rsub|>|^><rsub|Y*Z><rsub|><around*|(|k*r|)>=<wide|G<rsub|>|^><rsub|Z*Y><rsub|><around*|(|k*r|)>=0
  </equation*>

  <section|Calcul du terme transverse>

  Les conditions de bord imposent:

  \;

  <\equation*>
    <around*|\<nobracket\>|<frac|\<partial\><wide|G|^><rsub|Z*Z><around*|(|k*r|)>|\<partial\>z>|\|><rsub|z=\<pm\>d/2>=0
  </equation*>

  Et:

  <\equation*>
    <wide|G|^><rsub|X*X><around*|(|k*r|)><around*|\||<rsub|z=\<pm\>d/2>|\<nobracket\>>=0
  </equation*>

  On a donc:

  \;

  <\equation*>
    <wide|G|^><rsub|Z*Z><around*|(|k*r|)>=<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>>-<frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|<around*|(|n*d|)><rsup|2>|r<rsub|n><rsup|2>>|]>
  </equation*>

  <section|Calcul des termes longitudinaux >

  <subsection|Termes diagonaux>

  Dans le cas des termes diagonaux on a une divergence de la partie r�elle
  car <math|\<rho\>=0>, on consid�re donc uniquement la partie imaginaire.

  \;

  <\equation*>
    <wide|G|^><rsub|X*X><around*|(|k*r|)>=-i*\<Im\><around*|(|<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|x<rsup|2>|r<rsub|n><rsup|2>>|]>|)>
  </equation*>

  <\equation*>
    <wide|G|^><rsub|Y*Y><around*|(|k*r|)>=-i*\<Im\><around*|(|<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|y<rsup|2>|r<rsub|n><rsup|2>>|]>|)>
  </equation*>

  On traite � part le cas <math|n=0> qui cr�e une divergence particuli�re,
  pour cela on d�finit <math|h> comme:

  <\equation*>
    h<around*|(|\<rho\>,z|)>=<frac|\<mathe\><rsup|i**<sqrt|\<rho\><rsup|2>+z<rsup|2>>>|*<sqrt|\<rho\><rsup|2>+z<rsup|2>>><frac|1|2<around*|(|\<rho\><rsup|2>+z<rsup|2>|)>><around*|(|2P*<around*|(|i***<sqrt|\<rho\><rsup|2>+z<rsup|2>>|)><around*|(|\<rho\><rsup|2>+z<rsup|2>|)>+Q*<around*|(|i**<sqrt|\<rho\><rsup|2>+z<rsup|2>>|)>\<rho\><rsup|2>|)>
  </equation*>

  En tracant partie r�elle et imaginaire on en d�duit la valeur limite de la
  partie imaginaire:

  <\big-figure|<image|hre.png|0.5par|||><image|h.png|0.5par|||>>
    Partie r�lle de <math|h> � gauche, partie imaginaire � droite.
  </big-figure>

  Donc:

  <\equation*>
    lim<rsub|\<rho\>\<rightarrow\>0> lim<rsub|z\<rightarrow\>0>\<Im\><around*|(|h<around*|(|\<rho\>,z|)>|)>=<frac|2|3>
  </equation*>

  Si on trace en fonction de <math|d> la partie imaginaire de
  <math|-G<rsub|X*X>>:

  <\big-figure|<image|diagIm_comp2.png|0.5par|||>>
    \;
  </big-figure>

  On en d�duit que la partie imaginaire de la somme suivante doit �tre nulle
  quand <math|d\<less\>\<pi\>>:

  <\equation*>
    *\<Im\><around*|(|<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|x<rsup|2>|r<rsub|n><rsup|2>>|]>|)>=0
  </equation*>

  Cela revient � montrer en isolant le terme <math|n=0>:

  <\equation*>
    2*\<Im\><around*|(|<big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|x<rsup|2>|r<rsub|n><rsup|2>>|]>|)>=-<frac|2|3>
  </equation*>

  En d�veloppant le produit on calcule la partie imaginaire:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<around*|(|cos<around*|(|k*r<rsub|n>|)>+i*sin<around*|(|k*r<rsub|n>|)>|)><around*|[|P<with|font|Bbb|>+Q<frac|x<rsup|2>|r<rsub|n><rsup|2>>|]>=>|<cell|cos<around*|(|k*r<rsub|n>|)>P+cos<around*|(|k*r<rsub|n>|)>Q<frac|x<rsup|2>|r<rsub|n><rsup|2>>+i*sin<around*|(|k*r<rsub|n>|)>P+i*sin<around*|(|k*r<rsub|n>|)>Q<frac|x<rsup|2>|r<rsub|n><rsup|2>>>>>>
  </eqnarray*>

  On calcule la partie imaginaire des 4 termes:

  <\enumerate-numeric>
    <item>

    <\equation*>
      \<Im\><around*|(|cos<around*|(|k*r<rsub|n>|)>P|)>=<frac|cos<around*|(|k*r<rsub|n>|)>|k*r<rsub|n>>
    </equation*>

    <item>

    <\equation*>
      \<Im\><around*|(|cos<around*|(|k*r<rsub|n>|)>Q<frac|x<rsup|2>|r<rsub|n><rsup|2>>|)>=-cos<around*|(|k*r<rsub|n>|)><frac|x<rsup|2>|r<rsub|n><rsup|2>><frac|1|k*r<rsub|n>>
    </equation*>

    <item>

    <\equation*>
      \<Im\><around*|(|i*sin<around*|(|k*r<rsub|n>|)>P|)>=*sin<around*|(|k*r<rsub|n>|)><around*|(|1-<frac|1|k<rsup|2>r<rsub|n><rsup|2>>|)>
    </equation*>

    <item>

    <\equation*>
      \<Im\><around*|(|i*sin<around*|(|k*r<rsub|n>|)>Q<frac|x<rsup|2>|r<rsub|n><rsup|2>>|)>=sin<around*|(|k*r<rsub|n>|)><frac|x<rsup|2>|r<rsub|n><rsup|2>><around*|(|-1+<frac|3|k<rsup|2>r<rsub|n><rsup|2>>|)>
    </equation*>
  </enumerate-numeric>

  Au total:

  <\equation*>
    <big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>|4\<pi\>r<rsub|n>><around*|[|<frac|cos<around*|(|k*r<rsub|n>|)>|k*r<rsub|n>><around*|(|1-<frac|x<rsup|2>|r<rsub|n><rsup|2>>|)>+*sin<around*|(|k*r<rsub|n>|)><around*|(|1-<frac|1|k<rsup|2>r<rsub|n><rsup|2>>+<frac|x<rsup|2>|r<rsub|n><rsup|2>><around*|(|-1+<frac|3|k<rsup|2>r<rsub|n><rsup|2>>|)>|)>|]>
  </equation*>

  \;

  <subsection|Termes anti-diagonaux>

  <\equation*>
    <wide|G|^><rsub|X*Y><around*|(|k*r|)>=<wide|G|^><rsub|**Y*X><around*|(|k*r|)>=-<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><around*|[|Q<around*|(|i*k*r<rsub|n>|)><frac|x*y|r<rsub|n><rsup|2>>|]>
  </equation*>

  Alors au total on a en consid�rant uniquement les composantes dans le plan:

  <\equation*>
    <wide|G|^><rsub|3D><rsup|\<perp\>><around*|(|k*r|)>=-<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><matrix|<tformat|<table|<row|<cell|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|x<rsup|2>|r<rsub|n><rsup|2>>>|<cell|Q<around*|(|i*k*r<rsub|n>|)><frac|x*y|r<rsub|n><rsup|2>>>>|<row|<cell|Q<around*|(|i*k*r<rsub|n>|)><frac|x*y|r<rsub|n><rsup|2>>>|<cell|P<around*|(|i*k*r<rsub|n>|)><with|font|Bbb|>+Q<around*|(|i*k*r<rsub|n>|)><frac|y<rsup|2>|r<rsub|n><rsup|2>>>>>>>
  </equation*>

  On peut passer en polarisation circulaire:

  <\equation*>
    d=<matrix|<tformat|<table|<row|<cell|<frac|1|<sqrt|2>>>|<cell|<frac|i|<sqrt|2>>>>|<row|<cell|-<frac|1|<sqrt|2>>>|<cell|<frac|i|<sqrt|2>>>>>>>
  </equation*>

  et:

  <\equation*>
    <wide|g|^><rsub|3D><rsup|\<perp\>><around*|(|k*r|)>=d<wide|G|^><rsub|3D><rsup|\<perp\>><around*|(|k*r|)>d<rsup|\<dagger\>>
  </equation*>

  Le r�sultat de ce produit matriciel donne apr�s simplification (on omet les
  arguments dans les expressions des fonctions <math|P> et <math|Q>):

  <\equation*>
    <wide|g|^><rsub|3D><rsup|\<perp\>><around*|(|k*r|)>=-<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><matrix|<tformat|<table|<row|<cell|<frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>|<cell|<frac|1|2r<rsub|n><rsup|2>>Q<around*|(|-i*x+y<rsup|>|)><rsup|2>>>|<row|<cell|<frac|1|2r<rsub|n><rsup|2>>Q<around*|(|i*x+y<rsup|>|)><rsup|2>>|<cell|<frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>>>>
  </equation*>

  Dans toute cette discussion on a consid�r� <math|r\<neq\>0>, il faut
  maintenant trait� ce cas, on s'int�resse donc au calcul de (on continue �
  consid�rer <math|d\<neq\>0>):

  <\equation*>
    -lim<rsub|\<rho\>\<rightarrow\>0><around*|(|<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i*k*r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>|)>
  </equation*>

  On va avoir une divergence sur la partie r�elle que l'on ne consid�re pas
  car on la cache dans le shift <math|\<omega\>-\<omega\><rsub|0>>.

  \;

  En remettant le facteur <math|*-i*k/4\<pi\>> on obtient le terme constant
  de la diagonale:

  <\equation*>
    <wide|g|^><rsup|\<perp\>><rsub|3N><around*|(|k*r|)>=<matrix|<tformat|<table|<row|<cell|-<frac|i*k|6\<pi\>>+\<cdots\>>|<cell|<wide|g|^><rsub|3D><rsup|\<perp\>><around*|(|k*r<rsub|1,2>|)>>|<cell|\<ldots\>
    >|<cell|<wide|g|^><rsub|3D><rsup|\<perp\>><around*|(|k*r<rsub|1,N>|)>>>|<row|<cell|<wide|g|^><rsub|3D><rsup|\<perp\>><around*|(|k*r<rsub|2,1>|)>>|<cell|\<ddots\>>|<cell|>|<cell|\<vdots\>>>|<row|<cell|\<vdots\>>|<cell|>|<cell|>|<cell|>>|<row|<cell|<wide|g|^><rsub|3D><rsup|\<perp\>><around*|(|k*r<rsub|N,1>|)>>|<cell|\<ldots\>>|<cell|>|<cell|-<frac|i*k|6\<pi\>>+\<cdots\>>>>>>
  </equation*>

  Calculons maintenant le reste de la somme que l'on regoupe par sym�trie, on
  enl�ve ici le <math|k> qui n'est qu'une constante de dimension:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<cwith|1|1|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><rsub|><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>|<cell|=>|<cell|<big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>|<row|<cell|>|<cell|+>|<cell|<big|sum><rsub|n=-1><rsup|-\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>|<row|<cell|>|<cell|+>|<cell|<big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|-n>\<mathe\><rsup|i**r<rsub|-n>>|4\<pi\>r<rsub|-n>><frac|1|2r<rsub|-n><rsup|2>><around*|(|2P*r<rsub|-n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>>>
  </eqnarray*>

  On peut sans probl�me intervertir somme et limite car on a convergence
  absolue sur la partie imaginaire:

  <\equation*>
    lim<rsub|\<rho\>\<rightarrow\>0><big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>=<big|sum><rsub|n=1><rsup|+\<infty\>>lim<rsub|\<rho\>\<rightarrow\>0><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**r<rsub|n>>|4\<pi\>r<rsub|n>><frac|1|r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>
  </equation*>

  En particulier:

  <\equation*>
    Q*\<rho\><rsup|2>=<around*|(|-1+<frac|3|i<sqrt|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>>+<frac|3|\<rho\><rsup|2>+<around*|(|n*d|)><rsup|2>>|)>\<rho\><rsup|2><long-arrow|\<rubber-equal\>||\<rho\>\<rightarrow\>0>0
  </equation*>

  Donc de la somme il reste, apr�s application de la limite:

  <\equation*>
    <frac|1|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**n*d>|n*d><frac|1|<around*|(|n*d|)><rsup|2>>P*<around*|(|i*n*d|)><around*|(|n*d|)><rsup|2>=<frac|1|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**n*d>|n*d>P*<around*|(|i*n*d|)>
  </equation*>

  On aboutit au calcul de 3 sommes distinctes:

  <\equation*>
    <frac|1|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>\<mathe\><rsup|i**n*d>|n*d>P*<around*|(|i*n*d|)>=<frac|1|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><frac|\<mathe\><rsup|i*<around*|(|*n*d+\<pi\>|)>>|n*d><around*|(|1-<frac|1|i*n*d>-<frac|1|<around*|(|n*d|)><rsup|2>>|)>
  </equation*>

  Introduisons la fonction de Bose:

  <\equation*>
    \<cal-B\><rsub|\<sigma\>><around*|(|z|)>=<big|sum><rsub|n=1><rsup|+\<infty\>><frac|z<rsup|n>|n<rsup|\<sigma\>>>
  </equation*>

  Qui se trouve �tre d�veloppable de la fa�on suivante:

  <\equation*>
    \<cal-B\><rsub|\<sigma\>><around*|(|z|)>=<frac|1|\<Gamma\><around*|(|\<sigma\>|)>><big|int><rsub|0><rsup|+\<infty\>><frac|t<rsup|\<sigma\>-1>|z<rsup|-1>\<mathe\><rsup|t>-1>\<mathd\>t<space|1em>\<sigma\>\<gtr\>1
  </equation*>

  Donc:

  <\equation*>
    <frac|1|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><frac|\<mathe\><rsup|i*<around*|(|*n*d+\<pi\>|)>>|n*d><around*|(|1-<frac|1|i*n*d>-<frac|1|<around*|(|n*d|)><rsup|2>>|)>=<frac|1|2\<pi\>><around*|(|<frac|1|d>\<cal-B\><rsub|1><around*|(|\<mathe\><rsup|i*<around*|(|**d+\<pi\>|)>>|)>+<frac|i|d<rsup|2>>\<cal-B\><rsub|2><around*|(|\<mathe\><rsup|i*<around*|(|**d+\<pi\>|)>>|)>-<frac|1|d<rsup|3>>\<cal-B\><rsub|3><around*|(|\<mathe\><rsup|i*<around*|(|**d+\<pi\>|)>>|)>|)>
  </equation*>

  La fonction de Bose en <math|\<sigma\>=1> n'admet pas de d�veloppement en
  int�grale on calcule donc la somme directement en introduisant la fonction
  enti�re: <with|color|red|erreur>\ 

  <\equation*>
    \<cal-B\><rsub|1><around*|(|\<mathe\><rsup|i*<around*|(|*n*d+n\<pi\>|)>>,t|)>=<big|sum><rsub|n=1><rsup|+\<infty\>><frac|\<mathe\><rsup|<around*|(|i***<around*|(|**d+\<pi\>|)>|)>>|n*><rsup|n>=d<big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|-1|)><rsup|n><around*|(|<big|int><rsub|0><rsup|1><around*|(|\<mathe\><rsup|x*i**d>|)><rsup|n>\<mathd\>x+<frac|1|n>|)>=d<big|int><rsub|0><rsup|1><frac|\<mathe\><rsup|i<around*|(|x*d+\<pi\>|)>>|1-\<mathe\><rsup|i<around*|(|x*d+\<pi\>|)>>>-d*ln2
  </equation*>

  On permute somme et int�grale:

  <\equation*>
    <big|int><rsub|0><rsup|t><big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|x\<mathe\><rsup|i**<around*|(|d+\<pi\>|)>>|)><rsup|n>\<mathd\>x=<big|int><rsub|0><rsup|t><frac|x\<mathe\><rsup|i**<around*|(|d+\<pi\>|)>>|1-x\<mathe\><rsup|i**<around*|(|d+\<pi\>|)>>>\<mathd\>x=-t+<big|int><rsup|t><rsub|0><frac|1|1-x\<mathe\><rsup|i**<around*|(|d+\<pi\>|)>>>\<mathd\>x
  </equation*>

  La deuxi�me int�grale se r��crit en:

  <\equation*>
    <big|int><rsub|0><rsup|t><frac|<around*|(|1-x\<mathe\><rsup|-i**<around*|(|d+\<pi\>|)>>|)>|<around*|(|1-x\<mathe\><rsup|i**<around*|(|d+\<pi\>|)>>|)><around*|(|1-x\<mathe\><rsup|-i**<around*|(|d+\<pi\>|)>>|)>>\<mathd\>x=<big|int><rsub|0><rsup|t><frac|<around*|(|1-x\<mathe\><rsup|-i**<around*|(|d+\<pi\>|)>>|)>|<around*|(|sin<around*|(|d|)><rsup|>|)><rsup|2>+<around*|(|x+cos<around*|(|d|)>|)><rsup|2>>\<mathd\>x
  </equation*>

  On peut en extraire la partie imaginaire, et on �value en <math|t=1:>

  <\equation*>
    \<Im\><around*|(|<big|sum><rsub|n=1><rsup|+\<infty\>><frac|\<mathe\><rsup|i*<around*|(|*n*d+n\<pi\>|)>>|n*d>|)>=<big|int><rsup|1><rsub|0><frac|x*sin<around*|(|d+\<pi\>|)>|<around*|(|sin<around*|(|d|)><rsup|>|)><rsup|2>+<around*|(|x+cos<around*|(|d|)>|)><rsup|2>>\<mathd\>x
  </equation*>

  Concernant les autres fonctions de Bose:

  <\equation*>
    \<cal-B\><rsub|2><around*|(|\<mathe\><rsup|i*<around*|(|**d+\<pi\>|)>>|)>=<big|int><rsub|0><rsup|+\<infty\>><frac|t|\<mathe\><rsup|-i*<around*|(|*n*d+\<pi\>|)>>\<mathe\><rsup|t>-1>\<mathd\>t=<big|int><rsub|0><rsup|+\<infty\>><frac|t<around*|(|\<mathe\><rsup|i*<around*|(|**d+\<pi\>|)>>\<mathe\><rsup|t>-1|)>|<around*|(|\<mathe\><rsup|i*<around*|(|*n*d+\<pi\>|)>>\<mathe\><rsup|t>-1|)><around*|(|\<mathe\><rsup|-i*<around*|(|*n*d+\<pi\>|)>>\<mathe\><rsup|t>-1|)>>\<mathd\>t
  </equation*>

  Ici on s'int�resse � la partie r�elle car le pr�facteur est un <math|i>:

  <\equation*>
    <big|int><rsub|0><rsup|+\<infty\>><frac|t<around*|(|cos<around*|(|d+\<pi\>|)>\<mathe\><rsup|t>-1|)>|\<mathe\><rsup|2t>-2\<mathe\><rsup|t>cos<around*|(|d+\<pi\>|)>+1>\<mathd\>t
  </equation*>

  Enfin:

  <\equation*>
    \<cal-B\><rsub|3><around*|(|\<mathe\><rsup|i*<around*|(|**d+\<pi\>|)>>|)>=<frac|1|2><big|int><rsub|0><rsup|+\<infty\>><frac|t<rsup|2>|\<mathe\><rsup|-i*<around*|(|*n*d+\<pi\>|)>>\<mathe\><rsup|t>-1>\<mathd\>t<long-arrow|\<rubber-rightarrow\>||\<Im\>><frac|1|2><big|int><rsub|0><rsup|+\<infty\>><frac|t<rsup|2><around*|(|sin<around*|(|d+\<pi\>|)>\<mathe\><rsup|t>|)>|\<mathe\><rsup|2t>-2\<mathe\><rsup|t>cos<around*|(|d+\<pi\>|)>+1>\<mathd\>t
  </equation*>

  On regroupe tout le monde:

  <\equation*>
    -\<Im\><around*|(|<frac|1|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><frac|\<mathe\><rsup|i*<around*|(|*n*d+\<pi\>|)>>|n*d><around*|(|1-<frac|1|i*n*d>-<frac|1|<around*|(|n*d|)><rsup|2>>|)>|)>=
  </equation*>

  <with|font-base-size|10|<\equation*>
    <block*|<tformat|<table|<row|<cell|<with|font-base-size|10|-<frac|1|2\<pi\>><around*|(|<big|int><rsup|1><rsub|0><frac|-x*sin<around*|(|d|)>|<around*|(|sin<around*|(|d|)><rsup|>|)><rsup|2>+<around*|(|x+cos<around*|(|d|)>|)><rsup|2>>\<mathd\>x+<frac|1|d<rsup|2>><big|int><rsub|0><rsup|+\<infty\>><frac|t<around*|(|cos<around*|(|d|)>\<mathe\><rsup|t>+1|)>|\<mathe\><rsup|2t>+2\<mathe\><rsup|t>cos<around*|(|d|)>+1>\<mathd\>t+<frac|1|2d<rsup|3>><big|int><rsub|0><rsup|+\<infty\>><frac|t<rsup|2>sin<around*|(|d|)>\<mathe\><rsup|t>|\<mathe\><rsup|2t>+2\<mathe\><rsup|t>cos<around*|(|d|)>+1>\<mathd\>t|)>>>>>>>
  </equation*>>

  On peut tracer cette fonction et observer les paliers:

  <\big-figure|<image|diagIm.png|0.8par|||>>
    \;
  </big-figure>

  <strong|Simulation num�rique>: pour plusieurs valeurs de <math|d> je tente
  des simulations num�riques, celles ci n'ont pas de sens physiques car les
  valeurs propres ont des parties imaginaires n�gatives.
</body>

<\initial>
  <\collection>
    <associate|page-even|15mm>
    <associate|page-medium|paper>
    <associate|page-odd|15mm>
    <associate|page-right|15mm>
    <associate|page-screen-left|5mm>
    <associate|page-screen-right|5mm>
    <associate|page-screen-top|15mm>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-3|<tuple|3|1>>
    <associate|auto-4|<tuple|4|2>>
    <associate|auto-5|<tuple|4.1|2>>
    <associate|auto-6|<tuple|1|2>>
    <associate|auto-7|<tuple|2|3>>
    <associate|auto-8|<tuple|4.2|4>>
    <associate|auto-9|<tuple|3|6>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Partie r�lle de <with|mode|<quote|math>|h> � gauche, partie
        imaginaire � droite.
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-9>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Position
      du probl�me> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Calcul
      des termes de couplage longitudinal-transverse>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Calcul
      du terme transverse> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Calcul
      des termes longitudinaux > <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Termes diagonaux
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|4.2<space|2spc>Termes anti-diagonaux
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>
    </associate>
  </collection>
</auxiliary>