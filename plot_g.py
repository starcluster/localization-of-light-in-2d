import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

g_scalar = pd.read_csv("g_scalar.dat")
scalar_nf = pd.read_csv("scalar_nf.dat")
vector_nf = pd.read_csv("vector_nf.dat")
g_vector = pd.read_csv("g_vector.dat")

cm = plt.cm.get_cmap("RdYlBu")

fig, axs = plt.subplots(2, 2)
# Upper-left

axs[0, 0].scatter(g_scalar["R"], g_scalar["g"], cmap="jet", marker=".")
axs[0, 0].scatter(g_vector["R"], g_vector["g"], cmap="jet", marker=".")
axs[0, 0].set_yscale("log")
# axs[0,0].set(ylabel="Without near-field")
# axs[0, 0].set_title('Scalar Kernel')
axs[0, 0].axis((0, 10, 1e-14, 1e2))

N = len(g_scalar["R"])
delta = np.zeros(N - 1)
lng = np.zeros(N - 1)
for i in range(N - 1):
    delta[i] = np.log(g_scalar["g"][i + 1]) - np.log(
        g_scalar["g"][i]
    )  # /(np.log(g_scalar["R"][i+1])-np.log(g_scalar["R"][i]))
    lng[i] = np.log(g_scalar["g"][i + 1])
print(delta)

axs[0, 1].scatter(lng, delta, cmap="jet", marker=".")
# axs[0,1].set_yscale('log')
# axs[0, 1].set_title('Vector Kernel')
im = axs[1, 1].scatter(
    vector_nf["x"],
    vector_nf["y"],
    c=vector_nf["ipr"],
    cmap="jet",
    marker=".",
    vmin=0,
    vmax=0.5,
)

# axs[1,0].set_yscale('log')
# axs[1, 0].set_title('Axis [1, 0]')

# axs[1,0].set(ylabel="With near-field")
axs[0, 1].axis((-20, 0, 2, -6))
axs[0, 1].tick_params(axis="y", which="both", labelleft=False)
axs[0, 1].yaxis.set_label_position("right")
axs[0, 1].set(ylabel=r"$\gamma_n$")
axs[1, 0].axis((-60, 60, -10, 10))
axs[1, 1].set_yscale("log")
axs[1, 1].yaxis.set_label_position("right")
axs[1, 1].tick_params(axis="y", which="both", labelleft=False)
axs[1, 1].set(ylabel=r"$\gamma_n$")
axs[1, 1].axis((-60, 60, 1e-14, 1e2))


fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax)


# axs[1, 1].set_title('Axis [1, 1]')

for ax in axs.flat:
    ax.set(xlabel=r"$\omega_n$")

# Hide x labels and tick labels for top plots and y ticks for right plots.
for ax in axs.flat:
    pass
# ax.label_outer()

plt.show()
