#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

// gcc -shared -Wall -lm G_images.c -o G_images.o

double complex P(double complex x){
  return 1.-1./x+1./(x*x);
}

double complex Q(double complex x){
  return -1.+3./x-3./(x*x);
}

double complex f(double  x, double y, double d, int n){
  double dbln = (double)n;
  double x2 = x*x, y2=y*y, dn2 = d*dbln*d*dbln;
  double complex f = cexp(I*csqrt(x2+y2+dn2))/csqrt(x2+y2+dn2)*(P(I*csqrt(x2+y2+dn2))+Q(I*csqrt(x2+y2+dn2))*y2/(x2+y2+dn2))*pow(-1,n);

  return f;
}

double complex f0(double  x, double y, double d, int n){
  double dbln = (double)n;
  double x2 = x*x, y2=y*y, dn2 = d*dbln*d*dbln;
  double complex f = cexp(I*csqrt(x2+y2+dn2))/csqrt(x2+y2+dn2)/M_PI;

  return f;
}

double complex sumf(double x, double y, double d){
  double complex s = 0+0*I;
  int N = 1000;
  for(int n = -N; n < N+1; n++){
    s += f(x,y,d,n);
  }
  return s;
}

double complex sumf0(double x, double y, double d){
  double complex s = 0+0*I;
  int N = 50000;
  if (x==0.1){
    printf("dernier terme im = %Lf\n", cimagl(f(x,y,d,N-1)));
      printf("dernier terme re = %Lf\n", creall(f(x,y,d,N-1)));
  }
  for(int n = 0; n < N+1; n++){
    //    printf("%Lf\n", creall(f(x,y,d,n)));
    s += f0(x,y,d,n);
  }
  return 2.*s;
}

double sumfre(double x, double y, double d){
  return creall(sumf(x,y,d));
}

double sumfim(double x, double y, double d){
  return cimagl(sumf(x,y,d));
}

double sumf0re(double x, double y, double d){
  return creall(sumf0(x,y,d));
}

double sumf0im(double x, double y, double d){
  return cimagl(sumf0(x,y,d));
}

int main (void){
  return 0;  
} 
