import numpy as np
import lattice_f as lf
import lattice_r as lr
import matplotlib.pyplot as plt
import matplotlib
import tex
import six_cell
import chern as ch
from scipy.linalg import eig
from numpy import linalg as LA
from functools import partial
from modele import Modele
from kanemele import KaneMele
from pylatexenc.latex2text import LatexNodes2Text

import plot

tex.useTex()

A = 3 * np.sqrt(3) / 2

plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)


class HoneycombTightBinding(Modele):
    """Describes a tight binding model on a honeycomb lattice such as
    the one in this paper: https://arxiv.org/pdf/1605.08822.pdf"""

    def __init__(self, a, t1, t2):
        super().__init__()
        self.a = a
        self.t1 = t1
        self.t2 = t2

    def compute_ham(self, k):
        self.a1 = np.array([np.sqrt(3), 0]) * self.a
        self.a2 = np.array([np.sqrt(3) / 2, 3 / 2]) * self.a
        self.H1 = np.array(
            [
                [0, 1, 0, 0, 0, 1],
                [1, 0, 1, 0, 0, 0],
                [0, 1, 0, 1, 0, 0],
                [0, 0, 1, 0, 1, 0],
                [0, 0, 0, 1, 0, 1],
                [1, 0, 0, 0, 1, 0],
            ]
        )
        self.H2 = np.array(
            [
                [0, 0, 0, np.exp(1j * np.dot(k, self.a1)), 0, 0],
                [0, 0, 0, 0, np.exp(1j * np.dot(k, self.a2)), 0],
                [0, 0, 0, 0, 0, np.exp(1j * np.dot(k, self.a2 - self.a1))],
                [np.exp(-1j * np.dot(k, self.a1)), 0, 0, 0, 0, 0],
                [0, np.exp(-1j * np.dot(k, self.a2)), 0, 0, 0, 0],
                [0, 0, np.exp(-1j * np.dot(k, self.a2 - self.a1)), 0, 0, 0],
            ]
        )

        self.ham = -self.t1 * self.H1 - self.t2 * self.H2

    def compute_bands(self, path):
        super().compute_bands(path)


def band_diagram_tb(lattice, a, t1, t2):
    Ndots = lattice.shape[0]
    omegas = []
    gammas = []
    color_bands = []
    bands = []
    for k in lattice:
        htb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
        htb.compute_ham(k)
        H = htb.ham
        lambdas, vects = np.linalg.eig(H)
        omega = [0] * 6
        gamma = [0] * 6
        vects = [vects[:, i] for i in range(6)]
        for j in range(6):
            gamma[j] = lambdas[j].imag
            omega[j] = lambdas[j].real

        try:
            tuples = sorted(zip(omega, vects, gamma))
        except ValueError:
            print(k)
        omega, vects, gamma = [[t[i] for t in tuples] for i in range(3)]
        omegas += omega
        gammas += gamma
        cbs = []
        for j in range(6):
            corr = six_cell.project_on_basis(vects[j])

            pp = corr[1]  # p+
            pm = corr[2]  # p-
            dp = corr[3]  # d+
            dm = corr[4]  # d-

            cb = max(LA.norm(pp) ** 2, LA.norm(pm) ** 2) - max(
                LA.norm(dp) ** 2, LA.norm(dm) ** 2
            )

            # cb = LA.norm(pp)-LA.norm(dp)
            cbs.append(cb)
        color_bands.append(cbs)
        bands.append(omega)

    dim_fig = 0, res_bands, -4, 4.2
    title = r"$\textrm{Correlation on band diagram }$" + f"$ t_2 = {t2}$"
    color_bands = (color_bands, -1, 1)
    plot.plot_bands(
        lattice,
        bands,
        title,
        [0, res_bands / 2, res_bands],
        [r"$K$", r"$\Gamma$", r"$M$"],
        color_bands,
        dim_fig,
        6,
    )
    return omegas


# def get_sigma(n):
#     if n % 2 != 0 or n <= 4:
#         raise ValueError("n must be an even number greater than 4")

#     diagonal = np.zeros(n)
#     diagonal[n // 2 - 1] = -1
#     diagonal[n // 2] = 1
#     diagonal[n // 2 - 2] = 1
#     diagonal[n // 2 + 1] = -1

#     return np.diag(diagonal)


def get_sigma():
    return 1j/np.sqrt(3) * np.array([[0,1,0,0,0,-1],
                                      [-1,0,1,0,0,0],
                                      [0,-1,0,1,0,0],
                                      [0,0,-1,0,1,0],
                                      [0,0,0,-1,0,1],
                                      [1,0,0,0,-1,0]])


def histogramme(bz, a, t1, t2):
    omegas = band_diagram_tb(bz, a, t1, t2)

    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.hist(omegas, bins=100, color="purple")
    plt.title(f"$t_1={t1},t_2={t2}$", fontsize=20)
    plt.savefig("hist_tb.pdf")
    plt.show()


# def get_p_sigma_p(vl, vr, sigma):
#     n = vl[:,0].shape[0]
#     n2 = vl[0].shape[0]
#     P = np.zeros((n, n), dtype="complex128")
#     for i in range(n2):
#         vi = np.conj(np.expand_dims(vl[:,i], axis=1))
#         vit = np.transpose(np.expand_dims(vr[:,i], axis=1))

#         A = np.dot(vit, vi)
#         vi = vi / np.sqrt(A)
#         vit = vit / np.sqrt(A)

#         P += np.dot(vi, vit)
#     return P @ sigma @ P


def get_p_sigma_p(vl, vr, sigma):
    n = vl[0].shape[0]
    P = np.zeros((n, n), dtype="complex128")
    for i in range(3):
        vi = np.conj(np.expand_dims(vl[i], axis=1))
        vit = np.transpose(np.expand_dims(vr[i], axis=1))
        A = np.dot(vit, vi)
        vi = vi / np.sqrt(A)
        vit = vit / np.sqrt(A)
        P += np.dot(vi, vit)
    return P @ sigma @ P


def get_H(k, a, t1, t2):
    htb = HoneycombTightBinding(a, t1, t2)
    htb.compute_ham(k)
    H = htb.ham
    psp = psp_from_H(H)
    return psp


def get_H_km(k, t, λ_SO, λ_R, B_z):
    km = KaneMele(t, λ_SO, λ_R, B_z)
    km.compute_ham(k)
    H = km.ham
    psp = psp_from_H(H)
    l, vl, vr = eig(psp, left=True, right=True)
    sorted_indices = l.argsort()[::-1]
    vl = vl[:, sorted_indices]
    vr = vr[:, sorted_indices]
    P = make_projector(vl[0], vr[0])
    return P


def psp_from_H_2(H):
    N = H.shape[0]
    # φ = np.array([(2*i-3)*np.pi/6 for i in range(6)])
    φ = np.array([i*np.pi/3 for i in range(6)])
    λ, ψ_l, ψ_r = eig(H, left=True, right=True)
    λ, ψ_r = np.linalg.eigh(H)

    η_l = np.zeros((12,6),dtype=complex)
    η_r = np.zeros((12,6),dtype=complex)
    for i in range(12):
        for j in range(6):
            η_l[i,j] = ψ_l[2*j,i]*np.cos(φ[j]) + ψ_l[2*j+1,i]*np.sin(φ[j])
            η_r[i,j] = ψ_r[2*j,i]*np.cos(φ[j]) + ψ_r[2*j+1,i]*np.sin(φ[j])
    # print(η[0,:])
    ω = -0.5*np.real(λ)
    idx = ω.argsort()
    ω = ω[idx]
    ψ_l = ψ_l[:,idx]
    ψ_r = ψ_r[:,idx]
    η_r = η_r[idx]
    η_l = η_l[idx]
    P = np.zeros((6,6),dtype=complex)
    # P = np.zeros((12,12),dtype=complex)
    for i in range(6):
        vi = η_r[i,:]
        vit = η_r[i,:]

        # vi = ψ_r[i,:]
        # vit = ψ_r[i,:]
        P += np.outer(vi, vit.conj())/np.dot(vit.conj(), vi)
            

    w_p , _ = np.linalg.eig(P)
    print(f"{w_p=}")
    sigma = get_sigma()
    psp = P@sigma@P
    w_psp , _ = np.linalg.eig(psp)
    print(f"{w_psp=}")


    # exit()
    return psp

def psp_from_H(H):
    N = H.shape[0]
    lambdas, vects = np.linalg.eig(H)
    lambdas, vl, vr = eig(H, left=True, right=True)
    omega = [0] * N
    gamma = [0] * N
    vl = [vl[:, i] for i in range(6)]
    vr = [vr[:, i] for i in range(6)]
    for j in range(N):
        gamma[j] = lambdas[j].imag
        omega[j] = -0.5 * lambdas[j].real


    tuples = sorted(zip(omega, vl, vr, gamma))
    omega, vl, vr, gamma = [[t[i] for t in tuples] for i in range(4)]

    sigma = get_sigma()
    psp = get_p_sigma_p(vl, vr, sigma)
    return psp



def make_projector(x, y):
    return np.outer(x, np.conj(y))


def plot_bands_htb(a=1, t1=1, t2=1, res_bands=1000):
    title = f"$a = {a}" + r"\quad" + f"t_1 = {t1}" + r"\quad" + r"t_2 = {t2}$"
    honeycomb_tb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
    path_in_bz = lf.generate_bz_tb(a, res_bands) + (1e-4, 1e-4)

    honeycomb_tb.compute_bands(path_in_bz)
    bands = honeycomb_tb.bands
    pos_xticks = [0, res_bands / 2, res_bands]
    name_xticks = [r"$K$", r"$\Gamma$", r"$M$"]
    plot.plot_bands(path_in_bz, bands, title, pos_xticks, name_xticks)


def plot_psp_on_path(a=1, t1=1, t2=1, res_bands=1000):
    honeycomb_tb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
    path_in_bz = lf.generate_bz_tb(a, res_bands) + (1e-4, 1e-4)
    bands = []
    for k in path_in_bz:
        honeycomb_tb.compute_ham(k)
        H = honeycomb_tb.ham
        psp = psp_from_H(H)
        eigenvalues, _ = LA.eig(psp)
        energy = np.real(eigenvalues)
        gamma = np.imag(eigenvalues)
        id_sorted = energy.argsort()
        energy = energy[id_sorted]
        gamma = gamma[id_sorted]
        bands.append(energy)

    title = r"$\textrm{Band diagram of } P\sigma P \quad" f" t_2 = {t2}$"
    plot.plot_bands(
        path_in_bz,
        bands,
        title,
        [0, res_bands / 2, res_bands],
        [r"$K$", r"$\Gamma$", r"$M$"],
    )


def plot_psp_on_bz(a=1, t1=1, t2=1, res_bands=1000):
    honeycomb_tb = HoneycombTightBinding(a=a, t1=t1, t2=t2)
    bz, _ = lf.generate_bz(a, 0.05)
    bands = []
    for k in bz:
        honeycomb_tb.compute_ham(k)
        H = honeycomb_tb.ham
        psp = psp_from_H(H)
        eigenvalues, _ = LA.eig(psp)
        energy = np.real(eigenvalues)
        bands += energy.tolist()
    bands.sort()

    title = (
        r"$\textrm{Spectrum of } P\sigma P \textrm{ on Brillouin zone - } "
        + f" t_2 = {t2}$"
    )
    plt.scatter(np.linspace(0, 1, len(bands)), bands, color="blue")
    plt.xticks([])
    plt.yticks(fontsize=20)
    plt.title(title, fontsize=20)
    plt.savefig(
        f"{LatexNodes2Text().latex_to_text(title)}.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.show()


if __name__ == "__main__":
    a = 1
    res_bands = 1000
    t1 = 1
    t2 = 1.5
    # path_KGM = lf.generate_bz_tb(a, res_bands) + (1e-4, 1e-4)

    # band_diagram_tb(path_KGM, a, t1, 1)
    # band_diagram_tb(path_KGM, a, t1, 0.5)
    # band_diagram_tb(path_KGM, a, t1, 1.5)
    # exit()

    # bz1, bz2 = gl.generate_bz(a, 0.01)
    # bz = np.concatenate((bz1, bz2), axis=0)
    # histogramme(bz, a, t1, t2)

    # plot_psp_on_bz(a, t1, 1.5, res_bands)
    # plot_psp_on_bz(a,t1,0.5, res_bands)

    # lattice  = gl.generate_bz_6cell(a,Ndots)+ (0.0001,0.0001)
    step = 0.1
    lattice, BZ, Dk = lf.generate_bz(a, step)
    lattice = lattice + (1e-4, 1e-4)
    # lattice = np.flip(lattice, axis=1) * np.array([1, -1])
    # lr.display_lattice(BZ, "red")
    # lr.display_lattice(lattice, "blue")
    # plt.show()

    # print(np.max
    # k = np.array([1,0])
    # htb.compute_ham(k)
    # print(LA.eig(psp_from_H(htb.get_ham())))
    # exit()

    # ax = 4 * np.pi / (3 * np.sqrt(3) * a)
    # ux = step * ax * np.sqrt(3)
    # uy = step * ax
    # Dk = np.array([uy, ux])

    lr.display_lattice(lattice)
    lr.display_lattice(BZ, "red")
    plt.show()
    dim = 6

    print(Dk)
    ch.plot_lattice_field(partial(get_H, a=a, t1=t1, t2=t2), lattice, Dk, dim)
    # title = f"$a = {a}" + r"\quad" + f"t_1 = {t1}" + r"\quad" + f"t_2 = {t2}$"
    plt.title("titre")
    plt.savefig(
        f"titre.pdf",
        format="pdf",
        bbox_inches="tight",
    )

    # C = ch.chern(partial(get_H, a=a, t1=t1, t2=t2), lattice, Dk, dim)

    ### TODO: generate_dicsrete must also generate the Dk !###############################
    ### TODO: renaming and coherence is emergency: generate_bz -> gen_path_in_bz###########

    t2s = np.linspace(0.999, 1.5, 100)
    Cs = []
    for t2 in t2s:
        C = ch.chern(partial(get_H, a=a, t1=t1, t2=t2), lattice, Dk, dim, [0])
        print(t2,C)
        Cs.append(C)
    plt.cla()
    plt.clf()
    # plt.plot(t2s, -np.sign(t2s-np.ones_like(t2s)), color="red")
    plt.scatter(t2s, np.real(Cs), color="blue")
    plt.xlabel(r"$t_2$", fontsize=20)
    plt.ylabel(r"$C_s$", fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.title(r"$\textrm{Honeycomb tight-binding}$", fontsize=20)
    plt.savefig(
        "spin_chern_number_tight_binding.pdf", format="pdf", bbox_inches="tight"
    )

    print(C)
    t = 1
    λ_SO = 0
    λ_R = 0.1
    B_z = 0.5
    # ch.plot_lattice_field(
    #     partial(get_H, t=t, λ_SO=λ_SO, λ_R=λ_R, B_z=B_z), BZ, Dk, dim
    # )
