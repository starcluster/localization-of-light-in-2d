import numpy as np
import matplotlib.pyplot as plt
from pylab import *

x = np.linspace(-2, 2, 200)
y = np.linspace(-20, 20, 200)
X, Y = np.meshgrid(x, y)


def E(x, y):
    N = 1000
    y0 = 1.5
    x0 = 0.1
    d = 3
    S = 0
    for i in range(-N, N):
        S += 1 / np.sqrt(x**2 + (y - y0 - 2 * i * d) ** 2) - 1 / np.sqrt(
            x**2 + (y + y0 - 2 * i * d) ** 2
        )
    #        S += 1/np.sqrt((x-x0)**2+(y-y0-2*i*d)**2) - 1/np.sqrt((x-x0)**2+(y+y0-2*i*d)**2)
    return S * 100


Z = E(X, Y)  # calcul du tableau des valeurs de Z

plt.pcolor(Y, X, Z, cmap=plt.cm.hot)
plt.colorbar()

plt.show()

plt.contour(X, Y, Z, 30, cmap="RdGy")
plt.show()
