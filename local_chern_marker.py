import numpy as np
import matplotlib.pyplot as plt
import ll2D
import Gd
import generate_lattice as gl


def local_Chern(acoords, eigvec, eigv, ef=3):
    N_sites = np.size(acoords, 0)
    P = np.diag(eigv < ef)
    """
    NON C'est faux car on a w-w0 = Re lambda !!!
    """

    Q = np.eye(2 * N_sites) - P
    x = np.array([acoords[i, 0] for i in range(N_sites)])
    x = np.repeat(x, 2)
    y = np.array([acoords[i, 1] for i in range(N_sites)])
    y = np.repeat(y, 2)
    # avoid inversions, eigenvalue matrix should be unitary
    # assert np.allclose(eigvec @ eigvec.T.conj(), np.eye(eigvec.shape[0]))
    # x = eigvec.T.conj() @ np.diag(x) @ eigvec
    # y = eigvec.T.conj() @ np.diag(y) @ eigvec
    x = np.linalg.inv(eigvec) @ np.diag(x) @ eigvec
    y = np.linalg.inv(eigvec) @ np.diag(y) @ eigvec
    PxQ, PyQ, QxP, QyP = P @ x @ Q, P @ y @ Q, Q @ x @ P, Q @ y @ P
    C1 = 2 * np.pi * np.imag(QxP @ PyQ - QyP @ PxQ)
    C2 = -2 * np.pi * np.imag(PxQ @ QyP - PyQ @ QxP)
    C = (C1 + C2) / 2
    C = eigvec @ C @ np.linalg.inv(eigvec)

    return C


def bulk_value(lc, g):
    x = g[:, 0]
    y = g[:, 1]
    N_lc = lc.shape[0]
    lc_im = []
    lc_im2 = []
    lc_im3 = []
    lc_im4 = []

    lc_im5 = []
    lc_im6 = []
    # print(x)

    size_bulk = 5

    for i in range(int(N_lc / 2)):
        for j in range(int(N_lc / 2)):
            if i == j:
                lc_im.append(np.real(lc[2 * i, 2 * j]))
                lc_im2.append(np.real(lc[2 * i + 1, 2 * j + 1]))
    a = 1
    m = 2
    e = 0.1
    xstep = a * np.sqrt(3)
    xmax = (m + 0.5) * xstep + e
    xmin = (-m + 0.5) * xstep - e
    ymax = xmax * np.tan(np.pi / 6.0)
    ymin = -ymax + a
    x0 = 0.5 * xstep
    y0max = 2 * ymax - a / 2
    y0min = 2 * ymin - a / 2

    for i in range(int(N_lc / 2)):
        lx = x[i]
        ly = y[i]

        yy = []

        a1 = (ymax - y0max) / (xmin - x0)
        b1 = y0max - a1 * x0
        a2 = (y0max - ymax) / (x0 - xmax)
        b2 = ymax - a2 * xmax
        a3 = (ymin - y0min) / (xmax - x0)
        b3 = y0min - a3 * x0
        a4 = (ymin - y0min) / (xmin - x0)
        b4 = y0min - a4 * x0
        if (lx >= xmin) and (lx <= xmax) and (ly >= ymin) and (ly <= ymax):
            lc_im3.append(np.real(lc[2 * i, 2 * i]))
            lc_im4.append(np.real(lc[2 * i + 1, 2 * i + 1]))

            # plt.scatter(x[i],y[i],c="red")
        elif (ly >= ymax) and (lx <= x0) and (ly <= a1 * lx + b1):
            lc_im3.append(np.real(lc[2 * i, 2 * i]))
            lc_im4.append(np.real(lc[2 * i + 1, 2 * i + 1]))
            # plt.scatter(x[i],y[i],c="red")
        elif (ly >= ymax) and (lx >= x0) and (ly <= a2 * lx + b2):
            lc_im3.append(np.real(lc[2 * i, 2 * i]))
            lc_im4.append(np.real(lc[2 * i + 1, 2 * i + 1]))
            # plt.scatter(x[i],y[i],c="red")
        elif (ly <= ymin) and (lx >= x0) and (ly >= a3 * lx + b3):
            lc_im3.append(np.real(lc[2 * i, 2 * i]))
            lc_im4.append(np.real(lc[2 * i + 1, 2 * i + 1]))
            # plt.scatter(x[i],y[i],c="red")
        elif (ly <= ymin) and (lx <= x0) and (ly >= a4 * lx + b4):
            lc_im3.append(np.real(lc[2 * i, 2 * i]))
            lc_im4.append(np.real(lc[2 * i + 1, 2 * i + 1]))
            # plt.scatter(x[i],y[i],c="red")
        else:
            pass
            # plt.scatter(x[i],y[i],c="blue")

    # plt.show()

    radius = 8
    for i in range(int(N_lc / 2)):
        if x[i] ** 2 + y[i] ** 2 < radius**2:
            lc_im5.append(np.real(lc[2 * i, 2 * i]))
            lc_im6.append(np.real(lc[2 * i + 1, 2 * i + 1]))
            plt.scatter(x[i], y[i], c="red")
        else:
            plt.scatter(x[i], y[i], c="blue")
    xmax = xmax - e
    xmin = xmin + e

    length_side = (xmax - xmin) / 2  # (max(x)-min(x))/2
    # print(length_side)
    # print((sum(lc_im3)+sum(lc_im4)))
    print(
        (sum(lc_im3) + sum(lc_im4))
        / (3 * np.sqrt(3) / 2 * (length_side**2))
        / (np.pi / 2)
    )
    print(2 * (sum(lc_im5) + sum(lc_im6)) / (np.pi * radius**2) / (np.pi / 2))
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle(
        r"Local Chern Marker for polarisation $x$ and $y$. $\Delta_{AB}=5$ and  $\Delta_{B}=5.5$"
    )
    lc_im = np.array(lc_im)
    lc_im2 = np.array(lc_im2)

    lc_im = lc_im + lc_im2
    # lc_im = lc_im/(3*np.sqrt(3)/2)
    print(lc_im)
    im1 = ax1.scatter(x, y, c=lc_im, cmap="jet")
    im2 = ax2.scatter(x, y, c=lc_im2, cmap="jet")

    fig.colorbar(im1, ax=ax1)
    fig.colorbar(im2, ax=ax2)

    plt.show()


if __name__ == "__main__":
    # N = 100
    # rho = 1
    # R = np.sqrt(N/np.pi/rho)
    # disc = ll2D.createDisc(R,N)
    # M = ll2D.createMatrixScalar(disc)

    # w, v = np.linalg.eig(M)
    # N = M.shape[0]

    # omega = [0]*N
    # gamma = [0]*N

    # print(disc)

    # ll2D.print_matrix(local_Chern(disc, v, w))

    N = 14
    a = 1
    g = gl.generate_hex_hex(m=5)
    # deprecated, was replaced 03/05/2023 by
    # generate_hex_hex_stretch
    # g = gl.generateHexGrid(N, a)
    g2 = gl.generate_hex_hex(m=7)
    M = Gd.createMatrixVectorSum(g, 200, 12, 0)
    w, v = np.linalg.eig(M)
    N = M.shape[0]

    for omega in [i for i in range(20)]:
        lc = local_Chern(g, v, w, omega)
        bulk_value(lc, g)
