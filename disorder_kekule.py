import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import eig
import csv
import random
import pandas as pd

import Gd
import lattice_r as lr
import tight_binding as tb

from utils import Color,timer,colors_colorblind
import tex
import spin_bott as sb
import six_cell_tb as sctb

sys.path.append('../chapter-2-thesis/code')
import bott

tex.useTex()

def compute_ipr(k, v):
    N = len(v[0])
    h = 0
    b = 0
    for i in range(N):
        t = abs(v[i][k]) ** 4
        h += t
    for i in range(N):
        b += abs(v[i][k]) ** 2
    return h / (b**2)*100


def impact_of_disorder(N_cluster_side=6, a=1, t1=1, t2=1, n_batch=10, half=False):
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    N_φ = 50
    N_s = 50
    φs = np.linspace(0,0.3,N_φ)
    ss = np.linspace(0,0.3,N_s)
    c_sb_s = [0]*n_batch

    for j in range(n_batch):
        # c_sb_φ = []
        # print(f"{j=}")
        # for i,φ in enumerate(φs):
        #     grid = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=1, φ=φ, s=0, half=half)
        #     M = create_tb_matrix_six_cell_periodic_bc(grid, t1=1, t2=t2, a=1)
        #     # M = create_tb_matrix_six_cell_2(grid, t1=1, t2=t2, a=1)
        #     w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
        #     threshold_energy = -0.05
        #     c_sb = sb.spin_bott(grid, vr, vr, w, 0, threshold_energy, N_cluster)
        #     print(φ,c_sb)
        #     c_sb_φ.append(c_sb)
        # c_sb_s[j] = c_sb_φ

        c_sb_sd = []
        print(f"{j=}")
        for i,s in enumerate(ss):
            grid = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=1, φ=0, s=0, half=half)
            M = sctb.create_tb_matrix_six_cell_periodic_bc(grid, t1=1, t2=t2, a=1, r=s)
            # M = create_tb_matrix_six_cell_2(grid, t1=1, t2=t2, a=1)
            w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
            threshold_energy = -0.05
            c_sb = sb.spin_bott(grid, vr, vr, w, 0, threshold_energy, N_cluster)
            print(s,c_sb)
            c_sb_sd.append(c_sb)
        c_sb_s[j] = c_sb_sd 
    c_sb_s = np.array(c_sb_s)

    with open("./spin_bott_evolv_disorder_size.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        # for k,l,m in zip(φs,np.mean(c_sb_s, axis=0),np.std(c_sb_s, axis=0)):
        #     writer.writerow([k,l,m])
        for k,l,m in zip(ss,np.mean(c_sb_s, axis=0),np.std(c_sb_s, axis=0)):
            writer.writerow([k,l,m])

def impact_of_disorder_anderson(N_cluster_side=6, a=1, t1=1, t2=1, n_batch=10, half=False):
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    N_s = 50
    ss = np.linspace(0,10,N_s)
    c_sb_s = [0]*n_batch
    grid = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=1, φ=0, s=0, half=half)
    M0 = sctb.create_tb_matrix_six_cell_periodic_bc(grid, t1=1, t2=t2, a=1, r=0)
    # M0 = sctb.create_tb_matrix_six_cell_2(grid, t1=1, t2=t2, a=1)
    for j in range(n_batch):
        c_sb_sd = []
        print(f"{j=}")
        for i,s in enumerate(ss):
            M = M0 + np.diag(np.random.uniform(-s/2, s/2, N_sites))
            w, vl, vr = sctb.sort_and_listify(*eig(M, left=True, right=True))
            threshold_energy = -0.05
            c_sb = sb.spin_bott(grid, vr, vr, w, 0, threshold_energy, N_cluster)
            # botts = sb.spin_all_bott(grid, vr, vr, w, 0, N_cluster)
            botts = sb.all_spin_bott(grid, vr, vr, w, threshold_energy, N_cluster)
            iprs = {}
            for k,l in enumerate(w):
                iprs[l] = compute_ipr(k,vr)
            plot_histo(np.real(w), botts, iprs, N_sites, t2, s)
            print(s,c_sb)
            c_sb_sd.append(c_sb)
        c_sb_s[j] = c_sb_sd 
    c_sb_s = np.array(c_sb_s)

    with open("./spin_bott_evolv_disorder_size.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m in zip(ss,np.mean(c_sb_s, axis=0),np.std(c_sb_s, axis=0)):
            writer.writerow([k,l,m])

def plot_impact_of_disorder(N_sites, t2, half):
    data = np.genfromtxt("./spin_bott_evolv_disorder_size.csv", delimiter=',', dtype=float, encoding=None)

    φs = data[:, 0]
    avgs = data[:, 1]
    stds = data[:, 2]
    
    # plt.plot(φs,avgs,color=colors_colorblind[0],ls="--",label=r"$216 \textrm{ (PBC)}$")
    # plt.plot(φs,std,color=colors_colorblind[1],ls="--", label=r"$216 \textrm{ (OBC)}$")
    x_error = np.zeros_like(stds)
    # plt.errorbar(φs, avgs, xerr=x_error, yerr=stds, fmt='o', color=colors_colorblind[0],label=r"$384 \textrm{ (PBC)}$")
    plt.scatter(φs, avgs, color=colors_colorblind[0],label=f"$N={N_sites}" +r"\textrm{ (PBC)}$")
    # plt.xlabel(r"$\phi$",fontsize=20)
    plt.xlabel(r"$\delta t_1$",fontsize=20)
    plt.ylabel(r"$\overline{C_B}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(fontsize=20)
    plt.axis((-0.01,5,-0.05,1.1))
    plt.title(f"$t_2 = {t2}$",fontsize=20)
    plt.savefig(f"sbi_t2{t2}_N{N_sites}_half{half}disorder_anderson_t1_size_pbc.pdf",format="pdf",bbox_inches='tight')
    plt.show()


def plot_histo(dos, botts, iprs, N_sites, t2, s):
    plt.hist(dos, bins=100, histtype='step', edgecolor='black')
    plt.scatter(np.real(list(botts.keys())), np.real(list(botts.values())), color='red')
    plt.scatter(np.real(list(iprs.keys())), np.real(list(iprs.values())), c=np.real(list(iprs.values())), vmin=1, vmax=3)
    plt.axvline(x=-np.abs(1-t2), ls = "--", color="purple")
    plt.axvline(x=np.abs(1-t2), ls = "--", color="purple")
    clb = plt.colorbar()
    clb.ax.tick_params(labelsize=20)
    clb.ax.set_title(r"$\textrm{IPR}^{\times 100} $",fontsize=20)
    plt.xlabel(r"$\omega$",fontsize=20)
    plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    title = f"$t_2 = {t2} \quad N = {N_sites} \quad W = {np.round(s,2)}$"
    plt.title(title, fontsize=20)
    plt.axis((-4,4,0,8))
    save_title = f"histo_kekule/histo_{t2}_{N_sites}_{np.round(s,2)}.pdf"
    plt.savefig(save_title,format="pdf",bbox_inches='tight')
    plt.show()

if __name__ == "__main__":
    N_cluster_side = 4
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    a = 1
    t1 = 1
    t2 = 0.5
    s = 0.5
    φ = 0
    half = False


    impact_of_disorder_anderson(N_cluster_side=N_cluster_side, a=1, t1=t1, t2=t2, n_batch=1, half=half)
