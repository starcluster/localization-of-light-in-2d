"""Code to compute the Bott index following the definition given by
T. A. Loring and M. B. Hastings in
https://iopscience.iop.org/article/10.1209/0295-5075/92/67004/meta
"""
import time
import sys

import numpy as np

import lattice_r as lr
from utils import Color,timer
import tex
import Gd

tex.useTex()

def compute_vxvy_p(g, eigvec):
    """
    Compute Vx and Vy matrices.

    Parameters:
        g (ndarray): Array of shape (N_sites, 2) containing the coordinates of the lattice sites.
        eigvec (ndarray): Array of shape (2 * N_sites, 2 * N_sites) containing the eigenvectors.

    Returns:
        Vx (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vx matrix.
        Vy (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vy matrix.
    """
    N_sites = g.shape[0]
    x = g[:N_sites, 0]
    y = g[:N_sites, 1]
    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y) # must be a parameter
    # Lx = np.max(x)-np.min(x)-a/2
    # Ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    Vx = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex)
    Vy = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex)

    for alpha in range(N_sites):
        v_even = eigvec[2 * alpha, :]
        v_odd = eigvec[2 * alpha + 1, :]
        phase_x = np.exp(2 * np.pi * 1j * x[alpha] / Lx)
        phase_y = np.exp(2 * np.pi * 1j * y[alpha] / Ly)
        v_odd_even = np.outer(np.conj(v_odd), v_odd) + np.outer(np.conj(v_even), v_even)
        Vx += v_odd_even * phase_x 
        Vy += v_odd_even * phase_y 

    
    return Vx, Vy


def compute_vxvy_w(g, eigvec, k):
    """
    Compute Vx and Vy matrices.

    Parameters:
        g (ndarray): Array of shape (N_sites, 2) containing the coordinates of the lattice sites.
        eigvec (ndarray): Array of shape (2 * N_sites, 2 * N_sites) containing the eigenvectors.

    Returns:
        Vx (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vx matrix.
        Vy (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vy matrix.
    """
    N_sites = g.shape[0]
    x = g[:N_sites, 0]
    y = g[:N_sites, 1]
    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y) # must be a parameter
    Vx = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex)
    Vy = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex)

    print(f"{Lx=}")
    print(f"{Ly=}")

    x = np.repeat(x,2)
    y = np.repeat(y,2)

    W = np.column_stack([eigvec[:,i] for i in range(k)])
    # for i in rang(k):
    #     if np.sum(np.abs(eigvec[:,i])) < 10:
    #         print(eigvec[:,i])
    phase_x = np.diag(np.exp(2*np.pi*1j*x/Lx))
    phase_y = np.diag(np.exp(2*np.pi*1j*y/Ly))
    # print(W.shape)
    # print(phase_x.shape)
    Vx = np.conj(W.T)@phase_x@W
    Vy = np.conj(W.T)@phase_y@W

    return Vx, Vy


def contient_doublons_avec_precision(lst, precision):
    """
    Vérifie si une liste contient plusieurs fois le même réel avec une précision arbitraire.

    :param lst: La liste à vérifier.
    :param precision: La précision à utiliser pour la comparaison.
    :return: True si la liste contient des doublons avec la précision spécifiée, False sinon.
    """
    for i, x in enumerate(lst):
        for j, y in enumerate(lst):
            if i != j and np.allclose(x, y, atol=precision):
                return True
    return False

def compute_vxvy_w_no_pol(g, eigvec, k, vl=None):
    """
    Compute Vx and Vy matrices.

    Parameters:
        g (ndarray): Array of shape (N_sites, 2) containing the coordinates of the lattice sites.
        eigvec (ndarray): Array of shape (2 * N_sites, 2 * N_sites) containing the eigenvectors.

    Returns:
        Vx (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vx matrix.
        Vy (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vy matrix.
    """
    N_sites = g.shape[0]
    x = g[:N_sites, 0]
    y = g[:N_sites, 1]
    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y) # must be a parameter
    Vx = np.zeros((N_sites, N_sites), dtype=complex)
    Vy = np.zeros((N_sites, N_sites), dtype=complex)

    W = np.column_stack([eigvec[:,i] for i in range(k)])
    w_w, v_w = np.linalg.eig(np.conj(W.T)@W)
    ss = []
    for i in range(k):
        s = np.sum(np.abs(eigvec[:,i]))
        ss.append(s)
    print(contient_doublons_avec_precision(ss, 1e-10))
    # print("eigenvalue of W", w_w[np.abs(w_w) < 1e-13])

    phase_x = np.diag(np.exp(2*np.pi*1j*x/Lx))
    phase_y = np.diag(np.exp(2*np.pi*1j*y/Ly))
    # print(W.shape)
    # print(phase_x.shape)
    if vl is None:
        Vx = np.conj(W.T)@phase_x@W
        Vy = np.conj(W.T)@phase_y@W
    else:
        Wl = np.column_stack([vl[:,i] for i in range(k)])
        Vx = np.conj(W.T)@phase_x@W
        Vy = np.conj(W.T)@phase_y@W

    return Vx, Vy

def compute_vxvy_no_pol(lattice, eigvec):
    """
    Compute Vx and Vy matrices.

    Parameters:
        lattice (ndarray): Array of shape (N_sites, 2) containing the coordinates of the lattice sites.
        eigvec (ndarray): Array of shape (2 * N_sites, 2 * N_sites) containing the eigenvectors.

    Returns:
        Vx (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vx matrix.
        Vy (ndarray): Array of shape (2 * N_sites, 2 * N_sites) representing the Vy matrix.
    """
    N_sites = lattice.shape[0]
    x,y = lattice.T
    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y)
    a = 1
    # Lx = np.max(x)-np.min(x)-a/2
    # Ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    Vx = np.zeros((N_sites, N_sites), dtype=complex)
    Vy = np.zeros((N_sites, N_sites), dtype=complex)



    for alpha in range(N_sites):
        v_even = eigvec[alpha, :]
        phase_x = np.exp(2 * np.pi * 1j * x[alpha] / Lx)
        phase_y = np.exp(2 * np.pi * 1j * y[alpha] / Ly)
        v_even =  np.outer(np.conj(v_even), v_even)
        Vx += v_even * phase_x #+ v_even
        Vy += v_even * phase_y #+ v_even

    return Vx, Vy

def compute_frequencies(eigv, eigvec):
    """Computing and sorting frequencies and eigenvectors accordingly"""
    frequencies = -np.real(eigv) / 2
    frequencies_ind = np.argsort(frequencies)
    frequencies = frequencies[frequencies_ind]
    return frequencies, eigvec[frequencies_ind]


def bott(lattice, eigvec, frequencies, omega, pol=False, dagger=False, projector=False, verbose=False, vl=None):
    """
    Compute the Bott index.

    Parameters:
        lattice (ndarray): Array of shape (N_sites, 2) containing the coordinates of the lattice sites.
        eigvec (ndarray): Array of shape (2 * N_sites, 2 * N_sites) containing the eigenvectors.
        frequencies (ndarray): Array of shape (2 * N_sites,) containing the frequencies.
        omega (float): Value of omega for computing the Bott index.
        pol (bool): indicates if polarisation have to be taken into account.
        dagger (bool): two methods to cumpute Bott index exist, one with dagger of the projected position operator, the other by computing the inverse of the said operator.
        verbose (bool): to print the bott index with real and imaginary parts.

    Returns:
        float: The Bott index value.
    """
    k = np.searchsorted(frequencies, omega)
    if k==0:
        return 0
    # print(f"{frequencies=}")
    # print(f"{omega=}")
    print(k)
    # print(eigvec[:,1])
    t0 = time.time()
    if pol:
        if projector:
            U, V = compute_vxvy_p(lattice, eigvec, k)
            U, V = U[:k, :k], V[:k, :k]
        else:
            U, V = compute_vxvy_w(lattice, eigvec, k)
    else:

        U, V = compute_vxvy_w_no_pol(lattice, eigvec, k,vl=vl)
        U, V = U[:k, :k], V[:k, :k]

    # print(U[np.abs(U) < 1e-13])

    np.savetxt('U_real.csv', np.real(U), delimiter=',')
    np.savetxt('U_imag.csv', np.imag(U), delimiter=',')
    np.savetxt('V_real.csv', np.real(V), delimiter=',')
    np.savetxt('V_imag.csv', np.imag(V), delimiter=',')
    t1 = time.time()

    uv_vu = U@V - V@U
    uu_i = U@np.conj(U.T) - np.eye(U.shape[0])
    vv_i = V@np.conj(V.T) - np.eye(V.shape[0])

    if verbose:
        print(f"|UV-VU|={np.linalg.norm(uv_vu)}")
        print(f"|UU†-𝟙|={np.linalg.norm(uu_i)}")
        print(f"|VV†-𝟙|={np.linalg.norm(vv_i)}")

    nuc_uv_vu = np.linalg.norm(uv_vu, ord='nuc')
    print(f"{nuc_uv_vu=}")
    t2 = time.time()
    if nuc_uv_vu < 4:
        ebott = 1
    elif dagger:
        ebott, _ = np.linalg.eig(U @ V @ np.conj(U.T) @ np.conj(V.T))
    else:
        ebott, _ = np.linalg.eig(U @ V @ np.linalg.inv(U) @ np.linalg.inv(V))

    # import matplotlib.pyplot as plt
    # plt.scatter(np.real(ebott),np.imag(ebott))
    # plt.show()

    print(ebott)
    print(np.max(np.real(ebott)))
    print(np.max(np.real(np.linalg.inv(U))))
    w_u, v_u = np.linalg.eig(U)
    print(w_u[np.abs(w_u) < 1e-1])

    t3 = time.time()
    print("rapport" , (t3-t2)/(t1-t0))

    cbott = np.sum(np.log(ebott)) / (2 * np.pi)            

    if verbose:
        print(f"Bott={cbott}")
        
    return np.imag(cbott)



def all_bott(lattice, eigvec, frequencies):
    """Comput Botte Index for all the frequencies"""
    N_sites = np.size(lattice, 0)
    # print(f"{Color['RED']}Warning: not considering polarisations.{Color['END']}")
    
    Vx, Vy = compute_vxvy_w(lattice, eigvec,2*N_sites)

    botts = {}
    for k in range(2 * N_sites):
        Vxk, Vyk = Vx[0:k, 0:k], Vy[0:k, 0:k]
        ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.conj(Vxk.T) @ np.conj(Vyk.T))
        # ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.linalg.inv(Vxk) @ np.linalg.inv(Vyk))
        bott = np.imag(np.sum(np.log(ebott))) / (2 * np.pi)
        botts[frequencies[k]] = bott

    return botts


if __name__ == "__main__":
    print(
        "Python {:s} {:03d}bit on {:s}\n".format(
            " ".join(item.strip() for item in sys.version.split("\n")),
            64 if sys.maxsize > 0x100000000 else 32,
            sys.platform,
        )
    )


    N = 40
    a = 1
    s = 1.00

    g = lr.generate_hex_grid(N, a)
    g = lr.generate_hex_grid_lb_center(N, a)

    M = Gd.create_matrix_TE(g, -10, -4, 0.05 * 2 * np.pi)
    N = M.shape[0]

    w, v = np.linalg.eig(M)
    f = -np.real(w)/2

    # print(f"{v[:,1]=}") 

    idx = f.argsort()
    f = f[idx]
    v = v[:,idx]

    
    
    print(f)

    print(v[:,0])
    # print(v)

    b = bott(g, v, f, 7, pol=True, dagger=False, projector=False, verbose=False, vl=None)
    print(f"{b=}")
