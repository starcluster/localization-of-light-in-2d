"""
Plotting bands or other data created from class(Modele).
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from pylatexenc.latex2text import LatexNodes2Text
import tex


tex.useTex()


def plot_bands(
    path,
    bands,
    title="",
    pos_xticks=[],
    name_xticks=[],
    colors="blue",
    dim_fig=0,
    size=0.1,
):
    """Simple band diagram."""
    res_bands = len(path)
    nb_of_bands = len(bands[0])
    e_max = np.max(bands)
    e_min = np.min(bands)
    quasimomentum = np.linspace(0, res_bands, res_bands)

    if not (isinstance(colors, str)):
        colors, color_min, color_max = colors

    for i in range(nb_of_bands):
        if isinstance(colors, str):
            plt.scatter(quasimomentum, [b[i] for b in bands], color=colors, s=size)
        else:
            plt.scatter(
                quasimomentum,
                [b[i] for b in bands],
                c=[c[i] for c in colors],
                cmap="viridis",
                norm=matplotlib.colors.Normalize(vmin=color_min, vmax=color_max),
                s=size,
            )

    if not (isinstance(colors, str)):
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=20)

    plt.xticks(pos_xticks, name_xticks, fontsize=20)
    plt.yticks(fontsize=20)
    plt.ylabel(r"$E[\bf{k}]$", fontsize=20)
    plt.xlabel(r"$\textrm{Quasimomentum}\;[\bf{k}]$", fontsize=20)
    plt.title(title, fontsize=20)
    if dim_fig == 0:
        plt.axis((0, res_bands, e_min * 1.1, e_max * 1.1))
    else:
        plt.axis(dim_fig)
    plt.savefig(
        f"{LatexNodes2Text().latex_to_text(title)}.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.show()
