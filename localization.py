from scipy.linalg import eig
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np

import lattice_r as lr
import six_cell_tb as sctb
import spin_bott as sb

def localization_map(lattice, vl, i):
    N = lattice.shape[0]
    w_localized = []
    x,y = lattice.T
    for j in range(N):
        s = np.abs(vl[j][i])**2
        w_localized.append(s)
    plt.scatter(x,y,w_localized)
    plt.show()


if __name__ == "__main__":
    N_cluster_side = 8
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    a = 1
    t1 = 1
    t2 = 0.999
    lattice =  lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=a)
    # lr.display_lattice(lattice)
    # plt.show()
    N = lattice.shape[0]
    # M = sctb.create_tb_matrix_six_cell_periodic_bc(lattice, t1=1, t2=t2, a=1)
    M = sctb.create_tb_matrix_six_cell_2(lattice, t1=1, t2=t2, a=1)

    print(N)
    print(np.count_nonzero(M==t2))
    w, vl, vr = sctb.sort_and_listify(*eig(M, left=True, right=True))
    # localization_map(lattice, vl, N/2)

    sb0 = sb.spin_bott(grid=lattice, vl=vl, vr=vr, w=w, threshold_psp=-0.4, threshold_energy=-0.5, N_cluster=N_cluster)
    print(sb0)
    plt.hist(w.real, color="black", fill=False)
    plt.show()
    

