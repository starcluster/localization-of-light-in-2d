#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <complex>
#include <bits/stdc++.h>
#include <math.h>
#include <Eigen/Dense>

#if defined(_WIN32)
#  define DLL00_EXPORT_API __declspec(dllexport)
#else
#  define DLL00_EXPORT_API
#endif

using namespace std;
using namespace Eigen;

extern "C" {
  DLL00_EXPORT_API void VXVY(double *x, double *y, double *eigvec_re, double *eigvec_im, double *Vx_re, double *Vx_im, double *Vy_re, double *Vy_im, double Lx, double Ly, int N);
  DLL00_EXPORT_API void chk_eig(double *m_re, double *m_im, int N);
}

void chk_eig(double *m_re, double *m_im, int N){
  MatrixXcd M(N, N);
  for (int m =0; m <  N; m++){
    for (int n =0;n <  N; n++ ){
	  complex<double> c(m_re[m*N + n],m_im[m*N + n]);
	  M(m,n) =  c;
    }
  }
  ComplexEigenSolver<MatrixXcd> eigensolver(M);
  // complex<double> eigenvalues[2*N];
  for (int i = 0; i < N; i++){
    complex<double> c =  -(eigensolver.eigenvalues()(i))/2.;
    cout << c << endl;
  }
}

void VXVY(double *x, double *y, double *eigvec_re, double *eigvec_im, double *Vx_re, double *Vx_im, double *Vy_re, double *Vy_im, double Lx, double Ly, int N)
{
  MatrixXcd eigvec(2*N, 2*N);
  for (int m =0; m <  2*N; m++){
    for (int n =0;n <  2*N; n++ ){
	  complex<double> c(eigvec_re[m*2*N + n],eigvec_im[m*2*N + n]);
	  eigvec(m,n) =  c;
    }
  }
  complex<double> argexpx1(0,2*M_PI*x[1]/Lx);
  eigvec.transposeInPlace();
  complex<double> coeff_x = 1.;
  complex<double> coeff_y = 0.;
  
  for (int m =0;  m <  2*N; m++){
    for (int n =0;  n <  2*N; n++){
      coeff_x = 0.;
      coeff_y = 0.;
      for (int alpha =0; alpha <  N; alpha++ ){
	complex<double> argexpx(0,2*M_PI*x[alpha]/Lx);
	complex<double> argexpy(0,2*M_PI*y[alpha]/Ly);
	coeff_x += (conj(eigvec.col(m)(2*alpha))*eigvec.col(n)(2*alpha) + conj(eigvec.col(m)(2*alpha+1))*eigvec.col(n)(2*alpha+1))*exp(argexpx);
	coeff_y += (conj(eigvec.col(m)(2*alpha))*eigvec.col(n)(2*alpha) + conj(eigvec.col(m)(2*alpha+1))*eigvec.col(n)(2*alpha+1))*exp(argexpy);
      }

      Vx_re[m*2*N + n] = real(coeff_x);
      Vx_im[m*2*N + n] = imag(coeff_x);
      Vy_re[m*2*N + n] = real(coeff_y);
      Vy_im[m*2*N + n] = imag(coeff_y);
    }
  }
}
