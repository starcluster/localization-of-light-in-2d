import numpy as np
import lattice_f as lf
import ctypes
from numpy.ctypeslib import ndpointer
from os.path import exists
import os
import matplotlib.pyplot as plt
import matplotlib
import tex

tex.useTex()


libc = ctypes.CDLL("./generateMr.o")

generateMr_C = libc.generateMr
generateMr_C.restype = None
generateMr_C.argtypes = [
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ctypes.c_double,
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
    ctypes.c_int,
]


def generateMr_wrapper(k, a, deltaB, deltaAB, k0, N):
    Mr = np.array([0] * 16, dtype=complex)
    fname = "Mrs/" + str(k[0]) + str(k[1])
    if exists(fname) and os.stat(fname).st_size != 0 and False:
        Mr = np.loadtxt(fname, delimiter=",", dtype=complex)
    else:
        d = 1 / np.sqrt(2) * np.array([[1, 1j], [-1, 1j]])
        M_C_re = np.array([0] * 16, dtype=float)
        M_C_im = np.array([0] * 16, dtype=float)

        generateMr_C(k, a, M_C_re, M_C_im, N)
        Mr_C = np.array(
            [
                [
                    M_C_re[0] + 1j * M_C_im[0],
                    M_C_re[1] + 1j * M_C_im[1],
                    M_C_re[2] + 1j * M_C_im[2],
                    M_C_re[3] + 1j * M_C_im[3],
                ],
                [
                    M_C_re[4] + 1j * M_C_im[4],
                    M_C_re[5] + 1j * M_C_im[5],
                    M_C_re[6] + 1j * M_C_im[6],
                    M_C_re[7] + 1j * M_C_im[7],
                ],
                [
                    M_C_re[8] + 1j * M_C_im[8],
                    M_C_re[9] + 1j * M_C_im[9],
                    M_C_re[10] + 1j * M_C_im[10],
                    M_C_re[11] + 1j * M_C_im[11],
                ],
                [
                    M_C_re[12] + 1j * M_C_im[12],
                    M_C_re[13] + 1j * M_C_im[13],
                    M_C_re[14] + 1j * M_C_im[14],
                    M_C_re[15] + 1j * M_C_im[15],
                ],
            ],
            dtype=complex,
        )

        Mr = Mr_C
        Mr = 3 / 2 * Mr

        np.savetxt(fname, Mr, delimiter=",")

    # Mr = np.array([[-11.06816408 -1.02936007j, -47.97566148 +0.05030657j,
    #     -18.27787704 -6.23567601j, 145.93928989+14.22050876j],
    #    [-48.70491927 +0.08864182j, -11.06816408 -1.02936007j,
    #     145.1542656 +14.14710141j, -18.27787704 -6.23567601j],
    #    [-18.78708896 +6.18151518j, 146.5117954 -14.12097099j,
    #     -11.06816408 -1.02936007j, -47.97566148 +0.05030657j],
    #    [145.84832499-13.97632879j, -18.78708896 +6.18151518j,
    #     -48.70491927 +0.08864182j, -11.06816408 -1.02936007j]])
    # print(repr(Mr))
    # exit()
    Mr[0, 0] += 1j + 2 * deltaB + 2 * deltaAB
    Mr[1, 1] += 1j - 2 * deltaB + 2 * deltaAB
    Mr[2, 2] += 1j + 2 * deltaB - 2 * deltaAB
    Mr[3, 3] += 1j - 2 * deltaB - 2 * deltaAB

    return Mr


# print(np.linalg.eig(generateMr_wrapper(np.array([0,2],dtype=float), 1, 12, 0, 2*np.pi*0.05, 20)))

if __name__ == "__main__":
    k = np.array([1.0, 1.0])
    a = 1
    deltaB = 12
    deltaAB = 0
    k0 = 2 * np.pi * 0.05
    N = 100
    X, Y = [], []
    lattice = lf.generate_bz(a, 100, 1)
    omegas, gammas = [], []
    for kxx, kyy in lattice:
        X.append(kxx)
        Y.append(kyy)
        k = np.array([kxx, kyy], dtype=float)

        Mr = generateMr_wrapper(k, a, deltaB, deltaAB, k0, N)

        lambdas, vects = np.linalg.eig(Mr)

        omega = [0] * 4
        gamma = [0] * 4
        k_norm = np.linalg.norm(k)

        vects = [vects[:, 0], vects[:, 1], vects[:, 2], vects[:, 3]]
        for j in range(4):
            gamma[j] = lambdas[j].imag
        tuples = sorted(zip(lambdas, vects, gamma))

        omega, vects, gamma = (
            [t[0] for t in tuples],
            [t[1] for t in tuples],
            [t[2] for t in tuples],
        )

        for j in range(4):
            omega[j] = -0.5 * omega[j].real

        # B1.append(omega[0])
        # B2.append(omega[1])
        # B3.append(omega[2])
        # B4.append(omega[3])
        omegas += omega
        gammas += gamma
    xprime = [i for i in range(len(omegas))]
    f = plt.figure()
    ax = f.add_subplot(111)
    im = ax.scatter(
        xprime,
        omegas,
        c=gammas,
        cmap="plasma",
        marker="o",
        s=5,
        norm=matplotlib.colors.Normalize(vmin=0.001, vmax=100),
        zorder=2,
    )
    ax.set_ylabel(r"$\textrm{Frequency}$ $(\omega-\omega_0)/\Gamma_0$", fontsize=12)
    ax.set_xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=12)
    ax.axis((0, len(xprime), -200, 200))
    cbar_ax = f.add_axes([0.91, 0.11, 0.05, 0.73])
    clb = f.colorbar(im, cax=cbar_ax)
    clb.ax.set_title(r"$\Gamma/\Gamma_0$")
    plt.savefig("nearest_neighbour.pdf", format="pdf")
    plt.show()
