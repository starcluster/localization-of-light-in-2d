import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import tex
import chern

from kanemele import KaneMele

tex.useTex()

plasma_blue = (13 / 255, 8 / 255, 135 / 255)
plasma_purple = (139 / 255, 10 / 255, 165 / 255)
plasma_pink = (219 / 255, 92 / 255, 104 / 255)
plasma_orange = (254 / 255, 188 / 255, 43 / 255)


def compute_gap_for_heatmap(x, y, λ_SO):
    kmh = KaneMele(t=1, λ_SO=λ_SO, λ_R=y, B_z=x)
    P = np.array([(kx, 0) for kx in np.linspace(0, 4 * pi, 100)])
    kmh.compute_bands(P)
    return kmh.compute_gap(P)


def plot_gap_heatmap(res_heatmap=10, λ_SO=0):
    N = res_heatmap
    xmin, xmax = 0, 2
    ymin, ymax = 0, 0.5
    X, Y = np.meshgrid(np.linspace(xmin, xmax, N), np.linspace(ymax, ymin, N))
    Z = np.vectorize(compute_gap_for_heatmap)(X, Y, λ_SO)
    plt.imshow(Z, cmap="jet", interpolation="nearest")
    plt.xticks(
        [0, N / 2, N - 1],
        [tex.f_to_Tex(xmin), tex.f_to_Tex(xmax / 2), tex.f_to_Tex(xmax)],
    )
    plt.yticks(
        [0, N / 2, N - 1],
        [tex.f_to_Tex(ymax), tex.f_to_Tex(ymax / 2), tex.f_to_Tex(ymin)],
    )
    plt.ylabel(r"$\lambda_R$", fontsize=20)
    plt.xlabel(r"$B_z$", fontsize=20)
    plt.title(r"$\lambda_{SO}= $" + tex.f_to_Tex(λ_SO), fontsize=20)
    plt.savefig(f"lambda_SO={λ_SO}_N={N}.pdf", format="pdf", bbox_inches="tight")
    plt.colorbar()
    plt.show()


def plot_bands(km, res_bands):
    P = np.array([(kx, 0) for kx in np.linspace(0, 4 * pi, res_bands)])
    bands = km.get_bands(P)
    x = np.linspace(0, 100, 100)
    for i in range(4):
        plt.scatter(x, [b[i] for b in bands], color="blue")
    plt.xticks([0, res_bands], ["$0$", r"$4\pi$"], fontsize=20)
    plt.yticks(fontsize=20)
    plt.ylabel(r"$E[\bf{k}]$", fontsize=20)
    plt.xlabel(r"$\textrm{Quasimomentum}\;[\bf{k}]$", fontsize=20)
    plt.title(
        r"$\lambda_{SO}= $"
        + tex.f_to_Tex(λ_SO)
        + r"$\quad\lambda_{R}= $"
        + tex.f_to_Tex(λ_R)
        + r"$\quad B_z= $"
        + tex.f_to_Tex(B_z),
        fontsize=20,
    )
    plt.axis((0, res_bands, -3.5, 3.5))
    plt.savefig(
        f"lambda_SO={λ_SO}_lambda_R={λ_R}_B_z={B_z}.pdf",
        format="pdf",
        bbox_inches="tight",
    )
    plt.show()


if __name__ == "__main__":
    t = 1
    λ_SO = 0.05
    λ_R = 0.3
    B_z = 0.5
    res_heatmap = 100
    res_bands = 100

    km = KaneMele(t=t, λ_SO=λ_SO, λ_R=λ_R, B_z=B_z)
    # plot_bands(km, res_bands)
    plot_gap_heatmap(res_heatmap=res_heatmap, λ_SO=λ_SO)
