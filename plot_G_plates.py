import numpy as np
import ctypes
from numpy.ctypeslib import ndpointer
from os.path import exists
import os
import matplotlib.pyplot as plt

A = 3 * np.sqrt(3) / 2

# gcc -c -Wextra -Wall -O2 generate_mk.c Faddeeva.c Li2.c Li3.c && gcc -shared -o generate_mk_plates.so generate_mk.o Faddeeva.o Li2.o Li3.o -lm && python3 GS15.py

lib = ctypes.cdll.LoadLibrary("./generate_mk_plates.so")

G_plates_re_C = lib.gpre
G_plates_re_C.restype = ctypes.c_double
G_plates_re_C.argtypes = [ctypes.c_double]

G_plates_im_C = lib.gpim
G_plates_im_C.restype = ctypes.c_double
G_plates_im_C.argtypes = [ctypes.c_double]


N = 300
d = np.linspace(0.1, 10, N)
y = []
k0 = 2 * np.pi * 0.05

gpimc7 = G_plates_im_C(11)
# print(gpimc7)
# print(gpimc7*6*np.pi/k0)


for i in range(N):
    y.append(6 * np.pi / k0 * G_plates_im_C(d[i] * np.pi))

print(G_plates_im_C(1) * 6 * np.pi / k0)
print(G_plates_im_C(5) * 6 * np.pi / k0)
print(G_plates_im_C(11) * 6 * np.pi / k0)
# print(G_plates_im_C(11)*6*np.pi/k0)

# print(G_plates_re_C(1)*6*np.pi/k0)
# print(G_plates_re_C(10)*6*np.pi/k0)
# print(G_plates_re_C(11)*6*np.pi/k0)

plt.plot(d, y)
plt.show()
