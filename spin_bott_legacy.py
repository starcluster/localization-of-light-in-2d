import numpy as np
import matplotlib.pyplot as plt
import ll2D
import Gd
import generate_lattice as gl
import ctypes
import sys
import generate_mk as gMk
from multiprocessing import Pool
from numpy.ctypeslib import ndpointer
import time
import scipy
import csv
from scipy.linalg import eig
import bott2
import os

plt.rc("text.latex", preamble=r"\usepackage{amsmath}")

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)


def spin_bott(vl, vr, cutoff):
    N = int(vl.shape[0])
    P = np.zeros((N, N), dtype=complex)

    for i in range(cutoff):
        vll = np.conj(np.expand_dims(vl[:, i], axis=1))
        vrl = np.transpose(np.expand_dims(vr[:, i], axis=1))
        A = np.dot(vrl, vll)

        vll = vll / np.sqrt(A)
        vrl = vrl / np.sqrt(A)

        P += vll @ vrl

    sigma_z = np.diag([1, -1] * int(N / 2))
    return P @ sigma_z @ P


def projector(vl, vr, cutoff):
    N = int(vl.shape[0])
    P = np.zeros((N, N), dtype=complex)

    for i in range(cutoff):
        vll = np.conj(np.expand_dims(vl[:, i], axis=1))
        vrl = np.transpose(np.expand_dims(vr[:, i], axis=1))
        A = np.dot(vrl, vll)

        vll = vll / np.sqrt(A)
        vrl = vrl / np.sqrt(A)

        P += vll @ vrl

    return P


if __name__ == "__main__":
    print(
        "Python {:s} {:03d}bit on {:s}\n".format(
            " ".join(item.strip() for item in sys.version.split("\n")),
            64 if sys.maxsize > 0x100000000 else 32,
            sys.platform,
        )
    )
    print("NumPy: {0:s}\n".format(np.version.version))
    name_file = 0
    N = 2
    k0 = 2 * np.pi * 0.05
    s = 0.92
    a = 1
    d = 1
    phi = 0
    delta_B = 0
    delta_AB = 0
    for s in [1.1]:
        g = gl.generate_hex_hex_stretch(N, a, s, phi)
        g = gl.generate_hex_square_stretch(N, a, s, phi)
        print(g.shape)
        gl.display_lattice(g, "r")
        plt.show()
        hid = str(delta_B) + str(s) + str(N)
        print(hid)
        try:
            print("Try to open numpy file ...")
            fM = open("npy/M" + hid + ".npy", "rb")
            fw = open("npy/w" + hid + ".npy", "rb")
            fvl = open("npy/vl" + hid + ".npy", "rb")
            fvr = open("npy/vr" + hid + ".npy", "rb")
            M = np.load(fM)
            w = np.load(fw)
            vl = np.load(fvl)
            vr = np.load(fvr)
            print("File loaded.")

        except FileNotFoundError:
            print("Couldn't load file. Computing ...")
            t1 = time.time()
            # M = Gd.create_matrix_TE_plates(g, delta_B, delta_AB, k0, d)
            M = Gd.create_matrix_TE(g, delta_B, delta_AB, k0)
            w, vl, vr = eig(M, left=True, right=True)
            t2 = time.time()
            print(t2 - t1)
            fM = open("npy/M" + hid + ".npy", "wb")
            fw = open("npy/w" + hid + ".npy", "wb")
            fvl = open("npy/vl" + hid + ".npy", "wb")
            fvr = open("npy/vr" + hid + ".npy", "wb")
            np.save(fM, M)
            np.save(fw, w)
            np.save(fvl, vl)
            np.save(fvr, vr)
            print("Computation done.")

        N = M.shape[0]

        idx = w.argsort()[::-1]
        w = w[idx]
        vl = vl[:, idx]
        vr = vr[:, idx]

        N_sites = np.size(g, 0)
        frequencies = -np.real(w) / 2

        gammas = np.imag(w)

        polarisation = []
        for i in range(w.shape[0]):
            p = 0
            for j in range(w.shape[0]):
                if j % 2 == 0:
                    p += np.abs(vr[j, i]) ** 2
            polarisation.append(p)

        plt.figure(figsize=(16, 12))
        plt.axis((-150, 120, -3, 70))
        plt.xticks(fontsize=40)
        plt.yticks(fontsize=40)
        plt.xlabel(r" $(\omega_n-\omega_0)/2$", fontsize=40)
        plt.ylabel(r"$\Gamma_n$", fontsize=40)
        plt.scatter(frequencies, gammas, c=polarisation, vmin=0, vmax=1)
        plt.colorbar()
        plt.text(0, 50, "$s = " + str(np.round(s, 2)) + "$", fontsize=40)
        plt.text(0, 45, "$\Delta_B = " + str(np.round(delta_B, 1)) + "$", fontsize=40)
        plt.savefig("polarisations/" + str(name_file) + ".png")
        plt.show()
        plt.clf()
        plt.cla()
        # plt.show()

        omega = 7
        k = 0
        while k != 2 * N_sites and frequencies[k] < omega:
            k += 1

        try:
            print("Try to open numpy file ...")
            fS = open("npy/S" + hid + ".npy", "rb")
            S = np.load(fS)
            print("File loaded.")
        except FileNotFoundError:
            print("Couldn't load file. Computing ...")
            t0 = time.time()
            S = spin_bott(vl, vr, k)
            t1 = time.time()
            print(f"Time for computing S = PσP: {t1-t0}")
            fS = open("npy/S" + hid + ".npy", "wb")
            np.save(fS, S)
            print("Computation done.")

        print("Solving S𝑥 = λ𝑥...")
        ws, vsl, vsr = eig(S, left=True, right=True)
        print("Solved.")

        idx = ws.argsort()[::-1]
        ws = ws[idx]
        vsl = vsl[:, idx]
        vsr = vsr[:, idx]

        ws_re = -np.real(ws)
        ws_re = ws_re[np.abs(ws_re) > 1e-3]

        plt.scatter(np.arange(ws_re.shape[0]), ws_re)
        plt.show()
        ws_im = np.imag(ws)

        plt.figure(figsize=(16, 12))
        plt.axis((-2, 2, 0, 80))
        plt.xticks(fontsize=40)
        plt.yticks(fontsize=40)
        plt.xlabel(r"$\textrm{Real part of eig}(P\sigma P)$", fontsize=40)
        # plt.scatter(ws_re, ws_im,c='r')
        plt.hist(ws_re, bins=40, color="darkred")
        plt.text(0.25, 70, f"$s = {np.round(s,2)}$", fontsize=40)
        plt.text(0.25, 65, f"$N = {N_sites}$", fontsize=40)
        plt.savefig("polarisations_psp/" + str(name_file) + ".png")
        plt.show()
        plt.clf()
        plt.cla()
        name_file += 1

        p_minus = []
        p_plus = []
        ws_minus = []
        ws_plus = []

        k = 0
        while k != len(ws_re) and ws_re[k] < 0:
            k += 1
        P_minus = projector(vsl, vsr, k)

        gX = np.diag([g[i, 0] for _ in range(2) for i in range(len(g))])
        gY = np.diag([g[i, 1] for _ in range(2) for i in range(len(g))])
        Lx, Ly = np.max(gX) - np.min(gX), np.max(gY) - np.min(gY)
        VX = P_minus @ np.exp(2 * np.pi * 1j * gX / Lx) @ P_minus
        VY = P_minus @ np.exp(2 * np.pi * 1j * gY / Ly) @ P_minus

        ebott, _ = eig(VX @ VY @ np.conj(VX.T) @ np.conj(VY.T))
        print(np.imag(np.sum(np.log(ebott))) / (2 * np.pi))

        # VX = P_plus@np.exp(gX)@P_plus
        # VY = P_plus@np.exp(gY)@P_plus
        # ebott, _ = eig(VX @ VY @ np.conj(VX.T) @ np.conj(VY.T))
        # print(np.imag(np.sum(np.log(ebott)))/(2*np.pi))

        # ws_minus = np.array(ws_minus)
        # ws_plus = np.array(ws_plus)

        for x, bm in bott2.all_bott_c(g, vsr, ws):
            print(f"ω_s = {np.round(x,2)} ⇒ B- = {np.round(bm,2)}")

        for x in [-1 + i / 10 for i in range(20)]:
            bm = bott2.bott_c(g, vsr, np.real(ws), x)
            bp = bott2.bott_c(g, np.flip(vsr), -np.flip(np.real(ws)), -x)
            print(
                f"ω_s = {np.round(x,2)} ⇒ B- = {np.round(bm,2)} and B+ ={np.round(bp,2)}"
            )

        Bs = 0.5 * (bp - bm)
        print(f"s = {s}\nBs = {Bs}")
        print(f"B- = {bm}")
        print(f"B+ = {bp}")

        # plt.hist(ws_re)
        # plt.show()
