#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>
//#include "Faddeeva.h"
//#include "li.h"
#include <unistd.h>
//#include "sumG.h"
#include <unistd.h>

double complex my_sqrt(double x){
  if (x < 0) return I*sqrt(-x); else return sqrt(x);
}

double complex erfi(double complex z){
  double r = 0.;
  return Faddeeva_erfi(z,r);
}

double complex Lambda(double *q, double k0){
  double qnorm2 = q[0]*q[0] + q[1]*q[1];
  return my_sqrt(k0*k0-qnorm2);
}

double complex chi(double a, double k0){
  return 1./2./M_PI/(k0*k0)*cexp(-a*a*k0*k0/2);
}

double complex I_f(double *q, double k0, double a){ 
    double complex L = Lambda(q,k0);
    return M_PI*chi(a,k0)/L*(-I+erfi(a*L/sqrt(2)));
}

double complex gstar(int alpha, int beta, double *q, double k0, double a){
  if (alpha==0 && beta==0){
      return (k0*k0-q[0]*q[0])*I_f(q, k0, a);
  } else if ((alpha==0 && beta==1) || (alpha==1 && beta==0)){
    return -q[0]*q[1]*I_f(q,k0,a);
  } else {
    return (k0*k0-q[1]*q[1])*I_f(q, k0, a);
  }
}

double complex gstar_plates(int alpha, int beta, double *q, double k0, double a,  double d){
    double qnorm2 = q[0]*q[0] + q[1]*q[1];
    double complex s = my_sqrt(1-qnorm2/(k0*k0));
    double complex Iq = 0.;
    if (qnorm2 < k0*k0){   Iq = I/sqrt(k0*k0-qnorm2)/(1.+cexpf(-I*d*sqrt(1-qnorm2/(k0*k0))));
    } else {  Iq = 1./sqrt(qnorm2-k0*k0)/(1.+expf(d*sqrt(qnorm2/(k0*k0) - 1))); };

    if (alpha==0 && beta==0){

        return (k0*k0-q[0]*q[0])*I_f(q,k0,a) + Iq*(1-q[0]*q[0]/(k0*k0));
    } else if ((alpha==0 && beta==1) || (alpha==1 && beta==0)){
        return -q[0]*q[1]*I_f(q,k0,a) + Iq*(-q[0]*q[1]/(k0*k0));
    } else {
        return (k0*k0-q[1]*q[1])*I_f(q,k0,a) + Iq*(1-q[1]*q[1]/(k0*k0));
    }
}

double complex G_star(int alpha, int beta, double k0, double aho){
  if (alpha!=beta)
    return 0.;
  else
    return (erfi(k0*aho/sqrt(2))-I)/exp(k0*k0*aho*aho/2) - (-0.5+k0*k0*aho*aho)/(sqrt(M_PI/2)*cpow(k0*aho,3));
}

double complex G_plates(int alpha, int beta, double k0,double d){
  if (alpha!=beta)
    return 0.;
  else{
    k0 = 1;
    double complex z = cexpf(I*k0*d);
    double complex z2 = -clogf(1+z)/(k0*d)+I*cli2(-z)/cpowf((k0*d),2)-1/cpowf((k0*d),3)*cli3(-z);
    return  3.*z2;
  }
}

double dp2(double *x, double *y){
  return x[0]*y[0]+x[1]*y[1];
}

void M(double *kb, double *Mk, int alpha, int beta, int i, int j, double aho, double k0, double d, int N,  double a){
    double complex s = 0.;
    double R = 3;
    double complex A = R*R*sqrt(3)/4*2;


    double a_span[6][2];
    for(int i = 0; i < 6; i++){
      double phi = M_PI/6.*(2*i-3);
      a_span[i][0] = cos(phi)*a;
      a_span[i][1] = sin(phi)*a;
    }
    
    
    double aij[2] = {a_span[i][0]-a_span[j][0],a_span[i][1]-a_span[j][1]};
    for(int m = -N; m < N+1; m++){
        for(int n =  -N; n < N+1; n++){
            double g[] = {2*M_PI*(m-2*n)/R/sqrt(3)-kb[0], 2*M_PI/R/sqrt(3)*(sqrt(3)*m)-kb[1] };
            if (i==j){
	      if (d==0){
                s += gstar(alpha, beta, g, k0, aho);
	      } else {
		s += gstar_plates(alpha, beta, g, k0, aho, d);
	      }
            } else {
	      if (d==0){
                s += gstar(alpha, beta, g, k0, aho)*cexp(I*dp2(g,aij));
	      } else {
		s += gstar_plates(alpha, beta, g, k0, aho, d)*cexp(I*dp2(g,aij));
	      }
            }
        }
    }
    if (i==j){
      if (d!=0){
	s = s/A - (k0/6./M_PI)*(G_star(alpha, beta, k0, aho)-G_plates(alpha, beta,k0,d));
        Mk[0] = creall(s);
        Mk[1] = cimagl(s);
      } else {
	s = s/A - (k0/6./M_PI)*(G_star(alpha, beta, k0, aho));
	Mk[0] = creall(s);
        Mk[1] = cimagl(s);
      }
    }else{
        Mk[0] = creall(s/A);
        Mk[1] = cimagl(s/A);
    }
}

void Mr(double *kb, double *Mk, int alpha, int beta, int i, int j, double aho, double k0, double d, int N,  double a){

    double a_span[6][2] = {{a,0},
                           {a/2,a*sqrt(3)/2},
                           {-a/2,sqrt(3)*a/2},
                           {-a,0},
                           {-a/2,-sqrt(3)*a/2},
                           {a/2,-sqrt(3)*a/2}};

    double aij[2] = {a_span[i][1]-a_span[j][1],a_span[i][0]-a_span[j][0]};


    double complex s = 0.;
    double R = 3;

    for(int m = -N; m<N;m++){
        for(int n = -N; n<N;n++){
            double x = 0;
            double y = 0;
            //for(int p = 0; p < 6; ++p){
           /*  x = R*n + R/4*(1-pow(-1,m)); */
           /* y = R*sqrt(3)/2*m; */
	    y = R*n + R/4*(1-pow(-1,m));
            x = -R*sqrt(3)/2*m;
            double xij = x + aij[0];
            double yij = y + aij[1];

            // printf("k0 %lf\n", k0);
            //}
            /* printf("%f,%f\n", x,y); */
            /* printf("%f,%f\n", xij,yij); */


            if (n == 0 && m==0) { // && alpha!=beta){
                if (i!=j){
                    if (alpha == 0 && beta == 0)
                        s += gxx(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                    else if (alpha == 1 && beta == 0)
                        s += gyx(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                    else if (alpha == 0 && beta == 1)
                        s += gxy(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                    else

                        s += gyy(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                } else {s +=0;}} else {
                if (alpha == 0 && beta == 0)
                    s += gxx(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                else if (alpha == 1 && beta == 0)
                    s += gyx(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                else if (alpha == 0 && beta == 1)
                    s += gxy(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));
                else
                    s += gyy(xij*k0,yij*k0,0,0)*cexp(I*(kb[0]*x+kb[1]*y));

            }
            if (isnan(creall(s))){
                printf("%lf %lf\n", x, y);
                printf("%lf %lf\n", xij, yij);
                printf("%d %d %d %d %d %d\n", i, j, alpha, beta, m, n);
                exit(0);
            }
        }
    }
    // exit(0);
    Mk[0] = creall(s);
    Mk[1] = cimagl(s);

    /* printf("re mk %d %d %lf\n", i, j, Mk[0]); */
        /* printf("im mk %d %d %lf\n", i, j, Mk[1]); */
}


int main(void){
  return 0;
}
