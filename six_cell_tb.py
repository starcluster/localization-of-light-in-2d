"""Studying the evolution of Bott index when increasing disorder of a
six cell model of tight-binding atoms.

"""
import time
import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import eig
import csv
import random
import pandas as pd

import Gd
import lattice_r as lr
import tight_binding as tb

from utils import Color,timer,colors_colorblind
import tex
import autosave
import spin_bott as sb

sys.path.append('../chapter-2-thesis/code')
import bott
# from bott import all_bott, bott, compute_vxvy_no_pol

tex.useTex()

def distance(a,b):
    xa,ya = a
    xb, yb = b
    return np.sqrt((xa-xb)**2+(ya-yb)**2)

def are_nn(site_a, site_b, a):
    return distance(site_a,site_b) <= a+0.1

def pbc(site_a, site_b, a, lx, ly):
    dx = site_a[0]-site_b[0]
    dy = site_a[1]-site_b[1]
    gen = [(1,0),(1,1),(0,1),(-1,1),(-1,0),(-1,-1),(0,-1),(1,-1),(0,0)]
    list_quad = [(dx + c[0]*lx,dy+c[1]*ly) for c in gen]
    return list_quad

def are_nn_pbc(site_a, site_b, a, lx, ly):
    list_quad = pbc(site_a, site_b, a, lx, ly)
    list_are_nn = [np.sqrt(dx**2+dy**2) < a +0.1 for dx,dy in list_quad]
    return any(list_are_nn)

def are_in_same_cell(i,j):
    "We already know they are NN"
    return abs(i-j) <= 6

def create_tb_matrix_six_cell(g,t1=1,t2=1, a=1, N_cluster_side=2, φ=0):
    N = g.shape[0]
    G = np.zeros((N,N),dtype=complex)
    g_r  = []
    if φ != 0: 
        triang = lr.generate_triangular_lattice(N_cluster_side, 3*a)
        for point in triang:
            x,y = point
            θ = φ * (random.random() - 0.5) * 2
            hexc = list(map(lambda e: lr.rot((x, y), e, θ), lr.hex_coords(x, y, a)))
            g_r += hexc
        g_r = np.array(g_r)

    for i in range(N):
        for j in range(N):
            if i==j:
                G[i,j] = 0
            elif are_nn(g[i], g[j], a):
                if are_in_same_cell(i,j):
                    G[i,j] = t1
                else:
                    if φ != 0:
                        r = distance(g_r[i],g_r[j])
                    else:
                        r = a
                    # rc = 2.85*1e-3
                    rc = 0.1

                    G[i,j] = t2*np.exp(-(r-a)/rc)

    return G

def create_tb_matrix_six_cell_2(g,t1=1,t2=1, a=1):
    N = g.shape[0]
    G = np.zeros((N,N),dtype=complex)
    for i in range(N):
        for j in range(N):
            if i==j:
                G[i,j] = 0
            elif are_nn(g[i], g[j], a):
                if are_in_same_cell(i,j):
                    G[i,j] = t1
                else:
                    d = distance(g[i],g[j])
                    G[i,j] = t2*np.exp(a-d)

    return G

def create_tb_matrix_six_cell_periodic_bc(g,t1=1,t2=1, a=1, r=0):
    N = g.shape[0]
    G = np.zeros((N,N),dtype=complex)
    x,y = g.T
    lx = np.max(x)-np.min(x)-a/2
    ly = np.max(y)-np.min(y)+a/2*np.sqrt(3)
    distri_t2 = []
    distri_d = []
    for i in range(N):
        for j in range(N):
            if i==j:
                G[i,j] = 0
            elif are_nn_pbc(g[i], g[j], a, lx, ly):
                if are_in_same_cell(i,j) and distance(g[i],g[j]) < 1.1:
                    G[i,j] = t1 + np.random.uniform(-r,r)
                else:
                    if distance(g[i],g[j]) > 2:
                        ds = [np.sqrt(dx**2+dy**2) for dx,dy in pbc(g[i], g[j], a, lx, ly)]
                        d = np.min(ds)
                    else: d = distance(g[i],g[j])
                    G[i,j] = t2*np.exp(a-d)
                    distri_t2.append(G[i,j])
                    distri_d.append(d)

                    
    # plt.hist(np.real(distri_t2), bins=80, histtype="step",color=colors_colorblind[1])
    # plt.xlabel(r"$t_2$",fontsize=20)
    # plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    # plt.title(r"$\textrm{Distribution of } t_2 \textrm{ for } \phi=\pi/12$\quad" + f"$t_2={t2}$",fontsize=20)
    # plt.xticks(fontsize=20)
    # plt.yticks(fontsize=20)
    # plt.savefig(f"./distri_t_2_{N_cluster_side}_{t2}_pi12.pdf",format="pdf",bbox_inches='tight')
    # plt.show()

    return G

def sort_and_listify(w,vl,vr):
    w,vl,vr = sb.sort_vectors(w,vl,vr)
    vl = [vl[:,i] for i in range(vl.shape[0])]
    vr = [vr[:,i] for i in range(vr.shape[0])]
    return w,vl,vr

def reverse_sort_vectors(w,vl,vr):
    idx = w.argsort()[::-1]
    vl = vl[:,idx]
    vr = vr[:,idx]
    w = w[idx]
    return w,vl,vr

def plot_histo(w, t1, t2):
    plt.axvline(abs(t2-t1),color="red",linestyle='--')
    plt.axvline(-abs(t2-t1),color="red",linestyle='--')
    plt.yticks([])
    plt.hist(np.array(w).real, bins=100, histtype='step', edgecolor='black')
    plt.xlabel(r"$\textrm{Eigenvalues}$ $\lambda$",fontsize=20)
    plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    plt.title(f"$t_2={t2}$",fontsize=20)
    plt.savefig(f"DOS_{t1=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()

def compute_bs_for_all_energy(t1, t2, N_cluster_side):
    for r in [0+i/100 for i in range(10)]: 
        M = create_tb_matrix_six_cell(grid, t1=t1, t2=t2, a=a,N_cluster_side=N_cluster_side, φ=r)
        # M = create_tb_matrix_six_cell(grid,t1=t1,t2=t2, a=a)
        w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
        print(f"{r=}")
        for threshold_energy in [-1.5 + i/10 for i in range(30)]:
            bs = sb.spin_bott(grid, vr, w, -0.5, threshold_energy, N_cluster)
            print(f"{bs=}")
            bss.append(bs)
            energies.append(threshold_energy)
            rs.append(r)
    return rs, energies, bss

def compute_range_bott(grid, t2a, t2b):
    bss = []
    t2s = [ t2a + i/100*(t2b-t2a) for i in range(100)]
    N_cluster = N_cluster_side*N_cluster_side
    for t2 in t2s:
            M = create_tb_matrix_six_cell_2(grid, t1=1, t2=t2, a=1)
            w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
            bs = sb.spin_bott(grid, vl, vr, w, -0.5, 0, N_cluster)
            print(f"{t2=},{bs=}")
            bss.append(bs)
    return t2s,bss

def compute_t2_transition(grid):
    t2s = [ 1.1 + i/100 for i in range(100)]
    N_cluster = grid.shape[0]//6 # N_cluster_side*N_cluster_side
    for t2 in t2s:
        # print(f"{t2:4}",end="\r")
        # print(f"{t2=}")

        M = create_tb_matrix_six_cell_2(grid, t1=1, t2=t2, a=1)
        # M = create_tb_matrix_six_cell_periodic_bc(grid, t1=1, t2=t2, a=1)
        print(np.count_nonzero(M==t2))
        print(grid.shape[0])
        # Compter le nombre de proches voisins

        w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
        # w2, vl, vr = sort_and_listify(*eig(M2, left=True, right=True))
        # plt.hist(w2.real, color="red", fill=True,bins=31)
        # plt.hist(w.real, color="black", fill=False,bins=31)

        # plt.show()
        # print("Sorting eigenvectors")
        bs = sb.spin_bott(grid, vl, vr, w, -0.1, -0.1, N_cluster)
        print(f"{t2=},bs={np.round(bs,2)}")
        # print("Bott spin index computed")
        if abs(bs) > 0.95:
            return t2

    return 2

def plot_t2_transition():
    file_path = 't2_transition.csv'  
    data = pd.read_csv(file_path, header=None, names=['N', 't2'])

    file_path = 't2_transition_pbc.csv'  
    data_pbc = pd.read_csv(file_path, header=None, names=['N', 't2'])

    x = np.array(data['N'])**2*6
    x_pbc = np.array(data_pbc['N'])**2*6
    plt.scatter(x, np.array(data['t2'])-1, marker='o', color='blue', label=r"$\textrm{sim}$")
    plt.scatter(x_pbc, np.array(data_pbc['t2'])-1, marker='o', color='purple', label=r"$\textrm{sim pbc}$")
    plt.scatter(x, 4/np.sqrt(x), marker='o', color='red',label=r"$4/\sqrt{x}$")
    plt.scatter(x, 6/np.sqrt(x), marker='o', color='green',label=r"$6/\sqrt{x}$")
    print()
    # ajouter les conditions au bord périodiques
    plt.xlabel(r'\textrm{Number of sites}', fontsize =20)
    plt.ylabel(r'$t_2$ \textrm{ of transition}', fontsize =20)
    # plt.title('Scatter Plot des données')
    plt.legend()
    plt.loglog()
    plt.savefig("t2_transition.pdf",format="pdf",bbox_inches='tight')

    plt.grid()
    
    
    plt.show()

def map_e_psp(grid, w,vl,vr,N_cluster,t2,N_sites):
    σ_E_array = np.linspace(-0.5,2,40)
    σ_psp_array = np.linspace(-1,1,300)
    sea = []
    spa = []
    bss = []
    for σ_E in σ_E_array:
        for σ_psp in σ_psp_array:
            bs = sb.spin_bott(grid, vl, vr, w, σ_psp, σ_E, N_cluster)
            bss.append(bs)
            sea.append(σ_E)
            spa.append(σ_psp)
            print(σ_E,σ_psp,bs)
    plt.scatter(sea,spa,c=bss)
    plt.colorbar()
    plt.xlabel(r"$\sigma_E$",fontsize=20)
    plt.ylabel(r"$\sigma_{\textrm{psp}}$",fontsize=20)
    plt.title(r"$\textrm{Bott map}\quad $" +f"$t_2={t2}\quad N={N_sites}$",fontsize=20)
    plt.savefig(f"bott_map_psp_e_{t2}_{N_sites}.pdf",format="pdf",bbox_inches='tight')

    plt.show()

def bott_from_matrix(M, grid, threshold_psp, threshold_energy, N_cluster):
    w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
    return sb.spin_bott(grid, vr, vr, w, threshold_psp, threshold_energy, N_cluster)
    
def spin_bott_evolv():
    N_cluster_side = 6
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    a = 1
    t1 = 1
    t2 = 1
    threshold_energy = -0.1
    threshold_psp = 0

    grid_6 = lr.generate_hex_grid_stretch(N_cluster_side=6, a=1, φ=0)
    grid_8 = lr.generate_hex_grid_stretch(N_cluster_side=8, a=1, φ=0)
    t2s = np.linspace(0.,2,20)
    bott_s_6 = []
    bott_s_8 = []
    bott_s_pbc = []
    for t2 in t2s:
        M_pbc = create_tb_matrix_six_cell_periodic_bc(grid_6, t1=1, t2=t2, a=1,r=0.)
        M_6 = create_tb_matrix_six_cell_2(grid_6, t1=1, t2=t2, a=1)
        M_8 = create_tb_matrix_six_cell_2(grid_8, t1=1, t2=t2, a=1)

        b_pbc = bott_from_matrix(M_pbc, grid_6, threshold_psp, threshold_energy, 6*6)
        b_6 = bott_from_matrix(M_6, grid_6, threshold_psp, threshold_energy, 6*6)
        b_8 = bott_from_matrix(M_8, grid_8, threshold_psp, threshold_energy, 8*8)

        print(f"{t2=},bs={np.round(b_pbc,2)},bs={np.round(b_6,2)},bs={np.round(b_8,2)} ")
        bott_s_pbc.append(b_pbc)
        bott_s_6.append(b_6)
        bott_s_8.append(b_8)

    with open("./spin_bott_evolv.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)

        for k,l,m,n in zip(t2s,bott_s_pbc,bott_s_6,bott_s_8):
            writer.writerow([k,l,m,n])

def plot_spin_bott_evolv():
    data = np.genfromtxt("./spin_bott_evolv.csv", delimiter=',', dtype=float, encoding=None)

    t2 = data[:, 0]
    b_pbc = data[:, 1]
    b_6 = data[:, 2]
    b_8 = data[:, 3]
    
    plt.plot(t2,b_pbc,color=colors_colorblind[0],ls="--",label=r"$216 \textrm{ (PBC)}$")
    plt.plot(t2,b_6,color=colors_colorblind[1],ls="--", label=r"$216 \textrm{ (OBC)}$")
    plt.plot(t2,b_8,color=colors_colorblind[2],ls="--", label=r"$384 \textrm{ (OBC)}$")
    plt.xlabel(r"$t_2$",fontsize=20)
    plt.ylabel(r"$C_B$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(fontsize=20)
    plt.savefig("sbi_t2_pbc.pdf",format="pdf",bbox_inches='tight')
    plt.show()

def gen_hist(H, color,label):
    w, v = np.linalg.eig(H)
    f = np.real(w)
    plt.hist(f, bins=80, histtype="step",color=color,label=label)

def plot_dos(N_cluster_side=6, a=1,t2=1,φ=0):    
    grid = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=1, φ=φ)
    N_sites = grid.shape[0]

    t1 = 1
    M = create_tb_matrix_six_cell_periodic_bc(grid, t1=1, t2=t2, a=1)
    gen_hist(M,colors_colorblind[1],label=r"$\textrm{PBC}$")

    legend = plt.legend(fontsize=20,loc='upper right')
    plt.xlabel(r"$\textrm{Energy } \omega$",fontsize=20)
    plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    plt.title(r"$\textrm{Kekulé texture on }$" + f"${N_sites}$" + r"$\textrm{ sites with }$"+ f"$t_2={t2}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.axvline(x=np.abs(t1-t2),color="black")
    plt.axvline(x=-np.abs(t1-t2),color="black")
    # plt.axvline(x=2*np.abs(t1-t2),color="green")
    # plt.axvline(x=-2*np.abs(t1-t2),color="green")
    plt.savefig(f"./dos_6cell_{N_cluster_side}_{t2}_{φ}.pdf",format="pdf",bbox_inches='tight')
    plt.show()


if __name__ == "__main__":
    N_cluster_side = 6
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    a = 1
    t1 = 1
    t2 = 0.99
    s = 0.5
    φ = 0
    half = False


    # threshold_energy = -0.1
    # threshold_psp = 0

    # grid_6 = lr.generate_hex_grid_stretch(N_cluster_side=6, a=1, φ=0)
    # grid_8 = lr.generate_hex_grid_stretch(N_cluster_side=8, a=1, φ=0)
    # t2s = np.linspace(1.62,1.8,100)
    # bott_s_6 = []
    # bott_s_8 = []
    # bott_s_pbc = []
    # for t2 in t2s:
    #     M_pbc = create_tb_matrix_six_cell_periodic_bc(grid_6, t1=1, t2=t2, a=1,r=0.)
    #     M_6 = create_tb_matrix_six_cell_2(grid_6, t1=1, t2=t2, a=1)
    #     M_8 = create_tb_matrix_six_cell_2(grid_8, t1=1, t2=t2, a=1)

    #     b_pbc = bott_from_matrix(M_pbc, grid_6, threshold_psp, threshold_energy, 6*6)
    #     b_6 = bott_from_matrix(M_6, grid_6, threshold_psp, threshold_energy, 6*6)
    #     b_8 = bott_from_matrix(M_8, grid_8, threshold_psp, threshold_energy, 8*8)

    #     print(f"{t2=},bs={np.round(b_pbc,2)},bs={np.round(b_6,2)},bs={np.round(b_8,2)} ")
    

    spin_bott_evolv()
    # plot_spin_bott_evolv()
    exit()
    # plot_dos(N_cluster_side=22)
    # plot_dos(N_cluster_side=22,a=1,t2=0.8)
    # plot_dos(N_cluster_side=22,a=1,t2=1.2)
    # plot_dos(N_cluster_side=22,a=1,t2=0.8,φ=np.pi/6)
    # plot_dos(N_cluster_side=22,a=1,t2=1.2,φ=np.pi/6)
    # plot_dos(N_cluster_side=10,a=1,t2=1,φ=np.pi/6)

    grid = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=1, φ=φ, s=s, half=True)    
    lr.display_lattice(grid[::2], color="red")
    lr.display_lattice(grid[1::2], color="blue")
    plt.savefig(f"grid_{np.round(φ,3)}_{s}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    # M = create_tb_matrix_six_cell_2(grid, t1=1, t2=t2, a=1)

    exit()
    tes = np.linspace(-0.9,0.9,20)
    for threshold_energy in tes:
        bs = sb.spin_bott(grid, vr, vr, w, -0.09, threshold_energy, N_cluster) # psp, energy
        print(f"{threshold_energy=},bs={np.round(bs,2)}")

    exit()

    # map_e_psp(grid, w,vl,vr,N_cluster,t2,N_sites)
    # exit()
    

    exit()
    t2_transition = []
    N_cluster_side_s = list(range(6,18,2))
    for N_cluster_side in N_cluster_side_s:
        grid = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=1)
        t2p = compute_t2_transition(grid)
        t2_transition.append(t2p)
        print(f"{N_cluster_side},{t2p}")

    t2_transition = np.array(t2_transition)
    N_cluster_side_s = np.array(N_cluster_side_s)
    autosave.autosave_to_csv(N_cluster_side_s,t2_transition,globals())
    plt.scatter(N_cluster_side_s,t2_transition,color="red")
    exit()

    # ### PLOTTING Histogram
    φ = 0.
    # M = create_tb_matrix_six_cell(grid, t1=t1, t2=t2, a=a, N_cluster_side=N_cluster_side,φ=φ)
    # w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
    # plot_histo(w, t1, t2)

    t2s,bss = compute_range_bott(grid, 1, 2)
    plt.scatter(t2s,bss,color="black")
    plt.xlabel(r"$t_2$",fontsize=20)
    plt.ylabel(r"$B_s$",fontsize=20)
    plt.title("",fontsize=20)
    plt.savefig(f"Bott_transition_{N_sites=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    exit()
    

    bss = []
    rs = []
    energies = []

    # rs,energies,bss = autosave.autoload_from_csv(rs,energies,bss,globals())
    bss_batch = []
    N_batch = 1
    for batch in range(N_batch):
        rs, energies, bss = compute_bs_for_all_energy(t1, t2, N_cluster_side)        
        bss_batch.append(bss)
    bss_means = [sum(bss)/N_batch for bss in list(zip(*bss_batch))]
            
    autosave.autosave_to_csv(rs,energies,bss_means,globals())

    # rs, energies, bss_means = autosave.autoload_from_csv(rs, energies, bss_means, globals())
    plt.tricontourf(rs,energies,bss)
    plt.colorbar()
    plt.show()
    
    from matplotlib.tri import Triangulation
    tri = Triangulation(rs, energies)
    plt.tripcolor(tri, bss_means, cmap='jet', shading='gouraud')
    plt.xlabel(r"\textrm{Disorder }",fontsize=20)
    plt.ylabel(r"\textrm{Energy of } $H$",fontsize=20)
    plt.title(r"\textrm{Bott Index } " + f"$N={N_sites}$",fontsize=20)
    plt.savefig(f"bott_map_{t2=}_{N_sites=}.pdf",format="pdf",bbox_inches='tight')
    plt.colorbar()
    plt.show()
    exit()

    filename = f"bott_map_disorder_{t2=}_{N_sites=}.csv"
    
    with open(filename, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["rs", "e", "BSS"])
        writer.writerows(zip(rs, energies, bss))

    print(f"Data saved to {filename}")
    exit()

    
    exit()

    
    t2s = [0.5 + i/100 for i in range(100)]
    bss = []
    for t2 in t2s:
        M = create_tb_matrix_six_cell(grid, t1=t1, t2=t2, a=a)
        w, vl, vr = sort_and_listify(*eig(M, left=True, right=True))
        bs = sb.spin_bott(grid, vr, w, -0.5, 0, N_cluster)
        print(f"{t2=},{bs=}")
        bss.append(bs)
    plt.scatter(t2s, bss, color="black")
    plt.ylabel(r"\textrm{Bott Index}",fontsize=20)
    plt.xlabel(r"$t_2$",fontsize=20)
    plt.title(r"\textrm{Bott Index } " + f"$N={N_sites}$",fontsize=20)
    plt.savefig(f"bott_evolution_{N_sites=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()

    
    bss = []
    xs = []
    te = []
    tp = []
    threshold_energy_l = [ -1.5 + i/10 for i in range(30)]
    threshold_psp_l = [ -1.5 + i/10 for i in range(30)]
    for threshold_energy in threshold_energy_l:
        for threshold_psp in threshold_psp_l:
            bs = sb.spin_bott(grid, vr, w, threshold_psp, threshold_energy, N_cluster)
            te.append(threshold_energy)
            tp.append(threshold_psp)
            bss.append(bs)
        print(threshold_energy)

    # Saving the data to a CSV file
    filename = f"bott_map_{t2=}_{N_sites=}"
    
    with open(filename, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(["Threshold Energy", "Threshold PSP", "BSS"])
        writer.writerows(zip(te, tp, bss))

    print(f"Data saved to {filename}")

    # plt.scatter(tp, te, c=bss, cmap="jet")
    from matplotlib.tri import Triangulation
    tri = Triangulation(tp, te)
    plt.tripcolor(tri, bss, cmap='jet', shading='gouraud')
    plt.xlabel(r"\textrm{Threshold in } $P\sigma P$",fontsize=20)
    plt.ylabel(r"\textrm{Threshold in } $H$",fontsize=20)
    plt.title(r"\textrm{Bott Index } " + f"$N={N_sites}$",fontsize=20)
    plt.savefig(f"bott_map_{t2=}_{N_sites=}.pdf",format="pdf",bbox_inches='tight')
    plt.colorbar()
    plt.show()
    exit()
    
    plt.scatter([i for i in range(N_sites)],w_psp,color="black")
    plt.savefig(f"spectrum_psp_p_{t1=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    vl_psp_m = [vl_psp[i] for i in range(len(vl_psp)) if w_psp[i] < -0.25]
    vl_psp_p = [vl_psp[i] for i in range(len(vl_psp)) if w_psp[i] > 0.25]
    vr_psp_m = [vr_psp[i] for i in range(len(vl_psp)) if w_psp[i] < -0.25]
    vr_psp_p = [vr_psp[i] for i in range(len(vl_psp)) if w_psp[i] > 0.25]
    print(len(vr_psp_p))
    proj_p = make_projector(vl_psp_p, vr_psp_p)
    proj_m = make_projector(vl_psp_m, vr_psp_m)
    w_proj_p, vl_proj_p, vr_proj_p = reverse_sort_vectors(*eig(proj_p, left=True, right=True))
    w_proj_m, vl_proj_m, vr_proj_m = reverse_sort_vectors(*eig(proj_m, left=True, right=True))
    plt.hist(w_proj_p.real, bins=100, histtype='step', edgecolor='black')
    plt.title(r"Histogramme des valeurs propres de $P^+$")
    plt.savefig(f"histo_pp__{t1=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    plt.hist(w_proj_m.real, bins=100, histtype='step', edgecolor='black')
    plt.show()
    b_p = bott(grid, vr_proj_p, -w_proj_p, 1)
    b_m = bott(grid, vr_proj_m, -w_proj_m, 1)
    # print(b_p)
    # print(b_m)

    print(f"spin bott = {(b_p-b_m)/2}")
    # all_bott(grid,vr_proj_p)

    
    exit()
    ###############
    σ = tb.get_sigma(6)

    nb_of_cells = M.shape[0]
    geni = np.eye(nb_of_cells//6)

    vm = v[:,w < -0.1]
    vp = v[:,w > 0.1]

    psp_m = tb.get_p_sigma_p(vm,vm,np.kron(geni, σ))
    psp_p = tb.get_p_sigma_p(vp,vp,np.kron(geni, σ))
    
    λ_psp_m, vects_psp_m = np.linalg.eig(psp_m)
    λ_psp_p, vects_psp_p = np.linalg.eig(psp_p)


    ### PLOTTING Histogram
    # plt.axvline(abs(t2-t1),color="red",linestyle='--')
    # plt.axvline(-abs(t2-t1),color="red",linestyle='--')
    plt.yticks([])
    plt.hist(λ_psp_p.real, bins=100, histtype='step', edgecolor='black')
    plt.xlabel(r"$\textrm{Eigenvalues } \lambda \textrm{ of } P\sigma P^+ $",fontsize=20)
    plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    plt.title(f"$t_2={t2}$",fontsize=20)
    plt.savefig(f"DOS_psp_p_{t1=}_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()

    idx = λ_psp_p.argsort()#[::-1]
    vects_psp_p = vects_psp_p[:, idx]
    λ_psp_p = λ_psp_p[idx]

    xp = np.arange(0,λ_psp_p.shape[0])
    plt.scatter(xp,λ_psp_p,color="black")
    plt.xlabel("",fontsize=20)
    plt.ylabel("",fontsize=20)
    plt.title(r"$\textrm{Spectrum of } P\sigma P^+\quad" + f" t_2={t2}$",fontsize=20)
    plt.savefig(f"spectrum_psp_p_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()

    idx = λ_psp_m.argsort()#[::-1]
    vects_psp_m = vects_psp_m[:, idx]
    λ_psp_m = λ_psp_m[idx]
    
    xm = np.arange(0,λ_psp_m.shape[0])
    plt.scatter(xm,λ_psp_m,color="black")
    plt.xlabel("",fontsize=20)
    plt.ylabel("",fontsize=20)
    plt.title(r"$\textrm{Spectrum of } P\sigma P^- \quad" + f" t_2={t2}$",fontsize=20)
    plt.savefig(f"spectrum_psp_m_{t2=}.pdf",format="pdf",bbox_inches='tight')
    plt.show()
    
    ω = -0.1
    # plt.scatter(λ_psp.real,λ_psp.imag)
    # plt.show()
    # exit()

    t0 = time.time()
    # print(bott(grid, vects_psp, λ_psp, ω))
    print(sum(all_bott(grid, vects_psp_p)))
    t1 = time.time()
    print(f"Bott python: {t1-t0}")
    print("\nDone.")
