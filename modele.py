"""
Module describing the class "Modele".
"""
import numpy as np
from numpy import linalg as LA

class Modele:
    """Modele is a generic class from which inherits all the physical
    modeles of this project. Hamiltonians are expected to be written
    in Fourier space and path within the first Brillouin zone."""

    def __init__(self):
        self.bands = []
        self.gammas = []
        self.ham = []
        self.weights = []

    def compute_ham(self, k):
        """Compute hamiltonian of a given modele. Here we just return k
        because this is a generic class."""
        return k

    def compute_energy_gamma(self, k):
        """Compute energy and gamma for a quasimomentum k."""
        self.compute_ham(k)
        eigenvalues, eigenvectors = LA.eig(self.ham)
        energy = np.real(eigenvalues)
        gamma = np.imag(eigenvalues)
        id_sorted = energy.argsort()
        energy = energy[id_sorted]
        gamma = gamma[id_sorted]
        weight = [np.abs(eigenvectors[i])**2 for i in range(eigenvectors.shape[0])]
        return energy, gamma, weight

    def compute_bands(self, path):
        """Compute bands for a path in the quasimomentum space."""
        self.bands = []
        for k in path:
            energy, gamma, weight = self.compute_energy_gamma(k)
            self.bands.append(energy)
            self.gammas.append(gamma)
            self.weights.append(weight)
        self.bands = np.array(self.bands)
        self.gammas = np.array(self.gammas)
        self.weights = np.array(self.weights)

