import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eig

import spin_bott as sb
import six_cell_tb as sctb
import lattice_r as lr


if __name__ == "__main__":
    N_cluster_side = 8
    N_cluster = N_cluster_side*N_cluster_side
    N_sites = N_cluster*6
    a = 1
    t1 = 1
    t2 = 1.3

    ## TODO: create graphical to see nearest neighbour visualize_neighbour.py

    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side, a=a)

    # M = sctb.create_tb_matrix_six_cell_periodic_bc(lattice,t1=t1,t2=t2, a=a)
    M = sctb.create_tb_matrix_six_cell_2(lattice, t1=t1, t2=t2, a=a)
    w, vl, vr = sctb.sort_and_listify(*eig(M, left=True, right=True))
    bs = sb.spin_bott(lattice, vl, vr, w, -0.5, 0, N_cluster)
    print(f"{bs=}")

    
    # sctb.compute_t2_transition(lattice)
    
    # print(bs)
    

