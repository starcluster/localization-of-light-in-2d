"""Old code to compute Bott index, kept to remember about what should
not be done and for the perf of the function `bott_c`

"""

import numpy as np
import matplotlib.pyplot as plt
import ll2D
import Gd
import lattice_r as lr
import ctypes
import sys
from numpy.ctypeslib import ndpointer
import time



def bott_legacy(g, eigvec, eigv, omega):
    N_sites = np.size(g, 0)
    frequencies = -np.real(eigv) / 2
    x = np.array([g[i, 0] for i in range(N_sites)])
    y = np.array([g[i, 1] for i in range(N_sites)])

    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y)

    frequencies_ind = np.argsort(frequencies)
    frequencies = frequencies[frequencies_ind]
    eigvec = eigvec[frequencies_ind]
    eigvec = eigvec.T

    Vx, Vy = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex), np.zeros(
        (2 * N_sites, 2 * N_sites), dtype=complex
    )

    for m in range(2 * N_sites):
        for n in range(2 * N_sites):
            coeff_x, coeff_y = 0, 0
            for alpha in range(N_sites):
                coeff_x += (
                    np.conj(eigvec[m, 2 * alpha]) * eigvec[n, 2 * alpha]
                    + np.conj(eigvec[m, 2 * alpha + 1]) * eigvec[n, 2 * alpha + 1]
                ) * np.exp(2 * np.pi * 1j * x[alpha] / Lx)
                coeff_y += (
                    np.conj(eigvec[m, 2 * alpha]) * eigvec[n, 2 * alpha]
                    + np.conj(eigvec[m, 2 * alpha + 1]) * eigvec[n, 2 * alpha + 1]
                ) * np.exp(2 * np.pi * 1j * y[alpha] / Ly)
            Vx[m, n], Vy[m, n] = coeff_x, coeff_y
        # print(m)

    k = 0

    while k != 2 * N_sites and frequencies[k] < omega:
        k += 1
    Vxk, Vyk = Vx[0:k, 0:k], Vy[0:k, 0:k]

    ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.conj(Vxk.T) @ np.conj(Vyk.T))

    return np.imag(np.sum(np.log(ebott))) / (2 * np.pi)


def compute_vxvy(g, eigvec):
    N_sites = g.shape[0]
    x = g[:N_sites, 0]
    y = g[:N_sites, 1]
    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y)
    Vx = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex)
    Vy = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex)

    for alpha in range(N_sites):
        v_even = eigvec[2 * alpha, :]
        v_odd = eigvec[2 * alpha + 1, :]
        phase_x = np.exp(2 * np.pi * 1j * x[alpha] / Lx)
        phase_y = np.exp(2 * np.pi * 1j * y[alpha] / Ly)
        v_odd_even = np.outer(np.conj(v_odd), v_odd) + np.outer(np.conj(v_even), v_even)
        Vx += v_odd_even * phase_x
        Vy += v_odd_even * phase_y
    return Vx, Vy


def bott(g, eigvec, eigv, omega):
    N_sites = g.shape[0]
    frequencies = -np.real(eigv) / 2

    frequencies_ind = np.argsort(frequencies)
    frequencies = frequencies[frequencies_ind]
    eigvec = eigvec[frequencies_ind]

    Vx, Vy = compute_vxvy(g, eigvec)

    k = np.searchsorted(frequencies, omega)
    Vxk, Vyk = Vx[:k, :k], Vy[:k, :k]

    ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.conj(Vxk.T) @ np.conj(Vyk.T))

    return np.imag(np.sum(np.log(ebott))) / (2 * np.pi)


def all_bott(g, eigvec, eigv):
    N_sites = np.size(g, 0)
    frequencies = -np.real(eigv) / 2

    frequencies_ind = np.argsort(frequencies)
    frequencies = frequencies[frequencies_ind]
    eigvec = eigvec[frequencies_ind]

    Vx, Vy = compute_vxvy(g, eigvec)

    botts = []
    for k in range(2 * N_sites):
        Vxk, Vyk = Vx[0:k, 0:k], Vy[0:k, 0:k]
        ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.conj(Vxk.T) @ np.conj(Vyk.T))
        botts.append(np.imag(np.sum(np.log(ebott))) / (2 * np.pi))

    return botts


def bott_c(g, eigvec, eigv, omega):
    N_sites = np.size(g, 0)
    frequencies = -np.real(eigv) / 2
    x = np.array([g[i, 0] for i in range(N_sites)])
    x = np.ascontiguousarray(x)
    y = np.array([g[i, 1] for i in range(N_sites)])
    y = np.ascontiguousarray(y)

    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y)

    frequencies_ind = np.argsort(frequencies)
    frequencies = frequencies[frequencies_ind]
    eigvec = eigvec[frequencies_ind]
    eigvec_tr = np.transpose(eigvec)
    eigvec_tr_flat = eigvec_tr.flatten()

    Vx, Vy = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex), np.zeros(
        (2 * N_sites, 2 * N_sites), dtype=complex
    )

    Vx_re = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)
    Vx_im = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)
    Vy_re = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)
    Vy_im = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)

    lib = ctypes.cdll.LoadLibrary("./bott.so")
    vxvy_C = lib.VXVY
    vxvy_C.restype = None
    vxvy_C.argtypes = [
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # x
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # y
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # eigvec_re double array !!!!
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # eigvec_im
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vx_re
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vx_im
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vy_re
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vy_im
        ctypes.c_double,  # Lx
        ctypes.c_double,  # Ly
        ctypes.c_int,
    ]  # N
    eigvec_re = np.real(eigvec_tr_flat)
    eigvec_re = np.ascontiguousarray(eigvec_re)
    eigvec_im = np.imag(eigvec_tr_flat)
    eigvec_im = np.ascontiguousarray(eigvec_im)
    vxvy_C(x, y, eigvec_re, eigvec_im, Vx_re, Vx_im, Vy_re, Vy_im, Lx, Ly, N_sites)
    Vx = Vx_re + 1j * Vx_im
    Vy = Vy_re + 1j * Vy_im
    Vx = np.reshape(Vx, (2 * N_sites, 2 * N_sites))
    Vy = np.reshape(Vy, (2 * N_sites, 2 * N_sites))

    k = np.searchsorted(frequencies, omega)
    Vxk, Vyk = Vx[:k, :k], Vy[:k, :k]

    ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.conj(Vxk.T) @ np.conj(Vyk.T))

    return np.imag(np.sum(np.log(ebott))) / (2 * np.pi)


def all_bott_c(g, eigvec, eigv):
    N_sites = np.size(g, 0)
    frequencies = -np.real(eigv) / 2
    x = np.array([g[i, 0] for i in range(N_sites)])
    x = np.ascontiguousarray(x)
    y = np.array([g[i, 1] for i in range(N_sites)])
    y = np.ascontiguousarray(y)

    Lx, Ly = np.max(x) - np.min(x), np.max(y) - np.min(y)

    frequencies_ind = np.argsort(frequencies)
    frequencies = frequencies[frequencies_ind]
    eigvec = eigvec[frequencies_ind]
    eigvec_tr = np.transpose(eigvec)
    eigvec_tr_flat = eigvec_tr.flatten()

    Vx, Vy = np.zeros((2 * N_sites, 2 * N_sites), dtype=complex), np.zeros(
        (2 * N_sites, 2 * N_sites), dtype=complex
    )

    Vx_re = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)
    Vx_im = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)
    Vy_re = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)
    Vy_im = np.ascontiguousarray([0] * 2 * N_sites * 2 * N_sites, dtype=float)

    lib = ctypes.cdll.LoadLibrary("./bott.so")
    vxvy_C = lib.VXVY
    vxvy_C.restype = None
    vxvy_C.argtypes = [
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # x
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # y
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # eigvec_re double array !!!!
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # eigvec_im
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vx_re
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vx_im
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vy_re
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # vy_im
        ctypes.c_double,  # Lx
        ctypes.c_double,  # Ly
        ctypes.c_int,
    ]  # N
    eigvec_re = np.real(eigvec_tr_flat)
    eigvec_re = np.ascontiguousarray(eigvec_re)
    eigvec_im = np.imag(eigvec_tr_flat)
    eigvec_im = np.ascontiguousarray(eigvec_im)
    vxvy_C(x, y, eigvec_re, eigvec_im, Vx_re, Vx_im, Vy_re, Vy_im, Lx, Ly, N_sites)
    Vx = Vx_re + 1j * Vx_im
    Vy = Vy_re + 1j * Vy_im
    Vx = np.reshape(Vx, (2 * N_sites, 2 * N_sites))
    Vy = np.reshape(Vy, (2 * N_sites, 2 * N_sites))

    botts = []

    for k in range(2 * N_sites):
        Vxk, Vyk = Vx[0:k, 0:k], Vy[0:k, 0:k]
        ebott, _ = np.linalg.eig(Vxk @ Vyk @ np.conj(Vxk.T) @ np.conj(Vyk.T))
        botts.append((frequencies[k], np.imag(np.sum(np.log(ebott))) / (2 * np.pi)))

    return botts


if __name__ == "__main__":
    print(
        "Python {:s} {:03d}bit on {:s}\n".format(
            " ".join(item.strip() for item in sys.version.split("\n")),
            64 if sys.maxsize > 0x100000000 else 32,
            sys.platform,
        )
    )
    print("NumPy: {0:s}\n".format(np.version.version))

    N = 2
    a = 1
    s = 1.00

    g = lr.generate_hex_grid(N, a)
    g = lr.generate_hex_grid_lb_center(N, a)
    # g,c = gl.generate_hex_grid_stretched(N, a, s)
    # lr.display_lattice(g, "r")
    # gl.display_lattice(c,"b")
    # print(g.shape)
    # print(g)

    M = Gd.create_matrix_TE(g, 12, 0, 0.05 * 2 * np.pi)
    N = M.shape[0]
    # for i in range(N):
    #     for j in range(N):
    #         print("(",np.real(M[i,j]),",",np.imag(M[i,j]),")")

    w, v = np.linalg.eig(M)
    f = -np.real(w)/2
    plt.hist(f, bins=80, histtype="step", color="red")
    plt.show()
    print(f)
    print(v)
    exit()

    # print("v00", v[0,0])
    # print(-w/2)

    lib = ctypes.cdll.LoadLibrary("./bott.so")
    chk_eig = lib.chk_eig
    chk_eig.restype = None
    chk_eig.argtypes = [
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # m_re
        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  # m_im
        ctypes.c_int,
    ]  # N
    # M = M.flatten()
    # m_re = np.real(M)
    # m_re = np.ascontiguousarray(m_re)
    # m_im = np.imag(M)
    # m_im = np.ascontiguousarray(m_im)
    # chk_eig(m_re, m_im, N)
    # w = w/np.linalg.norm(w)

    # w = -np.real(w)+1j*np.imag(w)
    idx = w.argsort()[::-1]
    w = w[idx]
    v = v[:, idx]

    omega, gamma, iprs = N * [0], N * [0], N * [0]
    for i in range(N):
        omega[i], gamma[i], iprs[i] = -w[i].real / 2, w[i].imag, ll2D.IPRVector(i, v)

    plt.scatter(np.array(omega), np.array(gamma), marker=".", c=iprs, cmap="jet")
    omega = 7
    # for omega in [-10 + i for i in range(30)]:

    # M = 10
    # g = np.array([ (i,0) for i in range(M//2)])
    # w = np.array([1 for _ in range(M)])
    # omega = np.array([1 for _ in range(M)])
    # v = np.ones((M,M))
    t0 = time.time()
    print(all_bott(g, v, w))
    t1 = time.time()
    # botts = all_bott_c(g,v,w)
    print(all_bott_c(g, v, w))
    t2 = time.time()
    # plt.plot(botts[:,0],botts[:,1],color="red")

    # plt.show()

    print(f"Bott python: {t1-t0}")
    print(f"Bott C/C++: {t2-t1}")
    print("\nDone.")
