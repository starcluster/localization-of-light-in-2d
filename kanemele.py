import numpy as np
from numpy import cos, sin, exp, sqrt

from modele import Modele


class KaneMele(Modele):
    def __init__(self, t, λ_SO, λ_R, B_z):
        super().__init__()
        self.t = t
        self.λ_SO = λ_SO
        self.λ_R = λ_R
        self.B_z = B_z

    def compute_ham(self, k):
        k_x, k_y = k[0], k[1]
        Γ_k = 2 * cos(k_x / 2) * exp(-1j * k_y / 2 / sqrt(3)) + exp(1j * k_y / sqrt(3))
        M_k = 2 * sin(k_x) - 4 * sin(k_x / 2) * cos(k_y * sqrt(3) / 2)
        N_k = -2 * sqrt(3) * cos(k_x / 2) * sin(sqrt(3) * k_y / 2) - 1j * 2 * (
            sin(k_x / 2) * cos(sqrt(3) * k_y / 2) + sin(k_x)
        )
        ham_up = np.zeros((4, 4), dtype="complex")
        ham_diag = np.diag(
            [
                self.λ_SO * M_k - self.B_z,
                -self.λ_SO * M_k + self.B_z,
                -self.λ_SO * M_k - self.B_z,
                self.λ_SO * M_k + self.B_z,
            ]
        )
        ham_up[0, 1] = -self.λ_R * N_k
        ham_up[0, 2] = -self.t * Γ_k
        ham_up[1, 3] = -self.t * Γ_k
        ham_up[2, 3] = self.λ_R * N_k
        self.ham = ham_diag + ham_up + np.conj(ham_up.T)

    def compute_gap(self, path):
        B2 = [b[1] for b in self.bands]
        B3 = [b[2] for b in self.bands]
        return min(B3) - max(B2)
