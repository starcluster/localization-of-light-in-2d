import G_hex_poo as ghp
import generate_lattice as gl
import GS15 as gs15
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams["axes.unicode_minus"] = False
from labellines import *
from mpmath import polylog
from mpmath import exp
from scipy.optimize import curve_fit
import chern as ch


plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)

plt.rc("text.latex", preamble=r"\usepackage{amsmath}")


def band_diagram_weighta(with_distance):
    rebake = True
    plates = 0
    if with_distance:
        plates = 1
    plt.clf()
    plt.cla()
    plt.figure(figsize=(30, 16))
    Ndots = 300
    n = 4 * int(Ndots / 3)
    a = 1
    # lattice  = gl.generate_bz(a,Ndots,1)
    # gl.display_lattice(lattice,"red")
    # plt.show()
    N = 40
    K = 0
    aho = 0.1
    deltaB, deltaAB = 12, 0
    distance = 2
    k0 = 2 * np.pi * 0.05 / a
    step = 0.1
    vmin, vmax = 0, 0
    a = 1
    lattice = gl.generate_bz(a, Ndots, 1)
    gap_l = []

    hs = [0.05 + i / 1000 for i in range(65, 100)]
    i = 65
    cherns = []
    plates = 1
    for h in hs:
        if rebake:
            fig, ax = plt.subplots(1, 1, figsize=(28, 20))
            k0 = 2 * np.pi * h / a

            B1, B2, B3, B4, X, Y, omegas, gammas, weightas = gs15.band_diagram(
                lattice, distance, deltaB, deltaAB, k0, aho, K, N, plates, a, 1
            )
            xprime = [i for i in range(len(B1))]

            np.savetxt(
                "a/bd1" + str(h) + ".csv",
                [p for p in zip(xprime, B1)],
                delimiter=",",
                fmt="%s",
            )
            np.savetxt(
                "a/bd2" + str(h) + ".csv",
                [p for p in zip(xprime, B2)],
                delimiter=",",
                fmt="%s",
            )
            np.savetxt(
                "a/bd3" + str(h) + ".csv",
                [p for p in zip(xprime, B3)],
                delimiter=",",
                fmt="%s",
            )
            np.savetxt(
                "a/bd4" + str(h) + ".csv",
                [p for p in zip(xprime, B4)],
                delimiter=",",
                fmt="%s",
            )
        else:
            csv = np.genfromtxt("a/bd1" + str(h) + ".csv", delimiter=",")
            xprime = csv[:, 0]
            B1 = csv[:, 1]
            csv = np.genfromtxt("a/bd2" + str(h) + ".csv", delimiter=",")
            B2 = csv[:, 1]
            csv = np.genfromtxt("a/bd3" + str(h) + ".csv", delimiter=",")
            B3 = csv[:, 1]
            csv = np.genfromtxt("a/bd4" + str(h) + ".csv", delimiter=",")
            B4 = csv[:, 1]
        # print(h)
        # c = ch.chern(a, N, deltaB, deltaAB, distance, aho,  k0, step, plates)
        # print(c)
        # cherns.append(np.real(c))
        # im = plt.scatter(xprime, B1, c=weightas[0], cmap='jet', marker='o', s=25, norm=matplotlib.colors.Normalize(vmin=0,vmax=1))
        # im = plt.scatter(xprime, B2, c=weightas[1], cmap='jet', marker='o', s=25, norm=matplotlib.colors.Normalize(vmin=0,vmax=1))
        # plt.scatter(xprime, B3, c=weightas[2], cmap='jet', marker='o', s=25, norm=matplotlib.colors.Normalize(vmin=0,vmax=1))
        # plt.scatter(xprime, B4, c=weightas[3], cmap='jet',marker='o', s=25, norm=matplotlib.colors.Normalize(vmin=0,vmax=1))

        plt.axis((0, len(xprime), -50, 50))

        # print(h,',',abs(min(B2)-max(B3)) ,',',np.real(c))
        gap_l.append(abs(min(B2) - max(B3)))

        if plates:
            plt.text(
                25,
                40,
                r"$ \Delta_B="
                + str(deltaB)
                + " \qquad \\Delta_{AB}="
                + str(deltaAB)
                + "\qquad k_0d = "
                + str(distance)
                + "\qquad k_0a = 2\pi \\times "
                + str(np.round(h, 3))
                + "$",
                fontsize=60,
            )
        else:
            # plt.title(r"$ \Delta_B=" + str(deltaB) + " \qquad \\Delta_{AB}=" + str(deltaAB) + "\qquad k_0a = 2\pi \\times " + str(np.round(h,4)) + "$", fontsize=60)
            plt.text(
                16 * 3,
                40,
                r"$ \Delta_B="
                + str(deltaB)
                + " \qquad \\Delta_{AB}="
                + str(deltaAB)
                + "\qquad k_0a = 2\pi \\times "
                + str(np.round(h, 4))
                + "$",
                fontsize=60,
            )

        plt.text(
            40 * 3,
            -40,
            r"$ W_{\textrm{gap}} =  " + str(np.round(gap_l[-1], 2)) + "$",
            fontsize=60,
        )

        plt.xlabel(r"$\textrm{Quasimomentum}$ $k$", fontsize=60)
        ax.tick_params(labelsize=60)
        plt.ylabel(r"$\textrm{Frequency}$ $(\omega_n-\omega_0)/\Gamma_0$", fontsize=60)
        fig.subplots_adjust(wspace=0.03, hspace=0.12)
        cbar_ax = fig.add_axes([0.91, 0.15, 0.05, 0.7])
        clb = fig.colorbar(im, cax=cbar_ax)
        im.figure.axes[1].tick_params(axis="y", labelsize=60)
        clb.ax.set_title(r"$W_A^n$", fontsize=60)

        # plt.show()

        plt.savefig(
            "a/band_diagram_fig_" + str(i) + ".png", format="png", bbox_inches="tight"
        )
        i += 1
        # plt.savefig('band_diagram_fig_' + str(a) + '.pdf', format='pdf', bbox_inches='tight')
        plt.clf()
        plt.cla()

    np.savetxt(
        "gap_and_chern2.csv",
        [p for p in zip(hs, gap_l, cherns)],
        delimiter=",",
        fmt="%s",
    )
    # plt.show()


def plot_gap_chern():
    plt.figure(figsize=(10, 8))
    plt.axhline(y=24, xmin=0, xmax=1, linestyle="--", color="gray", zorder=1)
    plt.axhline(y=0, xmin=0, xmax=1, linestyle="--", color="black", zorder=1)

    csv = np.genfromtxt("gap_and_chern_trivial.csv", delimiter=",")
    hs = csv[:, 0]
    gap_l = csv[:, 1]

    # plt.scatter(hs,gap_l,color="blue",label="trivial k-space")
    plt.plot(hs, gap_l, color="blue", lw=2)

    csv = np.genfromtxt("gap_and_chern.csv", delimiter=",")
    hs = csv[:, 0]
    gap_l = csv[:, 1]
    cherns = csv[:, 2]

    hs0, hs1 = [], []
    gap_l0, gap_l1 = [], []
    for i in range(len(cherns)):
        if abs(cherns[i]) < 1e-1:
            hs0.append(hs[i])
            gap_l0.append(gap_l[i])
        else:
            hs1.append(hs[i])
            gap_l1.append(gap_l[i])

    tuples = sorted(zip(hs0, gap_l0))
    hs0, gap_l0 = [t[0] for t in tuples], [t[1] for t in tuples]

    tuples = sorted(zip(hs1, gap_l1))
    hs1, gap_l1 = [t[0] for t in tuples], [t[1] for t in tuples]

    plt.plot(hs0, gap_l0, color="blue", label=r"$C = 0$", lw=2)
    plt.plot(hs1, gap_l1, color="red", label=r"$C = 1$", lw=2)

    plt.axis((0.05, 0.24, -1, 27))
    plt.legend(fontsize=30)

    plt.xlabel(r"$k_0a/2\pi$", fontsize=30)
    plt.ylabel(r"$\textrm{Gap width}$", fontsize=30)
    plt.xticks(fontsize=30, rotation=30)
    plt.yticks(fontsize=30)

    plt.savefig("gap_and_chern.pdf", format="pdf", bbox_inches="tight")
    plt.show()


def gap_max(rebake):
    plates = 0
    plt.clf()
    plt.cla()
    plt.figure(figsize=(30, 16))
    Ndots = 100
    n = 4 * int(Ndots / 3)
    a = 1
    N = 40
    K = 0
    aho = 0.1
    deltaB, deltaAB = 0, 0
    distance = 11
    k0 = 2 * np.pi * 0.05 / a
    step = 0.05

    lattice, l2 = gl.generate_bz(a, 900, step)
    l = []
    for ljk in lattice:
        l.append(ljk)
    for ljk in l2:
        l.append(ljk)
    lattice = np.array(l)
    lattice += (0.001, 0.001)

    deltaBs = [i for i in range(250)]

    for h in [0.01 + i / 100 for i in range(6)]:
        k0 = 2 * np.pi * h / a
        gap_l = []
        if rebake:
            for deltaB in deltaBs:
                B1, B2, B3, B4, X, Y, omegas, gammas, _ = gs15.band_diagram(
                    lattice, distance, deltaB, deltaAB, k0, aho, K, N, plates, a
                )
                # print(min(B2))
                # print(max(B3))
                g = min(B2) - max(B3)
                if g < 0:
                    g = 0
                gap_l.append(g)
            np.savetxt(
                "gap2" + str(h) + ".csv",
                [p for p in zip(deltaBs, gap_l)],
                delimiter=",",
                fmt="%s",
            )
        else:
            csv = np.genfromtxt("gap2" + str(h) + ".csv", delimiter=",")
            deltaBs = csv[:, 0]
            gap_l = csv[:, 1]

        # for i in range(len(deltaBs)):
        #     plt.scatter(deltaBs[i], gap_l[i])
        # plt.show()

        # plt.scatter(h, max(gap_l))
        print(h, max(gap_l))

    plt.show()


def plot_gap():
    plt.figure(figsize=(8, 8))
    x = np.array([0.02 + i / 100 for i in range(11)])
    x2 = np.linspace(0.02, 0.13, 100)
    y = [
        358.74732057530093,
        107.37692146202085,
        45.94454017647327,
        23.953636389580335,
        14.171163647608326,
        9.15868746914277,
        6.321099858791805,
        4.591077793399086,
        3.4741015483721065,
        2.719329530900203,
        2.1901321889996286,
    ]

    def func(x, A):
        return A * x ** (-3)

    popt, pcov = curve_fit(func, x, y)
    A = popt[0]

    y2 = A * x2 ** (-3)

    print(popt)
    print(pcov)

    plt.xlabel(r"$k_0a/2\pi$", fontsize=30)
    plt.ylabel(r"$\textrm{plateau}$", fontsize=30)
    plt.plot(x2, y2, zorder=1)
    t1 = r"\begin{align*}"
    t2 = r"y &= A(k_0a/2\pi)^{-3}\\"
    t3 = r"A &= " + str(np.round(A, 3))
    t4 = r"\end{align*}"
    plt.text(x=0.05, y=150, s=t1 + t2 + t3 + t4, fontsize=20)
    plt.plot(x2, y2, zorder=1, color="blue")
    plt.scatter(x, y, color="red", zorder=2)
    plt.loglog()
    plt.minorticks_off()
    plt.xticks([])
    plt.xticks(
        [0.01 * i * 2 for i in range(1, 6)],
        ["$" + str(0.01 * i * 2) + "$" for i in range(1, 6)],
        fontsize=20,
        rotation=30,
    )
    plt.yticks(fontsize=20)
    plt.savefig("max_gap_k0a.pdf", format="pdf", bbox_inches="tight")

    plt.show()


if __name__ == "__main__":
    band_diagram_weighta(True)
    # gap_max(True)
    # plot_gap()
    # plot_gap_chern()
