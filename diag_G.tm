<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Diagonalisation de <math|G> pour <math|n=2> et
  <math|d\<less\>\<pi\>>>>

  Dans ce cas particulier la matrice <math|G> s'exprime comme:

  <with|font-base-size|8|<\equation*>
    G=<matrix|<tformat|<table|<row|<cell|0>|<cell|0>|<cell|<frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>|<cell|<frac|1|2r<rsub|n><rsup|2>>Q<around*|(|-i*x+y<rsup|>|)><rsup|2>>>|<row|<cell|0>|<cell|0>|<cell|<frac|1|2r<rsub|n><rsup|2>>Q<around*|(|i*x+y<rsup|>|)><rsup|2>>|<cell|<frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>>|<row|<cell|<frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>|<cell|<frac|1|2r<rsub|n><rsup|2>>Q<around*|(|-i*x+y<rsup|>|)><rsup|2>>|<cell|0>|<cell|0>>|<row|<cell|<frac|1|2r<rsub|n><rsup|2>>Q<around*|(|i*x+y<rsup|>|)><rsup|2>>|<cell|<frac|1|2r<rsub|n><rsup|2>><around*|(|2P*r<rsub|n><rsup|2>+Q*x<rsup|2>+Q*y<rsup|2>|)>>|<cell|0>|<cell|0>>>>>
  </equation*>>

  Donc on peut �crire cette matrice de fa�on symbolique en introduisant
  <math|a,b\<in\>\<bbb-C\>>:

  <\equation*>
    G=<matrix|<tformat|<table|<row|<cell|0>|<cell|0>|<cell|a>|<cell|b>>|<row|<cell|0>|<cell|0>|<cell|<wide|b|\<bar\>>>|<cell|a>>|<row|<cell|a>|<cell|b>|<cell|0>|<cell|0>>|<row|<cell|<wide|b|\<bar\>>>|<cell|a>|<cell|0>|<cell|0>>>>>
  </equation*>

  On peut facilement calculer le polynome caract�ristique:

  <\equation*>
    \<chi\><rsub|G><around*|(|\<lambda\>|)>=\<lambda\><rsup|4>-2\<lambda\><rsup|2><around*|(|a<rsup|2>+b*<wide|b|\<bar\>>|)>-2a<rsup|2>*b*b+b<rsup|2><wide|b|\<bar\>><rsup|2>
  </equation*>

  Qui a pour solutions (�quation bicarr�e):

  <\equation*>
    <around*|{|-a-<sqrt|b*<wide|b|\<bar\>>>,-a+<sqrt|b*<wide|b|\<bar\>>>,a-<sqrt|b*<wide|b|\<bar\>>>,a+<sqrt|b*<wide|b|\<bar\>>>|}>
  </equation*>

  Immanquablement ces valeurs propres se r�partissent dans les 4 quadrants du
  plan:

  <\big-figure|<image|/home/wulles/localization-of-light-in-2d/valp.png|0.4par|||>>
    \;
  </big-figure>

  On a donc toujours des valeurs propres qui v�rifient
  <math|\<Im\><around*|(|\<lambda\>|)>\<less\>0>, cela ne peut traduire
  <strong|aucune situation physique>. Donc soit la matrice est incorrecte
  (erreur de calcul peut-�tre ?). Soit la m�thode ne peut pas aboutir sous
  cette forme.

  Si l'on fait la comparaison avec la matrice qui se base sur les fonctions
  de Hankel:

  <\equation*>
    G<rsub|Hankel>=<matrix|<tformat|<table|<row|<cell|-<frac|1|2>>|<cell|0>|<cell|H<rsub|0>>|<cell|H<rsub|2>\<mathe\><rsup|2i\<varphi\>>>>|<row|<cell|0>|<cell|-<frac|1|2>>|<cell|H<rsub|2>\<mathe\><rsup|-2i\<varphi\>>>|<cell|H<rsub|0>>>|<row|<cell|H<rsub|0>>|<cell|H<rsub|2>\<mathe\><rsup|2i\<varphi\>>>|<cell|-<frac|1|2>>|<cell|0>>|<row|<cell|H<rsub|2>\<mathe\><rsup|-2i\<varphi\>>>|<cell|H<rsub|0>>|<cell|0>|<cell|-<frac|1|2>>>>>>
  </equation*>

  Prenons un cas particulier pour bien fixer les id�es, les deux atomes
  seront consid�r�s aux points:

  <\equation*>
    <around*|{|<matrix|<tformat|<table|<row|<cell|1>>|<row|<cell|0>>>>>,<matrix|<tformat|<table|<row|<cell|-1>>|<row|<cell|0>>>>>|}>
  </equation*>

  Dans ce cas <math|\<varphi\>=arctan<around*|(|0|)>=0> et <math|r=2>, donc
  <math|G<rsub|Hankel>> est de la forme symbolique:

  <\equation*>
    G<rsub|Hankel>=<matrix|<tformat|<table|<row|<cell|-1>|<cell|0>|<cell|a>|<cell|b>>|<row|<cell|0>|<cell|-1>|<cell|b>|<cell|a>>|<row|<cell|a>|<cell|b>|<cell|-1>|<cell|0>>|<row|<cell|b>|<cell|a>|<cell|0>|<cell|-1>>>>>
  </equation*>

  Dans ce cas particulier:

  <\equation*>
    a\<approx\>0.22+0.51i<infix-and>b\<approx\>0.35-0.61i
  </equation*>

  Et les valeurs propres sont:

  <\equation*>
    \<approx\><around*|{|0.42+ 0.10i,0.87+ 1.12i,1.12- 1.12i,1.57 - 0.10i|}>
  </equation*>

  On a bien des parties r�elles positives, mais uniquement gr�ce aux valeurs
  sur la diagonales �gales � 1. Sinon ce n'est pas le cas. J'arrive donc � un
  paradoxe:

  <\wide-block>
    <tformat|<table|<row|<\cell>
      Si les �l�ments de la diagonale sont nuls, alors on a des parties
      imaginaires (ou r�elles selon que l'on consid�re la matrice avec les
      fonctions de hankel ou celle avec la somme) qui sont n�gatives et donc
      cela ne correspond pas � un cas physique. Si on <em|shift> les valeurs
      propres alors la diagonale n'est plus nulle ce qui ne correspond plus �
      la valeur de la somme pour <math|d\<less\>\<pi\>>.
    </cell>>>>
  </wide-block>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../.TeXmacs/texts/scratch/no_name_2.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>