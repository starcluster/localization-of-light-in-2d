####################################################
# The goal of this script is to compute the width  #
# of the gap by substracting the density of states #
# between two distribution with a different number #
# of sites.                                        #
####################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import Gd
import ll2D
import generate_lattice as gl
import utils as u
import time
import G_hex_poo as ghp
from scipy.optimize import curve_fit
from scipy.stats import gaussian_kde

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": ["Helvetica"]}
)


def compute_kde(a, deltaB, deltaAB, d, k0, m):
    g1 = gl.generate_hex_hex(m=m, a=a)
    # deprecated, was replaced 03/05/2023 by
    # generate_hex_hex_stretch
    M1 = Gd.createMatrixVectorSum(g1, d, deltaB, deltaAB, k0)

    w, v = np.linalg.eig(M1)
    N = M1.shape[0]

    omega, gamma, iprs = [0] * N, [0] * N, [0] * N

    for i in range(N):
        omega[i], gamma[i], iprs[i] = w[i].real / 2, -w[i].imag, ll2D.IPRVector(i, v)

    density = gaussian_kde(omega)
    density.covariance_factor = lambda: 0.03
    density._compute_covariance()

    return density


def n_sites(m):
    return 16 * m**2 + 8 * m + 4


def compute_gap(N_x, xs, kde2, kde3):
    dkde = kde3(xs) - kde2(xs)
    sgndkde = np.sign(dkde)

    sgndkde30 = []
    for i in range(N_x):
        if abs(xs[i]) < 30:
            sgndkde30.append(sgndkde[i])

    init = sgndkde30[0]
    gaps = [0]
    for i in range(len(sgndkde30)):
        if np.sign(init) == np.sign(sgndkde30[i]):
            gaps[-1] += 1
        else:
            init = sgndkde30[i]
            gaps.append(0)
    return max(gaps) * 320 / N_x


if __name__ == "__main__":
    a = 1

    deltaB = 12
    deltaAB = 0
    d = 100
    h = 0.05
    k0 = 2 * np.pi * h / a
    N_x = 1000

    # h1 = plt.hist(omega, bins=100,density=True, range=(-160,160),  facecolor='#00dd10', edgecolor='#00870a', linewidth=0.5)

    xs = np.linspace(-160, 160, N_x)

    for h in [0.05 + i / 200 for i in range(100)]:
        k0 = 2 * np.pi * h / a
        kde2 = compute_kde(a, deltaB, deltaAB, d, k0, m=5)
        kde3 = compute_kde(a, deltaB, deltaAB, d, k0, m=8)
        print(h, " , ", compute_gap(N_x, xs, kde2, kde3))

    dkde = kde3(xs) - kde2(xs)
    sgndkde = np.sign(dkde)
    plt.plot(xs, kde2(xs), c="red", label=r"$N=" + str(n_sites(11)) + "$")
    plt.plot(xs, kde3(xs), c="blue", label=r"$N=" + str(n_sites(14)) + "$")
    plt.legend()
    plt.scatter(xs, dkde, c=sgndkde)
    plt.ylabel("DOS")
    plt.xlabel(r"$(\omega-\omega_0)/\Gamma_0$")
    plt.title(r"$k_0a = 2\pi \times " + str(np.round(h, 2)) + "$")
    plt.axis((-30, 30, -0.001, 0.005))
    plt.savefig(
        "diffkde_zoom" + str(np.round(h, 2)) + ".pdf", format="pdf", bbox_inches="tight"
    )
    plt.show()
