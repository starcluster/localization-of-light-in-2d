"""A module for computing the band diagram of a 2D lattice.

This module contains the `band_diagram` function, which computes the band diagram
of a 2D lattice with given lattice vectors and interaction parameters. The function
returns the band energies, lattice vectors, and other relevant information in a tuple.
"""

import numpy as np
import generate_mk as gMk

from utils import *

def band_diagram(lattice, distance, delta_B, delta_AB, k0, aho, N, a):
    """
    Calculate the band diagram of a lattice.

    Args:
        lattice (ndarray): A numpy array of shape (N, 2) containing the k vectors.
        distance (float): Distance between plates.
        delta_B (float): Zeeman shift.
        delta_AB (float): Frequency detuning between sites.
        k0 (float): Wavenumber of light.
        aho (float): cut-off term.
        N (int): Number of lattice sites.
        a (float): Lattice spacing.
    """
    X, Y = lattice.T
    B1, B2, B3, B4 = [], [], [], []
    omegas, gammas = [], []
    weightas = [[] for _ in range(4)]
    sigmas = [[] for _ in range(4)]
    for k in lattice:
        Mk = gMk.generate_mk_wrapper(k, distance, delta_B, delta_AB, k0, aho, N, a)
        lambdas, vects = np.linalg.eig(Mk)

        vects = [vects[:, i] for i in range(4)]
        gamma = [ga.imag for ga in lambdas]
        tuples = sorted(zip(lambdas, vects, gamma))

        omega, vects, gamma = map(list, zip(*tuples))

        weightas_state, sigmas_state = [], []
        for v in vects:
            weighta = np.linalg.norm(v[0]) ** 2 + np.linalg.norm(v[1]) ** 2
            sigma = np.linalg.norm(v[0]) ** 2 + np.linalg.norm(v[2]) ** 2
            weightas_state.append(weighta)
            sigmas_state.append(sigma)

        for i in range(4):
            weightas[i].append(weightas_state[i])
            sigmas[i].append(sigmas_state[i])

        omega = [-0.5 * om.real for om in omega]

        B1.append(omega[0])
        B2.append(omega[1])
        B3.append(omega[2])
        B4.append(omega[3])
        omegas += omega
        gammas += gamma

    return B1, B2, B3, B4, X, Y, omegas, gammas, weightas, sigmas
