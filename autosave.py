import os
import numpy as np
import csv

def namestr(obj, namespace):
    names = [name for name in namespace if namespace[name] is obj]
    if len(names) == 0:
        raise ValueError("Cannot find name of the list in the namespace.")
    return [name for name in namespace if namespace[name] is obj][0]

def autosave_to_csv(*args):
    namespace = args[-1]
    if not(isinstance(namespace,dict)):
        raise ValueError("Missing namespace, add globals() as an argument to the function")
    args = args[0:len(args)-1]

    for arg in args:
        if not isinstance(arg, (list, np.ndarray)):
            raise ValueError("Arguments must be lists or NumPy arrays of the same dimension.")

    dimensions = set(len(arg) for arg in args)
    if len(dimensions) > 1:
        raise ValueError("Lists or arrays do not have the same dimension.")

    filename = "_".join(namestr(arg, namespace) for arg in args)
    base_filename = filename
    index = 2
    while os.path.exists(f"{filename}.csv"):
        filename = f"{base_filename}_{index}"
        index += 1

    with open(f"{filename}.csv", mode="w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow([namestr(arg, namespace) for arg in args])  
        writer.writerows(zip(*args))  

def autoload_from_csv(*args):
    namespace = args[-1]
    if not(isinstance(namespace,dict)):
        raise ValueError("Missing namespace, add globals() as an argument to the function")
    args = args[0:len(args)-1]

    names_columns = [namestr(arg, namespace) for arg in args]
    
    filename = "_".join(namestr(arg, namespace) for arg in args)
    filename = filename + ".csv"

    columns = []
    with open(filename, "r") as file:
        reader = csv.reader(file)
        header = next(reader)
        num_columns = len(header)
        data = [[] for _ in range(num_columns)]

        for row in reader:
            for i, value in enumerate(row):
                data[i].append(float(value))
        for i in range(num_columns):
            column_name = header[i]
            column_data = data[i]
            columns.append(column_data)
    return columns


if __name__ == "__main__":
    x = [1, 2, 3, 4]
    y = [5, 6, 7, 8]
    z = [9, 10, 11, 12]
    t = np.array([1, 2, 3, 4])

    autosave_to_csv(x, y, z, t, globals())
    a,b,c,d = autoload_from_csv(x,y,z,t,globals())
    print(a,b,c,d)
