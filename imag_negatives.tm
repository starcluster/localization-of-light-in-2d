<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  <doc-data|<doc-title|Parties imaginaires n�gatives ?>>

  Le calcul de la structure de bande dans l'espace de Fourier fait appara�tre
  des valeurs propres avec des parties imaginaires n�gatives. Cela pose
  probl�me pour deux raisons:

  <\itemize>
    <item>Physiquement cela n'a pas de sens

    <item>Ces parties imaginaires n�gatives n'apparaissent pas dans le calcul
    en espace r�el.
  </itemize>

  On pr�sente ici le r�sultat des investigations pour trouver l'origine de ce
  probl�me.

  Si on essaye de localiser dans les bandes ou apparaissent les valeurs
  n�gatives on peut voir que cela change en fonction du nombre d'atomes
  consid�r� !\ 

  <\itemize>
    <item>Plus le nombre d'atomes augmente plus les valeurs positives et
    n�gatives semblent se morceler.

    <item>La proportion de valeurs n�gatives reste toujours la m�me quelque
    soit le nombre d'atomes (<math|\<approx\>45>%)

    <item>Dans les deux bandes du bas, pr�s de <math|\<Gamma\>> il n'apparait
    jamais de valeurs n�gatives.
  </itemize>

  \;

  <\big-figure>
    <image|biz.png|0.5par|||><image|biz2.png|0.5par|||>

    <image|biz3.png|0.5par|||><image|biz4.png|0.5par|||>
  <|big-figure>
    \;
  </big-figure>

  Essayons maintenant de faire le lien entre les valeurs propres dans
  l'espace r�el et l'espace de Fourier, on va pour �a prendre <math|N=222>
  puis afficher les valeurs propres qui ont les parties imaginaires les plus
  n�gatives:

  <\shell>
    coupable = -96.7525611439639 +i*-6.654961743285425

    coupable = -98.86500385346929 +i*-6.8764674648362245

    coupable = -41.21179668447624 +i*-7.104259490490297

    coupable = -34.493736256194694 +i*-6.940043080101263

    coupable = -95.63530923871383 +i*-6.612032140790298

    \;
  </shell>

  On fait ensuite le lien avec les valeurs propres dans l'espace r�el:

  <\big-figure>
    <image|97.png|0.5par|||><image|40.png|0.5par|||>

    <space|11em><image|34.png|0.5par|||>
  <|big-figure>
    On obtient trois figures car les valeurs propres avec parties r�elles
    -96, -98 et -95 sont toutes trois proches de la valeur propres dans
    l'espace r�el -97
  </big-figure>

  Les deux premi�res images semblent montrer des �tats plut�t localis�s sur
  le bord. Ce n'est pas concluant, on peut alors se demander ce qu'il se
  passe si on affiche les valeurs propres qui posent probl�me dans l'espace
  r�el:

  <\big-figure|<image|neg_part.png|1par|||>>
    Ici pour 40 valeurs qui ont une partie n�gatives dans l'espace de
    Fourier.
  </big-figure>

  Il semble que les valeurs soient regroup�s en petits clusters mais ce n'est
  pas tr�s s�r, pour en savoir plus il fautpeut-�tre afficher toutes les
  valeurs qui posent probl�me et comparer avec la carte dans l'espace de
  Fourier:

  <\big-figure>
    <image|neg_fourier.png|0.8par|||>

    <image|neg_real.png|0.8par|||>
  <|big-figure>
    En haut dans l'espace de Fourier, en bas dans l'espace r�el.
  </big-figure>

  On remarque que la r�partition des valeurs propres, bien que ressemblante
  n'est pas la m�me.

  Testons maintenant le cas\ 

  <\equation*>
    \<Delta\><rsub|B>=0
  </equation*>

  <\big-figure>
    <image|neg_fourier_B0.png|1par|||>

    <image|neg_real_B0.png|1par|||>
  <|big-figure>
    En haut dans l'espace de Fourier, en bas dans l'espace r�el.
  </big-figure>

  Si on se concentre sur les �tats de valeurs propres � partie imaginaire la
  plus grande:

  <\big-figure>
    <image|95.06.png|0.5par|||><image|64.81.png|0.5par|||>

    <image|32.79.png|0.5par|||>
  <|big-figure>
    Ces �tats ne semblent pas localis�s sur les bords.
  </big-figure>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|2>>
    <associate|auto-3|<tuple|3|3>>
    <associate|auto-4|<tuple|4|3>>
    <associate|auto-5|<tuple|5|4>>
    <associate|auto-6|<tuple|6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        On obtient trois figures car les valeurs propres avec parties r�elles
        -96, -98 et -95 sont toutes trois proches de la valeur propres dans
        l'espace r�el -97
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Ici pour 40 valeurs qui ont une partie n�gatives dans l'espace de
        Fourier.
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        En haut dans l'espace de Fourier, en bas dans l'espace r�el.
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        \;
      </surround>|<pageref|auto-5>>
    </associate>
  </collection>
</auxiliary>