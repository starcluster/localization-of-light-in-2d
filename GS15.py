import numpy as np
from scipy.special import erfi
import scipy
import lattice_f as lf
import matplotlib.pyplot as plt
import time
import io
from scipy.optimize import curve_fit
import chern as ch
import generate_mk as gMk
import generateMr as gMr
from multiprocessing import Pool


def band_diagram_cache_dB_dAB(Mks, deltaB, deltaAB):
    B1, B2, B3, B4, X, Y = [], [], [], [], [], []
    omegas, gammas = [], []
    weightas = [[], [], [], []]
    for Mk in Mks:
        lambdas, vects = np.linalg.eig(Mk)

        omega = [0] * 4
        gamma = [0] * 4
        k_norm = np.linalg.norm(k)

        vects = [vects[:, 0], vects[:, 1], vects[:, 2], vects[:, 3]]
        for j in range(4):
            gamma[j] = lambdas[j].imag
        tuples = sorted(zip(lambdas, vects, gamma))

        omega, vects, gamma = (
            [t[0] for t in tuples],
            [t[1] for t in tuples],
            [t[2] for t in tuples],
        )

        v1 = vects[0]
        v2 = vects[1]
        v3 = vects[2]
        v4 = vects[3]

        weighta1 = np.linalg.norm(v1[0]) ** 2 + np.linalg.norm(v1[1]) ** 2
        weighta2 = np.linalg.norm(v2[0]) ** 2 + np.linalg.norm(v2[1]) ** 2
        weighta3 = np.linalg.norm(v3[0]) ** 2 + np.linalg.norm(v3[1]) ** 2
        weighta4 = np.linalg.norm(v4[0]) ** 2 + np.linalg.norm(v4[1]) ** 2

        weightas[0].append(weighta1)
        weightas[1].append(weighta2)
        weightas[2].append(weighta3)
        weightas[3].append(weighta4)
        for j in range(4):
            omega[j] = -0.5 * omega[j].real

        B1.append(omega[0])
        B2.append(omega[1])
        B3.append(omega[2])
        B4.append(omega[3])
        omegas += omega
        gammas += gamma

    return B1, B2, B3, B4, X, Y, omegas, gammas, weightas


def title(distance, deltaB, deltaAB, aho):
    # | $d = $"+str(np.round(distance,2))
    plt.title(
        r"$\Delta_B = $"
        + str(deltaB)
        + " | $\Delta_{AB} = $"
        + str(deltaAB)
        + " | $a_{ho}=$ "
        + str(aho)
        + "| $d = $"
        + str(np.round(distance, 2)),
        fontsize=20,
    )


def plot_band_diagram(B1, B2, B3, B4, Ndots, distance, deltaB, deltaAB, aho):
    xprime = [i for i in range(0, len(B1))]
    plt.figure(figsize=(12, 8))
    plt.axis((0, len(B1), -150, 150))
    plt.scatter(xprime, B1)
    plt.scatter(xprime, B2)
    plt.scatter(xprime, B3)
    plt.scatter(xprime, B4)
    n = int(Ndots / 3)
    plt.axvline(x=n + n * 3 * np.sqrt(3) * 0.05 / 2, color="red")
    plt.axvline(x=n - n * 3 * np.sqrt(3) * 0.05 / 2, color="red")
    title(distance, deltaB, deltaAB, aho)

    # plt.show()


def plot_band_diagram_with_gamma(
    omegas, gammas, Ndots, distance, deltaB, deltaAB, aho, a, k0
):
    x = [i for i in range(0, len(omegas))]
    plt.figure(figsize=(12, 8))
    sc = plt.scatter(x, omegas, marker=".", c=gammas, cmap="jet")
    plt.colorbar()
    n = 4 * int(Ndots / 3)
    print(n)

    plt.axvline(x=n + n * 3 * np.sqrt(3) * 0.05 / 2, color="red", label=r"+$k_0$")
    plt.axvline(x=n - n * 3 * np.sqrt(3) * 0.05 / 2, color="red", label=r"-$k_0$")
    plt.axis((0, len(x), -200, 150))
    plt.legend()
    title(distance, deltaB, deltaAB, aho)
    plt.xticks([0, n, 2 * n, 3 * n], ["$M$", "$\Gamma$", "$K$", "$M$"], fontsize=20)
    plt.savefig("d/" + str(distance) + ".png")

    # plt.show()


def plot_band_diagram_3D(B1, B2, B3, B4, X, Y):
    fig = plt.figure(figsize=(20, 20), constrained_layout=True)
    ax = fig.add_subplot(projection="3d")
    ax.scatter(X, Y, B1)
    ax.scatter(X, Y, B2)
    ax.scatter(X, Y, B3)
    ax.scatter(X, Y, B4)

    title()
    plt.savefig("1.png", bbox_inches="tight")
    plt.show()


def plot_gap_map(lattice, distance, deltaB, deltaAB, k0, aho, plates):
    db = 12
    deltaB_l = np.linspace(-db, db, 24)  # [i for i in range(-12,13)]
    deltaAB_l = deltaB_l

    gapl = np.zeros((deltaAB_l.shape[0], deltaAB_l.shape[0]))

    for i in range(deltaB_l.shape[0]):
        for j in range(deltaAB_l.shape[0]):
            print(i, j)
            B1, B2, B3, B4, X, Y, omegas, gammas = band_diagram(
                lattice, distance, deltaB_l[i], deltaAB_l[j], k0, aho, plates
            )
            # print(B3)
            gapl[i, j] = min(B3) - max(B2)

    plt.imshow(gapl, extent=[-db, db, -db, db])  # , interpolation='spline16')
    plt.colorbar()
    plt.xlabel(r"$\Delta_B$", fontsize=16)
    plt.ylabel(r"$\Delta_{AB}$", fontsize=16)
    plt.title("Gap width", fontsize=16)
    plt.show()


def showing_convergence():
    """plotting convergence of sum as a function of aho"""
    kkk = lf.generate_contour_hexa(a)  # removed 03/05/2023 probably
    # possible to use generate_bz
    # instead
    q1 = kkk[0]
    ahos = np.linspace(0.01, 1, 200)
    for aho in ahos:
        z.append(S1_real_C(q1, 0, 0, aho, k0, distance, K, N))
    plt.plot(ahos, z, label=r"$N=$" + str(N))
    z = []
    N = 10
    for aho in ahos:
        z.append(S1_real_C(q1, 0, 0, aho, k0, distance, K, N))
    plt.plot(ahos, z, label=r"$N=$" + str(N))
    z = []
    N = 15
    for aho in ahos:
        z.append(S1_real_C(q1, 0, 0, aho, k0, distance, K, N))
    plt.plot(ahos, z, label=r"$N=$" + str(N))
    z = []
    N = 20
    for aho in ahos:
        z.append(S1_real_C(q1, 0, 0, aho, k0, distance, K, N))
    plt.plot(ahos, z, label=r"$N=$" + str(N))
    z = []
    N = 30
    for aho in ahos:
        z.append(S1_real_C(q1, 0, 0, aho, k0, distance, K, N))
    plt.plot(ahos, z, label=r"$N=$" + str(N))
    plt.legend()
    plt.title(r"Convergence of Sum as a function of $a_{ho}$", fontsize=20)
    plt.xlabel(r"$a_{ho}$")
    plt.ylabel(r"$R(S_{11})$")
    plt.axis((0, 1, -2, 1))
    plt.show()


def plotting_S_k0a():
    k0s = np.linspace(0.01, 1, 1000)
    s1_reals = []
    s1_imags = []
    s2_reals = []
    s2_imags01 = []
    for k0 in k0s:
        s1_reals.append(S1_real_C(q1, 0, 0, aho, k0 * 2 * np.pi, distance, K, N))
        s1_imags.append(S1_imag_C(q1, 0, 0, aho, k0 * 2 * np.pi, distance, K, N))
        s2_reals.append(S2_real_C(q1, 0, 0, aho, k0 * 2 * np.pi, distance, K, N))
        s2_imags01.append(S2_imag_C(q1, 1, 0, aho, k0 * 2 * np.pi, distance, K, N))
    s1_reals = np.array(s1_reals)
    s1_imags = np.array(s1_imags)
    s2_reals = np.array(s2_reals)
    s2_imags01 = np.array(s2_imags01)

    plt.plot(k0s, -s1_reals, label=r"$-Re(S_1^{00})$")
    plt.plot(k0s, s1_imags, label=r"$Im(S_1^{00})$")
    plt.plot(k0s, s2_reals, label=r"$Re(S_2^{00})$")
    plt.plot(k0s, -s2_imags01, label=r"$-Im(S_2^{01})$", linestyle="--")
    plt.legend()
    plt.loglog()
    plt.title(r"$S(k_0a)$", fontsize=20)
    plt.xlabel(r"$\times 2\pi$", fontsize=20)
    plt.show()


def chern_for_parallel(dAB, dB):
    shift = 0.1
    f_chern = open("chern_map_hd.csv", "a")
    c = np.real(ch.chern(a, N, dB + shift, dAB, distance, aho, k0, step))
    print(a, N, dB, dAB, distance, aho, k0, step, c)
    f_chern.write(str(dB + shift) + "," + str(dAB) + "," + str(c) + "\n")
    f_chern.flush()


if __name__ == "__main__":
    """-------------------"""
    N = 50  # 80
    K = 0
    a = 1
    aho = 0.1  # 0.001
    deltaB, deltaAB = 0, 0
    distance = 11
    k0 = 2 * np.pi * 0.05 / a
    step = 0.1  # for chern, lower value means higher precision.
    Ndots = 200
    """-------------------"""
    # cs = []
    # cs2 = []
    # for dAB in [i for i in range(-12,13)]:
    #     c =  np.real(ch.chern(a, N, deltaB, dAB, distance, aho, k0, step))
    #     # c2 = np.real(ch.chern_continuous(a, N, deltaB, dAB, distance, aho, k0, step))
    #     print(c)
    #     # print(c2)
    #     cs.append(c)
    # print(cs)
    # print(cs2)
    # exit()
    """-------------------"""

    """-------------------
    # vertKprime = np.array([2*np.pi/(3*np.sqrt(3)),2*np.pi/3])
    vertK = np.array([4*np.pi/(3*np.sqrt(3)),0])
    vertK0 = np.array([0.,0.])
    l1,l2 = lf.generate_bz(a, 900, step)

    x,y = [],[]

    # for h in [0.01 + 0.001*i for i in range(60)]:

    #     k0 = 2*np.pi*h
    #     M0 = gMk.generate_mk_wrapper(vertK0, distance, deltaB, deltaAB, k0, aho,N,  a)
    #     M1 = gMk.generate_mk_wrapper(vertK, distance, deltaB, deltaAB, k0, aho,N, a)

    #     x.append(h)
    #     # y.append(np.imag(M0[0,0]))
    #     y.append(M0[2,0])
    # print(y)

    # x = np.array(x)
    # y = np.real(np.array(y))

    # def powerl(x,C01,C02,C03):
    #     return C03/x**3  + C02/x**2 + C01/x


    # popt, pcov = curve_fit(powerl, x, y)

    # print(popt, pcov)

    # # g, A  = popt[0],popt[1]
    # C01,C02,C03 = popt[0],popt[1],popt[2]
    # # C03 = popt[0]


    # plt.plot(x,y)
    # plt.plot(x,C03/x**3)
    # plt.loglog()
    # plt.show()

    # k0 = 2*np.pi*0.05
    M0 = gMk.generate_mk_wrapper(vertK0, distance, deltaB, deltaAB, k0, aho,N,  a)
    M1 = gMk.generate_mk_wrapper(vertK, distance, deltaB, deltaAB, k0, aho,N, a)

    # print("ACE", C03/(k0*a/2/np.pi)**3   + C02/(k0*a/2/np.pi)**2 + C01/(k0*a/2/np.pi))
    print(np.round(M0,2))
    print(-np.real(np.linalg.eig(M0)[0])/2)
    print(np.round(M1,2))
    print(-np.real(np.linalg.eig(M1)[0]/2))

    lattice  = lf.generate_bz(1,Ndots,1)

    B1,B2,B3,B4,X,Y,omegas,gammas,weightas = band_diagram(lattice, distance, deltaB, deltaAB, k0, aho,  N, a)

    plt.plot(B1)
    plt.plot(B2)
    plt.plot(B3)
    plt.plot(B4)
    plt.show()

    -------------------"""
    # B1,B2,B3,B4 = [],[],[],[]
    # distances = [i for i in range(10,200)]
    # for distance in distances:
    #     k0 = 2*np.pi*0.05
    #     deltaB, deltaAB = 12,0

    #     M1 = gMk.generate_mk_wrapper(vertK0, distance/10, deltaB, deltaAB, k0, aho,N,a)
    #     # M2 = gMk.generate_mk_wrapper(vertK, distance, deltaB, deltaAB, k0, aho,N,a)
    #     print(np.round(M1,2))
    #     # print(np.round(M2,2))

    #     omega, vects = scipy.linalg.eig(M1)
    #     omega.sort()
    #     freqs = -np.real(np.round(omega,2)/2)
    #     B1.append(freqs[0])
    #     B2.append(freqs[1])
    #     B3.append(freqs[2])
    #     B4.append(freqs[3])
    #     print(-np.real(np.round(omega,2)/2))
    # plt.plot(distances, B1)
    # plt.plot(distances, B2)
    # plt.plot(distances, B3)
    # plt.plot(distances, B4)
    # plt.show()
    # omega, vects = scipy.linalg.eig(M2)
    # print(-np.round(omega,2)/2)

    # omega.sort()

    # exit()
    """-------------------"""
    # hs = [0.01 + i*0.001 for i in range(1000)]
    # center = []
    # guess = 1/np.array(hs)**(2.5)/100
    # for h in hs:
    #     k0 = 2*np.pi*h/a
    #     vertK = np.array([4*np.pi/(3*np.sqrt(3)),0])
    #     vertK0 = np.array([2*np.pi/(3*np.sqrt(3)),2*np.pi/3])
    #     Mk = gMk.generate_mk_wrapper(vertK, distance, deltaB, deltaAB, k0, aho,N,a )
    #     Mk[0,0] -=   2*deltaB + 2*deltaAB
    #     Mk[1,1] -=  -2*deltaB + 2*deltaAB
    #     Mk[2,2] -=   2*deltaB - 2*deltaAB
    #     Mk[3,3] -=  -2*deltaB - 2*deltaAB
    #     # print(np.round(Mk,2))
    #     center.append(np.round(Mk[0,0],2))
    # plt.plot(hs, -np.array(center))
    # plt.plot(hs, guess)
    # print(hs, center)

    # plt.loglog()
    # plt.show()
    # exit()
    """-------------------"""
    # for i in range(
    f_chern = open("chern_map_hd.csv", "w")
    f_chern.write("deltaB,deltaAB,C\n")
    f_chern.close()
    pool = Pool(8)
    size_chern_map = 25
    dbs = np.linspace(-12, 12, size_chern_map)
    dabs = np.linspace(-12, 12, size_chern_map)

    print(
        pool.starmap(
            chern_for_parallel,
            [
                (dbs[i], dabs[j])
                for j in range(size_chern_map)
                for i in range(size_chern_map)
            ],
        )
    )
    exit()

    f_chern = open("chern_map_hd.csv", "w")
    f_chern.write("deltaB,deltaAB,C\n")
    dB, dAB, C = [], [], []
    # for deltaB_ind in range(-12,12,2):
    #     for deltaAB_ind in range(-12,12,2):

    for deltaB_ind, deltaAB_ind in [(2, 2), (12, 0), (0, 12), (6, 6)]:
        deltaB = deltaB_ind + 0.2
        deltaAB = deltaAB_ind

        c = np.real(ch.chern(a, N, deltaB, deltaAB, distance, aho, k0, step))
        dB.append(deltaB)
        dAB.append(deltaAB)

        print(deltaB, deltaAB, c)
        f_chern.write(str(deltaB) + "," + str(deltaAB) + "," + str(c) + "\n")
        f_chern.flush()
        C.append(np.round(c, 5))

    exit()
    """-------------------"""

    kspacea = lf.generate_bz(a, Ndots, 1)
    # kspacea = lf.generate_contour_hexa(a)n
    # gl.display_lattice(kspacea)

    for distance in [1 + i / 10 for i in range(200)]:
        B1, B2, B3, B4, X, Y, omegas, gammas = band_diagram(
            kspacea, distance, deltaB, deltaAB, k0, aho, N, a
        )
        # print(B1)
        # print(sorted(gammas))
        # plt.scatter(omegas, gammas)
        # plt.show()
        plot_band_diagram_with_gamma(
            omegas, gammas, Ndots, distance, deltaB, deltaAB, aho, a, k0
        )
        # plt.scatter(omegas,gammas)
        # plt.show()
        # plt.clf()
        # plt.cla()
        # plt.scatter(omegas, gammas)
        # plt.show()

    # plot_gap_map(kspacea,distance,deltaB,deltaAB,k0,aho)

    # plot_band_diagram(B1,B2,B3,B4)

    # plot_band_diagram_3D(B1,B2,B3,B4,X,Y)
