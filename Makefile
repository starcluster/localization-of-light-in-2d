CC=gcc
CFLAGS=-c -Wextra -Wall -O2
LDFLAGS=-shared -o generate_mk_plates.so
LIBS=-lm

all: generate_mk_plates.so

generate_mk_plates.so: generate_mk.o Faddeeva.o Li2.o Li3.o
	$(CC) $(LDFLAGS) generate_mk.o Faddeeva.o Li2.o Li3.o $(LIBS)

generate_mk.o: generate_mk.c
	$(CC) $(CFLAGS) generate_mk.c

Faddeeva.o: Faddeeva.c
	$(CC) $(CFLAGS) Faddeeva.c

Li2.o: Li2.c
	$(CC) $(CFLAGS) Li2.c

Li3.o: Li3.c
	$(CC) $(CFLAGS) Li3.c

clean:
	rm -rf *.o generate_mk_plates.so
