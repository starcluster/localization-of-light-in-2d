<TeXmacs|2.1>

<style|<tuple|generic|french>>

<\body>
  \ <doc-data|<doc-title|Fonction de Green dans l'espace de Fourier>>

  <section|R�seau 2D>

  On consid�re un r�seau 2D de <math|N> sites. On distinguera deux types de
  sites <math|A> et <math|B>. Le r�seau est donc constitu� de <math|N/2>
  cellules <math|<around*|{|A,B|}>>.

  Le but de ce document est de montrer comment la fonction de Green suivante:

  <\equation*>
    <wide|<math-bf|G><rsub|0>|\<wide-varleftrightarrow\>><around*|(|<math-bf|><math-bf|r>|)>=-<frac|\<mathe\><rsup|i*k*r>|4\<pi\>r><around*|(|P<around*|(|i*k*r|)><with|font|Bbb|1>+<frac|<math-bf|r>\<otimes\><math-bf|r>|r<rsup|2>>Q<around*|(|i*k*r|)>|)>
  </equation*>

  avec:

  <\equation*>
    P<around*|(|x|)>=1-<frac|1|x>+<frac|1|x<rsup|2>><space|2em>Q<around*|(|x|)>=-1+<frac|3|x>-<frac|3|x<rsup|2>>
  </equation*>

  peut se r��crire dans l'espace de Fourier. Pour cela on supposera le r�seau
  infini p�riodique, on peut donc appliquer le th�or�me de Bloch sur la
  fonction propre <math|\<Psi\>> solution de l'�quation propre:

  <\equation*>
    G\<Psi\>=\<Lambda\>\<Psi\>
  </equation*>

  o� <math|G<rsup|>> est une matrice <math|2N\<times\>2N>. Pour chaque maille
  du r�seau <math|n> on a alors:

  <\equation*>
    \<Psi\><rsub|n>=\<psi\><around*|(|k|)>\<mathe\><rsup|i*k*r<rsub|n>>
  </equation*>

  o�:

  <\equation*>
    \<psi\><around*|(|k|)>=<rsup|t><around*|[|A<rsub|\<sigma\><rsup|+>>,A<rsub|\<sigma\><rsup|->>,B<rsub|\<sigma\><rsup|+>>,B<rsub|\<sigma\><rsup|->>|]><rsup|>
  </equation*>

  Et:

  <\equation*>
    \<Psi\>=<around*|[|\<Psi\><rsub|1>,\<ldots\>,\<Psi\><rsub|N/2>|]>
  </equation*>

  En r�injectant dans l'�quation propre on obtient la forme compacte suivante
  pour calculer les modes propres:

  <\equation*>
    <wide|M|^>\<psi\><around*|(|k|)>=\<Lambda\>\<psi\><around*|(|k|)>
  </equation*>

  o�:

  <\equation*>
    M=<matrix|<tformat|<table|<row|<cell|S<rsub|1><rsup|\<sigma\><rsub|+>\<sigma\><rsub|+>>>|<cell|S<rsub|1><rsup|\<sigma\><rsub|+>\<sigma\><rsub|->>>|<cell|S<rsub|2><rsup|\<sigma\><rsub|+>\<sigma\><rsub|+>>>|<cell|S<rsub|2><rsup|\<sigma\><rsub|+>\<sigma\><rsub|->>>>|<row|<cell|S<rsub|1><rsup|\<sigma\><rsub|->\<sigma\><rsub|+>>>|<cell|S<rsub|1><rsup|\<sigma\><rsub|->\<sigma\><rsub|->>>|<cell|S<rsub|2><rsup|\<sigma\><rsub|->\<sigma\><rsub|+>>>|<cell|S<rsub|2><rsup|\<sigma\><rsub|->\<sigma\><rsub|->>>>|<row|<cell|S<rsub|3><rsup|\<sigma\><rsub|+>\<sigma\><rsub|+>>>|<cell|S<rsub|3><rsup|\<sigma\><rsub|+>\<sigma\><rsub|->>>|<cell|S<rsub|4><rsup|\<sigma\><rsub|+>\<sigma\><rsub|+>>>|<cell|S<rsub|4><rsup|\<sigma\><rsub|+>\<sigma\><rsub|->>>>|<row|<cell|S<rsub|3><rsup|\<sigma\><rsub|->\<sigma\><rsub|+>>>|<cell|S<rsub|3><rsup|\<sigma\><rsub|->\<sigma\><rsub|->>>|<cell|S<rsub|4><rsup|\<sigma\><rsub|->\<sigma\><rsub|+>>>|<cell|S<rsub|4><rsup|\<sigma\><rsub|->\<sigma\><rsub|->>>>>>>
  </equation*>

  et:

  <\eqnarray*>
    <tformat|<table|<row|<cell|S<rsub|1><rsup|\<alpha\>\<beta\>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsub|A*A><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>>|<row|<cell|S<rsub|2><rsup|\<alpha\>\<beta\>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>+<math-bf|a><rsub|1>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>>|<row|<cell|S<rsub|3><rsup|\<alpha\>\<beta\>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>-<math-bf|a><rsub|1>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>>|<row|<cell|S<rsub|4><rsup|\<alpha\>\<beta\>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsub|B*B><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>>>>
  </eqnarray*>

  Notons bien que:

  <\eqnarray*>
    <tformat|<table|<row|<cell|G<rsub|A*A><around*|(|0|)>>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|i+2\<Delta\><rsub|A*B>+2\<Delta\><rsub|B>>|<cell|0>>|<row|<cell|0>|<cell|i+2\<Delta\><rsub|A*B>-2\<Delta\><rsub|B>>>>>>>>|<row|<cell|G<rsub|B*B><around*|(|0|)>>|<cell|=>|<cell|<matrix|<tformat|<table|<row|<cell|i-2\<Delta\><rsub|A*B>+2\<Delta\><rsub|B>>|<cell|0>>|<row|<cell|0>|<cell|i-2\<Delta\><rsub|A*B>-2\<Delta\><rsub|B>>>>>>>>>>
  </eqnarray*>

  Nous allons maintenant simplifier les sommes
  <math|><math|S<rsub|\<nu\>><rsup|\<alpha\>\<beta\>>> � l'aide de la formule
  de sommation de Poisson:

  \;

  On obtient donc:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|sum><rsub|<tabular|<tformat|<table|<row|<cell|<math-bf|r><rsub|m>\<in\>A>>|<row|<cell|<math-bf|r><rsub|m>\<neq\>0>>>>>>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|\<cal-A\>><big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>>g<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|g><rsub|m>-<math-bf|k>;0|)>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>>>>>
  </eqnarray*>

  Avec <math|\<cal-A\>> l'aire de la maille dans le r�seau r�ciproque qui
  v�rifie:

  <\equation*>
    \<cal-A\>=<frac|3<sqrt|3>|2><frac|1|a<rsup|2>>
  </equation*>

  Et:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|sum><rsub|<tabular|<tformat|<table|<row|<cell|<math-bf|r><rsub|m>\<in\>A>>>>>>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>\<pm\><math-bf|a><rsub|1>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>|<cell|=>|<cell|<frac|1|\<cal-A\>><big|sum><rsub|<math-bf|q><rsub|m>\<in\>A<rprime|'>>g<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q><rsub|m>-<math-bf|k>;0|)>\<mathe\><rsup|\<pm\>i*<math-bf|a><rsub|1>\<cdot\><around*|(|<math-bf|q><rsub|m>-<math-bf|k>|)>>>>>>
  </eqnarray*>

  o� <math|<math-bf|q>=q<rsub|x><wide|x|^>+q<rsub|y><wide|y|^>> est un
  vecteur du r�seau r�ciproque.

  Rappelons que la fonction de Green <math|<wide|<math-bf|G><rsub|0>|\<wide-varleftrightarrow\>><around*|(|<math-bf|><math-bf|r>|)>>
  est solution de l'�quation dyadique suivante:

  <\equation*>
    \<nabla\>\<times\>\<nabla\>\<times\><wide|<math-bf|G><rsub|0>|\<wide-varleftrightarrow\>><around*|(|<math-bf|><math-bf|r>,<math-bf|><math-bf|r><rprime|'>|)>-k<rsup|2><wide|<math-bf|G><rsub|0>|\<wide-varleftrightarrow\>><around*|(|<math-bf|><math-bf|r<math|,<math-bf|><math-bf|r><rprime|'>>>|)>=-<wide|<math-bf|I>|\<wide-varleftrightarrow\>>\<delta\><around*|(|<math-bf|r>-<math-bf|r><rprime|'>|)>
  </equation*>

  qui donne le champ �lectrique en <math|<math-bf|r>> caus� par une
  excitation ponctuelle en <math|<math-bf|r><rprime|'>> pour toutes les
  orientations de la source. On passe cette �quation dans le domaine de
  fourier pour la variable <math|<math-bf|r>>:

  <\equation*>
    -<math-bf|K>\<times\><math-bf|K>\<times\><wide|G|^><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q><rsub|>;<math-bf|r><rprime|'>|)>-k<rsup|2><wide|G|^><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>;<rsub|><math-bf|r><rprime|'>|)>=-<wide|<math-bf|I>|\<wide-varleftrightarrow\>>\<mathe\><rsup|i<math-bf|q>\<cdot\><math-bf|r><rprime|'>>
  </equation*>

  Alors on peut en d�duire sous r�serve d'inversibilit�:

  <\equation*>
    <wide|G|^><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q><rsub|>;<math-bf|r><rprime|'>|)>=<around*|(|<math-bf|K>\<times\><math-bf|K>+k<rsup|2>I<rsub|3>|)><rsup|-1>\<mathe\><rsup|i<math-bf|q>\<cdot\><math-bf|r><rprime|'>>
  </equation*>

  L'op�rateur <math|<math-bf|K>> s'�crit:

  <\equation*>
    <math-bf|K>=<matrix|<tformat|<table|<row|<cell|0>|<cell|-q<rsub|z>>|<cell|q<rsub|y>>>|<row|<cell|-q<rsub|z>>|<cell|0>|<cell|-q<rsub|x>>>|<row|<cell|q<rsub|y>>|<cell|q<rsub|x>>|<cell|0>>>>>
  </equation*>

  Alors:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<around*|(|<math-bf|K>\<times\><math-bf|K>+k<rsup|2>I<rsub|3>|)><rsup|-1>>|<cell|=>|<cell|<frac|1|k<rsup|2>><frac|1|q<rsub|x><rsup|2>+q<rsub|y><rsup|2>+q<rsub|z><rsup|2>-k<rsup|2><rsup|>><matrix|<tformat|<table|<row|<cell|q<rsub|x><rsup|2>-k<rsup|2>>|<cell|q<rsub|x>q<rsub|y>>|<cell|q<rsub|x>q<rsub|z>>>|<row|<cell|q<rsub|x>q<rsub|y>>|<cell|q<rsub|y><rsup|2>-k<rsup|2>>|<cell|q<rsub|y>q<rsub|z>>>|<row|<cell|q<rsub|x>q<rsub|z>>|<cell|q<rsub|y>q<rsub|z>>|<cell|q<rsub|z><rsup|2>-k<rsup|2>>>>>>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|k<rsup|2>><frac|1|q<rsub|x><rsup|2>+q<rsub|y><rsup|2>+q<rsub|z><rsup|2>-k<rsup|2>><around*|(|<math-bf|q><rsup|t><math-bf|q>-k*<rsup|2>I<rsub|3>|)>>>>>
  </eqnarray*>

  Cela donne pour <math|g>:

  <\equation*>
    g<rsub|\<alpha\>\<beta\>><around*|(|<math-bf|q><rsub|>;<math-bf|r><rprime|'>|)>=<frac|<around*|(|<math-bf|>k<rsup|2>*\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>|2\<pi\>k<rsup|2>>\<mathe\><rsup|i<math-bf|q>\<cdot\><math-bf|r><rprime|'>><big|int><frac|1|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2><rsup|>>\<mathd\>q<rsub|z>
  </equation*>

  Pour assurer la convergence de l'int�grale on introduit un cut-off
  gaussien:

  <\equation*>
    g<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|q><rsub|>;<math-bf|r><rprime|'>|)>=<frac|<around*|(|<math-bf|>k<rsup|2>*\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>|2\<pi\>k<rsup|2>>\<mathe\><rsup|i<math-bf|q>\<cdot\><math-bf|r><rprime|'>><big|int><frac|\<mathe\><rsup|-a<rsup|2><around*|(|<math-bf|q><rsup|2>+q<rsub|z><rsup|2>|)>/2>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2>>\<mathd\>q<rsub|z>=<frac|<around*|(|<math-bf|>k<rsup|2>*\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>|2\<pi\>k<rsup|2>>\<mathe\><rsup|i<math-bf|q>\<cdot\><math-bf|r><rprime|'>>B<around*|(|<math-bf|q>|)>
  </equation*>

  On se concentre maintenant sur le calcul de l'int�grale:

  <\equation*>
    B<around*|(|<math-bf|q>|)>=<big|int><frac|\<mathe\><rsup|-a<rsup|2><around*|(|<math-bf|q><rsup|2>+q<rsub|z><rsup|2>|)>/2>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2>>\<mathd\>q<rsub|z>
  </equation*>

  Rappelons d'abord que l'on a la formule suivante (d�mo en annexe):

  <\equation*>
    <block|<tformat|<table|<row|<cell|w<around*|(|z|)>=\<mathe\><rsup|-z<rsup|2>><around*|(|1+<frac|2i|<sqrt|\<pi\>>><big|int><rsub|0><rsup|z>\<mathe\><rsup|t<rsup|2>>\<mathd\>t|)>=<frac|2i*z|\<pi\>><big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-t<rsup|2>>\<mathd\>t|z<rsup|2>-t<rsup|2>><space|1em>,\<Im\><around*|(|z|)>\<gtr\>0>>>>>
  </equation*>

  \;

  On peut maintenant calculer notre int�grale en posant
  <math|t<rsup|2>=><math|a<rsup|2>q<rsub|z><rsup|2>/2> et
  <math|2\<Lambda\><around*|(|<math-bf|q>|)><rsup|2>=a<rsup|2><around*|(|k<rsup|2>-<math-bf|q><rsup|2>|)>>

  <\equation*>
    B<around*|(|<math-bf|q>|)>=<big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|-a<rsup|2>q<rsub|z><rsup|2>/2>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2>>\<mathd\>q<rsub|z>=2<big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-a<rsup|2>q<rsub|z><rsup|2>/2>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2>>\<mathd\>q<rsub|z>=<sqrt|2>a<big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-t<rsup|2>>|\<Lambda\><around*|(|<math-bf|q>|)><rsup|2>-t<rsub|><rsup|2>>\<mathd\>t
  </equation*>

  D'apr�s la formule pr�c�demment d�montr�e:

  <\equation*>
    <sqrt|2>a<big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-t<rsup|2>>|\<Lambda\><around*|(|<math-bf|q>|)><rsup|2>-t<rsub|><rsup|2>>\<mathd\>t=<frac|a|<sqrt|2>><frac|\<pi\>\<mathe\><rsup|-\<Lambda\><around*|(|<math-bf|q>|)><rsup|2>>|i*\<Lambda\><around*|(|<math-bf|q>|)>><around*|(|1+<frac|2i|<sqrt|\<pi\>>><big|int><rsub|0><rsup|\<Lambda\><around*|(|<math-bf|q>|)>>\<mathe\><rsup|t<rsup|2>>\<mathd\>t|)>
  </equation*>

  On introduit la fonction erreur complexe:

  <\equation*>
    erfi<around*|(|x|)>=-i*erf<around*|(|i*x|)>=<frac|2|<sqrt|\<pi\>>><big|int><rsub|0><rsup|x>\<mathe\><rsup|t<rsup|2>>\<mathd\>t
  </equation*>

  Donc:

  <\equation*>
    <frac|a|<sqrt|2>><frac|\<pi\>\<mathe\><rsup|-\<Lambda\><around*|(|<math-bf|q>|)><rsup|2>>|i*\<Lambda\><around*|(|<math-bf|q>|)>><around*|(|1+<frac|2i|<sqrt|\<pi\>>><big|int><rsub|0><rsup|\<Lambda\><around*|(|<math-bf|q>|)>>\<mathe\><rsup|t<rsup|2>>\<mathd\>t|)>=<frac|a|<sqrt|2>><frac|\<pi\>\<mathe\><rsup|-\<Lambda\><around*|(|<math-bf|q>|)><rsup|2>>|\<Lambda\><around*|(|<math-bf|q>|)>><around*|(|-i+*erfi<around*|(|\<Lambda\><around*|(|<math-bf|q>|)>|)>|)>
  </equation*>

  On remet le facteur <math|\<mathe\><rsup|-a<rsup|2><math-bf|q><rsup|2>/2>>:

  <\equation*>
    B<around*|(|<math-bf|q>|)>=<big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|-a<rsup|2><around*|(|<math-bf|q><rsup|2>+q<rsub|z><rsup|2>|)>/2>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2>>\<mathd\>q<rsub|z>=<frac|a|<sqrt|2>><frac|\<pi\>\<mathe\><rsup|-k<rsup|2>a<rsup|2>>|\<Lambda\><around*|(|<math-bf|q>|)>><around*|(|-i+*erfi<around*|(|\<Lambda\><around*|(|<math-bf|q>|)>|)>|)>
  </equation*>

  Pour �tre en accord avec l'article de J. Perczel on pose
  <math|\<Lambda\><rprime|'>=<sqrt|2>\<Lambda\>/a>

  <\equation*>
    B<around*|(|<math-bf|q>|)>=<frac|\<pi\>\<mathe\><rsup|-k<rsup|2>a<rsup|2>>|\<Lambda\><rprime|'><around*|(|<math-bf|q>|)>><around*|(|-i+*erfi<around*|(|a\<Lambda\><rprime|'><around*|(|<math-bf|q>|)>/<sqrt|2>|)>|)>
  </equation*>

  En rajoutant le facteur <math|<frac|1|2\<pi\>k<rsup|2>>> on obtient
  finalement:

  <\equation*>
    <block|<tformat|<table|<row|<cell|<frac|1|2\<pi\>k<rsup|2>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|-a<rsup|2><around*|(|<math-bf|q><rsup|2>+q<rsub|z><rsup|2>|)>/2>|k<rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2>>\<mathd\>q<rsub|z>=<frac|1|2\<pi\>k<rsup|2>><frac|\<pi\>\<mathe\><rsup|-k<rsup|2>a<rsup|2>>|\<Lambda\><rprime|'><around*|(|<math-bf|q>|)>><around*|(|-i+*erfi<around*|(|a\<Lambda\><rprime|'><around*|(|<math-bf|q>|)>/<sqrt|2>|)>|)>>>>>>
  </equation*>

  Ce qui donne:

  <\equation*>
    <block|<tformat|<table|<row|<cell|g<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|q>;0|)>=<around*|(|\<delta\><rsub|\<alpha\>\<beta\>>k<rsup|2>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>\<cal-I\><around*|(|<math-bf|q>|)>>>>>>
  </equation*>

  Avec:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<cal-I\><around*|(|<math-bf|q>|)>>|<cell|=>|<cell|\<chi\><around*|(|<math-bf|q>|)><frac|\<pi\>|\<Lambda\><around*|(|<math-bf|q<strong|>>|)>><around*|(|-i+erfi<around*|(|a\<Lambda\><around*|(|<math-bf|q<strong|>>|)>/<sqrt|2>|)>|)>>>|<row|<cell|\<chi\><around*|(|<math-bf|q>|)>>|<cell|=>|<cell|<frac|1|2\<pi\>k<rsup|2>>\<mathe\><rsup|-<around*|(|k*a|)><rsup|2>/2>>>|<row|<cell|\<Lambda\><around*|(|<math-bf|q>|)>>|<cell|=>|<cell|<sqrt|k<rsup|2>-q<rsup|2>>>>>>
  </eqnarray*>

  <strong|Cela correspond aux �quations S10-S13 de supplementarial material.>

  On veut ensuite calculer <math|G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>>
  en passant par la tranform�e de Fourier inverse de
  <math|g<rsub|\<alpha\>\<beta\>><rsup|\<ast\>><around*|(|<math-bf|q<strong|>>|)>>,
  �valu�e en 0, c'est � dire que:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int>g<rsub|\<alpha\>\<beta\>><rsup|\<ast\>><around*|(|<math-bf|q<strong|>>|)>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>
  </equation*>

  Donc:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><frac|1|k<rsup|2>><frac|k<rsub|><rsup|2>\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|k<rsub|><rsup|2>-q<rsup|2>>\<mathe\><rsup|-q<rsup|2>a<rsup|2>>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>
  </equation*>

  On peut donc �crire en ajoutant et soustrayant
  <math|q<rsup|2>\<delta\><rsub|\<alpha\>\<beta\>>>:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><frac|1|k<rsup|2>><around*|(|1+<frac|q<rsup|2>\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|k<rsub|><rsup|2>-q<rsup|2>>|)>\<mathe\><rsup|-q<rsup|2>a<rsup|2>>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>
  </equation*>

  Calculons donc la triple int�grale de Gauss:

  <\equation*>
    <frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><frac|1|k<rsup|2>>\<mathe\><rsup|-q<rsup|2>a<rsup|2>>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><frac|1|k<rsup|2>><big|int><rsub|\<bbb-R\>>\<mathe\><rsup|-q<rsub|z><rsup|2>a<rsup|2>><big|int><rsub|\<bbb-R\>>\<mathe\><rsup|-q<rsub|y><rsup|2>a<rsup|2>><big|int><rsub|\<bbb-R\>>\<mathe\><rsup|-q<rsub|x><rsup|2>a<rsup|2>>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>=<around*|(|<frac|<sqrt|\<pi\>>|2\<pi\>>|)><rsup|3><frac|1|k<rsup|2>><frac|1|a<rsup|3>>
  </equation*>

  Finalement:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><frac|1|k<rsup|2>><around*|(|<frac|q<rsup|2>\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|k<rsub|><rsup|2>-q<rsup|2>>|)>\<mathe\><rsup|-q<rsup|2>a<rsup|2>>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>+<frac|\<delta\><rsub|\<alpha\>\<beta\>>|<around*|(|2a<sqrt|\<pi\>>|)><rsup|3>k<rsup|2>>
  </equation*>

  \;

  Il faut maintenant calculer:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><frac|1|k<rsup|2>><frac|q<rsub|><rsup|2>\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|k<rsub|><rsup|2>-q<rsup|2>>\<mathe\><rsup|-q<rsup|2>a<rsup|2>>\<mathd\>q<rsub|x>\<mathd\>q<rsub|y>\<mathd\>q<rsub|z>
  </equation*>

  On passe en coordonn�es sph�riques en posant <with|color|red|(convention
  <math|\<theta\>\<leftrightarrow\>\<varphi\>>)>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|q<rsub|x>>|<cell|=>|<cell|r*cos<around*|(|\<theta\>|)>sin<around*|(|\<varphi\>|)>>>|<row|<cell|q<rsub|y>>|<cell|=>|<cell|r*sin<around*|(|\<theta\>|)>sin<around*|(|\<varphi\>|)>>>|<row|<cell|q<rsub|z>>|<cell|=>|<cell|r*cos<around*|(|\<varphi\>|)>>>>>
  </eqnarray*>

  On obtient l'int�grale triple suivante:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|r=0><rsup|r=\<infty\>><big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><big|int><rsub|\<varphi\>=0><rsup|\<varphi\>=\<pi\>><frac|1|k<rsup|2>><frac|q<rsub|><rsup|2>\<delta\><rsub|\<alpha\>\<beta\>>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|k<rsup|2>-r<rsup|2>>\<mathe\><rsup|-r<rsup|2>a<rsup|2>>r<rsup|2>sin<around*|(|\<varphi\>|)>\<mathd\>\<varphi\>\<mathd\>\<theta\>\<mathd\>r
  </equation*>

  On distingue alors 3 cas:

  <\itemize>
    <item><math|\<alpha\>=\<beta\>=x>
  </itemize>

  <\equation*>
    G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|r=0><rsup|r=\<infty\>><frac|1|k<rsup|2>><frac|\<mathe\><rsup|-r<rsup|2>a<rsup|2>>r<rsup|4>|k<rsup|2>-r<rsup|2>><big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><big|int><rsub|\<varphi\>=0><rsup|\<varphi\>=\<pi\>><around*|(|1-<rsup|>cos<around*|(|\<theta\>|)><rsup|2>sin<around*|(|\<varphi\>|)><rsup|2>|)>sin<around*|(|\<varphi\>|)>\<mathd\>\<varphi\>\<mathd\>\<theta\>\<mathd\>r
  </equation*>

  On int�gre sur <math|\<varphi\>>:

  <\equation*>
    G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|r=0><rsup|r=\<infty\>><frac|1|k<rsup|2>><frac|\<mathe\><rsup|-r<rsup|2>a<rsup|2>>r<rsup|4>|k<rsup|2>-r<rsup|2>><big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><around*|(|2-<frac|4|3>cos<around*|(|\<theta\>|)><rsup|2>|)>\<mathd\>\<theta\>\<mathd\>r
  </equation*>

  Puis sur <math|\<theta\>>:

  <\equation*>
    G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|r=0><rsup|r=\<infty\>><frac|1|k<rsup|2>><frac|\<mathe\><rsup|-r<rsup|2>a<rsup|2>>r<rsup|4>|k<rsup|2>-r<rsup|2>><around*|(|2\<times\>2\<pi\>-<frac|4|3><rsup|><rsup|>\<pi\>|)>\<mathd\>r=<frac|1|3\<pi\><rsup|2>><big|int><rsub|r=0><rsup|r=\<infty\>><frac|\<mathe\><rsup|-r<rsup|2>a<rsup|2>>r<rsup|4>|k<rsup|2>-r<rsup|2>>\<mathd\>r
  </equation*>

  Et enfin sur <math|r>:

  <\equation*>
    3k<rsup|2>\<pi\><rsup|2>G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|\<pi\>k<rsup|3>\<mathe\><rsup|-k<rsup|2>a<rsup|2>><around*|(|i+erfi<around*|(|a*k|)>|)>|2>-i\<pi\>k<rsup|3>\<mathe\><rsup|-k<rsup|2>a<rsup|2>>-<frac|<sqrt|\<pi\>>k<rsup|2>|2a>-<frac|<sqrt|\<pi\>>|4a<rsup|3>>
  </equation*>

  En simplifiant on obtient:

  <\equation*>
    3k<rsup|2>\<pi\><rsup|2>G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|\<pi\>k<rsup|3>\<mathe\><rsup|-k<rsup|2>a<rsup|2>><around*|(|-i+erfi<around*|(|a*k|)>|)>|2>-<sqrt|\<pi\>><frac|2<around*|(|k*a|)><rsup|2>+1|4a<rsup|3>>
  </equation*>

  Puis:

  <\equation*>
    G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|k<rsup|>\<mathe\><rsup|-k<rsup|2>a<rsup|2>><around*|(|-i+erfi<around*|(|a*k|)>|)>|6\<pi\>>-<frac|<around*|(|k*a|)><rsup|2>+1/2|6a<rsup|3>\<pi\><sqrt|\<pi\>>k<rsup|2>>
  </equation*>

  On factorise par <math|k/6\<pi\>>:

  <\equation*>
    G<rsub|x*x><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|k|6\<pi\>><around*|[|<frac|-i+erfi<around*|(|a*k|)>|\<mathe\><rsup|k<rsup|2>a<rsup|2>>>-<frac|2<around*|(|k*a|)><rsup|2>+1|2a<rsup|3>k<rsup|3><sqrt|\<pi\>>>|]>
  </equation*>

  <\itemize>
    <item>Dans le cas <math|\<alpha\>=\<beta\>=y> le calcul est similaire:
  </itemize>

  <\equation*>
    G<rsub|y*y><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|k|6\<pi\>><around*|[|<frac|-i+erfi<around*|(|a*k|)>|\<mathe\><rsup|k<rsup|2>a<rsup|2>>>-<frac|2<around*|(|k*a|)><rsup|2>+1|2a<rsup|3>k<rsup|3><sqrt|\<pi\>>>|]>
  </equation*>

  <\itemize>
    <item>Dans le cas <math|\<alpha\>\<neq\>\<beta\>>:
  </itemize>

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|1|<around*|(|2\<pi\>|)><rsup|3>><big|int><rsub|r=0><rsup|r=\<infty\>><big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><big|int><rsub|\<varphi\>=0><rsup|\<varphi\>=\<pi\>><frac|1|k<rsup|2>><frac|<around*|(|-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>|k<rsup|2>-r<rsup|2>>\<mathe\><rsup|-r<rsup|2>a<rsup|2>>r<rsup|2>sin<around*|(|\<varphi\>|)>\<mathd\>\<varphi\>\<mathd\>\<theta\>\<mathd\>r
  </equation*>

  Dans ce cas le produit:

  <\equation*>
    q<rsub|\<alpha\>>q<rsub|\<beta\>>=r<rsup|2>cos<around*|(|\<theta\>|)>sin<around*|(|\<theta\>|)>sin<around*|(|\<varphi\>|)><rsup|2>
  </equation*>

  Donne une int�grale sur th�ta nulle donc:

  <\equation*>
    G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=0
  </equation*>

  Finalement on obtient:

  <\equation*>
    <block*|<tformat|<table|<row|<cell|G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)><rprime|'>=<frac|k|6\<pi\>><around*|(|<frac|-i+erfi<around*|(|a*k|)>|\<mathe\><rsup|k<rsup|2>a<rsup|2>>>-<frac|2<around*|(|k*a|)><rsup|2>+1|2a<rsup|3>k<rsup|3><sqrt|\<pi\>>>|)>\<delta\><rsub|\<alpha\>\<beta\>>>>>>>
  </equation*>

  \;

  \;

  Correspond bien � l'�quation (10) de Antezza et Castin. En rajoutant le
  terme <math|<frac|\<delta\><rsub|\<alpha\>\<beta\>>|<around*|(|2a<sqrt|\<pi\>>|)><rsup|3>k<rsup|2>>>
  et en posant <math|a\<rightarrow\>a/<sqrt|2>> <strong|on obtient bien
  l'�quation (S14)> de \S Topological quantum optics suppl \T:

  <\equation*>
    <block*|<tformat|<table|<row|<cell|G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>=<frac|k|6\<pi\>><around*|(|<frac|erfi<around*|(|k*a/<sqrt|2>|)>-i|\<mathe\><rsup|k<rsup|2>*a<rsup|2>/2>>-<frac|-1/2+<around*|(|k*a|)><rsup|2>|<sqrt|\<pi\>/2><around*|(|k*a|)><rsup|3>>|)>\<delta\><rsub|\<alpha\>\<beta\>>>>>>>
  </equation*>

  \;

  On peut maintenant s'int�resser aux conditions de convergence de la somme:

  <\equation*>
    <frac|1|\<cal-A\>><big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>>g<rsub|><rsup|><rsub|\<alpha\>\<beta\>><rsup|\<ast\>><around*|(|<math-bf|g><rsub|m>-<math-bf|k>;0|)>
  </equation*>

  On aura convergence � condition que pour une certaine valeur de
  <math|<around*|\||<math-bf|g><rsub|m>-<math-bf|k>|\|>> les termes dans la
  somme soient nuls, c-�-d qu'on va avoir besoin de:

  <\equation*>
    -i+*erfi<around*|(|a\<Lambda\><rprime|'><around*|(|<math-bf|q>|)>/<sqrt|2>|)>\<approx\>0
  </equation*>

  Or on sait que <math|erfi<around*|(|i*x|)><below|\<rightarrow\>|x\<rightarrow\>+\<infty\>>i>
  donc il nous faut:

  <\equation*>
    \<Lambda\><rprime|'><around*|(|<math-bf|q>|)>\<approx\><around*|\||<math-bf|g><rsub|m>|\|>\<ggg\><frac|1|a>
  </equation*>

  <section|R�seau confin� entre des plaques>

  On reprend maintenant l'analyse pr�c�dente mais en l'�tendant au cas d'un
  r�seau confin� entre deux plaques. On reprend au niveau de la tranformation
  de Poisson. Le r�seau <math|A<rprime|'>> repr�sente maintenant un r�seau
  hexagonal r�ciproque muni d'une troisi�me dimension par le biais des
  miroirs

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|sum><rsub|<tabular|<tformat|<table|<row|<cell|<math-bf|r><rsub|m>\<in\>A>>|<row|<cell|<math-bf|r><rsub|m>\<neq\>0>>>>>>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|r><rsub|m>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsub|m>>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|<math-bf|r><rsub|m>\<in\>A><big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n>G<rsup|\<alpha\>\<beta\>><rsub|free><rsub|><around*|(|<math-bf|r><rsub|m><rsup|n>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsup|n><rsub|m>>-<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n>G<rsup|\<alpha\>\<beta\>><rsub|free><rsub|><around*|(|<math-bf|0>+n*d*<math-bf|e<rsub|z>>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><big|sum><rsub|<math-bf|r><rsub|m>\<in\>A>G<rsup|\<alpha\>\<beta\>><rsub|free><rsub|><around*|(|<math-bf|r><rsub|m><rsup|n>|)>\<mathe\><rsup|i*<math-bf|k>\<cdot\><math-bf|r><rsup|n><rsub|m>>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|n=-\<infty\>><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>|\<cal-A\>><big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>>g<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|g><rsup|n><rsub|m>-<math-bf|k>;n*d|)>\<mathe\><rsup|i*<math-bf|a><rsub|1><around*|(|<math-bf|g><rsup|n><rsub|m>-<math-bf|k>|)>>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>>>|<row|<cell|>|<cell|=>|<cell|\<Omega\>-G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>|)>>>>>
  </eqnarray*>

  \;

  o� dans l'espace r�el on a <math|<math-bf|r><rsub|n><rsup|><rsub|>=<math-bf|R>+n*d*e<rsub|z>+z*e<rsub|z>>
  et dans l'espace r�ciproque <math|<math-bf|q>=<math-bf|Q>+q<rsub|z>u<rsub|z>>
  donc:

  <\equation*>
    <math-bf|q>\<cdot\><math-bf|r><rsub|n>=<math-bf|Q>\<cdummy\>*<math-bf|R>+q<rsub|z>n*d+q<rsub|z>z
  </equation*>

  On calcule alors la transform�e de Fourier pour <math|n\<neq\>0>:

  <\equation*>
    g<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|g><rsup|n><rsub|m>-<math-bf|k>;n*d|)>=<frac|<around*|(|\<delta\><rsup|\<alpha\>\<beta\>>k<rsub|0><rsup|2>-q<rsup|\<alpha\>>q<rsup|\<beta\>>|)>|2\<pi\>k<rsub|0><rsup|2>><big|int><frac|\<mathe\><rsup|i*q<rsub|z>n*d>|k<rsub|0><rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2><rsup|>>\<mathd\>q<rsub|z>=<around*|(|\<delta\><rsup|\<alpha\>\<beta\>>k<rsub|0><rsup|2>-q<rsup|\<alpha\>>q<rsup|\<beta\>>|)>D<around*|(|<math-bf|q>,n|)>
  </equation*>

  \;

  On pose:

  <\equation*>
    D<around*|(|<math-bf|q>,n|)>=<frac|1|2\<pi\>k<rsub|0><rsup|2>><big|int><frac|\<mathe\><rsup|i*q<rsub|z>n*d>|k<rsub|0><rsup|2>-<math-bf|q><rsup|2>-q<rsub|z><rsup|2><rsup|>>\<mathd\>q<rsub|z>=<frac|1|2\<pi\>k<rsub|0><rsup|2>><big|int><rsub|><frac|\<mathe\><rsup|i*q<rsub|z>*n*d>|\<omega\><rsup|2>-q<rsub|z><rsub|><rsup|2><rsup|>>\<mathd\>q<rsub|z>
  </equation*>

  \;

  Remarquons que on peut avoir <math|\<omega\><rsup|2>=k<rsub|0><rsup|2>-><math|<math-bf|q<rsub|\<bot\>><rsup|>><rsup|2>\<less\>0>,
  les p�les se trouvent donc des fois sur l'axe r�el et d'autres fois sur
  l'axe imaginaire. Quand ils se trouvent sur l'axe r�el on d�calera le p�le
  positif vers le haut et le p�le n�gatif vers le bas.\ 

  <\equation*>
    D<around*|(|<math-bf|q>,n|)>=<frac|1|2\<pi\>k<rsub|0><rsup|2>><big|int><rsub|><frac|\<mathe\><rsup|i*q<rsub|z>*n*d>|<around*|(|\<omega\><rsup|>-q<rsub|z><rsub|><rsup|><rsup|>|)><around*|(|\<omega\><rsup|>+q<rsub|z><rsub|><rsup|><rsup|>|)>>\<mathd\>q<rsub|z>=<frac|1|2\<pi\>k<rsub|0><rsup|2>><big|int><rsub|><frac|-\<mathe\><rsup|i*q<rsub|z>*n*d>|<around*|(|q<rsub|z><rsub|><rsup|><rsup|>-\<omega\>|)><around*|(|q<rsub|z><rsub|><rsup|><rsup|>+\<omega\><rsup|>|)>>\<mathd\>q<rsub|z>
  </equation*>

  On peut alors calculer l'int�grale en passant par le plan complexe et en
  distinguant le cas <math|n\<gtr\>0> et <math|n\<less\>0> car on passera une
  fois au dessus de l'axe r�elle (si <math|n\<gtr\>0> on veut
  <math|sin<around*|(|\<theta\>|)>\<gtr\>0> pour appliquer le lemme de
  Jordan), une fois en dessous (si <math|n\<less\>0> on veut
  <math|sin<around*|(|\<theta\>|)>\<less\>0>):

  <\equation*>
    D<around*|(|<math-bf|q>,n|)>=<frac|-1|2\<pi\>k<rsub|0><rsup|2>>2\<pi\>i<around*|(|\<theta\><around*|(|n|)>Res<around*|(|\<omega\>|)>-\<theta\><around*|(|-n|)>Res<around*|(|-\<omega\>|)>|)>
  </equation*>

  Or:

  <\equation*>
    Res<around*|(|\<omega\>|)>=<frac|\<mathe\><rsup|i\<omega\>n*d>|2\<omega\>>
    <infix-and> Res<around*|(|-\<omega\>|)>=<frac|-\<mathe\><rsup|-i\<omega\>n*d>|2\<omega\>>
  </equation*>

  \;

  Donc:

  <\equation*>
    D<around*|(|<math-bf|q>,n|)>=<frac|-i|2\<omega\>k<rsub|0><rsup|2>><around*|(|\<theta\><around*|(|n|)>\<mathe\><rsup|i\<omega\>n*d>+\<theta\><around*|(|-n|)>\<mathe\><rsup|-i\<omega\>n*d>|)>=<frac|-i|2\<omega\>k<rsub|0><rsup|2>>\<mathe\><rsup|i\<omega\><around*|\||n|\|>*d>
  </equation*>

  En r�introduisant dans la somme et en posant
  <math|\<Delta\><rsup|\<alpha\>\<beta\>>=><math|\<delta\><rsup|\<alpha\>\<beta\>>k<rsub|0><rsup|2>-q<rsup|\<alpha\>>q<rsup|\<beta\>>>

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Omega\><rsup|\<alpha\>\<beta\>>>|<cell|=>|<cell|<big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>><big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>|\<cal-A\>>\<Delta\><rsup|\<alpha\>\<beta\>>D<around*|(|<math-bf|q>,n|)>\<mathe\><rsup|i*<math-bf|a><rsub|1><around*|(|<math-bf|g><rsup|><rsub|m>-<math-bf|k>|)>>+<frac|1|\<cal-A\>><big|sum>\<Delta\><rsup|\<alpha\>\<beta\>>\<cal-I\><around*|(|<math-bf|q>|)>\<mathe\><rsup|i*<math-bf|a><rsub|1><around*|(|<math-bf|g><rsup|><rsub|m>-<math-bf|k>|)>>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>><frac|\<Delta\><rsup|\<alpha\>\<beta\>>|\<cal-A\>><frac|\<pi\>i|\<omega\>k<rsub|0><rsup|2>><frac|1|1+\<mathe\><rsup|-i\<omega\>d>>\<mathe\><rsup|i*<math-bf|a><rsub|1><around*|(|<math-bf|g><rsup|><rsub|m>-<math-bf|k>|)>>+<frac|1|\<cal-A\>><big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>>\<Delta\><rsup|\<alpha\>\<beta\>>\<cal-I\><around*|(|<math-bf|q>|)>\<mathe\><rsup|i*<math-bf|a><rsub|1><around*|(|<math-bf|g><rsup|><rsub|m>-<math-bf|k>|)>>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|<math-bf|g><rsub|m>\<in\>A<rsup|<rprime|'>>><frac|\<Delta\><rsup|\<alpha\>\<beta\>>|\<cal-A\>>\<mathe\><rsup|i*<math-bf|a><rsub|1><around*|(|<math-bf|g><rsup|><rsub|m>-<math-bf|k>|)>><around*|(|\<cal-I\><around*|(|<math-bf|q>|)>+<frac|i|\<omega\>k<rsub|0><rsup|2>><frac|1|1+\<mathe\><rsup|-i\<omega\>d>>|)>>>>>
  </eqnarray*>

  O� on a calcul� la somme:

  <\equation*>
    <big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|-i|2\<omega\>k<rsub|0><rsup|2>>\<mathe\><rsup|i\<omega\><around*|\||n|\|>*d>=<frac|-i|\<omega\>k<rsub|0><rsup|2>><big|sum><rsup|+\<infty\>><rsub|n=1><around*|(|-<rsup|>\<mathe\><rsup|i\<omega\>*d>|)><rsup|n>=<frac|i|\<omega\>k<rsub|0><rsup|2>><frac|<rsup|>\<mathe\><rsup|i\<omega\>*d>|1+<rsup|>\<mathe\><rsup|i\<omega\>*d>>
  </equation*>

  Et cela sous r�serve que <math|\<omega\>d\<neq\>\<pi\>> !

  Finalement:

  <\equation*>
    <block|<tformat|<table|<row|<cell|g<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|q>;0|)>=<around*|(|\<delta\><rsub|\<alpha\>\<beta\>>k<rsup|2>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)><around*|(|\<cal-I\><around*|(|<math-bf|q>|)>+<frac|i|\<omega\>k<rsub|0><rsup|2>><frac|1|1+\<mathe\><rsup|-i\<omega\>d>>|)>>>>>>
  </equation*>

  On peut maintenant proc�der � un calcul similaire pour
  <math|G<rsub|\<alpha\>\<beta\>><rsup|*><around*|(|<math-bf|0>|)>-G<rsup|n=0><rsub|\<alpha\>\<beta\>><rsup|*><around*|(|<math-bf|0>|)>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n>G<rsub|><rsup|\<alpha\>\<beta\>><around*|(|<math-bf|0>+n*d*<math-bf|e<rsub|z>>|)>>|<cell|=>|<cell|<big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|1|<around*|(|2\<pi\>|)><rsup|3>k<rsub|0><rsup|2>><big|int><frac|<around*|(|\<delta\><rsub|\<alpha\>\<beta\>>k<rsub|0><rsup|2>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>\<mathe\><rsup|i*q<rsub|z>*n*d>|\<omega\><rsup|2>-q<rsub|z><rsub|><rsup|2><rsup|>>\<mathd\>q<rsub|z>\<mathd\><rsup|2>q<rsub|\<bot\>>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|1|<around*|(|2\<pi\>|)><rsup|2><rsub|><rsup|>><big|int><frac|<around*|(|\<delta\><rsub|\<alpha\>\<beta\>>k<rsub|0><rsup|2>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)><around*|(|-i|)>\<mathe\><rsup|i\<omega\><around*|\||n|\|>*d>|2\<omega\>>\<mathd\><rsup|2>q<rsub|\<bot\>>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|<around*|(|-i|)>|2<around*|(|2\<pi\>|)><rsup|2><rsup|>>F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>>>>>
  </eqnarray*>

  \;

  On peut maintenant calculer notre double int�grale en faisant un changement
  de variable polaire pour par exemple <math|\<alpha\>=\<beta\>=x>:

  <\equation*>
    F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>=<big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><big|int><rsub|r=0><rsup|+\<infty\>><frac|<around*|(|k<rsub|0><rsup|2>-<around*|(|r<rsup|2>*cos<around*|(|\<theta\>|)><rsup|2>|)>|)>|<sqrt|k<rsub|0><rsup|2>-r<rsup|2>>>\<mathe\><rsup|i<sqrt|k<rsub|0><rsup|2>-r<rsup|2>><around*|\||n|\|>*d>r\<mathd\>r<rsup|>\<mathd\>\<theta\>
  </equation*>

  On factorise partout par <math|k<rsub|0>>:

  <\equation*>
    F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>=<big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><big|int><rsub|r<rprime|'>=0><rsup|+\<infty\>><frac|k<rsub|0><rsup|2><around*|(|1-<around*|(|r<rprime|'><rsup|2>*cos<around*|(|\<theta\>|)><rsup|2>|)>|)>|k<rsub|0><sqrt|1-r<rprime|'><rsup|2>>>\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rprime|'><rsup|2>><around*|\||n|\|>*d>r<rprime|'>k<rsub|0><rsup|2>\<mathd\>r<rprime|'><rsup|>\<mathd\>\<theta\>
  </equation*>

  On obtient donc:

  <\equation*>
    F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>=k<rsub|0><rsup|3><big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><big|int><rsub|r<rprime|'>=0><rsup|+\<infty\>><frac|<around*|(|1-<around*|(|r<rprime|'><rsup|2>*cos<around*|(|\<theta\>|)><rsup|2>|)>|)>|<sqrt|1-r<rprime|'><rsup|2>>>\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rprime|'><rsup|2>><around*|\||n|\|>*d>r<rprime|'>\<mathd\>r<rprime|'><rsup|>\<mathd\>\<theta\>=<big|int><rsub|\<theta\>=0><rsup|\<theta\>=2\<pi\>><around*|(|1|)>+<around*|(|2|)>\<mathd\>\<theta\>
  </equation*>

  On s�pare l'int�grale sur <math|r> en deux:

  <\equation*>
    <around*|(|1|)>=k<rsub|0><rsup|3><big|int><rsub|r=0><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rsup|2>><around*|\||n|\|>*d>|<sqrt|1-r<rsup|2>>>r\<mathd\>r=k<rsub|0><rsup|3><big|int><rsub|r=0><rsup|+\<infty\>><frac|\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rsup|2>><around*|\||n|\|>*d>|<sqrt|1-r<rsup|2>>>r\<mathd\>r
  </equation*>

  Il vient:

  <\equation*>
    <around*|(|1|)>=k<rsub|0><rsup|3><big|int><rsub|0><rsup|1><frac|\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rsup|2>><around*|\||n|\|>*d>|<sqrt|1-r<rsup|2>>>r\<mathd\>r+k<rsub|0><rsup|3><big|int><rsub|1><rsup|+\<infty\>><frac|\<mathe\><rsup|-k<rsub|0><sqrt|r<rsup|2>-1><around*|\||n|\|>*d>|i<sqrt|r<rsup|2>-1>>r\<mathd\>r
  </equation*>

  On pose <math|t=<sqrt|1-r<rsup|2>>> (<math|r<rsup|2>=1-t<rsup|2>\<rightarrow\>\<mathd\>r=<frac|-t\<mathd\>t|<sqrt|1-t<rsup|2>>>>)
  et <math|t=<sqrt|r<rsup|2>-1>> (<math|r<rsup|2>=t<rsup|2>+1\<rightarrow\>r\<mathd\>r=t\<mathd\>t>):

  <\equation*>
    <around*|(|1|)>=k<rsub|0><rsup|3><big|int><rsub|0><rsup|1><frac|\<mathe\><rsup|i*k<rsub|0>t<around*|\||n|\|>*d>t\<mathd\>t|t>+k<rsub|0><rsup|3><big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-k<rsub|0>t<around*|\||n|\|>*d>t\<mathd\>t|i*t>
  </equation*>

  On obtient donc:

  <\equation*>
    <around*|(|1|)>=k<rsub|0><rsup|3><around*|(|<big|int><rsub|0><rsup|1>\<mathe\><rsup|i*k<rsub|0>t<around*|\||n|\|>*d>\<mathd\>t+<big|int><rsub|r=0><rsup|+\<infty\>><frac|\<mathe\><rsup|-k<rsub|0>t<around*|\||n|\|>*d>|i*>\<mathd\>t|)>=k<rsub|0><rsup|3><around*|(|<frac|\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>-1|i*k<rsub|0><around*|\||n|\|>*d>+<frac|1|i*k<rsub|0><around*|\||n|\|>*d>|)>=<frac|k<rsub|0><rsup|2>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>|i<around*|\||n|\|>*d>
  </equation*>

  Et d'autre part:

  <\equation*>
    <around*|(|2|)>=k<rsub|0><rsup|3><big|int><rsub|0><rsup|+\<infty\>><frac|r<rsup|2>\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rsup|2>><around*|\||n|\|>*d>|<sqrt|1-r<rsup|2>>>r\<mathd\>r=k<rsub|0><rsup|3><around*|(|<big|int><rsub|0><rsup|1><frac|\<mathe\><rsup|i*k<rsub|0><sqrt|1-r<rsup|2>><around*|\||n|\|>*d>|<sqrt|1-r<rsup|2>>>r<rsup|3>\<mathd\>r+<big|int><rsub|1><rsup|+\<infty\>><frac|\<mathe\><rsup|-k<rsub|0><sqrt|r<rsup|2>-1><around*|\||n|\|>*d>|i<sqrt|r<rsup|2>-1>>r<rsup|3>\<mathd\>r|)>
  </equation*>

  En refaisant les m�mes changements de variables:

  <\equation*>
    k<rsub|0><rsup|3><around*|(|<big|int><rsub|0><rsup|1><frac|\<mathe\><rsup|i*k<rsub|0>t<around*|\||n|\|>*d>|t><around*|(|1-t<rsup|2>|)>t\<mathd\>t+<big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-k<rsub|0>t<around*|\||n|\|>*d>|i*t><rsup|><around*|(|t<rsup|2>+1|)>t\<mathd\>t|)>
  </equation*>

  Puis:

  <\equation*>
    k<rsub|0><rsup|3><around*|(|<big|int><rsub|0><rsup|1>\<mathe\><rsup|i*k<rsub|0>t<around*|\||n|\|>*d><around*|(|1-t<rsup|2>|)>\<mathd\>t-i<big|int><rsub|0><rsup|+\<infty\>>\<mathe\><rsup|-k<rsub|0>t<around*|\||n|\|>*d><rsup|><around*|(|t<rsup|2>+1|)>t\<mathd\>t|)>
  </equation*>

  Il suffit de calculer:

  <\equation*>
    <big|int><rsub|r=0><rsup|1>\<mathe\><rsup|i*k<rsub|0>t<around*|\||n|\|>*d><around*|(|1-t<rsup|2>|)>=<frac|i<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|2>-2<around*|(|k<rsub|0><around*|\||n|\|>*d+i|)>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>+2i|<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|3>>
  </equation*>

  <\equation*>
    -i<big|int><rsub|r=0><rsup|+\<infty\>>\<mathe\><rsup|-k<rsub|0>t<around*|\||n|\|>*d><rsup|><around*|(|t<rsup|2>+1|)>t\<mathd\>t=-i<frac|2+<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|2>|<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|3>>
  </equation*>

  On fait la somme:

  <\equation*>
    <around*|(|1|)>+<around*|(|2|)>=<frac|k<rsub|0><rsup|2>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>|i<around*|\||n|\|>*d>-<around*|(|<frac|i<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|2>-2<around*|(|k<rsub|0><around*|\||n|\|>*d+i|)>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>+2i|<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|3>>-i<frac|2+<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|2>|<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|3>>|)>k<rsub|0><rsup|3>
  </equation*>

  Ce qui donne apr�s simplification:

  <\equation*>
    <around*|(|1|)>+<around*|(|2|)>=<frac|<rsup|>k<rsub|0><rsup|2>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>|i<around*|\||n|\|>*d>-<around*|(|<frac|i<neg|<around*|(|k<rsub|0><around*|\||n|\|>*d|)>><rsup|2>-2<around*|(|k<rsub|0><around*|\||n|\|>*d+i|)>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>+<neg|2i>-<neg|2i>-<neg|i<around*|(|k<rsub|0><around*|\||n|\|>*d|)>><rsup|2>|<around*|(|<around*|\||n|\|>*d|)><rsup|3>>|)>
  </equation*>

  En rajoutant l'int�grale sur th�ta on a un facteur <math|2\<pi\>> sur
  l'int�grale <math|<around*|(|1|)>> et un facteur <math|\<pi\>> sur (2):

  <\equation*>
    F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>=<frac|-i2<rsup|>k<rsub|0><rsup|2><around*|(|n*d|)><rsup|2>+2<around*|(|k<rsub|0><around*|\||n|\|>*d+i|)>|<around*|(|<around*|\||n|\|>*d|)><rsup|3>>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>\<pi\>=<frac|2<rsup|>k<rsub|0><rsup|2><around*|(|n*d|)><rsup|2>+2<around*|(|i*k<rsub|0><around*|\||n|\|>*d-1|)>|<around*|(|<around*|\||n|\|>*d|)><rsup|3>>\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d>\<pi\><around*|(|-i|)>
  </equation*>

  On remet �galement le facteur <math|<frac|1|<around*|(|2\<pi\>|)><rsup|2>k<rsub|0><rsup|2>>>:

  \;

  <\equation*>
    <frac|-i|2<around*|(|2\<pi\>|)><rsup|2>k<rsub|0><rsup|2>>F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>=<frac|-*k<rsub|0>|4\<pi\>><around*|(|\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d><around*|(|*k<rsub|0><rsup|2><around*|(|<around*|\||n|\|>*d|)><rsup|2>+i*k<rsub|0><around*|\||n|\|>*d-1|)>|)><frac|1|<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|3>>
  </equation*>

  \;

  On peut maintenant effectuer la somme en rajoutant le
  <math|<around*|(|-1|)><rsup|n>>, on double la somme pour sommer sur
  <math|\<bbb-N\><rsup|\<ast\>>>:

  <\equation*>
    <big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|<around*|(|-i|)>|2<around*|(|2\<pi\>|)><rsup|2>k<rsub|0><rsup|2>>F<rsup|\<alpha\>\<beta\>><around*|(|<math-bf|q>|)>=<frac|-*k<rsub|0>|2\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|\<mathe\><rsup|i*k<rsub|0><around*|\||n|\|>*d><around*|(|*k<rsub|0><rsup|2><around*|(|<around*|\||n|\|>*d|)><rsup|2>+i*k<rsub|0><around*|\||n|\|>*d-1|)>|)><frac|<around*|(|-1|)><rsup|n>|<around*|(|k<rsub|0><around*|\||n|\|>*d|)><rsup|3>>
  </equation*>

  \;

  En introduisant les fonctions polylogarithmiques:

  <\equation*>
    Li<rsub|s><around*|(|z|)>=<big|sum><rsub|n=1><rsup|+\<infty\>><frac|z<rsup|n>|n<rsup|s>>
  </equation*>

  On obtient:

  <\equation*>
    <frac|-*k<rsub|0>|4\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|\<mathe\><rsup|i*k<rsub|0>n*d><around*|(|*k<rsub|0><rsup|2><around*|(|n*d|)><rsup|2>-i*k<rsub|0>n*d+1|)>|)><frac|<around*|(|-1|)><rsup|n>|<around*|(|k<rsub|0>n*d|)><rsup|3>>=<frac|-*k<rsub|0>|2\<pi\>><around*|(|<frac|Li<rsub|1><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|k<rsub|0>d>+i<frac|Li<rsub|2><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|2>>-<frac|Li<rsub|3><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|3>>|)>
  </equation*>

  Qui peut aussi s'�crire compte tenu de la relation
  <math|Li<rsub|1><around*|(|z|)>=-ln<around*|(|1-z|)>>:

  <\equation*>
    <frac|-*k<rsub|0>|4\<pi\>><big|sum><rsub|n=1><rsup|+\<infty\>><around*|(|\<mathe\><rsup|i*k<rsub|0>n*d><around*|(|*k<rsub|0><rsup|2><around*|(|n*d|)><rsup|2>-i*k<rsub|0>n*d+1|)>|)><frac|<around*|(|-1|)><rsup|n>|<around*|(|k<rsub|0>n*d|)><rsup|3>>=<frac|-*k<rsub|0>|2\<pi\>><around*|(|<frac|-ln<around*|(|1+\<mathe\><rsup|i*k<rsub|0>d>|)>|k<rsub|0>d>+i<frac|Li<rsub|2><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|2>>-<frac|Li<rsub|3><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|3>>|)>
  </equation*>

  Donc au total:

  \;

  <\equation*>
    <with|font-base-size|8|<big|sum><rsub|<tabular|<tformat|<cwith|2|2|1|1|cell-halign|c>|<table|<row|<cell|n=-\<infty\>>>|<row|<cell|n\<neq\>0>>>>>><rsup|+\<infty\>><around*|(|-1|)><rsup|n><frac|1|<around*|(|2\<pi\>|)><rsup|3>k<rsub|0><rsup|2>><big|int><frac|<around*|(|\<delta\><rsub|\<alpha\>\<beta\>>k<rsub|0><rsup|2>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)>\<pi\>\<mathe\><rsup|i\<omega\><around*|\||n|\|>*d>|\<omega\>>\<mathd\><rsup|2>q<rsub|\<bot\>>=<frac|-*k<rsub|0>|2\<pi\>><around*|(|<frac|-ln<around*|(|1+\<mathe\><rsup|i*k<rsub|0>d>|)>|k<rsub|0>d>+i<frac|Li<rsub|2><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|2>>-<frac|Li<rsub|3><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|3>>|)>>
  </equation*>

  Si on trace cette expression comme une fonction de <math|d> on obtient:

  <\big-figure|<image|<tuple|<#89504E470D0A1A0A0000000D49484452000002320000019508020000006B5F8CC5000000097048597300000EC400000EC401952B0E1B0000200049444154789CEDDD79405455FF3FF0CF9D9D7D446415C41D71014545DC17122B4BD34AD3D2CC474BA134CAD29E6F69F5FCD2AC7C6C314DCBCCD2342DCDCC4845D11414057705371405411159641B9899DF1F17461E36E1DE3B73EF0CEFD75F3A5E673EC3286FCEB99F730E63341A090000401A646217000000F000620900002404B104000012825802000009412C0100808420960000404214167E3D83C1909999E9E4E4C4308C855F1A00000464341A0B0B0BBDBDBD6532214738968EA5CCCC4C5F5F5F0BBF28000098C98D1B375AB76E2DE0135A3A969C9C9C88E8C68D1BCECECE167E6900001050414181AFAF2FFB5D5D40968E2576EECED9D919B10400600304BF2383960700009010C41200004808620900002404B104000012825802000009412C01008084209600004042104B00002021882500009010C41200004808620900002404B1040000128258E2E55A4ED147BB2EDC2E2C15BB1000001B61E91DC46DCC94B589E9B9C5C7AFE5FE367B80D8B50000D8028C967849CF2D26A2E4F43CB10B0100B011882500009010C4122F6A05BE80000042C277555E5AD8ABC42E0100C0A6209678D1DA2BC52E0100C0A620967831C592D16814B7120000DBD0502CAD58B1C2DFDF5FA3D1848686262626D6794D5E5E5E6464A49797975AADEED4A9D3AE5DBBCC53A74469ED2A27F14ACAF5E2560200601BEA5DB7B479F3E6E8E8E855AB568586862E5FBE3C2222223535D5DDDDBDFA353A9DEE91471E717777DFBA75AB8F8FCFF5EBD7B55AADF96B96107BB59CFDC5BDE2727B1516810100F055EF77D265CB96CD983163DAB46944B46AD5AA3FFFFC73EDDAB5F3E7CFAF7ECDDAB56B737373E3E3E3954A2511F9FBFB9BB95AE9A99ABABB57A4F3D1DA895A0A00802DA87B124FA7D3252525858787575E2493858787272424D4B86CC78E1D6161619191911E1E1EDDBA75FBE8A38FF4FA3AE6B2CACACA0AAA11F60D88CB50754BE95EB14EDC4A00006C43DDB1949393A3D7EB3D3C3C4C8F787878646565D5B8ECEAD5AB5BB76ED5EBF5BB76ED7AF7DD773FFBECB3FFFCE73FB59F6DF1E2C52E557C7D7D05AC5E74A63E87DC22C4120080007875E2190C067777F7D5AB578784844C9830E1DFFFFEF7AA55AB6A5FB660C182FC2A376EDCE0F38A5263AC3689276A21000036A2EE7B4B6E6E6E72B93C3B3BDBF4487676B6A7A7678DCBBCBCBC944AA55C5E79DBBF4B972E5959593A9D4EA5FA9F45A66AB55AAD560B5AB65498464BF78ACBC5AC0300C056D43D5A52A954212121B1B1B1EC6F0D06436C6C6C5858588DCB060C1870F9F26583C1C0FEF6E2C58B5E5E5E3532C9B619716F09004050F54EE2454747AF59B3E6871F7EB870E1C2AC59B38A8A8AD8AEBC2953A62C58B080BD66D6AC59B9B9B973E6CCB978F1E29F7FFEF9D1471F4546465AA87069C0BD25000061D5DB203E61C2843B77EEBCF7DE7B595959C1C1C13131316C07447A7ABA4C561966BEBEBE7FFFFDF7EBAFBFDEA3470F1F1F9F3973E6BCFDF6DB162A5C22AA72290F9378000042602CBC6B4E4141818B8B4B7E7EBEB3B3B3255FD74C66FD94F4D7D92C220AF472DE356790D8E50000588E99BE9F634F3C5E8C0F464B98C40300100062891763D52C5E2E620900400888255E4CA3A5D27243B1AE42D45A00006C01628997EAF7E5EEDEC7800900802FC4122FD5FB45EEA2471C008037C4122FD5FB18EFDE2F13B1120000DB8058E20593780000C2422CF1527DB4945384D11200005F88255ED8506AE9A022A25C8C96000078432CF1C20E96DC1CD584960700002120967861474B6E4E2A22CA41CB0300006F88255ED87B4B95A3254CE20100F08658E2E57F27F1305A0200E00BB1C40BBB271E1B4BB9453A0B6FC70E00607B104BBC548D96544454AE371694625B3C00005E104BBCB0B1A456CA9DD40AC2460F0000BC2196786127F11822574715A1471C008037C4122FEC6889612A57D462B40400C0136289173696640CD3122B6A0100848058E2C53489C7763DE4142296000078412CF1629AC4637BC4B1D10300004F88255EAA962931AD9CD44474A710B10400C00B62891776FD2CC3502B473511DDC1680900801FC4122FEC688921C2680900401088255E4C9D78A658C2FE4300007C209678793089E7A426A292727D914E2F76510000560CB1C44BE5241E43F62A85A35A4198C70300E007B1C44B6583383184DB4B000042402CF1C22EA72586882A9BF16E17968A5A11008075432CF1627C904A182D01000800B1C44BD52E0F98C40300100662891783D148443276120FB10400C01B624900952D0FD8E801008037C4122FA6AD5A09A325000021209678311D6C418825000021209678319A36C5AB8AA59CFB657A03F61F0200E008B1C44B552A3144D4D2412563C860A4BB4518300100708458E2C558AD134F2197B1870166E72396000038422CF1527DDD121179BA688828AB001B3D0000708458E2C5B4552BCBDD494344D988250000AE104BBC541E6C51F55B4F17352196000078402CF15263B4E4E98CD11200002F88255EAA8EA2ADCC257767F6DE125A1E000038422CF152B99CB6C668291FA32500008E104BBCB0A32559552E79B0B18423970000B8422CF152FDBC25AA1A2DE515979796EB45AB0900C09A219678A9ECC4ABCA25673B85462923743D0000708558E2A5FAE64344C4304CE53C1EBA1E000038412CF152FD600B968733367A0000E00EB1C48B916A6E16CEDE5EBA8D580200E004B1C44B8D4E3C22F2705613D12DF4880300708258E2C5506B12CFCBC58E886EE597885411008075432CF1F43F9D7844E4ADD51051661E464B00005C209678A95AB7F42097BCB5182D0100708758E2A5C656AD54358977BBB0AC5C6F10A72600006B8658E2A5C6C11644D4D241A592CB8C46ACA80500E002B1C44BEDD1924CC6B067D4E2F61200000788255E6A1C9ACE62BB1E707B09008083866269C58A15FEFEFE1A8D263434343131B1812B376DDAC430CCD8B163852E4FEA0CB526F188C8DBC58E305A0200E0A4DE58DABC79737474F4C2850B9393938382822222226EDFBE5DE795D7AE5D7BF3CD37070D1A64B62225ACAED19217464B00005CD51B4BCB962D9B3163C6B469D302030357AD5A656F6FBF76EDDADA97E9F5FAC99327BFFFFEFBEDDAB533679D1255B555EBFF607BC433F3104B00004D56772CE974BAA4A4A4F0F0F0CA8B64B2F0F0F0848484DA577EF0C107EEEEEED3A74F37638D1256E3600B1626F100003853D4F9684E4E8E5EAFF7F0F0303DE2E1E191929252E3B243870E7DF7DD77274F9E6CF835CACACACACA2A0F7A282828E051ADE4D438D88285493C0000CEB877E2151616BEF0C20B6BD6AC7173736BF8CAC58B17BB54F1F5F5E5FC8A1254FB600BAA5A517BAFB8BC4487336A01009AA6EED1929B9B9B5C2ECFCECE363D929D9DEDE9E959FD9A2B57AE5CBB76ED89279E607F6B30188848A150A4A6A6B66FDFBEFA950B162C888E8E667F5D5050604BC964A86B12CFC54EE9A45114965664E41577707712A7320000EB54F76849A552858484C4C6C6B2BF35180CB1B1B1616161D5AF0908083873E6CCC92A4F3EF9E4B061C34E9E3C593B75D46AB57335E6781B62A95A4E5BA3E9815AB7B027A21BF7308F0700D034758F9688283A3A7AEAD4A9BD7BF7EEDBB7EFF2E5CB8B8A8AA64D9B464453A64CF1F1F159BC78B146A3E9D6AD9BE97AAD564B44D51F69162AB76AADC9476B77E156C14DC412004013D51B4B13264CB873E7CE7BEFBD979595151C1C1C1313C37640A4A7A7CB64D81BA292B1D6C116ACD62DEC88E8E6BD62CB97040060D5EA8D25228A8A8A8A8A8AAAF1605C5C5C9D17AF5BB74EA092AC49ED832D5855B184D1120040D360DCC34BEDAD5A59ECBD25C412004053219678A973392D558D963230890700D04488255E0CF54CE2F9B6B027A29CFB3A2C5D02006812C492006A8F969CED144E6A051165E461C00400D0048825EED8193CAAAB419C61189F167684A54B00004D8458E2AE2A95EA584E4BE87A0000E004B1C45D552AD5315A22538F782E26F100009A00B1C4DD8349BCBA72C9D7D59E88D2114B00004D8158E2CED0E0245E1B577B22BA7E17B10400D0048825EE8CD4D068A94DCBCAD19269500500000F8558E2EE41CB435D7FCA4EE2DD2FABC82DD259AE2600002B875812409D93781AA5DCD3594344D7717B0900A0D1104BDC353C5A22223F761E0FB79700001A0DB1C45DC3F796085D0F00004D8758E2CE345A92D5934B6CD7C3F5DC228B95040060ED104BDC191ED662E7D7D281886EE0DE120040A32196B87BB0CB0326F10000048258E2AE5ACB43439378B70BCB8A751516AB0A00C0AA21967878B0CB43DD7FAEB55769ED958401130040A32196B87BD08957FF356DDD1C88282D075D0F00008D8258E2AEE1832D588825008026412C7167EAC493D53F5C6AE7E6404457EF209600001A05B1C45DB54EBC06464B8E447435E7BE452A0200B07A8825EE1AB3333826F100009A04B1C41DDBF250FF488988C8DFCD9E88F28ACBEF611F71008046402CF160246AB00D8F88EC550A6F170D115DC5800900A011104BDCB173780DDC5862B56D85793C0080C6422C71676CC46889AA6E2F5DBD83AE07008087432C71C73688D7B77DB84965331E7AC401001A01B1C4DDC3F7782022A28EEE8E4474E976A1B9EB0100B0018825EE8C4623356212AF83BB23115DBF5B5CAE3798BF280000EB8658E2AEF2DED2C372C9CB45E3A09257188CD7EF621E0F00E021104B7CD577AAC5830B18861D305DCA46D70300C0432096B86BE4688988DABB3B12D1E5DB8825008087402C71C7EEF2F0D04E3C22EAE8EE444497104B00000F8358E2CED0B8754B54D5F580D11200C0432196B83336723D6D552C5DB9735F6F68C4F6AE0000CD186289BBCACD871A71A59FABBD4A212BAB30DCBC87D3D301001A8258E2AEAAE5E1E1C12497311D5A3912516A1616D502003404B1C4C7C30FB63009F07422C41200C0C32096B86BF4AD2522A2CE9E4E4494928D580200680862893BB67DA1310DE244D409A325008046402C71D798D3694DD849BCB49CA2B20ABD59AB0200B06A8825EE8C8DDC429C88883C9D35CE1A85DE60C4EA250080062096B86BFCE64344C4304C80A733115DC4ED250080FA2196B8AB9CC46BF4F59D3C1D892805B7970000EA8758E2AE49A32522EAE2E54C44E7330BCC56110080D5432CF1D5C84E3C22EAE6ED4244E7320B8C466C4104005037C4127786C69D4E6BD2D9D3492E63728B74B7F24BCD5715008055432C71D7F8CD87581AA5BCA3BB23119DCDC8375F550000560DB1C41D8799B86E3E2E84580200A81F62893BF61651E35B1E88A83B1B4BE87A0000A8076289BBCA832D9A124BDD7C9C09A3250080FA2196B8ABDAAAB509B9D4C5CB9961E87661D9ED02743D0000D401B1C41D3B89276BCA68C95EA568DFCA9188CE611E0F00A02E8825EEAA26F19A924B55B797CE601E0F00A02E8825EE9A74DE9249576FDC5E0200A81762893B23A75C627BC4318907005027C412779593784DFC5B81DECE44949157925BA413BC2400006BD7502CAD58B1C2DFDF5FA3D1848686262626D6BE60CD9A3583060D6AD1A2458B162DC2C3C3EBBCC6863575970796B346E9DFD29E88CE65621E0F00A0A67A6369F3E6CDD1D1D10B172E4C4E4E0E0A0A8A8888B87DFB768D6BE2E2E29E7BEEB9FDFBF7272424F8FAFA8E1C39322323C3CC054B48530FB630E986AE0700807AD41B4BCB962D9B3163C6B469D302030357AD5A656F6FBF76EDDA1AD76CD8B061F6ECD9C1C1C1010101DF7EFBADC160888D8D3573C112C28E961ABF83B849652CDD442C0100D454772CE974BAA4A4A4F0F0F0CA8B64B2F0F0F0848484069EA8B8B8B8BCBCDCD5D555F81AA5AAA9E72D99F4F4D5125172FA3D9C7001005083A2CE47737272F47ABD878787E9110F0F8F949494069EE8EDB7DFF6F6F636255975656565656565ECAF0B0A6CA703CDC865B35622A21EADB50A19935D50969157D2BA85BDB05501005835613AF1962C59B269D3A66DDBB669349ADA7FBA78F162972ABEBEBE82BCA214706B7920223B959C5DBD9474FD9EE055010058B5BA63C9CDCD4D2E976767679B1EC9CECEF6F4F4ACF3E24F3FFD74C99225BB77EFEED1A3479D172C58B020BFCA8D1B37F8172D11DC1AC459216D5C09B10400504BDDB1A452A94242424CFD0B6C2F43585858ED2B972E5DFAE1871FC6C4C4F4EEDDBBBED750ABD5CED50852B7147038D8C224A44D0B422C0100D452F7BD25228A8E8E9E3A756AEFDEBDFBF6EDBB7CF9F2A2A2A269D3A611D19429537C7C7C162F5E4C441F7FFCF17BEFBDB771E3467F7FFFACAC2C22727474747474B458F5E2E2DC894744BDDA6889E8C2AD82A2B20A0775BD9F0200407353EF37C4091326DCB973E7BDF7DECBCACA0A0E0E8E8989613B20D2D3D365B2CA31D6CA952B753ADDD34F3F6DFA5B0B172E5CB46891996B968ACA754B9C464B5E2E763E5ABB8CBC925337F2FA777013B8320000ABD5D0CFE9515151515151351E8C8B8B33FDFADAB56B6628C96A70DBAAD5A4579B1619792549D7EF219600004CB0271E77460EC7D35613E2A725A2A4749BBDBD54545631EEEBC35FEDBB24762100604D104BDCF1E9C4A3AA66BCE4EBF70C06DB5C549B9C7E2F393D6FD99E8B69394562D702005603B1C41D9F4E3C22EAE2E564A7941794565CBE735FC8B224A3C26024228391BEDE7F59EC5A00C06A2096B8E3395A52C865C1BE5AB2DD3671D32870DB898C1BB9C5E2160300D602B1C41D3B5AE2D620CE62572F1DBF669BB1A4AF8AA50A8371E5812BE2160300D602B1C41DE7AD5A4DFAB47525A22357EFDAE49EAD6C2C39691444B4F5F8CD5BF925625704005600B1C45DD5241EF75CEAE3DF42296732F24AD26D718E4B6F341251576FE7BE6D5D757AC33707AE8A5D11005801C41277469E379788EC558A9E7E2D88E8F0E5BBC2D42425EC68492E63E68CE848441B13D333F33060028087402C71C7F974DAEAFAB76F494487AFE4085191B4B0B1246398FEED5B86B675D55518BEC41A26007818C41277FCEF2D11D1800E6E4474E4CA5DDB5BBDC436882B640CC330F3223A13D12FC76F5EC31A2600681062893B3646F874E21151506BADBD4A7EB748979A5D284855D261A89AC423A2DEFEAEC33AB7D21B8CFFDD7B51ECBA0040D2104BDCF15C4ECB5229647DDBBA12D1E1CBB6368FC7B63CB0B144446F8CEC4C443B4E65A664D9CE09C5002038C41277555BB5F2BCBB54797B29E18AAD753DE80DFF134BDD7C5C1EEFE16534D2A77F63C00400F5422C71C7E7608BEAFAB77723A2A369B9157A03FFAAA4C3D4F2607A24FA914E7219B3F742F691ABB696C1002014C41277422D810DF472D6DA2BEF97559CBA992FCC334A83BEAAE5C1F448FB568ECFF5F525A2FFF7E705DB6BF10000412096B8ABEAC4E33B5C92C998B076EC3C9E4DDD5EAA1C2DC9FEE7EB3337BC93A35A712623FFF7531922D50500928658E28EF76ADA07D893000F5EB4AD5832D61C2D11919BA37AD6D0F644F4494C6A69B95E9CCA0040C2104BDC192AB76A15E0A986766A454449E9F7F28A75023C9D34E8F5FFD3F260327D605B6F174D667EE97787D2C4A80B00240DB1C48340937844E4EB6ADFD9C3496F301EB87887FFB349448D0671138D523E6F546722FA7AFFE5EC8252112A030009432C7127C8E64326C3BBB813D1BE94DB023D9FF82A1BC4EB8AED31413EC1BEDA229D7EF1AE0B16AF0B00240DB1C49D209B0F998C087027A2B8D43B36D3265E67CB034B26633E18D3956168FBC9CCA3681607806A104BDC5535380B934B3DFD5A68ED95F925E5C9E979823CA1E86A378857D7A3B576621F3F225AB8E39CCD243100F08758E24ED8D1925CC60CEBEC4E44B129D9C23CA3D81A182DB1E6457476B153A664156E4C4CB7605D0020698825EED87B4B8274E2B18607B813D1BE0B36727BA9CE06F1EA5C1D546F467426A24FFE4EBD8DDE07002022C4121F0681F6C43319DCA9955CC65CBA7D3FFDAE2D1C56DB40CB83C9A4BE7EDD7D5C0A4B2BDEDF79DE52750180A4219678106207F1EA5CEC947DFC5B10D13E9B98C77BE8241E11C965CCE271DDE532E6CFD3B76CE35D03004F8825EE2A7779102E96886844800711EDB58979BC865B1E4CBAF9B8BC34C09F88DEDD7EAEA8ACC202854944C295BB5B8EDF300AB5B52280AD402C7127D4C116D58DECEA41440957EFDEBD5F26E0D38AA231A325D6EB8F74F2D1DA65E4952CDBD38CCEBC98BBF9C4BCADA717EE38876402A80EB1C49DB12A9704D4A6A543771F17BDC1F8F739AB9FD17A68CB8389BD4AF19FA7BA11D1F787D392AEE79ABD3269C82F2927A2F509D7DFFFE33C9209C004B1C49D805BB556F7780F2F22FAF34CA6D04F6C69B5CF5B6AC0B0CEEEE37AF9188CF4C62FA78A75CD622AAF5C5F1945EBE2AF2199004C104BDCB19D788DFCB6DB788F77F722A2842B7773AC7C1EAFC6E9B40FB5F089AE9ECE9A6B778B97C6A49AB32E49D01B8CECD7E7ED510144B42EFEDA073B914C004488253E8C4277E2B17C5DED835ABB188C1473364BE0A7B6AC46B63C98B8D829973EDD8388D6C55F8BBF6C53677CD4565EB5B1C5E47E7E4BC67527A2EF0F63CC04408458E24FF0493C32CDE39DBE6586E7B69CC6B73C980CEED46A72A81F11CDDB7ABAB0B4DC5C95494045D5E1BC2AB96C625FBF8F9EEA4E44EBE2AFCDFFF58C1EE7F642F38658E24EA8D3696B7BACBB17111D4DBB7BBBD08AF73E68EA6889F5CE635D7C5DED32F24AFEB3D39637172FAFA81C2DB15F9F49A17E9F3E13246368F3F11B73369D28C72681D08C2196B813F6608BEA5AB7B00FF6D51A8CF4B735CFE3E98D4D68793071502B3E7D3A886168F3F11B7BCF5B7D3B627DD8E0619807F7DE9E0E69FDD5A45E4A39B3F3F4AD593F25E1E85E68B6104BDC19CDD48A474444A37B7811D18E5356DC8FD7D4960793D0762DA70F684B44F3B69EBA955F227C6512506E301291522EAB3EDA7EACBBD7EA177AAB15B2BD176EBFB4EED8FDE6B4B818C004B1C45D552A99259746F7F0963174ECDABD6B3945E6787E0BE01C4B44346F54E76E3ECEF78ACBE7FC7CD2268FBD6027F154F29AFF018705B8AF9BD6D741258FBF7277E2EA843B85D6DD8DC981D1687C73CBA97F6F3B9357AC13BB16100762893B8351E01DC4ABF374D10CEAD88A887E4DBE699617303F3EB1A456C8BF7AAE97A35A91782DF7F3D84B4297263E76124F21AFE38B13D6BEE5C619FD5A3AA8CE66148C5F199F66B53F977073F35EC9D6A49B1B8EA6872F3BF8F7392B9EC406CE104BDC097BDE526DCFF46E4D44BF26DDB4D2D62C6E2D0F26FE6E0E1F8DEB4E445FEDBF7CE892ADF58BB36B6995B5464BAC205FEDAFB3FAFBB9DAA7E7168F5F197FEA868D9C0CD918A69B6A39F7CB5EFE31E9B59F4FDC2BC2B0A979412CF165A6493C220AEFE2E1AC5164E697C65FB1CA6FCADC5A1EAA7B32C8FBB9BEBE4623CDDD7CD2C6A6B3D8D152ED493C137F37875F67F5EFE6E39C5BA49BB8FAC8FE145BD8BDB731CA2A0C44E4E6A89E35B4BD8CA11DA7321FF9EF016B5FC3074D8258E2CE4CCB694D344AF998601F22DA9A6495F378061E937826EF8DEEDAD9C329E77ED9DCCD27AC74D458A70626F14C5A39A937CD0C1BD4D1ADA45C3FFD8763DF1F4E6B0E8B6DCB2AF444E4A096BF3D2A60DBEC019D3C1C73EEEB5EF929297263B255AF9780C6432C7167EE493CAA9AC78B399BC56EEB695D2A8488253B95FCAB493DED94F2C397EF2EFD3B45A0D2C4D7F0249E89A35AF1DDD43ECF84B43618E9FD3FCEFFDFF6B336BFA4A9ACDC40446A858C88827CB57FBC3A3072587BF644AE119F1DD870F4BAC1867E3A813A2196B8ABFACF61C65CEAEEE3D2C9C3B1ACC2B0F3B4F5758AF36979A8AEA38713BB29D13707AE5A75C77C756CBA3C34968848A5902D7DBAC78247031886361C4D7FF1FBC4FC62EBFB19A5F1CAF46C2CC9D9DFAA15F2791101BF470E604F31FEF7B6B34FAF8A4FC92A10B54631ED399F3D6FCBA9E4F47B621762468825EECCDA89C76218E699105F22FAE5D80D33BE8C7908154B44F44490F72B43DA13D15B5B4F9DCBCCE7FF84A2AB8AA5467D711886797948FBD52FF4B657C90F5FBEFBD4CAC336DC9E577DB464D2CDC7657BE480454F043AA8E4C9E979A3BF38F4714C4A89AE39AE38FE72DFA52D4937C77D1D3FF9DB2347AEDEB5C9795DC412771698C423A271BD7C5472D9A99BF927ADAD1D8B6D79900BF4059A17D17948A756A5E58699EB9372ADBF35AB919378D53D12E8B1F595FEDE2E9AAB778AC6AE386CA58D300FC5DE5B522B6B7E65E432E6C5016DF7BE31645457CF0A837165DC9591CB0FC4A536974E1093FBA5958BAC0F5FBE3B71F59167BF493870F18E8D851362893BB32EA73569E9A81E1DE44544EBE3AF99F5850457D920DEB801C143C965CC17137BFAB7B4CFC82B89DC906CEDB7589A345A3209F476DE1E3520D8579B5F52FEC277896B0E5EB5B1EF4754D589679AC4ABC1CBC56ED50B216BA6F4F676D1DCC82D79F1FB63AFFC987423B7D8B2358AA9A45C4F44AB9EEFF542BF362AB9ECD8B57B53D7268E5D7178CFF96C9BF9C78058E2C1CC9D782653C3FC8968E7E95BD6750253938E016C0C177BE5EA29BD1D54F284AB77DFFFC3BACF1A6FFCBDA51ADC9D349B66F61BD7CB476F30FEBF5D17223726DBD81E4555B1D4D057E691408F3DD143FE35B0AD5CC6C49CCB1AB1ECC0B2DDA9CDE4F44836963AB83B7E38B6DB3F6F0F9B3EB0AD46293B75337FC6FAE38F7EFECFCED39936D0B08A58E2CE9C5BE2FD8F205F6D90AF56A7376CB6AA3B4C823488D7D0C9C3E9BF138219867E3A92BEFAE055019FD9C22A9A3E8967A251CA3F7B26E8C3B1DD947266D799AC315F1DBA7CBB50E802455356AE27225583B144440E6AC5FF8D0EDCF5DAA0FEED5BEA2A0C5FECBB3CE2B3033B4E655AF50F2B8DC1DE51D328E544E4E1AC797774E0A1B787CF1EDADE51AD48C92A8CDA7862F867713F265CB3EA1B6F8825EECC77B0456D53C3DA10D14F47AE5BD10671823488D736B2ABE7FF3D1E48448BFF4AB1C60645968ED3249E09C3302FF46BB3F9E5304F67CD953B4563BE3ABCEB8C759FCE65D298D19249674FA70DFF0A5DF57C2F1FADDDADFCD2D77E3E31E19B23B6D114532783C1C87E7DEC940F2639DD1CD56F8D0A38FCF6F0B9E11DB5F6CAEB778BDFFDFD5CFF25B1CBF65CB4AEF91513C4127746B2DCCF658FF7F06AE9A0BA955FBAF782D59CF5C0762A0A1E4B44347D60DB6903FC89287AF3A9C4B45CC19FDF02AA96D3F2FA0FD8CBAFC5CED706F66BE75AA4D3CFDE90FCE1CEF3BA0AABF9A9A53E0DDF5BAA8D619851DDBC62DF1812FD48278D5296782DF7892F0FBDB3ED8C8D6D0BC22AA9DA99C94E55F3EBE362AF9C1BDE297EFEF00FC674F573B5BF575CFE45ECA5014BF6BDB3EDCCD53BF72D5E292F8825EED8295C01EF9D3440AD903FD7D78F88D61EBE668197134405BF3DF11AF67F8F074674F5D0E90D33D61FBF626DFFEBA86A12AF81CD871AC9CD51FDD3F4D0006937E700002000494441549707B723A2EF0EA5D9C0D6AEBAA68C964C344AF96B233AC6BE3174740F2F8391361E4D1FF2C9FECFF75E2AB2AD1B6FA658D2D413DBF62AC59430FFFD6F0EFD7A72AF205F6D598561E3D1F411CB0ECC5C7F3CE97AAEB5CC702296B8B34C83B8C9E47E7E4A399398966B2D2BE9046F79A84E2E63964FE8C9F6A4BDF87D627681956D4BC37312AF3A855CB6E0B12EAB5F08D1DA2BCF64E48FFEE29FDFAC76D779AABF41BC317CB4765F4DEAB57966BFA0D62EC53AFD7FF75E1CFA69DC86A3D634F5DDB0AA1B4B3259833FEDC965CC63DDBDB6CFEEFFCBCB61E15DDC8D46DA7D3E7BFCCA84B12B0E6F3F9121FD213562893BF39D4E5B272F17BBB1C13E44B42AEE8AA55E931773B43C5467A7927F3BB5779B96F637724B9EFFF6A8752D66E2DC89579F915D3DFF9A33A86F5BD7229D3EFA9753D19B4F5A69875E5327F16A0B6DD7727BE480AF26F5F473B5BF5358F6EF6D6723961FDC7D2ECB5AC60A0D60B757AF7E63A9010CC3F46DEBFAEDD43E7BA3874CECE3AB52C84EDDCC9FBBF9E4808FF77DBEF792942739114B3C5876B444442F0F69CF30B4FB7CB655745E99A9E5A13A760ACBC3597DE9F6FDA96B130B4BAD66571E3E9D78F5F172B1FB7946BFE8473AC918FAED44C6E82FFE397DD3CA9660533DBB3C3415C330A37B78EF8D1EB2F089C016F6CA2B778A66FE98F4EC3709C7AF59E59D489392A6C492490777C725E37B24CC1FFEE6C84E1ECEEA3B8565FFDD7B71C0927DD1BF9C3C9B21C5F610C41277950DE216CCA50EEE8E11819E44B432CE0A7AA3CDD7F2509DAFABFD867F85BA3AA8CE64E44F5F77DC5AFA62B92DA77D28B98C796D44C7CD2F8779BB68AEDD2D1EF775FCF2BD17AD6BE971E5241EBF5862A914B26903DA1E786BD8ECA1EDD50AD9B16BF79E5E9530756DA235A635AB7212AF56BF4363B47454470DEFF8CF5BC33F9F181CECABD5E90DBF25674873CD096289BBCA832D2CFBA2AF0C6D4F44BF9FCCC8C82BB1EC2B379905464BAC0EEE4EEB5FEAEBA456245ECB7DF9A724F6FB9AC4E9849EC4ABAE8FBFEB5F73063FDEDDABC2605CBEF7D2B8AFE32F655BC1F09A553989D7C40141039C35CAB74605C4CD1B3AB18FAF5CC61CB878E7C9AF0ECF587FFCC22DEBDBEF95DB68A93A95423626D8677BE4806DB3FB8F09F69EDADF5FB0E2848358E2CE92EB964C827DB5FDDBB7AC3018D7487B31A9C15039932FD49E780DEBE6E3F2FDB43E764AF9C18B77A2369E90FE4D5D76128F678378035CEC955F4DEAF9F9C460173BE5998CFCC7BF3CB4FAE015AB58FFCFC612FF1EC51ABC5CEC968CEF111B3D645C4F1F19437BCE673FFAF93F911B92AD28B0A96AB4C427964C7AFAB5F87C62CF0EEE8EFC9F4A708825EE0C16BFB7C49A35B43D11FD9C982EE5F6337DD5ED650B8C9658BDFD5D574F095129647BCE67CF92FC98A9EA745A337E7118861913ECB3FBF5C1433BB7D255183EDA9532E19B846B926F1F677779E0D689F750FE6E0ECB2604EF7E7DC8E81E5E44F4E7995B23971F9CBBE98455DCAC25D36889D3249E15412C7167E14E3C93811DDC7AB769515661F872DF258BBF7863997E30B7582C11D1A08EADBE9DD25BAD90C5A6DC9EB93EA9B45CBAC964D649BCEA3C9C35DFBFD867C9B8EE0E2AF9F1EBF7467D7E70F5C12B526E98E6DF89F7501DDC1DBF9AD4EBAF398346067A188DB4FD64E623FF3D387B4392F4B787E03F8967151AFA5FB162C50A7F7F7F8D46131A1A9A989858E7355BB66C090808D06834DDBB77DFB56B97798A94280BAF5B326118E6CD88CE44B429F18664F74E36587CB4C41ADCA9D5F72FF6B153CA0F5CBC3363BD743B20CC3D89571DC33013FBFAC5CC1D1CD6AE6569B9E1A35D2963BF3E2CCD162CE2BA9C96832E5ECEABA7F4FE236A201B4EBBCE643DFEC5A197D61D4BBA2EDD7581959378CD76B4B479F3E6E8E8E8850B17262727070505454444DCBE5DF36893F8F8F8E79E7B6EFAF4E9274E9C183B76ECD8B163CF9E3D6BE68225C7DC075BD4A95FBB96833ABAB137B42DFFEA8D5121C66889D5BF83DBF7D3FAD8ABE4FF5CCA7969DD31692EDFB1C0245E0DBEAEF61B67842E1DDFC359A3389B513066C5E1C5BB2E4830B605ECC46B8CEEAD5D564FE91D3377D09341DE3286F6A5DC1EBF327ED29A23F1977324B8CEA949EB96AC57BD9FFDB265CB66CC98316DDAB4C0C0C055AB56D9DBDBAF5DBBB6C6359F7FFEF9A851A3E6CD9BD7A54B970F3FFCB057AF5E5F7DF595990B9610A3A50EB6A8D31B233B13D1B61337A5392D6E30C592185FA07EED5AFEF0525FF6088CC96B8EDC95DE8695822FA76D0C86619EEDE3BBF78D218FF7F0D21B8CDF1CBC1AB1FCE03F97EE58B2868712BC13AF31023C9DBF78AE67EC1B439FEDDD5A2163E2AFDC9DF4EDD1A7BE8EDF75E696A4FA44D8493C4DF38C259D4E979494141E1E5E79914C161E1E9E909050E3B2848404D3354414111151FB1A222A2B2B2BA846A0CAC567B1832DEA14ECAB7D24D0C360A4657B2E8A544243441C2DB1FAF8BB6E9CD1AF85BDF2D4CDFC675625DCBC27ADD94E9D0527F16A7077D2AC98D4EBBBA9BDBD5C34E9B9C52F7C9718B921395332EB0D9AB483B8B0DABA392C7D3AE8C05BC3A686B5512964276FE4CDDE903CECD3B8F509D72472985389CE40CD76122F272747AFD77B7878981EF1F0F0C8CACAAA71595656D643AF21A2C58B17BB54F1F5F515A26C4930548E96C40A267A63642786A15D67B28E496FEDBAA172433C31BF3E41BEDA2DAFF4F7D1DA5DCD291ABF32FEA2945A812BCCB39CB6F14674F1D8133DE4C5FEFE3286FE3C736BC4670756ECBF2C85F6C5CA4E3C316289E5A3B57B7F4CB7C36F0F7F6D7807ADBD323DB7F8BDDFCFF55FB2EFB3DDA9A26FD8839607C12C58B020BFCA8D1B525C54CC8D582D0F26019ECE13FBF812D1077F9C374869AA81AA1AC4C51A2A99747077DC3A2BAC9387637641D9D32BE3A5B3F74CD5BD25315B611DD58A454F76DDF9EAA03EFE2D4ACAF59FFC9DFAE8F27F0E5C14794E4F9449BCDA5A39A9A347768E9F3FFCC3315DDBB4B4CF2B2EFF72DFE5011FEF9BFFEBE9D42CD17EBE29D15510917DF31C2DB9B9B9C9E5F2ECEC0747FB6467677B7A7AD6B8CCD3D3F3A1D710915AAD76AE4688B225A16A124FCCEFBC6F8CECECA8569CC9C8FF55629B46B39D66A2C7121179B9D8FDF272582F3F6D4169C5A46F8FFE7E3243EC8A88449DC4AB21D0DBF99797C3963D1BE4E6A8BE9A5334756DE2CCF5C7C55ADE64341ACDB49C961B7B95E28530FF7D6F0C5D39B957B0AF565761D874EC46C4F283135727C49CBD65F93EFB667D6F49A552858484C4C6C6B2BF35180CB1B1B1616161352E0B0B0B335D43447BF6ECA97D8D0D137DB444446E8EEA57877720A2A57FA74AEA6899CA0DF1C4FDEA54D1DAAB36FCABDFC8400F5D8561CEA693CBF75E14BDC94AF449BCEA188619D7ABF5BE3787BC34A0AD5CC6EC3E9FFDC87F0FFC67E7F9FC624B6F7DABABFA466FA6E5B4DCC865CCA3DDBDB6CDEEBFE595B0C7BA7BCA65CC91ABB9AFFC943C78E9FEAFE32E5B72EBFA92F29A47D3DAA47A3FFBE8E8E8356BD6FCF0C30F172E5C98356B565151D1B469D38868CA94290B162C60AF9933674E4C4CCC679F7D969292B268D1A2E3C78F47454559A870491067396D0D2F0EF06FD3D2FE4E61D9D77197C5AEE501B6E5A1E153612CC94E255FF57CC8CCC1ED8868F9DE4BAF6F3E29EE7D14294CE2D5E0AC51BEF744E05F73060DEED4AA5C6FFCF650DA904FF77F7F38CD923BBD9655ED1A25E2BDA5FA300CD3C7DFF5EBC921FFBC356CF6D0F6AE0EAACCFCD2A531A9FD16C7BEB9E59465D6819536F3754B13264CF8F4D34FDF7BEFBDE0E0E093274FC6C4C4B0DD0DE9E9E9B76EDD62AFE9DFBFFFC68D1B57AF5E1D1414B475EBD6EDDBB777EBD6CD42854B8014464B44A456C8DF79AC0B11AD399876F9B654CE693598F3685A6E6432E69DC7BA7CF45477B98CD97E3273F29AA322DEC12E670FB690DE37DF4E1E4EEB5FEABB6E5A9F4E1E8E79C5E5EFFF713EE2BF963BACC8B499A1A402BB066FADDD5BA302E2E70FFFE4E91EDD7C9C751586AD4937477F7968FCCAF86D276E9A756F9166D2F2A068E0CFA2A2A26A8F7EE2E2E2AAFFF699679E79E69967042FCB2A88B2556B9D46067A0CEDDC2A2EF5CEBFB79DD934B39F144AB2D8F6E14D3529D4CFCFD57ED686A4E3D7EF3DF1E5A1552F8404FB6A2D5F063B0491546C5737B4B3FBC00E6E9B8FDF58B6FBE2D59CA2993F26F5F2D3CE8B08086BDFD2ACAF6BEA0E97C2BFE1866994F2677AFB3E1DD23A393D6F5DFCB5BFCEDC4ABA7E2FE9FABD453BCE3FD5D36752A85F270F27C15FB459DF5B82C63088BA9CB63A86613E1CD3CD4E293F9A96BB254912BD0F7AA9C612110DECE8B66DF68076AD1CB20A4A9F5D95B0F958BAE56B1065396D9328E4B2C9A16DE2E60D9D3DB4BD46294B4ECF7B6ECD9117BE3B7AEA86190F2B12BD3BBCA918860969D3E2CBE77AC6CF1FFE7A78271FAD5D7E49F9BAF86B23FF7B70FCCAF8AD493785DD47A3B96F3E040F25854E3C135F57FBD71FE948441FEDBA20854D0D24D5F2505B0777C7DF23078C0CF4D0E90D6FFF7AE6DFDBCE58F82C0C76124F25F9EFBF4E1AE55BA3020ECE1B3625AC8D52CEFC732967CC8AC3AFFC9864A6F32024D21DCE81BBB3664E78C7836F0D5B37AD4F44570FB98C49BA7EEFCD2DA7FA7EB477E1EF67853ADB891D2D35D30671680C89DC5B329936A06D172FE7BCE2F20F779E17BB96AA493C69749AD5C949A35CF57CC81B8F746218DA7034FDD96F122CB9EFADC427F16A7077D67C30A6DBBE37868EEBE5C33014732E2B62F9C1E85F4EA6DF15F82B26E2160F8290CB98A19DDDBF79A177C2FCE1F3223AFBBADA159656FC9070FDD1CFFF79E2CB43EB0EA7F16CDB13F0BC2529B3D68F5F0AC43AD8A23E4AB96CF1B8EE3286B69FCC8C395BC7761B96C4B63C4876B4C492C9985747745C3BB58FB34671F246DEE35FFC63B1AF9BF427F16AF375B55FF66CF0DF73074774F53018E9B7E48C619FC545FF72F2CA1DC11A6DAC6E12AF3EEECE9AC8611D0EBC39ECC7E97D1FEBEEA990316732F217FD713EF4A3BD33D71FDF7D2E8BC3E8DC6834E2DE123C8CC4464B4414ECAB9D39B83D11FD7BDB991C51A7F2A4D620DE806101EE7FBE3628D8575B505AF1CA4F498B769CB340EFB8B54CE2D5D6C9C3E99B177AFF1E396070A7567A83F1B7E48CF065075EFDF984207B1F54AEA535E7614B96249331833AB6FA7A72C8D177462C7C22B09B8F73B9DEB8FB7CF6CC1F93FA2D8E5DB4E3DCD98CFCC6B7389ABAE7716F09EA25A97B4B26AF3FD231C0D3E96E916EC16F67445C342AC106F106F8BADA6F7925ECE5C1ED88685DFCB5F12BE3051C01D4663018F556F5F5A92DC857BBFEA5BEDB23078477F1301AE98F539911CB0FBEF26312CFE53BD63E89579F968EEA6903DAEE7C75D0DF7307CF1CDCAE95933AB748B72EFEDAE82F0F8D5AFECFCAB82B8DD94AD8D43DA1B1B9AF4F0D36FEF6CC4A3A9D78D5A915F265CF062BE5CC9EF3D9BF268BB6D10EBB279E4C6A5F9DFA29E5B2058F7559FB62EF16F6CAB319058F7FF1CF0FF1D7CCB4D360B9A1F2C75E09AE5B6A92605FEDB7537BFFF9DAC0C7BA7BB2F79C467F79E8A575C712D372B9FD4864E1C3962CAFB3A7D33B8F7549983FFCFB17FB3CDEC34BA590A566177E1C9332F0E3FDE357C6FF107FAD81B574EC0C9E4A2E93C29E556665E36FCFACA4B36EA986406FE7B9E19D8868D18E7369226D6EC64EE22924DCF250A7E1011E7FCD193CA8A35B69B961E18E7353D6269AE3C40776068FA4BD68B4F1BA7ABB7C3D3964F7DCC163822B4FD27BF69B84B15FC7FF79BAC98715E9ACB613AF491472D9B000F715937A1D7B27FCA3A7BAF76BE7CA309474FDDEC21DE7423FDAFBFCB7477F397623BFA4E6CE4F5537966CE1DF4CC36CFF1D9A8FB8E72D35ECE5C1EDFAFABBDE2FAB88DC906CD665E7F5B18A96873A79BA68D6BFD4F783315D354AD9A1CB3911CB0F6E3F9121EC74A8698B4FEB9DC4ABADA387D3E7137BC6BE317452A89F4A213B75232F7263F2D04FF7FF10DF84C38A6C7512AF3E2EF6CA49A17E9B66861D5930E2DDD18141BE5A83910E5DCE79EBD7D37DFEB377C6FAE37F9CCA34CDDDB1BFB05735B407826D682E1FBF39887B3A6DC31472D917CFF57475509DBF55F0FFFEBC60F902ACA8E5A1368661A684F9EF7A6D5090AFB6B0B462EEE693511B4F08D842C26E48CA30125D6ECC475B37878F9EEA1E3F7FF86B233AB6B057DEC82D59B8E35CD8E27D9FFE9D7ABBB0F4A17FDD663AF19ACAC359337D60DBDF23071C9837F4CD919D3A7938EAF4863DE7B35FFDF944AF0FF7446E48FEF3F4ADBB453A6A06FD0E8458E243CAA32522F274D12C7B3688887E3C727DD7995B167E75EB6A79A853BB568EBFBE1216FD4827858CF9F3CCADF06507B626DD1464D854B9219EDC0AB6D8E1C6CD511DFD48A7F8F923D8C38AF24BCABFDA7F79C0927D73369D484EBFD7C0D7B06AB464FBDF79EBD3A6A543D4F08EBB5F1FF2F7DCC191C3DAFBBADA9594EBFF3C732B7263F28BDF275233E80E27C4122F52BDB76432B4B3FB2B43DA13D15B5B4F5FBE6DD1B3CB2A474B12FEE23486422E7B6D44C76DB307B0EB94DFDC72EAF9EF8EF23F8BA8F2540B6BCEECC6B053C94D8715F5F2D396EB8DBF9FCC1CF775FC935F1DDE9A54F796A655BB3CE0FB1275F6749A17117070DEB01D51035E19D2DECFD59E4D73374795D8A5991D3E7EEE2A97D34AFB7BCB1B233B85B675BD5F5631637D92258FCF61DB14ADAEE5A14EDD5BBBEC881AF0F6A800B54276F8F2DD88E507BF8EBBCCE7B887CAB5B4CD63AA8A3DACE8B7D90376440D18DFABB54A213B9391FFE69653FD97EC5B1A9352A3A384EDC4B38D4E1041300CD3A3B576FEA30107E60DFDF3B581EF8E0E5CF844A0D845991D3E7EEED82E5F298F96884829977D3DB9978FD62E2DA7E8B54D279ADA19C599DE26464B264AB96CD6D0F67FCF1D3CA043CBB20AC3D298D427BE3C94749DE311ECBA8ACA493C416B94BA1EADB59F3D1BC4EECAE3EDA2C92DD27D1D7765E0C7FBFEF5C3F1FD29B7D97F3065E5182DD58D6198AEDE2ED307B6EDE02EFCC6E452838F9F3BA96D3E549F968EEA6F5E08D12865072EDE591A9362991795ECC1167CF8BB39FC343DF4D36782B4F6CA94ACC2F12B135EDF7C32BBE0E177F26BA83048EE0C408B69E9A88E1CD6E1E05BC3563DDFAB5F3B578391F65EC89EB6EED8A08FF77DBEF752667E0935EF7B4B4088253EA4B6556B03BAF9B87CF27410117D73F0EAC6A39638C7C1065A1EEAC430CCD321ADF7460F99D0DB976168DB898CE19FC6AD3A70A549FB1555EED36A13339CDC28E4B251DDBC36CD0CDB1B3D64FAC0B65A7B65667EE97FF75EDC75268B9A65271E54878F9F3B696E3E549F2782BCE78CE84844EFFE7E767FCA6D73BF9C6DB43CD4C7CD51FDF1D33DB6CF1E10ECAB2DD2E997FC95326AF93F8DFFAA36CF49BC3A7570777C7774E09105233E9F18DCB7AD2BFB604B07DBBFAB0F0DC07F0CEEAC68B4C49A1BDEF1E990D67A83317263F2999BBCF62E7B285B6A79A84F90AFF6B759FD3F7B26C8CD519D9653346DDDB117BE3B7A2EF3E15F5876120FB164A251CAC704FBFCF272D8DEE8219F4F0C7EAA978FD8158198F01F830FEBB8B764C230CCE271DD0775742BD6E9A7AD4B34EBBE4436D6F2501F998C191FD27AFF9B43660E6EC79E9237FACB43AF6F3ED9F0CE9B55A75AD8F81787830EEE8E63827D706FA999432C71C736B559D7775EB6312FD0CB39E7BE6EF29A231966D8F08D25E543D305E7A451BEF35897D8E8A14F06791B8DEC0DA703FFD9793EAFB8EE33DF308907D000FCC7E0AE72B1BAB57DE375D228D74FEFDBAE9543667EE9E435471AB3250C07CD2A96587E2DEDBF78AEE71F5103077468A9D31BBE3D943678E9FE2F632F1596D65C2E563589D78CBE38008D8758E24EE29B0F35C0CD51BDE15FA13E5ABB6B778B5FF836F1AE190E0CACB0DAAD5A79EADEDAE5A7E9A13FBCD4B78B97734169C5677B2E0E5ABA7FC5FECBF7CB1E6C576A8D47D302580CFE637027D9832D1AC3CBC56EE38C507727756A76E1736B8E3470CA0B3706EB3CD842100CC30CE9D4EACF57077E3E31B85D2B87BCE2F24FFE4E1DF4F1BE9571578ACA2A88A81C937800F5C37F0CEEAC77B4C46AD3D2E1E799FD3C9CD517B3EF4F5C9D70BBE9CB421B60DB0DE28D21933163827DF6BC3E64F984E0B66E0EF78ACB3F8E4919BC74FFCAB82BB9C53AC2241E403D104BDC49F9608B466ADFCA71F3CC306F17CD953B4513561FB991FBF0939B1B896D106F56F796EA249731637BFAEC797DF067CF04B569697FB748F7714CCA92BF5288C8E6CF1805E006FF31F8B2EA5822227F3787CD2F87B56E61979653346E65FCF9CC02419EB619B63C344021978D0F69BD377AC8A7CF04B56FE5C03EA8411B34405D104BDCB103021B98A7F275B5FF7556FF004FA73B856513BE4988BF92C3FF39F5CDB5E5A1014AB9ECE990D6BB5F1FB27272AFD13DBC9EEFE727764500528458E24ED073B445E6E1ACD9FC7258685BD7C2B28A17D71EDB793A93E71356C6126E9FD4C21EF4F0D5A45E3DFD5A885D0B80142196B8B3EA4EBCDA5CEC943FBCD4F7B1EE9E3ABDE1D59F4FAC3E7885CF49ACCDB6411C0078422C71672D075B349E4629FFF2B95E53C3DA188DF4D1AE94E85F4ED57984686354EE89877B4B00D0448825EEAC6EABD6C690CB98454F767DFFC9AE7219B3ED44C6846F12B2F2B9348E573688239600A089104BDC59D7C1168DC730CCD4FEFE3FBED4B785BDF2D4CDFC27BE3A9474FD5E539FC480493C00E004B1C49D0DAC5B6A40FF0E6E3BA206B2ED791357277C7728AD49B79AD0F20000DC2096B83356EE202E761D66C3368E3FD6DDB35C6FFC70E7F919EB93EE15D5BD25766D681007006E104BDC558D1D6CF93BAF835AB16252AF0FC77455C9657B2F643FF6C53FC7AEE536E62FEAB1CB0300708258E2CEB627F14C18867921CC7F5B64FF766E0EB7F24B27AE3EB27CEF45760FEC065460970700E004B1C49DB56FD5DA245DBD5D76BC3AF0A99E3E7A8371F9DE4B4F7D7D3835ABB081EB0D882500E004B1C49D8D2DA77D2847B562D9B3415F3CD7536BAF3C9B51F0C49787BE8EBB5C51CFB0097BE20100378825EE9AD56889C530CC9341DEBBE70E0EEFE2AED31B96C6A48E5F9570E1561DBBBBA2E50100B8412C71C7DE5B9235BF2FA1BBB366CD94DE9F3E13E4A4519CBA9137FACB431FEDBA50ACABA87E0D5A1E00809BE6F73D5538959378CD6BBC54896198A7435AEF7E7DF0A3DD3CF506E3EA83571F597670CFF96CD30598C403006E104BDCB17BE235CB54AAE4E562B7F2F990B52FF6F6D1DA65E495CC587F7CFABA6357EFDC27C4120070A510BB002B666CEEA95469788047BFE8965FC45EFEF69FABB129B70F5CBC33B5BF7F416939219600A0E9305AE2AEB975E235C05EA598FF6840CCDCC1C303DC2B0CC6EF0EA59DCD2820B43C0040D32196B86B869D780DEBE0EEB8F6C53E3FBCD4B7A3BB23FB885A897F6000D03498C4E3AE99ECF2D054433AB51A3067D0E6E3372E65DFEFD7AEA5D8E5008095412C7157B5552B72A926855C3639B48DD855008055C21C0B77B6773A2D0080E8104BDC197173090040688825EE6CF5745A0000112196B843CB030080E0104BDC610E0F0040708825EE2A3BF1B09101008070104BDC554EE2895D0600802D412C71573989875C0200100E6289BBCA06718C970000848358E2AE72392D52090040388825EE70B0050080E0EA8EA5DCDCDCC993273B3B3B6BB5DAE9D3A7DFBF7FBFCE6B5E7DF5D5CE9D3BDBD9D9F9F9F9BDF6DA6BF9F9F966AE565A70B0050080E0EA8EA5C993279F3B776ECF9E3D3B77EE3C78F0E0CC99336B5F939999999999F9E9A79F9E3D7B76DDBA75313131D3A74F3773B5D2C276E2A13F1C0040408CB1EAC6BDC9850B170203038F1D3BD6BB776F228A898979ECB1C76EDEBCE9EDEDDDC0136DD9B2E5F9E79F2F2A2A52281ADA95BCA0A0C0C5C5253F3FDFD9D9997FF5E20A5B1C7B2BBFF48FA881DD5BBB885D0B0080A599E9FB791DA3A5848404AD56CB661211858787CB64B2A3478F36FC446C650D67928DA99AC413BB0E00001B52478A646565B9BBBB3FB842A1707575CDCACA6AE0597272723EFCF0C33AE7FA88A8ACACACACAC8CFD754141018F6AA5C54835079A0000C0938CF95F2929294D7D8A828282C71F7F3C303070D1A245755EB078F162972ABEBEBE7C4B960C8C96000004A7B870E142F5DFB76BD7CED3D3F3F6EDDBA6472A2A2A7273733D3D3DEBFCFB858585A3468D727272DAB66D9B52A9ACF39A050B16444747B3BF2E2828B09964C2C11600008253040404D478282C2C2C2F2F2F29292924248488F6EDDB6730184243436BFFE5828282888808B55ABD63C70E8D4653DF6BA8D56AB55A2D6CDD5250D98987A55F0000C2A9E37B6A972E5D468D1A3563C68CC4C4C4C3870F4745454D9C38916DC3CBC8C8080808484C4C24A28282829123471615157DF7DD770505055959595959597ABDDED2EF403C55CB69315A0200104CDD8D731B366C888A8A1A3162844C261B3F7EFC175F7CC13E5E5E5E9E9A9A5A5C5C4C44C9C9C96C7B5E870E1D4C7F312D2DCDDFDFDFEC554B03B66A0500105CDDB1E4EAEABA71E3C6DA8FFBFBFB9BD6390D1D3AB4F69AA76605075B0000080E3746B8C368090040708825EE70B0050080E0104BDC554EE221950000848358E28E1D2DC9904B0000C2412C7157B59C160000048358E20E9378000082432C7187CD870000048758E20E5BB50200080EB1C41D0EB60000101C62893B03464B000042432CF18006710000A12196B86327F1904A000002422C7187832D0000048758E20E5BB50200080EB1C41D0EB60000101C62893B6C200E002038C41277D8AA1500407088258E8C386D0900C00C104B1C99CE8B67305A0200100E628923D3BE4308250000012196387A3089875C0200100E6289A36AA325E412008060104B1C19318B0700600688258E0C55B924432C01000807B1C4173AF10000048458E2E84183B8A8650000D818C41247A6A3693158020010106289A36AA325E412008060104B1C3D68C4432A01000807B1C49101CB690100CC00B1C41126F10000CC01B1C4D583AD5A452D0300C0B620963832E21440000033402C7184832D0000CC01B1C411B6C403003007C4124738D80200C01C104B1C1930890700600688258ED8960744120080B0146217D074462395178B5D0491AED48E4A6544A42B12BB1400004E94F612FCE1DAFA6269CFA9AB8F6CEF257615E44E74414344441F895C09000047EF6492CA41EC226AB2BE49BCD4ECFB6297000000E6627DA3A5B0CEBEEBD4F1625751A98FBF6B576F67B1AB0000E044692F760575B0BE580AF1770DF17715BB0A0000300BEB9BC40300001B865802000009412C01008084209600004042104B00002021882500009010C41200004808620900002404B104000012825802000009412C01008084209600004042104B000020E40F8BAB0000077D494441542196DE41DC68341251414181855F17000084C57E2767BFAB0BC8D2B15458584844BEBEBE167E5D00003087C2C242171717019F90113CE81A6630183233339D9C9C184E07C8171414F8FAFADEB871C3D9D9660FDFB3F9F768F36F90F01E6D82CDBF41E2FD1E8D46636161A1B7B7B74C26E4FD204B8F96643259EBD6AD793E89B3B3B30DFF4361D9FC7BB4F93748788F36C1E6DF20F17B8FC28E9358687900000009412C01008084C8172D5A24760D4D2397CB870E1DAA50587AFAD1926CFE3DDAFC1B24BC479B60F36F9024F91E2DDDF2000000D0004CE201008084209600004042104B000020218825000090102B8BA5152B56F8FBFB6B349AD0D0D0C4C444B1CB11D2A2458B986A020202C4AE4818070F1E7CE28927BCBDBD1986D9BE7DBBE971A3D1F8DE7BEF797979D9D9D98587875FBA7449C42279AAEF3DBEF8E28BD53FD351A3468958241F8B172FEED3A78F939393BBBBFBD8B1635353534D7F545A5A1A1919D9B2654B4747C7F1E3C76767678B58271F0DBCC7A1438756FF1C5F79E51511EBE46CE5CA953D7AF46057CE868585FDF5D75FECE312FC04AD2996366FDE1C1D1DBD70E1C2E4E4E4A0A0A0888888DBB76F8B5D9490BA76ED7AABCAA14387C42E47184545454141412B56ACA8F1F8D2A54BBFF8E28B55AB561D3D7AD4C1C1212222A2B4B454940AF9ABEF3D12D1A851A34C9FE9CF3FFF6CF9DA0471E0C081C8C8C823478EECD9B3A7BCBC7CE4C891454545EC1FBDFEFAEB7FFCF1C7962D5B0E1C38909999396EDC38714BE5AC81F748443366CC307D8E4B972E15B14ECE5AB76EBD64C992A4A4A4E3C78F0F1F3E7CCC9831E7CE9D23697E8246EBD1B76FDFC8C848F6D77ABDDEDBDB7BF1E2C5E29624A0850B17060505895D851911D1B66DDBD85F1B0C064F4FCF4F3EF984FD6D5E5E9E5AADFEF9E79FC5AB4E18D5DFA3D1689C3A75EA98316344ACC71CD89F050F1C3860341AF3F2F2944AE5962D5BD83FBA70E10211252424885AA000AABF47A3D13864C8903973E6885B92E05AB468F1EDB7DF4AF313B49AD1924EA74B4A4A0A0F0F677F2B93C9C2C3C3131212C4AD4A58972E5DF2F6F66ED7AEDDE4C993D3D3D3C52EC78CD2D2D2B2B2B24C9FA68B8B4B6868A88D7D9AACB8B8387777F7CE9D3BCF9A35EBEEDDBB62972380FCFC7C2272757525A2A4A4A4F2F272D3E7181010E0E7E767039F63F5F7C8DAB061839B9B5BB76EDD162C58505C5C2C5E6902D0EBF59B366D2A2A2A0A0B0B93E62728A195BD0DCBC9C9D1EBF51E1E1EA6473C3C3C525252442C4958A1A1A1EBD6ADEBDCB9F3AD5BB7DE7FFFFD4183069D3D7BD6C9C949ECBACC222B2B8B886A7C9AEC83B664D4A851E3C68D6BDBB6ED952B57DE79E79D471F7D342121412E978B5D1777068361EEDCB903060CE8D6AD1B11656565A9542AAD566BBAC0063EC71AEF9188264D9AD4A64D1B6F6FEFD3A74FBFFDF6DBA9A9A9BFFDF69BB8457273E6CC99B0B0B0D2D2524747C76DDBB60506069E3C7952829FA0D5C492CD7BF4D147D95FF4E8D1233434B44D9B36BFFCF2CBF4E9D3C5AD0AF898387122FB8BEEDDBBF7E8D1A37DFBF671717123468C10B72A3E222323CF9E3D6B33373EEB54FB3DCE9C3993FD45F7EEDDBDBCBC468C1871E5CA95F6EDDB8B5420779D3B773E79F2647E7EFED6AD5BA74E9D7AE0C001B12BAA9BD54CE2B9B9B9C9E5F2EA5D22D9D9D99E9E9E2296643E5AADB653A74E972F5F16BB1073613FB866F269B2DAB56BE7E6E666D59F695454D4CE9D3BF7EFDF6F3A9BC6D3D353A7D3E5E5E599AEB1F6CFB1F67BAC2134349488ACF47354A9541D3A7408090959BC78715050D0E79F7F2ECD4FD06A6249A552858484C4C6C6B2BF35180CB1B1B1616161E2566526F7EFDFBF72E58A979797D885984BDBB66D3D3D3D4D9F664141C1D1A3476DF5D364DDBC79F3EEDDBB56FA991A8DC6A8A8A86DDBB6EDDBB7AF6DDBB6A6C7434242944AA5E9734C4D4D4D4F4FB7D2CFB1BEF758C3C9932789C84A3FC7EA0C0643595999443F41713B2E9A64D3A64D6AB57ADDBA75E7CF9F9F3973A656ABCDCACA12BB28C1BCF1C61B717171696969870F1F0E0F0F777373BB7DFBB6D84509A0B0B0F0C48913274E9C20A265CB969D3871E2FAF5EB46A371C992255AADF6F7DF7F3F7DFAF4983163DAB66D5B52522276B11CD5F91E0B0B0BDF7CF3CD848484B4B4B4BD7BF7F6EAD5AB63C78EA5A5A56217CBC5AC59B35C5C5CE2E2E24C4DD2C5C5C5EC1FBDF2CA2B7E7E7EFBF6ED3B7EFC78585858585898B8A57256DF7BBC7CF9F2071F7C70FCF8F1B4B4B4DF7FFFBD5DBB7683070F16BB582EE6CF9F7FE0C081B4B4B4D3A74FCF9F3F9F6198DDBB771B25F9095A532C198DC62FBFFCD2CFCF4FA552F5EDDBF7C89123629723A4091326787979A9542A1F1F9F0913265CBE7C59EC8A84B17FFFFE1A3F094D9D3AD568341A0C8677DF7DD7C3C343AD568F183122353555EC4AB9ABF33D1617178F1C39B255AB564AA5B24D9B363366CCB0DE9FA26AFF38FBFDF7DFB37F545252327BF6EC162D5AD8DBDB3FF5D453B76EDD12B552EEEA7B8FE9E9E983070F76757555ABD51D3A7498376F5E7E7EBED8C572F1D24B2FB569D346A552B56AD56AC488116C261925F909E2600B00009010ABB9B7040000CD01620900002404B104000012825802000009412C01008084209600004042FE3F1382B41C067154300000000049454E44AE426082>|png>|0.5par|||>>
    La ligne orange est <math|y=-1/3>
  </big-figure>

  \;

  Finalement:

  <\with|font-base-size|8>
    <\equation*>
      <block*|<tformat|<table|<row|<cell|G<rsub|plates><rsup|*\<ast\>><around*|(|<math-bf|d>|)>=<frac|k<rsub|0>|6\<pi\>><around*|(|<frac|erfi<around*|(|k<rsub|0>*a/<sqrt|2>|)>-i|\<mathe\><rsup|k<rsub|0><rsup|2>*a<rsup|2>/2>>-<frac|-1/2+<around*|(|k<rsub|0>*a|)><rsup|2>|<sqrt|\<pi\>/2><around*|(|k<rsub|0>*a|)><rsup|3>>-3<around*|(|<frac|ln<around*|(|1+\<mathe\><rsup|i*k<rsub|0>d>|)>|k<rsub|0>d>+i<frac|Li<rsub|2><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|2>>-<frac|Li<rsub|3><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|3>>|)>|)>\<delta\><rsub|\<alpha\>\<beta\>>>>>>>
    </equation*>
  </with>

  <strong|Ce qui est fait dans le code:>

  D'abord <math|<tabular|<tformat|<table|<row|<cell|\<Lambda\><around*|(|<math-bf|q>|)>>|<cell|=>|<cell|<sqrt|k<rsup|2>-q<rsup|2>>>>>>>>

  <\cpp>
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    double complex Lambda(double *q, double k0){

    \ \ double qnorm2 = q[0]*q[0] + q[1]*q[1];

    \ \ return csqrt(k0*k0-qnorm2); }\ 
  </cpp>

  Puis <math|<tabular|<tformat|<table|<row|<cell|\<chi\><around*|(|<math-bf|q>|)>>|<cell|=>|<cell|<frac|1|2\<pi\>k<rsup|2>>\<mathe\><rsup|-<around*|(|k*a|)><rsup|2>/2>>>>>>>

  <\cpp>
    double complex chi(double a, double k0){\ 

    return 1./2./M_PI/(k0*k0)*cexp(-a*a*k0*k0/2);}
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 
  </cpp>

  Et <math|<tabular|<tformat|<table|<row|<cell|\<cal-I\><around*|(|<math-bf|q>|)>>|<cell|=>|<cell|\<chi\><around*|(|<math-bf|q>|)><frac|\<pi\>|\<Lambda\><around*|(|<math-bf|q<strong|>>|)>><around*|(|-i+erfi<around*|(|a\<Lambda\><around*|(|<math-bf|q<strong|>>|)>/<sqrt|2>|)>|)>>>>>>>

  <\cpp>
    double complex I_f(double *q, double k0, double a){
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ double complex L = Lambda(q,k0); \ \ \ \ \ \ \ \ \ \ 

    \ \ return M_PI*chi(a,k0)/L*(-I+erfi(a*L/sqrt(2))) ; }
  </cpp>

  Ensuite on calcule <math|g<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|q>;0|)>=<around*|(|\<delta\><rsub|\<alpha\>\<beta\>>k<rsup|2>-q<rsub|\<alpha\>>q<rsub|\<beta\>>|)><around*|(|\<cal-I\><around*|(|<math-bf|q>|)>+<frac|i|\<omega\>k<rsub|0><rsup|2>><frac|1|1+\<mathe\><rsup|-i\<omega\>d>>|)>>
  <with|color|red|Grosse incertitude sur le signe> <with|color|red|plus ou
  moins ?.>

  <\cpp>
    double complex gstar_plates(int alpha, int beta, double *q, double k0,
    double a, \ double d){ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ double qnorm2 = q[0]*q[0] + q[1]*q[1]; \ \ \ \ 

    \ \ double complex s = csqrt(k0*k0-qnorm2); \ \ \ \ \ \ 

    \ \ double complex Iq = I_f(q,k0,a) + I/s/cpow(k0,2)/(1+cexpf(-I*s*d));
    \ 

    \ \ if (alpha==0 && beta==0){ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ return (k0*k0-q[0]*q[0])*Iq; \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ } else if ((alpha==0 && beta==1) \|\| (alpha==1 && beta==0)){ \ \ \ 

    \ \ \ \ return -q[0]*q[1]*Iq; \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ } else { \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ return (k0*k0-q[1]*q[1])*Iq; \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ } \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    }
  </cpp>

  Ce calcul est effectu� pour S1 par exemple via le code suivant:

  <\cpp>
    double complex S1(double *kb, int alpha, int beta, double a, double k0,
    double d, int N){ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ double complex s = 0.; \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ for(int i = -N; i \<less\> N+1; i++){
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ for(int j = -N; j \<less\> N+1; j++){
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ \ \ double g[] = {2*M_PI/(sqrt(3))*i-kb[0],2*M_PI*2/3*(0.5*i+j)-kb[1]};
    \ \ \ \ \ \ \ \ 

    \ \ \ \ \ \ if (i==j) { \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ \ \ \ \ s += gstar_plates(alpha, beta, g, k0, a, d);
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ \ \ } else { \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ \ \ \ \ s += gstar(alpha, beta, g, k0, a);
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ \ \ } \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ } \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ } \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ double
    complex A = 3*sqrt(3)/2; \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ return s/A-(G_star(alpha, beta, k0, a)-G_plates(alpha,beta,k0,d));
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    }
  </cpp>

  O�: <math|G<rsub|\<alpha\>\<beta\>><rsup|*\<ast\>><around*|(|<math-bf|0>|)>=<frac|k|6\<pi\>><around*|(|<frac|erfi<around*|(|k*a/<sqrt|2>|)>-i|\<mathe\><rsup|k<rsup|2>*a<rsup|2>/2>>-<frac|-1/2+<around*|(|k*a|)><rsup|2>|<sqrt|\<pi\>/2><around*|(|k*a|)><rsup|3>>|)>\<delta\><rsub|\<alpha\>\<beta\>>>
  donc dans le code:

  <\cpp>
    double complex G_star(int alpha, int beta, double k0, double a){ \ \ 

    \ \ if (alpha!=beta) \ return 0.; \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ else return k0/6./M_PI*((erfi(k0*a/sqrt(2))-I)/exp(k0*k0*a*a/2) -
    (-0.5+k0*k0*a*a)/(sqrt(M_PI/2)*cpow(k0*a,3)));
    \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    }
  </cpp>

  et \ <math|G<rsub|plates><around*|(|d|)>=<frac|-*k<rsub|0>|2\<pi\>><around*|(|<frac|-ln<around*|(|1+\<mathe\><rsup|i*k<rsub|0>d>|)>|k<rsub|0>d>+i<frac|Li<rsub|2><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|2>>-<frac|Li<rsub|3><around*|(|-\<mathe\><rsup|i*k<rsub|0>d>|)>|<around*|(|k<rsub|0>d|)><rsup|3>>|)>>
  donc dans le code:

  <\cpp>
    double complex G_plates(int alpha, int beta, double k0,double d){\ 

    \ \ if (alpha!=beta) return 0.; \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ else{ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ \ \ double complex z = cexpf(I*k0*d); \ \ \ \ \ \ \ \ \ 

    \ \ \ \ double complex z2 = -clogf(1+z)/(k0*d)+I*cli2(-z)/cpowf((k0*d),2)+1/cpowf((k0*d),3)*cli3(-z);
    \ 

    \ \ \ \ return z2*k0/2/M_PI; \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    \ \ } \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ 

    } \ \ 
  </cpp>

  <section|ANNEXE>

  <\theorem>
    <dueto|Poisson>Formule de Sommation

    Soit <math|f> une fonction de <math|\<bbb-R\>> dans <math|\<bbb-R\>> et
    int�grable au sens suivant:

    <\equation*>
      \<exists\>C\<gtr\>0,\<exists\>\<alpha\>\<gtr\>1,\<forall\>x\<in\>\<bbb-R\>,<around*|\||f<around*|(|x|)>|\|>\<leq\><frac|C|<around*|(||\<nobracket\>>1+<around*|\||x<around*|\|||\<nobracket\>>|)><rsup|\<alpha\>>>
    </equation*>

    On definit la tranform�e de Fourier de <math|f> par:

    <\equation*>
      <wide|f|^><around*|(|\<omega\>|)>=<big|int><rsub|-\<infty\>><rsup|+\<infty\>>f<around*|(|t|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>t
    </equation*>

    Si la s�rie:

    <\equation*>
      <big|sum><rsub|n\<in\>\<bbb-Z\>><around*|\||<wide|f|^><around*|(|n\<omega\>|)>|\|>
    </equation*>

    converge alors:

    <\equation*>
      <big|sum><rsub|n\<in\>\<bbb-Z\>>f<around*|(|t+n*a|)>=<frac|1|a><big|sum><rsub|n\<in\>\<bbb-Z\>><wide|f|^><around*|(|n\<omega\>|)>\<mathe\><rsup|i*n\<omega\>t>
    </equation*>

    Imm�diatement, cette formule peut se g�n�raliser au cas o� <math|f> est
    une fonction de <math|\<bbb-R\><rsup|n>> dans <math|\<bbb-R\><rsup|n>> et
    donc par cons�quent pour un r�seau <math|2D> <math|\<Gamma\>> auquel on
    associe une maille �lementaire de surface <math|\<cal-A\>> et un r�seau
    r�ciproque <math|\<Gamma\><rsup|<rprime|'>>>:

    <\equation*>
      <big|sum><rsub|y\<in\>\<Gamma\>>f<around*|(|y|)>=<frac|1|\<cal-A\>><big|sum><rsub|x\<in\>\<Gamma\><rsup|<rprime|'>>><wide|f|^><around*|(|x|)>
    </equation*>
  </theorem>

  <\proposition>
    <dueto|Abrahomitz et Stegun>
  </proposition>

  <\equation*>
    <block|<tformat|<table|<row|<cell|w<around*|(|z|)>=\<mathe\><rsup|-z<rsup|2>><around*|(|1+<frac|2i|<sqrt|\<pi\>>><big|int><rsub|0><rsup|z>\<mathe\><rsup|t<rsup|2>>\<mathd\>t|)>=<frac|2i*z|\<pi\>><big|int><rsub|0><rsup|+\<infty\>><frac|\<mathe\><rsup|-t<rsup|2>>\<mathd\>t|z<rsup|2>-t<rsup|2>><space|1em>,\<Im\><around*|(|z|)>\<gtr\>0>>>>>
  </equation*>

  <\proof>
    On va montrer l'�galit� en utilisant la formule de Parseval-Plancherel
    dans l'�quation de droite, rappelons que pour toutes fonction <math|f> et
    <math|g> r�elles admettant des transform�es de Fourier <math|<wide|f|^>>
    et <math|<wide|g|^>>:

    <\equation*>
      <big|int><rsub|-\<infty\>><rsup|+\<infty\>>f<around*|(|t|)>g<around*|(|t|)>\<mathd\>t=<big|int><rsub|-\<infty\>><rsup|+\<infty\>><wide|f|^><around*|(|\<omega\>|)><wide|g|^><around*|(|\<omega\>|)>\<mathd\>\<omega\>
    </equation*>

    On pose alors:

    <\equation*>
      f<around*|(|t|)>=\<mathe\><rsup|-t<rsup|2>>
      <infix-and><space|1em>g<around*|(|t|)>=<frac|1|z<rsup|2>-t<rsup|2>>
    </equation*>

    Et on calcule les transform�es de Fourier:

    <\equation*>
      <wide|f|^><around*|(|\<omega\>|)>=\<cal-F\><around*|(|f|)><around*|(|w|)>=<frac|1|<sqrt|2\<pi\>>><big|int><rsub|-\<infty\>><rsup|+\<infty\>>f<around*|(|t|)>\<mathe\><rsup|-i\<omega\>t>\<mathd\>t=<frac|\<mathe\><rsup|-<frac|\<omega\><rsup|2>|4>>|<sqrt|2>>
    </equation*>

    <\equation*>
      <wide|g|^><around*|(|\<omega\>|)>=\<cal-F\><around*|(|g|)><around*|(|w|)>=<frac|1|<sqrt|2\<pi\>>><big|int><rsub|-\<infty\>><rsup|+\<infty\>><frac|\<mathe\><rsup|-i\<omega\>t>\<mathd\>t|z<rsup|2>-t<rsup|2>>=-<frac|i<sqrt|<frac|\<pi\>|2>><around*|(|\<theta\><around*|(|-\<omega\>|)>\<mathe\><rsup|-i\<omega\>z>+\<theta\><around*|(|\<omega\>|)>\<mathe\><rsup|i\<omega\>z>|)>|z>
    </equation*>

    Notons ici que l'int�grale consid�r�e est deux fois l'int�grale que l'on
    veut obtenir (par parit�).

    On en revient donc � calculer:

    <\equation*>
      -i<frac|<sqrt|\<pi\>>|2z><big|int><rsub|-\<infty\>><rsup|+\<infty\>><around*|(|\<theta\><around*|(|-\<omega\>|)>\<mathe\><rsup|-i\<omega\>z>+\<theta\><around*|(|\<omega\>|)>\<mathe\><rsup|i\<omega\>z>|)>\<mathe\><rsup|-<frac|\<omega\><rsup|2>|4>>\<mathd\>\<omega\>
    </equation*>

    Que l'on coupe en deux:

    <\equation*>
      -i<frac|<sqrt|\<pi\>>|2z><around*|(|<big|int><rsub|-\<infty\>><rsup|0>\<mathe\><rsup|-i\<omega\>z>\<mathe\><rsup|-<frac|\<omega\><rsup|2>|4>>\<mathd\>\<omega\>+<big|int><rsub|0><rsup|+\<infty\>>\<mathe\><rsup|i\<omega\>z>\<mathe\><rsup|-<frac|\<omega\><rsup|2>|4>>\<mathd\>\<omega\>|)>=-i<frac|<sqrt|\<pi\>>|z><big|int><rsub|0><rsup|+\<infty\>>\<mathe\><rsup|i\<omega\>z>\<mathe\><rsup|-<frac|\<omega\><rsup|2>|4>>\<mathd\>\<omega\>
    </equation*>

    On fait appara�tre une int�grale de gauss en posant
    <math|u=z+<frac|i\<omega\>|2>>:

    <\equation*>
      -i<frac|<sqrt|\<pi\>>|z><big|int><rsub|0><rsup|+\<infty\>>\<mathe\><rsup|<around*|(|z+<frac|i\<omega\>|2>|)><rsup|2>-z<rsup|2>>\<mathd\>\<omega\>=-i<frac|<sqrt|\<pi\>>|z>\<mathe\><rsup|-z<rsup|2>><big|int><rsub|z><rsup|i\<infty\>>\<mathe\><rsup|u<rsup|2>>\<mathd\><around*|(|<frac|2*u|i>|)>
    </equation*>

    Puis en appliquant la relation de Chasles:

    <\equation*>
      -<frac|2<sqrt|\<pi\>>|z>\<mathe\><rsup|-z<rsup|2>><around*|(|<big|int><rsub|z><rsup|0>\<mathe\><rsup|u<rsup|2>>\<mathd\>u+<big|int><rsub|0><rsup|i\<infty\>>\<mathe\><rsup|u<rsup|2>>\<mathd\>u|)>=-<frac|2<sqrt|\<pi\>>|z>\<mathe\><rsup|-z<rsup|2>><around*|(|<big|int><rsub|z><rsup|0>\<mathe\><rsup|u<rsup|2>>\<mathd\>u-i<big|int><rsub|0><rsup|+\<infty\>>\<mathe\><rsup|-u<rsup|2>>\<mathd\>u|)>
    </equation*>

    On calcule l'int�grale de Gauss:

    <\equation*>
      -<frac|2<sqrt|\<pi\>>|z>\<mathe\><rsup|-z<rsup|2>><around*|(|-<big|int><rsub|0><rsup|z>\<mathe\><rsup|u<rsup|2>>\<mathd\>u-i<frac|<sqrt|\<pi\>>|2>|)>
    </equation*>

    On multiplie par le facteur <math|<frac|2*i*z|\<pi\>>> et on divise par
    deux pour obtenir l'�galit� voulue:

    <\equation*>
      <frac|1|2><frac|4i |<sqrt|\<pi\>>>\<mathe\><rsup|-z<rsup|2>><around*|(|<big|int><rsub|0><rsup|z>\<mathe\><rsup|u<rsup|2>>\<mathd\>u+i<frac|<sqrt|\<pi\>>|2>|)>=\<mathe\><rsup|-z<rsup|2>><around*|(|<frac|2i|<sqrt|\<pi\>>><big|int><rsub|0><rsup|z>\<mathe\><rsup|u<rsup|2>>\<mathd\>u+1|)>
    </equation*>

    \;

    \;
  </proof>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|10>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|2|6>>
    <associate|auto-3|<tuple|1|10>>
    <associate|auto-4|<tuple|3|12>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        La ligne orange est <with|mode|<quote|math>|y=-1/3>
      </surround>|<pageref|auto-3>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>R�seau
      2D> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>R�seau
      confin� entre des plaques> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>ANNEXE>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>